// ----------------------------------------------------------------------------
// markItUp!
// ----------------------------------------------------------------------------
// Copyright (C) 2008 Jay Salvat
// http://markitup.jaysalvat.com/
// ----------------------------------------------------------------------------
// BBCode tags example
// http://en.wikipedia.org/wiki/Bbcode
// ----------------------------------------------------------------------------
// Feel free to add more tags
// ----------------------------------------------------------------------------
mySettings = {
	previewParserPath:	'', // path to your BBCode parser
	markupSet: [
		{name:'Heading 1', key:'1', openWith:'[h1]', closeWith:'[/h1]' },
		{name:'Heading 2', key:'2', openWith:'[h2]', closeWith:'[/h2]' },
		{name:'Heading 3', key:'3', openWith:'[h3]', closeWith:'[/h3]' },
		{name:'Paragraph', openWith:'[p]', closeWith:'[/p]' },
		{separator:'---------------' },
		{name:'Bold', key:'B', openWith:'[b]', closeWith:'[/b]'},
		{name:'Italic', key:'I', openWith:'[i]', closeWith:'[/i]'},
		{name:'Underline', key:'U', openWith:'[u]', closeWith:'[/u]'},
		{name:'Stroke through', key:'S', openWith:'[del]', closeWith:'[/del]' },
		{separator:'---------------' },
		{name:'Bulleted list', openWith:'[list]\n', closeWith:'\n[/list]'},
		{name:'Numeric list', openWith:'[enum]\n', closeWith:'\n[/enum]'}, 
		{name:'List item', openWith:'[*] ', closeWith:'[/*]'},
		{separator:'---------------' },
		{name:'Code', openWith:'[code]', closeWith:'[/code]'},
		{name:'Link', key: 'L', openWith:'[a]', closeWith:'[/a]',placeHolder:'http://the_url | the_text'}, 
	]
}