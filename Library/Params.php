<?php

// EasyGiant is a PHP framework for creating and managing dynamic content
//
// Copyright (C) 2009 - 2011  Antonio Gallo
// See COPYRIGHT.txt and LICENSE.txt.
//
// This file is part of EasyGiant
//
// EasyGiant is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// EasyGiant is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with EasyGiant.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

//class containing all the parameters necessary to EasyGiant to work properly
class Params
{

	//allowed database type
	public static $allowedDb = array('Mysql','Mysqli','None');

	//allowed sanitize functions
	public static $allowedSanitizeFunc = 'sanitizeAll,sanitizeDb,sanitizeHtml,forceInt,forceNat,none,md5,sha1';

	//allowed hash functions
	public static $allowedHashFunc = array('md5','sha1');
	
	//conventional null value for the value of the field in the createWhereClause method of the Model class
	public static $nullQueryValue = false;

	//use HTTPS for links or not
	public static $useHttps = false;

	//class name of the div that contains the error strings
	public static $errorStringClassName = 'alert';
	
	//table name in the returning structure of the select queries in the case of an aggregate function. Ex count(*),sum(*)
	public static $aggregateKey = 'aggregate';
	
	//htmlentities function charset
	//see http://php.net/manual/en/function.htmlentities.php for a complete list of the allowed values
	public static $htmlentititiesCharset = DEFAULT_CHARSET;
	
	//list of symbols used in the statements of the where clause of the select queries
	public static $whereClauseSymbolArray = array('<','>','!=','<=','>=','in(','not in(','like');
	
	//is the mbstring extension enabled?
	public static $mbStringLoaded = false;

	//subfolder of the View folder where to look for view files
	public static $viewSubfolder = null;

	//global website language used by the models and by the helpers
	public static $language = 'It';

}
