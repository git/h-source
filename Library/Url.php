<?php

// EasyGiant is a PHP framework for creating and managing dynamic content
//
// Copyright (C) 2009 - 2011  Antonio Gallo
// See COPYRIGHT.txt and LICENSE.txt.
//
// This file is part of EasyGiant
//
// EasyGiant is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// EasyGiant is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with EasyGiant.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class Url {

	//get the url starting from the root folder
	public static function getRoot($pathFromRootFolder = null) {

		$protocol = Params::$useHttps ? "https" : "http";

		$url = MOD_REWRITE_MODULE === true ? "$protocol://" . DOMAIN_NAME . '/' . $pathFromRootFolder : "$protocol://" . DOMAIN_NAME . '/index.php/' . $pathFromRootFolder;
		return $url;
	}

	//create an url string (element1/element2/element4) from the values of the array $valuesArray considering only the elements indicated in the numeric string $numericString (in this case '1,2,4')
	public static function createUrl($valuesArray,$numericString = null) {
		$elementsArray = explode(',',$numericString);
		$valuesArray = array_values($valuesArray);
		$urlString = null;
		for ($i = 0; $i < count($valuesArray); $i++)
		{
			if (isset($numericString)) {
				if (isset($valuesArray[$i]) and in_array($i,$elementsArray)) {
					$urlString .= "/".$valuesArray[$i];
				}
			} else {
				if (isset($valuesArray[$i])) {
					$urlString .= "/".$valuesArray[$i];
				}
			}
		}
		return $urlString;
	}

} 
