-- MySQL dump 10.13  Distrib 5.5.62, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: h_node_website
-- ------------------------------------------------------
-- Server version	5.5.62-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesses`
--

DROP TABLE IF EXISTS `accesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesses` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `ip` char(20) NOT NULL,
  `data` char(10) NOT NULL,
  `ora` char(8) NOT NULL,
  `username` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=693 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admingroups`
--

DROP TABLE IF EXISTS `admingroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admingroups` (
  `id_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_group`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adminsessions`
--

DROP TABLE IF EXISTS `adminsessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adminsessions` (
  `uid` char(32) NOT NULL,
  `token` char(32) NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `creation_date` int(10) unsigned NOT NULL,
  `user_agent` char(32) NOT NULL,
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adminusers`
--

DROP TABLE IF EXISTS `adminusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adminusers` (
  `id_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` char(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_failure` int(10) unsigned NOT NULL,
  `has_confirmed` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `username` (`username`),
  KEY `username_2` (`username`,`password`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adminusers_groups`
--

DROP TABLE IF EXISTS `adminusers_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adminusers_groups` (
  `id_user` int(11) unsigned NOT NULL,
  `id_group` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_group` (`id_group`,`id_user`),
  KEY `group_indx` (`id_group`),
  KEY `user_indx` (`id_user`),
  CONSTRAINT `adminusers_groups_ibfk_1` FOREIGN KEY (`id_group`) REFERENCES `admingroups` (`id_group`),
  CONSTRAINT `adminusers_groups_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `adminusers` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `boxes`
--

DROP TABLE IF EXISTS `boxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boxes` (
  `id_box` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(150) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id_box`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deletion`
--

DROP TABLE IF EXISTS `deletion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deletion` (
  `id_del` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `object` char(15) NOT NULL,
  `message` text NOT NULL,
  `id_hard` int(10) unsigned NOT NULL,
  `id_duplicate` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_del`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `distros`
--

DROP TABLE IF EXISTS `distros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distros` (
  `id_distro` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `clean_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `full_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `id_order` int(10) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_distro`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hardware`
--

DROP TABLE IF EXISTS `hardware`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hardware` (
  `id_hard` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `kernel` varchar(100) NOT NULL,
  `vendor` varchar(50) NOT NULL,
  `model` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `compatibility` char(15) NOT NULL,
  `description` text NOT NULL,
  `distribution` varchar(300) NOT NULL,
  `video_card_type` varchar(100) NOT NULL,
  `video_card_works` varchar(30) NOT NULL,
  `wifi_type` varchar(100) NOT NULL,
  `wifi_works` varchar(30) NOT NULL,
  `comm_year` char(15) NOT NULL,
  `ask_for_del` char(4) NOT NULL DEFAULT 'no',
  `deleted` char(4) NOT NULL DEFAULT 'no',
  `pci_id` char(10) NOT NULL,
  `subtype` char(25) NOT NULL DEFAULT 'not-specified',
  `driver` varchar(50) NOT NULL,
  `interface` char(15) NOT NULL DEFAULT 'not-specified',
  `bios` char(20) NOT NULL DEFAULT 'not-specified',
  `webcam_type` varchar(100) NOT NULL,
  `webcam_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `sound_card_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `bluetooth_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `fingerprint_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `architecture` char(15) NOT NULL DEFAULT 'not-specified',
  `other_names` text NOT NULL,
  `approved` char(4) NOT NULL DEFAULT 'yes',
  `cleared` char(3) NOT NULL DEFAULT 'no',
  `ethernet_card_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `sd_card_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `can_free_systems_be_installed` varchar(30) NOT NULL DEFAULT 'not-specified',
  `it_tracks_users` varchar(30) NOT NULL DEFAULT 'not-specified',
  `prevent_wifi` char(20) NOT NULL DEFAULT 'not-specified',
  PRIMARY KEY (`id_hard`)
) ENGINE=InnoDB AUTO_INCREMENT=2228 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hardware_users`
--

DROP TABLE IF EXISTS `hardware_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hardware_users` (
  `id_user` int(11) unsigned NOT NULL,
  `id_hard` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_user` (`id_user`,`id_hard`),
  KEY `hard_indx` (`id_hard`),
  KEY `user_indx` (`id_user`),
  CONSTRAINT `hardware_users_ibfk_1` FOREIGN KEY (`id_hard`) REFERENCES `hardware` (`id_hard`),
  CONSTRAINT `hardware_users_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `regusers` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `id_history` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` char(15) NOT NULL,
  `action` char(15) NOT NULL,
  `id` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `gr` char(15) NOT NULL,
  PRIMARY KEY (`id_history`)
) ENGINE=MyISAM AUTO_INCREMENT=10015 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `issues`
--

DROP TABLE IF EXISTS `issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issues` (
  `id_issue` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `topic` char(35) NOT NULL,
  `title` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `priority` char(15) NOT NULL,
  `status` char(15) NOT NULL,
  `notice` text NOT NULL,
  `deleted` char(4) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_issue`)
) ENGINE=InnoDB AUTO_INCREMENT=384 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id_mes` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` text NOT NULL,
  `id_issue` int(10) unsigned NOT NULL,
  `deleted` char(4) NOT NULL DEFAULT 'no',
  `has_read` char(4) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_mes`)
) ENGINE=MyISAM AUTO_INCREMENT=1049 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id_news` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(150) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id_news`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `params`
--

DROP TABLE IF EXISTS `params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `params` (
  `id_par` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `updating` char(4) NOT NULL DEFAULT 'no',
  `boxes_xml` text NOT NULL,
  PRIMARY KEY (`id_par`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `id_prof` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `real_name` varchar(100) NOT NULL,
  `where_you_are` varchar(100) NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `fav_distro` varchar(100) NOT NULL,
  `projects` text NOT NULL,
  `publish_mail` char(4) NOT NULL DEFAULT 'no',
  `description` text NOT NULL,
  `website` varchar(100) NOT NULL,
  `send_notification` char(4) NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id_prof`)
) ENGINE=InnoDB AUTO_INCREMENT=4780 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `regaccesses`
--

DROP TABLE IF EXISTS `regaccesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regaccesses` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `ip` char(20) NOT NULL,
  `data` char(10) NOT NULL,
  `ora` char(8) NOT NULL,
  `username` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=47196 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reggroups`
--

DROP TABLE IF EXISTS `reggroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reggroups` (
  `id_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_group`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `regsessions`
--

DROP TABLE IF EXISTS `regsessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regsessions` (
  `uid` char(32) NOT NULL,
  `token` char(32) NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `creation_date` int(10) unsigned NOT NULL,
  `user_agent` char(32) NOT NULL,
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `regusers`
--

DROP TABLE IF EXISTS `regusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regusers` (
  `id_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` char(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_failure` int(10) unsigned NOT NULL,
  `has_confirmed` int(10) unsigned NOT NULL,
  `e_mail` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `confirmation_token` char(32) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creation_time` int(10) unsigned NOT NULL,
  `temp_field` char(32) NOT NULL,
  `deleted` char(4) NOT NULL DEFAULT 'no',
  `forgot_token` char(32) NOT NULL,
  `forgot_time` int(10) unsigned NOT NULL,
  `blocked` char(4) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `username` (`username`),
  KEY `username_2` (`username`,`password`)
) ENGINE=InnoDB AUTO_INCREMENT=5863 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `regusers_groups`
--

DROP TABLE IF EXISTS `regusers_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regusers_groups` (
  `id_user` int(11) unsigned NOT NULL,
  `id_group` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_group` (`id_group`,`id_user`),
  KEY `group_indx` (`id_group`),
  KEY `user_indx` (`id_user`),
  CONSTRAINT `regusers_groups_ibfk_1` FOREIGN KEY (`id_group`) REFERENCES `reggroups` (`id_group`),
  CONSTRAINT `regusers_groups_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `regusers` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `revisions`
--

DROP TABLE IF EXISTS `revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revisions` (
  `id_rev` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `kernel` varchar(100) NOT NULL,
  `vendor` varchar(50) NOT NULL,
  `model` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `compatibility` char(15) NOT NULL,
  `description` text NOT NULL,
  `distribution` varchar(300) NOT NULL,
  `video_card_type` varchar(100) NOT NULL,
  `video_card_works` varchar(30) NOT NULL,
  `wifi_type` varchar(100) NOT NULL,
  `wifi_works` varchar(30) NOT NULL,
  `comm_year` char(15) NOT NULL,
  `id_hard` int(10) unsigned NOT NULL,
  `pci_id` char(10) NOT NULL,
  `subtype` char(25) NOT NULL DEFAULT 'not-specified',
  `driver` varchar(50) NOT NULL,
  `interface` char(15) NOT NULL DEFAULT 'not-specified',
  `bios` char(20) NOT NULL DEFAULT 'not-specified',
  `webcam_type` varchar(100) NOT NULL,
  `webcam_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `sound_card_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `bluetooth_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `fingerprint_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `architecture` char(15) NOT NULL DEFAULT 'not-specified',
  `other_names` text NOT NULL,
  `approved` char(4) NOT NULL DEFAULT 'yes',
  `cleared` char(3) NOT NULL DEFAULT 'no',
  `ethernet_card_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `sd_card_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `can_free_systems_be_installed` varchar(30) NOT NULL DEFAULT 'not-specified',
  `it_tracks_users` varchar(30) NOT NULL DEFAULT 'not-specified',
  `prevent_wifi` char(20) NOT NULL DEFAULT 'not-specified',
  PRIMARY KEY (`id_rev`)
) ENGINE=InnoDB AUTO_INCREMENT=4166 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `talk`
--

DROP TABLE IF EXISTS `talk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `talk` (
  `id_talk` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `id_hard` int(10) unsigned NOT NULL,
  `deleted` char(4) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_talk`)
) ENGINE=InnoDB AUTO_INCREMENT=712 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendors`
--

DROP TABLE IF EXISTS `vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendors` (
  `id_vendor` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `vendorid` char(4) NOT NULL,
  `clean_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `full_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `bus` enum('PCI','USB') DEFAULT NULL,
  PRIMARY KEY (`id_vendor`),
  UNIQUE KEY `bus` (`bus`,`vendorid`)
) ENGINE=InnoDB AUTO_INCREMENT=9846 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiki`
--

DROP TABLE IF EXISTS `wiki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki` (
  `id_wiki` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `title` varchar(200) NOT NULL,
  `title_clean` varchar(200) NOT NULL,
  `page` text NOT NULL,
  `deleted` char(3) NOT NULL DEFAULT 'no',
  `blocked` char(3) NOT NULL DEFAULT 'no',
  `is_main` char(3) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_wiki`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiki_revisions`
--

DROP TABLE IF EXISTS `wiki_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_revisions` (
  `id_rev` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `title` varchar(200) NOT NULL,
  `title_clean` varchar(200) NOT NULL,
  `page` text NOT NULL,
  `id_wiki` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_rev`)
) ENGINE=InnoDB AUTO_INCREMENT=698 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiki_talk`
--

DROP TABLE IF EXISTS `wiki_talk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_talk` (
  `id_talk` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `id_wiki` int(10) unsigned NOT NULL,
  `deleted` char(4) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_talk`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiki_users`
--

DROP TABLE IF EXISTS `wiki_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_users` (
  `id_user` int(11) unsigned NOT NULL,
  `id_wiki` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_user` (`id_user`,`id_wiki`),
  KEY `wiki_indx` (`id_wiki`),
  KEY `user_indx` (`id_user`),
  CONSTRAINT `wiki_users_ibfk_1` FOREIGN KEY (`id_wiki`) REFERENCES `wiki` (`id_wiki`),
  CONSTRAINT `wiki_users_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `regusers` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-01 20:15:05
