drop table distros;

create table distros (
	id_distro INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	creation_date timestamp default CURRENT_TIMESTAMP,
	clean_name varchar(200) CHARACTER SET utf8 not null,
	full_name varchar(200) CHARACTER SET utf8 not null,
	id_order INT UNSIGNED NOT NULL,
	active tinyint(1) NOT NULL DEFAULT 1
)engine=innodb;

insert into distros (clean_name, full_name, id_order, active) values ('blag_90001',           'BLAG 90001',                        1, 0);
insert into distros (clean_name, full_name, id_order, active) values ('blag_120000',          'BLAG 120000',                       2, 0);
insert into distros (clean_name, full_name, id_order, active) values ('blag_140000',          'BLAG 1400000',                      3, 0);
insert into distros (clean_name, full_name, id_order, active) values ('debian',               'Debian GNU/Linux Testing/Unstable', 4, 1);
insert into distros (clean_name, full_name, id_order, active) values ('debian_6',             'Debian GNU/Linux 6 squeeze',        5, 0);
insert into distros (clean_name, full_name, id_order, active) values ('debian_7',             'Debian GNU/Linux 7 wheezy',         6, 0);
insert into distros (clean_name, full_name, id_order, active) values ('debian_8',             'Debian GNU/Linux 8 jessie',         7, 1);
insert into distros (clean_name, full_name, id_order, active) values ('debian_9',             'Debian GNU/Linux 9 stretch',        8, 1);
insert into distros (clean_name, full_name, id_order, active) values ('debian_10',            'Debian GNU/Linux 10 buster',        9, 1);
insert into distros (clean_name, full_name, id_order, active) values ('debian_11',            'Debian GNU/Linux 11 bullseye',     10, 1);
insert into distros (clean_name, full_name, id_order, active) values ('debian_12',            'Debian GNU/Linux 12 bookworm',     11, 1);
insert into distros (clean_name, full_name, id_order, active) values ('dragora_1_1',          'Dragora 1.1',                      12, 0);
insert into distros (clean_name, full_name, id_order, active) values ('dragora_2_0',          'Dragora 2.0 Ardi',                 13, 0);
insert into distros (clean_name, full_name, id_order, active) values ('dragora_2_2',          'Dragora 2.2 Rafaela',              14, 0);
insert into distros (clean_name, full_name, id_order, active) values ('dynebolic_2_5_2',      'Dyne:bolic 2.5.2 DHORUBA',         15, 0);
insert into distros (clean_name, full_name, id_order, active) values ('dynebolic_3_0_X',      'Dyne:III 3.0.X MUNIR',             16, 0);
insert into distros (clean_name, full_name, id_order, active) values ('gnewsense_2_3',        'gNewSense 2.3 Deltah',             17, 0);
insert into distros (clean_name, full_name, id_order, active) values ('gnewsense_3_0',        'gNewSense 3.0 Metad (beta)',       18, 0);
insert into distros (clean_name, full_name, id_order, active) values ('gnewsense_3_0_parkes', 'gNewSense 3.0 Parkes',             19, 0);
insert into distros (clean_name, full_name, id_order, active) values ('gnewsense_4_0',        'gNewSense 4.0 Ucclia',             20, 0);
insert into distros (clean_name, full_name, id_order, active) values ('guix_0_10',            'GuixSD 0.10',                      21, 0);
insert into distros (clean_name, full_name, id_order, active) values ('guix',                 'GNU Guix System',                  22, 1);
insert into distros (clean_name, full_name, id_order, active) values ('hyperbola',            'Hyperbola GNU/Linux',              23, 1);
insert into distros (clean_name, full_name, id_order, active) values ('librecmc_1_4_9',       'LibreCMC v1.4.9',                  24, 0);
insert into distros (clean_name, full_name, id_order, active) values ('librecmc_1_5_0',       'LibreCMC v1.5.0',                  25, 0);
insert into distros (clean_name, full_name, id_order, active) values ('librecmc_1_5_1',       'LibreCMC v1.5.1',                  26, 0);
insert into distros (clean_name, full_name, id_order, active) values ('librecmc_1_5_2',       'LibreCMC v1.5.2',                  27, 0);
insert into distros (clean_name, full_name, id_order, active) values ('librecmc_1_5_3',       'LibreCMC v1.5.3',                  28, 0);
insert into distros (clean_name, full_name, id_order, active) values ('librecmc_1_5_4',       'LibreCMC v1.5.4',                  29, 0);
insert into distros (clean_name, full_name, id_order, active) values ('librecmc_1_5_5a',      'LibreCMC v1.5.4a',                 30, 0);
insert into distros (clean_name, full_name, id_order, active) values ('librecmc_1_5_7',       'LibreCMC v1.5.7',                  31, 1);
insert into distros (clean_name, full_name, id_order, active) values ('musix_2_0',            'Musix GNU+Linux 2.0 R0',           32, 0);
insert into distros (clean_name, full_name, id_order, active) values ('musix_3_0_1',          'Musix GNU+Linux 3.0.1',            33, 0);
insert into distros (clean_name, full_name, id_order, active) values ('parabola',             'Parabola GNU/Linux',               34, 1);
insert into distros (clean_name, full_name, id_order, active) values ('pureos_8_0',           'PureOS 8.0 Prometheus',            35, 1);
insert into distros (clean_name, full_name, id_order, active) values ('pureos_9_0',           'PureOS 9.0 Amber',                 36, 1);
insert into distros (clean_name, full_name, id_order, active) values ('pureos_10_0',          'PureOS 10.0 Byzantium',            37, 1);
insert into distros (clean_name, full_name, id_order, active) values ('replicant_1_5',        'Replicant 1.5',                    38, 0);
insert into distros (clean_name, full_name, id_order, active) values ('replicant_2_0',        'Replicant 2.0',                    39, 0);
insert into distros (clean_name, full_name, id_order, active) values ('replicant_2_2',        'Replicant 2.2',                    40, 0);
insert into distros (clean_name, full_name, id_order, active) values ('replicant_2_3',        'Replicant 2.3',                    41, 0);
insert into distros (clean_name, full_name, id_order, active) values ('replicant_4_0',        'Replicant 4.0',                    42, 0);
insert into distros (clean_name, full_name, id_order, active) values ('replicant_4_2',        'Replicant 4.2',                    43, 0);
insert into distros (clean_name, full_name, id_order, active) values ('replicant_6_0',        'Replicant 6.0',                    44, 1);
insert into distros (clean_name, full_name, id_order, active) values ('trisquel_3_5',         'Trisquel 3.5 Awen',                45, 0);
insert into distros (clean_name, full_name, id_order, active) values ('trisquel_4_0',         'Trisquel 4.0 Taranis',             46, 0);
insert into distros (clean_name, full_name, id_order, active) values ('trisquel_4_5',         'Trisquel 4.5 Slaine',              47, 0);
insert into distros (clean_name, full_name, id_order, active) values ('trisquel_5_0',         'Trisquel 5.0 Dagda',               48, 0);
insert into distros (clean_name, full_name, id_order, active) values ('trisquel_5_5',         'Trisquel 5.5 Brigantia',           49, 0);
insert into distros (clean_name, full_name, id_order, active) values ('trisquel_6_0',         'Trisquel 6.0 Toutatis',            50, 0);
insert into distros (clean_name, full_name, id_order, active) values ('trisquel_7_0',         'Trisquel 7.0 Belenos',             51, 1);
insert into distros (clean_name, full_name, id_order, active) values ('trisquel_8_0',         'Trisquel 8.0 Flidas',              52, 1);
insert into distros (clean_name, full_name, id_order, active) values ('trisquel_9_0',         'Trisquel 9.0 Etiona',              53, 1);
insert into distros (clean_name, full_name, id_order, active) values ('trisquel_10_0',        'Trisquel 10.0 Nabia',              54, 1);
insert into distros (clean_name, full_name, id_order, active) values ('ututo_xs_2009',        'UTUTO XS 2009',                    55, 0);
insert into distros (clean_name, full_name, id_order, active) values ('ututo_xs_2010',        'UTUTO XS 2010',                    56, 0);
insert into distros (clean_name, full_name, id_order, active) values ('ututo_xs_2012_04',     'UTUTO XS 2012.04',                 57, 0);
insert into distros (clean_name, full_name, id_order, active) values ('venenux_0_8',          'VENENUX 0.8',                      58, 0);
insert into distros (clean_name, full_name, id_order, active) values ('venenux_0_8_2',        'VENENUX-EC 0.8.2',                 59, 0);
insert into distros (clean_name, full_name, id_order, active) values ('venenux_0_9',          'VENENUX 0.9',                      60, 0);
insert into distros (clean_name, full_name, id_order, active) values ('debian_testing',       'Debian GNU/Linux Testing',         61, 0);
insert into distros (clean_name, full_name, id_order, active) values ('debian_unstable',      'Debian GNU/Linux Unstable',        62, 0);
