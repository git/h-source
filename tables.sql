-- h-source, a web software to build a community of people that want to share their hardware information.
-- Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
--
-- This file is part of h-source
--
-- h-source is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- h-source is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with h-source.  If not, see <http://www.gnu.org/licenses/>.


SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


-- --------------------------------------------------------

--
-- Table structure for table `accesses`
--

CREATE TABLE IF NOT EXISTS `accesses` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `ip` char(20) NOT NULL,
  `data` char(10) NOT NULL,
  `ora` char(8) NOT NULL,
  `username` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admingroups`
--

CREATE TABLE IF NOT EXISTS `admingroups` (
  `id_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_group`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `adminsessions`
--

CREATE TABLE IF NOT EXISTS `adminsessions` (
  `uid` char(32) NOT NULL,
  `token` char(32) NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `creation_date` int(10) unsigned NOT NULL,
  `user_agent` char(32) NOT NULL,
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `adminusers`
--

CREATE TABLE IF NOT EXISTS `adminusers` (
  `id_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` char(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_failure` int(10) unsigned NOT NULL,
  `has_confirmed` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `username` (`username`),
  KEY `username_2` (`username`,`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `adminusers_groups`
--

CREATE TABLE IF NOT EXISTS `adminusers_groups` (
  `id_user` int(11) unsigned NOT NULL,
  `id_group` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_group` (`id_group`,`id_user`),
  KEY `group_indx` (`id_group`),
  KEY `user_indx` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `boxes`
--

CREATE TABLE IF NOT EXISTS `boxes` (
  `id_box` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(150) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id_box`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `deletion`
--

CREATE TABLE IF NOT EXISTS `deletion` (
  `id_del` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `object` char(15) NOT NULL,
  `message` text NOT NULL,
  `id_hard` int(10) unsigned NOT NULL,
  `id_duplicate` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_del`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hardware`
--

CREATE TABLE IF NOT EXISTS `hardware` (
  `id_hard` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `kernel` varchar(100) NOT NULL,
  `vendor` varchar(50) NOT NULL,
  `model` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `compatibility` char(15) NOT NULL,
  `description` text NOT NULL,
  `distribution` varchar(300) NOT NULL,
  `video_card_type` varchar(100) NOT NULL,
  `video_card_works` varchar(30) NOT NULL,
  `wifi_type` varchar(100) NOT NULL,
  `wifi_works` varchar(30) NOT NULL,
  `comm_year` char(15) NOT NULL,
  `ask_for_del` char(4) NOT NULL DEFAULT 'no',
  `deleted` char(4) NOT NULL DEFAULT 'no',
  `pci_id` char(10) NOT NULL,
  `subtype` char(25) NOT NULL DEFAULT 'not-specified',
  `driver` varchar(50) NOT NULL,
  `interface` char(15) NOT NULL DEFAULT 'not-specified',
  `bios` char(20) NOT NULL DEFAULT 'not-specified',
  `webcam_type` varchar(100) NOT NULL,
  `webcam_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `sound_card_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `bluetooth_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `fingerprint_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `architecture` char(15) NOT NULL DEFAULT 'not-specified',
  `other_names` text NOT NULL,
  `approved` char(4) NOT NULL DEFAULT 'yes',
  `cleared` char(3) NOT NULL DEFAULT 'no',
  `ethernet_card_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `sd_card_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `can_free_systems_be_installed` varchar(30) NOT NULL DEFAULT 'not-specified',
  `it_tracks_users` varchar(30) NOT NULL DEFAULT 'not-specified',
  `prevent_wifi` char(20) NOT NULL DEFAULT 'not-specified',
  PRIMARY KEY (`id_hard`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hardware_users`
--

CREATE TABLE IF NOT EXISTS `hardware_users` (
  `id_user` int(11) unsigned NOT NULL,
  `id_hard` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_user` (`id_user`,`id_hard`),
  KEY `hard_indx` (`id_hard`),
  KEY `user_indx` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
  `id_history` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` char(15) NOT NULL,
  `action` char(15) NOT NULL,
  `id` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `gr` char(15) NOT NULL,
  PRIMARY KEY (`id_history`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8  ;

-- --------------------------------------------------------

--
-- Table structure for table `issues`
--

CREATE TABLE IF NOT EXISTS `issues` (
  `id_issue` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `topic` char(35) NOT NULL,
  `title` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `priority` char(15) NOT NULL,
  `status` char(15) NOT NULL,
  `notice` text NOT NULL,
  `deleted` char(4) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_issue`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id_mes` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` text NOT NULL,
  `id_issue` int(10) unsigned NOT NULL,
  `deleted` char(4) NOT NULL DEFAULT 'no',
  `has_read` char(4) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_mes`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id_news` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(150) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id_news`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `params`
--

CREATE TABLE IF NOT EXISTS `params` (
  `id_par` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `updating` char(4) NOT NULL DEFAULT 'no',
  `boxes_xml` text NOT NULL,
  PRIMARY KEY (`id_par`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

insert into params (updating) values ('no');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id_prof` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `real_name` varchar(100) NOT NULL,
  `where_you_are` varchar(100) NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `fav_distro` varchar(100) NOT NULL,
  `projects` text NOT NULL,
  `publish_mail` char(4) NOT NULL DEFAULT 'no',
  `description` text NOT NULL,
  `website` varchar(100) NOT NULL,
  `send_notification` char(4) NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id_prof`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `regaccesses`
--

CREATE TABLE IF NOT EXISTS `regaccesses` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `ip` char(20) NOT NULL,
  `data` char(10) NOT NULL,
  `ora` char(8) NOT NULL,
  `username` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Table structure for table `reggroups`
--

CREATE TABLE IF NOT EXISTS `reggroups` (
  `id_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_group`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Table structure for table `regsessions`
--

CREATE TABLE IF NOT EXISTS `regsessions` (
  `uid` char(32) NOT NULL,
  `token` char(32) NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `creation_date` int(10) unsigned NOT NULL,
  `user_agent` char(32) NOT NULL,
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `regusers`
--

CREATE TABLE IF NOT EXISTS `regusers` (
  `id_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` char(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_failure` int(10) unsigned NOT NULL,
  `has_confirmed` int(10) unsigned NOT NULL,
  `e_mail` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `confirmation_token` char(32) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creation_time` int(10) unsigned NOT NULL,
  `temp_field` char(32) NOT NULL,
  `deleted` char(4) NOT NULL DEFAULT 'no',
  `forgot_token` char(32) NOT NULL,
  `forgot_time` int(10) unsigned NOT NULL,
  `blocked` char(4) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `username` (`username`),
  KEY `username_2` (`username`,`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

-- --------------------------------------------------------

--
-- Table structure for table `regusers_groups`
--

CREATE TABLE IF NOT EXISTS `regusers_groups` (
  `id_user` int(11) unsigned NOT NULL,
  `id_group` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_group` (`id_group`,`id_user`),
  KEY `group_indx` (`id_group`),
  KEY `user_indx` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `revisions`
--

CREATE TABLE IF NOT EXISTS `revisions` (
  `id_rev` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `kernel` varchar(100) NOT NULL,
  `vendor` varchar(50) NOT NULL,
  `model` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `compatibility` char(15) NOT NULL,
  `description` text NOT NULL,
  `distribution` varchar(300) NOT NULL,
  `video_card_type` varchar(100) NOT NULL,
  `video_card_works` varchar(30) NOT NULL,
  `wifi_type` varchar(100) NOT NULL,
  `wifi_works` varchar(30) NOT NULL,
  `comm_year` char(15) NOT NULL,
  `id_hard` int(10) unsigned NOT NULL,
  `pci_id` char(10) NOT NULL,
  `subtype` char(25) NOT NULL DEFAULT 'not-specified',
  `driver` varchar(50) NOT NULL,
  `interface` char(15) NOT NULL DEFAULT 'not-specified',
  `bios` char(20) NOT NULL DEFAULT 'not-specified',
  `webcam_type` varchar(100) NOT NULL,
  `webcam_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `sound_card_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `bluetooth_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `fingerprint_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `architecture` char(15) NOT NULL DEFAULT 'not-specified',
  `other_names` text NOT NULL,
  `approved` char(4) NOT NULL DEFAULT 'yes',
  `cleared` char(3) NOT NULL DEFAULT 'no',
  `ethernet_card_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `sd_card_works` varchar(30) NOT NULL DEFAULT 'not-specified',
  `can_free_systems_be_installed` varchar(30) NOT NULL DEFAULT 'not-specified',
  `it_tracks_users` varchar(30) NOT NULL DEFAULT 'not-specified',
  `prevent_wifi` char(20) NOT NULL DEFAULT 'not-specified',
  PRIMARY KEY (`id_rev`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `talk`
--

CREATE TABLE IF NOT EXISTS `talk` (
  `id_talk` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `id_hard` int(10) unsigned NOT NULL,
  `deleted` char(4) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_talk`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wiki`
--

CREATE TABLE IF NOT EXISTS `wiki` (
  `id_wiki` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `title` varchar(200) NOT NULL,
  `title_clean` varchar(200) NOT NULL,
  `page` text NOT NULL,
  `deleted` char(3) NOT NULL DEFAULT 'no',
  `blocked` char(3) NOT NULL DEFAULT 'no',
  `is_main` char(3) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_wiki`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wiki_revisions`
--

CREATE TABLE IF NOT EXISTS `wiki_revisions` (
  `id_rev` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `title` varchar(200) NOT NULL,
  `title_clean` varchar(200) NOT NULL,
  `page` text NOT NULL,
  `id_wiki` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_rev`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wiki_talk`
--

CREATE TABLE IF NOT EXISTS `wiki_talk` (
  `id_talk` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `id_wiki` int(10) unsigned NOT NULL,
  `deleted` char(4) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_talk`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wiki_users`
--

CREATE TABLE IF NOT EXISTS `wiki_users` (
  `id_user` int(11) unsigned NOT NULL,
  `id_wiki` int(11) unsigned NOT NULL,
  UNIQUE KEY `id_user` (`id_user`,`id_wiki`),
  KEY `wiki_indx` (`id_wiki`),
  KEY `user_indx` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `adminusers_groups`
--
ALTER TABLE `adminusers_groups`
  ADD CONSTRAINT `adminusers_groups_ibfk_1` FOREIGN KEY (`id_group`) REFERENCES `admingroups` (`id_group`),
  ADD CONSTRAINT `adminusers_groups_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `adminusers` (`id_user`);

--
-- Constraints for table `hardware_users`
--
ALTER TABLE `hardware_users`
  ADD CONSTRAINT `hardware_users_ibfk_1` FOREIGN KEY (`id_hard`) REFERENCES `hardware` (`id_hard`),
  ADD CONSTRAINT `hardware_users_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `regusers` (`id_user`);

--
-- Constraints for table `regusers_groups`
--
ALTER TABLE `regusers_groups`
  ADD CONSTRAINT `regusers_groups_ibfk_1` FOREIGN KEY (`id_group`) REFERENCES `reggroups` (`id_group`),
  ADD CONSTRAINT `regusers_groups_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `regusers` (`id_user`);

--
-- Constraints for table `wiki_users`
--
ALTER TABLE `wiki_users`
  ADD CONSTRAINT `wiki_users_ibfk_1` FOREIGN KEY (`id_wiki`) REFERENCES `wiki` (`id_wiki`),
  ADD CONSTRAINT `wiki_users_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `regusers` (`id_user`);


create table vendors (
	id_vendor INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	creation_date timestamp default CURRENT_TIMESTAMP,
	update_date datetime NOT NULL,
	vendorid CHAR(4) not null,
	clean_name varchar(200) CHARACTER SET utf8 not null,
	full_name varchar(200) CHARACTER SET utf8 not null,
	bus ENUM('PCI', 'USB'),
	unique(bus,vendorid)
)engine=innodb;

insert into reggroups (name) values ('manager');
insert into reggroups (name) values ('admin');
insert into reggroups (name) values ('moderator');

insert into regusers (username,password) values ('admin',sha1('admin'));

insert into regusers_groups (id_user, id_group) values (1,1);
insert into regusers_groups (id_user, id_group) values (1,2);
insert into regusers_groups (id_user, id_group) values (1,3);
