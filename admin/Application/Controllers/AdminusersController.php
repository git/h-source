<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class AdminusersController extends Controller
{

	function __construct($model, $controller, $queryString)
	{
		parent::__construct($model, $controller, $queryString);

		$this->load('header_back');
		$this->load('footer_back','last');

		$this->helper('Menu','adminusers','panel/main');
		$this->helper('Array');

		$this->session('admin');
		$this->model();

		$this->m['AdminusersModel']->setFields('username:sanitizeAll,password:sha1','none');

		$this->m['AdminusersModel']->strongConditions['update'] = array('checkAlphaNum'=>'username','checkEqual'=>'password,confirmation');
		$this->m['AdminusersModel']->strongConditions['insert'] = array('checkAlphaNum'=>'username','checkEqual'=>'password,confirmation');

		$this->m['AdminusersModel']->databaseConditions['insert'] = array('checkUnique'=>'username');
		$this->m['AdminusersModel']->databaseConditions['update'] = array('checkUniqueCompl'=>'username');

		$this->setArgKeys(array('page:forceNat'=>1,'id_group:forceInt'=>'0','token:sanitizeAll'=>'token'));
		Params::$nullQueryValue = '0';
	}

	public function login()
	{
		$data['action'] = Url::getRoot('adminusers/login');
		$data['notice'] = null;
		
		$this->s['admin']->checkStatus();
		if ($this->s['admin']->status['status']=='logged')
		{
			$this->s['admin']->redirect('logged');
		}
		if (isset($_POST['username']) and isset($_POST['password']))
		{
			$username = ctype_alnum($_POST['username']) ? sanitizeAll($_POST['username']) : '';
			$choice = $this->s['admin']->login($username,$_POST['password']);

			switch($choice)
			{
				case 'logged':
					$this->redirect('panel/main',3,'You are already logged...');
					break;
				case 'accepted':
					$this->redirect('panel/main',3,'Hi '.$this->s['admin']->status['user'].'...');
					break;
				case 'login-error':
					$data['notice'] = '<div class="alert">Wrong username or password</div>';
					break;
				case 'wait':
					$data['notice'] = '<div class="alert">You have to wait 5 seconds before you can try to login another time</div>';
					break;
			}
		}
		$this->set($data);
		$this->load('login');
	}
	
	public function logout()
	{
		$res = $this->s['admin']->logout();
		if ($res == 'not-logged') {
			$data['notice'] = "<div class='alert'>You can't logout because you are not logged..</div>\n";

		} else if ($res == 'was-logged') {
			$data['notice'] = "<div class='executed'>Logout executed successfully!</div>\n";

		} else if ($res == 'error') {

		}

		$data['login'] = Url::getRoot('adminusers/login');
		$this->set($data);
		$this->load('logout');
	}

	public function forceout($id)
	{
		$this->shift(1);
		
		$this->s['admin']->check();
		$data['menù'] = $this->h['Menu']->render('back');
		$data['notice'] = null;
		$id = (int)$id;
		if (strcmp($this->s['admin']->status['id_user'],$id) !== 0)
			{
			if ($this->s['admin']->forceOut($id))
			{
				$data['notice'] = "<div class='executed'>User has been forced out..</div>\n";
			}
			else
			{
				$data['notice'] = "<div class='alert'>Error..</div>\n";
			}
		}
		
		$this->set($data);
		$this->load('forceout');
	}

	public function main()
	{
		$this->shift();

		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$this->loadScaffold('main',array('popup'=>false));
		$this->scaffold->setWhereQueryClause(array('id_group'=>$this->viewArgs['id_group']));
		$this->scaffold->loadMain('adminusers:id_user,adminusers:username','adminusers:id_user','edit,del');
		$this->scaffold->addItem('simpleLink','adminusers/forceout/;adminusers:id_user;',null,'ForceOut');
		$this->scaffold->update('del');
		$data['scaffold'] = $this->scaffold->render();
		$this->set($data);
		$this->load('main');
	}

	public function form($queryType = 'insert')
	{
		$this->shift(1);

		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$this->m['AdminusersModel']->updateTable('insert,update');

		$this->loadScaffold('form');
		$this->scaffold->loadForm($queryType,"adminusers/form/$queryType");
		$this->scaffold->getFormValues('sanitizeHtml');
		$this->scaffold->setFormEntries('model',array('password'=>'Password'));
		$this->scaffold->form->setEntry('confirmation','Password');
		$data['scaffold'] = $this->scaffold->render(null,'username,password,confirmation,id_user');
		
		$this->set($data);
		$this->load('main');
	}

// 	public function associate()
// 	{
// 		$this->shift(0);
// 		
// 		$this->s['admin']->check();
// 		
// 		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
// 		
// 		$this->m['AdminusersModel']->printAssError = 'yes';
// 		$this->m['AdminusersModel']->updateTable('associate,dissociate');
// 
// 		$data['notice'] = $this->m['AdminusersModel']->notice;
// 
// 		$data['menu'] = $this->h['Menu']->render('back');
// 
// 		$data['action'] = $this->baseUrl.'/adminusers/associate'.$this->viewStatus;
// 
// 		$data['groups'] = $this->m['AdminusersModel']->getFieldArray('admingroups:id_group','admingroups:name');
// 
// 		//get the name of the user whose id is $_POST['id_user']
// 		$users = $this->m['AdminusersModel']->db->select('adminusers','username','id_user='.(int)$_POST['id_user']);
// 		$data['user'] = $users[0]['adminusers']['username'];
// 		
// 		//get the groups inside which the user is inserted
// 		$this->m['AdminusersModel']->setWhereQueryClause(array('id_user'=>(int)$_POST['id_user']));
// 		$this->m['AdminusersModel']->orderBy = 'admingroups.id_group desc';
// 		$data['groupsUser'] = $this->m['AdminusersModel']->getAll('Boxes');
// // 		echo $this->m['AdminusersModel']->getQuery();
// 		
// 		$this->set($data);
// 		$this->load('associate');
// 	}

}