<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class AdminissuesController extends Controller {

	function __construct($model, $controller, $queryString) {
		parent::__construct($model, $controller, $queryString);

		$this->load('header_back');
		$this->load('footer_back','last');

		$this->session('admin');
		$this->model('IssuesModel');

		$this->modelName = 'IssuesModel';
		
		$this->m['IssuesModel']->setFields('deleted,status','sanitizeAll');

		$this->setArgKeys(array('page:forceNat'=>1,'priority:sanitizeAll'=>'undef','status:sanitizeAll'=>'undef','topic:sanitizeAll'=>'undef','deleted:sanitizeAll'=>'undef','token:sanitizeAll'=>'token'));
	}

	public function main()
	{
		$this->shift();

		Params::$nullQueryValue = 'undef';
		
		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$this->loadScaffold('main',array('popup'=>true,'popupType'=>'inclusive'));
		
		$whereClauseArray = array(
			'priority'	=>	$this->viewArgs['priority'],
			'status'	=>	$this->viewArgs['status'],
			'topic'		=>	$this->viewArgs['topic'],
			'deleted'	=>	$this->viewArgs['deleted'],
		);
		$this->scaffold->setWhereQueryClause($whereClauseArray);

		$this->scaffold->loadMain('issues:id_issue,issues:title,issues:topic,getUserName|issues:created_by,issues:priority,issues:status,smartDate|issues:creation_date,issues:deleted','issues:id_issue','edit,del');
		$this->scaffold->setHead('ISSUE ID,TITLE,TOPIC,CREATED BY,PRIORITY,STATUS,DATE');
		$this->scaffold->update('del');
		$data['scaffold'] = $this->scaffold->render();
		$this->append($data);
		$this->load('main');
	}

	public function form($queryType = 'insert')
	{
		$this->shift(1);

		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$this->m['IssuesModel']->updateTable('insert,update');

		$this->loadScaffold('form');
		$this->scaffold->loadForm($queryType,"adminissues/form/$queryType");
		$this->scaffold->getFormValues('sanitizeHtml');
		$data['scaffold'] = $this->scaffold->render();
		
		$this->append($data);
		$this->load('main');
	}

}