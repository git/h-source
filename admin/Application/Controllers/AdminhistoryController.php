<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class AdminhistoryController extends Controller
{

	function __construct($model, $controller, $queryString)
	{
		parent::__construct($model, $controller, $queryString);

		$this->load('header_back');
		$this->load('footer_back','last');

		$this->session('admin');
		$this->model('HistoryModel');

		$this->modelName = 'HistoryModel';

		$this->setArgKeys(array('page:forceNat'=>1,'type:sanitizeAll'=>'undef','action:sanitizeAll'=>'undef','created_by:sanitizeAll'=>'undef','token:sanitizeAll'=>'token'));
	}

	public function main()
	{
		$this->shift();

		Params::$nullQueryValue = 'undef';
		
		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$this->loadScaffold('main',array('popup'=>true,'popupType'=>'inclusive','mainMenu'=>'panel','recordPerPage'=>30));
		
		$whereClauseArray = array(
			'type'		=>	$this->viewArgs['type'],
			'action'	=>	$this->viewArgs['action'],
			'created_by'=>	$this->viewArgs['created_by'],
			'gr'		=>	'!="registered"',
		);
		$this->scaffold->setWhereQueryClause($whereClauseArray);
		$this->scaffold->model->orderBy('id_history desc');
		$this->scaffold->loadMain('history:id_history,history:type,history:action,getUserName|history:created_by,history:id,smartDate|history:creation_date,history:gr,history:message','history:id_history','');
		$this->scaffold->setHead('HISTORY ID,TYPE,ACTION,CREATED BY,OBJECT ID,CREATION DATE,GROUP,MESSAGE');
		$data['scaffold'] = $this->scaffold->render();
		$this->append($data);
		$this->load('main');
	}

}