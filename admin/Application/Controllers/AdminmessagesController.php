<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class AdminmessagesController extends Controller {

	function __construct($model, $controller, $queryString) {
		parent::__construct($model, $controller, $queryString);

		$this->load('header_back');
		$this->load('footer_back','last');

		$this->session('admin');
		$this->model('MessagesModel');

		$this->modelName = 'MessagesModel';
		
		$this->m['MessagesModel']->setFields('deleted,has_read','sanitizeAll');

		$this->setArgKeys(array('page:forceNat'=>1,'deleted:sanitizeAll'=>'undef','has_read:sanitizeAll'=>'undef','token:sanitizeAll'=>'token'));
	}

	public function main()
	{
		$this->shift();

		Params::$nullQueryValue = 'undef';
		
		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$this->loadScaffold('main',array('popup'=>true,'popupType'=>'inclusive','recordPerPage'=>20));
		
		$whereClauseArray = array(
			'deleted'	=>	$this->viewArgs['deleted'],
			'has_read'	=>	$this->viewArgs['has_read'],
		);
		$this->scaffold->setWhereQueryClause($whereClauseArray);
		$this->scaffold->model->orderBy = "id_mes desc";
		
		$this->scaffold->loadMain('messages:id_mes,messages:id_issue,getUserName|messages:created_by,smartDate|messages:creation_date,messages:deleted,messages:has_read','messages:id_mes','edit');
		$this->scaffold->setHead('MESSAGE ID,ISSUE ID,CREATED BY,DATE,DELETED?,ALREADY READ?');
		$data['scaffold'] = $this->scaffold->render();
		$this->append($data);
		$this->load('main');
	}

	public function form($queryType = 'insert')
	{
		$this->shift(1);

		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$this->m['MessagesModel']->updateTable('insert,update');

		$this->loadScaffold('form');
		$this->scaffold->loadForm($queryType,"adminmessages/form/$queryType");
		$this->scaffold->getFormValues('sanitizeHtml');
		$data['scaffold'] = $this->scaffold->render();
		
		$this->append($data);
		$this->load('main');
	}

}