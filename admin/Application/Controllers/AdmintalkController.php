<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class AdmintalkController extends Controller {

	function __construct($model, $controller, $queryString) {
		parent::__construct($model, $controller, $queryString);

		$this->load('header_back');
		$this->load('footer_back','last');

		$this->session('admin');
		$this->model('TalkModel');

		$this->modelName = 'TalkModel';
		
		$this->m['TalkModel']->setFields('title,message','sanitizeAll');

		$this->setArgKeys(array('page:forceNat'=>1,'token:sanitizeAll'=>'token'));
	}

	public function main()
	{
		$this->shift();

		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$this->loadScaffold('main',array('mainMenu'=>'panel'));
		
		$this->scaffold->loadMain('talk:id_talk,talk:id_hard,talk:title,getUserName|talk:created_by,smartDate|talk:creation_date','talk:id_talk','del');
		$this->scaffold->setHead('TALK ID,HARDWARE ID,TITLE,CREATED BY,DATE');
		$this->scaffold->update('del');
		$data['scaffold'] = $this->scaffold->render();
		$this->append($data);
		$this->load('main');
	}

}