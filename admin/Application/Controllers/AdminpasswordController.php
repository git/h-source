<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class AdminpasswordController extends Controller
{

	function __construct($model, $controller, $queryString)
	{
		parent::__construct($model, $controller, $queryString);

		$this->load('header_back');
		$this->load('footer_back','last');

		$this->helper('Menu','users','panel/main');
		$this->helper('Array');

		$this->session('admin');
		$this->model('AdminusersModel');

		$this->m['AdminusersModel']->setFields('password:sha1','none');

		$this->m['AdminusersModel']->strongConditions['update'] = array('checkEqual'=>'password,confirmation');
		$this->m['AdminusersModel']->strongConditions['insert'] = array('checkEqual'=>'password,confirmation');

		$this->m['AdminusersModel']->identifierName = 'id_user';

		$this->setArgKeys(array('token:sanitizeAll'=>'token'));
	}

	public function form()
	{
		$this->shift(0);

		$this->s['admin']->check();

		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$data['notice'] = null;
		
		$id = (int)$this->s['admin']->status['id_user'];
		if (isset($_POST['updateAction'])) {
			$pass = $this->s['admin']->getPassword();
			if (sha1($_POST['old']) === $pass)
			{
				$this->m['AdminusersModel']->updateTable('update',$id);
				$data['notice'] = $this->m['AdminusersModel']->notice;
			}
			else
			{
				$data['notice'] = "<div class='alert'>Vecchia password sbagliata</div>\n";
			}
		}
		$data['menù'] = $this->h['Menu']->render('panel');

		$values = $this->m['AdminusersModel']->selectId($id);
	
		$action = array('updateAction'=>'save');
		$form = new Form_Form('adminpassword/form'.$this->viewStatus,$action);
		$form->setEntry('old','Password');
		$form->entry['old']->labelString = 'old password:';
		$form->setEntry('password','Password');
		$form->setEntry('confirmation','Password');
		$data['form'] = $form->render($values,'old,password,confirmation');

		$this->append($data);
		$this->load('form');
	}
	
}