<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class AdminregusersController extends Controller {

	function __construct($model, $controller, $queryString) {
		parent::__construct($model, $controller, $queryString);

		$this->load('header_back');
		$this->load('footer_back','last');

		$this->session('admin');
		$this->model('UsersModel');

		$this->modelName = 'UsersModel';
		
		$this->helper('Menu','adminregusers','panel/main');
		
// 		$this->m['UsersModel']->setFields('username,e_mail,has_confirmed,deleted,creation_date','sanitizeAll');

		$this->setArgKeys(array('page:forceNat'=>1,'has_confirmed:sanitizeAll'=>'undef','deleted:sanitizeAll'=>'undef','id_group:sanitizeAll'=>'undef','token:sanitizeAll'=>'token'));
	}

	public function main()
	{
		$this->shift();

		Params::$nullQueryValue = 'undef';
		
		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$this->loadScaffold('main',array('popup'=>true,'popupType'=>'inclusive','mainMenu'=>'panel','recordPerPage'=>30));
		
		$whereClauseArray = array(
			'has_confirmed'	=>	$this->viewArgs['has_confirmed'],
			'deleted'		=>	$this->viewArgs['deleted'],
			'id_group'		=>	$this->viewArgs['id_group'],
		);
		$this->scaffold->setWhereQueryClause($whereClauseArray);

		$this->scaffold->loadMain('regusers:id_user,regusers:username,regusers:e_mail,regusers:has_confirmed,regusers:deleted,smartDate|regusers:creation_date','regusers:id_user','link');
		$this->scaffold->setHead('USER ID,USERNAME,E-MAIL,HAS CONFIRMED?,DELETED?,DATE');
		$data['scaffold'] = $this->scaffold->render();
		$this->append($data);
		$this->load('main');
	}

	public function associate()
	{
		$this->shift(0);
		
		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$this->m['UsersModel']->printAssError = 'yes';
		$this->m['UsersModel']->updateTable('associate,dissociate');

		$data['notice'] = $this->m['UsersModel']->notice;

		$data['menu'] = $this->h['Menu']->render('back');

		$data['action'] = $this->baseUrl.'/adminregusers/associate'.$this->viewStatus;

		$data['groups'] = $this->m['UsersModel']->getFieldArray('reggroups:id_group','reggroups:name');

		//get the name of the user whose id is $_POST['id_user']
		$users = $this->m['UsersModel']->db->select('regusers','username','id_user='.(int)$_POST['id_user']);
		$data['user'] = $users[0]['regusers']['username'];
		
		//get the groups inside which the user is inserted
		$this->m['UsersModel']->setWhereQueryClause(array('id_user'=>(int)$_POST['id_user']));
		$this->m['UsersModel']->orderBy = 'reggroups.id_group desc';
		$data['groupsUser'] = $this->m['UsersModel']->getAll('Boxes');
// 		echo $this->m['UsersModel']->getQuery();
		
		$this->set($data);
		$this->load('associate');
	}
}