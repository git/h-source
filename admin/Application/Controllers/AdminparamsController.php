<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class AdminparamsController extends Controller {

	function __construct($model, $controller, $queryString) {
		parent::__construct($model, $controller, $queryString);

		$this->load('header_back');
		$this->load('footer_back','last');

		$this->session('admin');
		$this->model('ParamsModel');

		$this->modelName = 'ParamsModel';
		
		$this->m['ParamsModel']->setFields('updating,boxes_xml','sanitizeAll');

		$this->setArgKeys(array('token:sanitizeAll'=>'token'));
	}

	public function form()
	{
		$this->shift();

		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$this->m['ParamsModel']->updateTable('update',1);
		if ($this->m['ParamsModel']->queryResult)
		{
			if (strcmp($this->m['ParamsModel']->values['updating'],'yes') === 0)
			{
				$this->m['ParamsModel']->query("delete from regsessions;");
			}
		}
// 		echo $this->m['ParamsModel']->getQuery();
		
		$this->loadScaffold('form',array('formMenu'=>'panel'));
		$this->scaffold->loadForm('update',"adminparams/form");
		$this->scaffold->getFormValues('sanitizeHtml',1);
		$data['scaffold'] = $this->scaffold->render();
		
		$this->append($data);
		$this->load('form');
	}

}