<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class AdminhardwareController extends Controller
{

	function __construct($model, $controller, $queryString)
	{
		parent::__construct($model, $controller, $queryString);

		$this->load('header_back');
		$this->load('footer_back','last');

		$this->session('admin');
		$this->model('HardwareModel');
		$this->model('DeletionModel');

		$this->modelName = 'HardwareModel';
		
		$this->m['HardwareModel']->setFields('deleted','sanitizeAll');

		$this->setArgKeys(array('page:forceNat'=>1,'type:sanitizeAll'=>'undef','ask_for_del:sanitizeAll'=>'undef','-deleted:sanitizeAll'=>'undef','token:sanitizeAll'=>'token'));
	}

	public function main()
	{
		$this->shift();

		Params::$nullQueryValue = 'undef';
		
		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$this->loadScaffold('main',array('popup'=>true,'popupType'=>'inclusive','mainMenu'=>'panel','recordPerPage'=>30));
		
		$whereClauseArray = array(
			'type'			=>	$this->viewArgs['type'],
			'ask_for_del'	=>	$this->viewArgs['ask_for_del'],
			'-deleted'		=>	$this->viewArgs['-deleted'],
		);
		$this->scaffold->setWhereQueryClause($whereClauseArray);

		$this->scaffold->loadMain('hardware:id_hard,hardware:model,hardware:type,getUserName|hardware:created_by,getUserName|hardware:updated_by,smartDate|hardware:creation_date,smartDate|hardware:update_date,hardware:ask_for_del,hardware:deleted','hardware:id_hard','edit');
		
		$this->scaffold->addItem('simpleLink','adminhardware/ask/;hardware:id_hard;',null,'who asked for deletion');
		$this->scaffold->setHead('HARD ID,MODEL,TYPE,CREATED BY,UPDATED BY,CREATION DATE,UPDATE DATE,ASK FOR DEL?,DELETED?,EDIT,VIEW');
		$data['scaffold'] = $this->scaffold->render();
		$this->append($data);
		$this->load('main');
	}

	public function ask($id_hard = 0)
	{
		$this->shift(1);
		
		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		$clean['id_hard'] = (int)$id_hard;
		
		$this->helper('Menu','adminhardware','panel');
		$this->h['Menu']->links['back']['text'] = 'Back';
		$this->h['Menu']->links['back']['url'] = 'main';
		
		$data["table"] = $this->m['DeletionModel']->select()->where(array("id_hard"=>$clean['id_hard']))->orderBy("id_del desc")->send();
		
		$data['menu'] = $this->h['Menu']->render('panel,back');
		
		$this->append($data);
		$this->load('ask');
		
	}

	public function form($queryType = 'update')
	{
		$this->shift(1);

		$this->s['admin']->check();
		
		if (!$this->s['admin']->checkCsrf($this->viewArgs['token'])) $this->redirect('panel/main/',2,'wrong token..');
		
		if (isset($_POST['updateAction']))
		{
			$clean['id_hard'] = $this->request->post('id_hard',0,'forceInt');
			$clean['deleted'] = $this->request->post('deleted',0,'sanitizeAll');
			
			$this->m['HardwareModel']->db->update('hardware','deleted',array($clean['deleted']),'id_hard='.$clean['id_hard']);
			
		}
		
		$this->loadScaffold('form');
		$this->scaffold->loadForm($queryType,"adminhardware/form/$queryType");
		$this->scaffold->getFormValues('sanitizeHtml');
		$data['scaffold'] = $this->scaffold->render();
		
		$this->append($data);
		$this->load('main');
	}

}