<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class BaseModel extends Model_Tree {

	public $type = ''; //device type
	public $diffFields = array();
	
	public function __construct() {
		$this->_tables = 'hardware';
		$this->_idFields = 'id_hard';
		
		$this->_where=array(
			'type'			=>	'hardware',
			'vendor'		=>	'hardware',
			'compatibility'	=>	'hardware',
			'comm_year'		=>	'hardware',
		);
		
		$this->orderBy = 'hardware.id_hard desc';
		parent::__construct();
	}

	public function checkType($id_hard = 0)
	{
		$clean['id_hard'] = (int)$id_hard;
		$res = $this->db->select('hardware','type','id_hard='.$clean['id_hard']);
		if (count($res) > 0)
		{
			return (strcmp($this->type,$res[0]['hardware']['type']) === 0 ) ? true : false;
		}
		return false;
	}
	
	public function getDiffArray($oldArray, $newArray)
	{
		$diffArray = array();
		foreach ($this->diffFields as $field => $label)
		{
			if (array_key_exists($field,$oldArray) and array_key_exists($field,$newArray))
			{
// 				echo htmlDiff($oldArray[$field], $newArray[$field]);
// 				echo $oldArray[$field].$newArray[$field];
				$diffArray[$label] = htmlDiff($oldArray[$field], $newArray[$field]);
			}
		}
		return $diffArray;
	}
}