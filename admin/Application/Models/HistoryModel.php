<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class HistoryModel extends Model_Tree {

	public function __construct() {
		$this->_tables = 'history';
		$this->_idFields = 'id_history';
		
		$this->orderBy = 'history.id_history';
		
		$this->_popupFunctions = array(
			'created_by'=>	'getUserName',
		);
		
		$this->_popupItemNames = array(
			'type'		=>	'type',
			'action'	=>	'action',
			'created_by'=>	'created_by',
		);
		
		$this->_popupLabels = array(
			'type'		=>	'TYPE',
			'action'	=>	'ACTION',
			'created_by'=>	'MODERATOR',
		);

		$this->_popupWhere = array(
			'created_by'		=>	'gr != "registered"',
		);
		
		
		parent::__construct();
	}

}