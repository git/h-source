<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class NotebooksModel extends BaseModel {

	public $type = 'notebook'; //device type
	
	public $diffFields = array(
		'vendor' 		=>	'vendor',
		'model' 		=>	'model name',
		'subtype' 		=>	'subtype (notebook or netbook) ?',
		'comm_year'		=>	'year of commercialization',
		'distribution' 	=>	'distribution used',
		'compatibility'	=>	'compatibility level',
		'kernel'		=>	'kernel libre version',
		'video_card_type' =>	'video card model',
		'video_card_works' =>	'does the video card work?',
		'wifi_type'		=>	'wifi model',
		'wifi_works'	=>	'does the wifi card works?',
		'description'	=>	'model description',
	);
	
	public $fieldsWithBreaks = array('model description');
	
	public function __construct()
	{
   
		$this->_popupItemNames = array(
			'vendor'		=>	'vendor',
			'compatibility'	=>	'compatibility',
			'comm_year'		=>	'comm_year',
			'subtype'		=>	'subtype',
		);
   
		$this->_popupLabels = array(
			'vendor'		=>	'vendor',
			'compatibility'	=>	'compatibility',
			'comm_year'		=>	'year',
			'subtype'		=>	'subtype',
		);
		
		$this->_popupWhere = array(
			'vendor'		=>	'type="notebook" and deleted="no"',
			'compatibility'	=>	'type="notebook" and deleted="no"',
			'comm_year'		=>	'type="notebook" and deleted="no"',
			'subtype'		=>	'type="notebook" and deleted="no"',
		);
		
		parent::__construct();
	}

}