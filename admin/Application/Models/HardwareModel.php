<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class HardwareModel extends Model_Map {

	public $id_user = 0;
	public $type = ''; //device type
	public $lastId = 0; //the id of the last record inserted
	
	public $formStruct = array(
		'entries' 	=> 	array(
			'deleted'=>	array('type'=>'Select','options'=>'no,yes'),
			'id_hard'		=>	array(
				'type'		=>	'Hidden'	
			)
		),
	);

	public function __construct() {
		$this->_tables='hardware,regusers,hardware_users';
		$this->_idFields='id_hard,id_user';
		$this->_where=array(
			'type'		=>	'hardware',
			'username'	=>	'regusers',
			'has_confirmed'		=>	'regusers',
			'deleted'			=>	'regusers',
			'-deleted'			=>	'hardware'
		);
		$this->orderBy = 'hardware.id_hard desc';
		$this->printAssError = "no";
		
		$this->_popupItemNames = array(
			'type'=>'type',
			'ask_for_del'=>'ask_for_del',
			'-deleted'=>'deleted',
		);
		
		$this->_popupLabels = array(
			'type'=>'TYPE',
			'ask_for_del'=>'ASK FOR DEL?',
			'-deleted'=>'DELETED?',
		);
		
		$this->databaseConditions['insert'] = array(
			'checkUnique'		=>	'model',
		);
		
		$this->databaseConditions['update'] = array(
			'checkUniqueCompl'	=>	'model',
		);
		
		parent::__construct();
	}

	public function insert()
	{
		$this->values['created_by'] = (int)$this->id_user;
		$this->values['updated_by'] = (int)$this->id_user;
		$this->values['update_date'] = date('Y-m-d H:i:s');
		
		//random ID
		$randomId = md5(uniqid(mt_rand(),true));
		$this->values["type"] = $randomId;

		parent::insert();
		
		//associate the user to the record
		if ($this->queryResult)
		{
			$resId = $this->db->select("hardware","id_hard","type='$randomId'");
			$clean['id'] = $resId[0]['hardware']['id_hard'];
			$this->lastId = $clean['id'];
			$this->db->update('hardware','type',array($this->type),'id_hard='.$clean['id']);

			$this->associate($clean['id']);
		}
		 
	}

	public function update($id)
	{
		$clean['id'] = (int)$id;
		
		$this->values['updated_by'] = (int)$this->id_user;
		$this->values['update_date'] = date('Y-m-d H:i:s');
		
		//save the old fields in the revisions table
		$this->setWhereQueryClause(array('id_hard'	=>	$clean['id']));
		$oldStruct = $this->getFields($this->fields.',created_by,updated_by,update_date,type,id_hard');
		
		if (count($oldStruct > 0))
		{
			if (strcmp($oldStruct[0]['hardware']['type'],$this->type) === 0)
			{
				$oldValues = $oldStruct[0]['hardware'];
				
				$revisions = new RevisionsModel();
				$revisions->values = $oldValues;
				if ($revisions->insert())
				{
					parent::update($clean['id']);
					if ($this->queryResult)
					{
						$this->lastId = $clean['id'];
						if (!$this->checkAssociation($clean['id'],(int)$this->id_user))
						{
							$this->associate($clean['id']);
						}
					}
				}
			}
		}
	}

	public function makeCurrent($id_rev)
	{
		$clean['id_rev'] = (int)$id_rev;
		
		$revisions = new RevisionsModel();
		
		$clean['id_hard'] = (int)$revisions->getIdHard($clean['id_rev']);
		
		//save the old fields in the revisions table
		$this->setWhereQueryClause(array('id_hard'=>$clean['id_hard']));
		$oldStruct = $this->getFields($this->fields.',created_by,updated_by,update_date,type,id_hard');
		
		if (count($oldStruct > 0))
		{
			if (strcmp($oldStruct[0]['hardware']['type'],$this->type) === 0)
			{
				//get the values of the revision
				$revisions->setWhereQueryClause(array('id_rev'=>$clean['id_rev']));
				$newStruct = $revisions->getFields($this->fields.',created_by,updated_by,update_date,type,id_hard');
				
				if (count($newStruct > 0))
				{
					$revisions->values = $oldStruct[0]['hardware'];
				
					$this->values = $newStruct[0]['revisions'];
					$this->values['updated_by'] = (int)$this->id_user;
					$this->values['update_date'] = date('Y-m-d H:i:s');
				
					if ($revisions->insert())
					{
						if (parent::update($clean['id_hard']))
						{
							$this->lastId = $clean['id_hard'];
							if (!$this->checkAssociation($clean['id_hard'],(int)$this->id_user))
							{
								$this->associate($clean['id_hard']);
							}
						}
					}
				}
			}
			else
			{
				$this->notice = "<div class='alert'>Wrong type..</div>\n";
			}
		}
		
	}

	public function associate($id_record)
	{
		return parent::associate((int)$id_record,(int)$this->id_user);
	}

	//get the model name
	public function getTheModelName($id)
	{
		$clean['id'] = (int)$id;
		$this->setWhereQueryClause(array('id_hard' => $clean['id']));
		$res = $this->getFields('model');
		$name = count($res) > 0 ? $res[0]['hardware']['model'] : '';
		return $name;
	}

	

}