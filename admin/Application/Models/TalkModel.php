<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class TalkModel extends Model_Tree {

	public function __construct() {
		$this->_tables = 'talk';
		$this->_idFields = 'id_talk';
		
		$this->_where=array(
			'id_hard'	=>	'talk'
		);
		
		$this->orderBy = 'talk.id_talk desc';
		
		$this->strongConditions['insert'] = array(
			"checkLength|99"	=>	'title',
			"+checkNotEmpty"	=>	'message',
			"++checkLength|5000"	=>	'message',
		);
		
		parent::__construct();
	}

	public $formStruct = array(
		'entries' 	=> 	array(
			'title'	=> 	array(),
			'message'	=> 	array('type'=>'Textarea'),
			'id_talk'		=>	array(
				'type'		=>	'Hidden'	
			)
		),
	);
	
}