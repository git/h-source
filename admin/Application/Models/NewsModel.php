<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class NewsModel extends Model_Tree {

	public function __construct() {
		$this->_tables = 'news';
		$this->_idFields = 'id_news';
		
		$this->orderBy = 'news.id_news desc';
		
		parent::__construct();
	}

	public $formStruct = array(
		'entries' 	=> 	array(
			'title'	=> 	array(),
			'message'	=> 	array('type'=>'Textarea','idName'=>'bb_code'),
			'id_news'	=>	array(
				'type'		=>	'Hidden'	
			)
		),
	);
	
}