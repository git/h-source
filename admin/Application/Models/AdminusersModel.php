<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class AdminusersModel extends Model_Map {

	public function __construct() {
		$this->_tables='adminusers,admingroups,adminusers_groups';
		$this->_idFields='id_user,id_group';
		$this->_where=array('id_group'=>'admingroups','id_user'=>'adminusers','name'=>'admingroups');
		$this->_popupItemNames = array('id_group'=>'name');
		$this->orderBy = 'adminusers.id_user desc';
		
// 		$this->on = "adminusers.id_user=adminusers_groups.id_user and admingroups.id_group=adminusers_groups.id_group";
		
		parent::__construct();
	}

}