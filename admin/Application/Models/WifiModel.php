<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class WifiModel extends BaseModel
{

	public $type = 'wifi'; //device type
	
	public $diffFields = array(
		'vendor' 		=>	'vendor',
		'model' 		=>	'model name',
		'pci_id'		=>	'VendorID:ProductID',
		'comm_year'		=>	'year of commercialization',
		'interface'		=>	'interface',
		'distribution' 	=>	'distribution used',
		'kernel'		=>	'kernel libre version',
		'wifi_works'	=>	'does it work with free software?',
		'description'	=>	'model description',
	);
	
	public $fieldsWithBreaks = array('model description');
	
	public function __construct()
	{
   
		$this->_popupItemNames = array(
			'vendor'		=>	'vendor',
			'comm_year'		=>	'comm_year',
			'wifi_works'	=>	'wifi_works',
			'interface'		=>	'interface',
		);
   
		$this->_popupLabels = array(
			'vendor'		=>	'vendor',
			'comm_year'		=>	'year',
			'wifi_works'	=>	'does it work?',
			'interface'		=>	'interface',
		);
		
		$this->_popupWhere = array(
			'vendor'		=>	'type="wifi" and deleted="no"',
			'comm_year'		=>	'type="wifi" and deleted="no"',
			'wifi_works'	=>	'type="wifi" and deleted="no"',
			'interface'		=>	'type="wifi" and deleted="no"',
		);
		
		parent::__construct();
	}

}