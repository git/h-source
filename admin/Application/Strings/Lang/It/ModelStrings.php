<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

class Lang_It_ModelStrings extends Lang_ResultStrings {
	
	public $string = array(
		"error" => "<div class='alert'>Errore nella query: contatta l'amministratore!</div>\n",
		"executed" => "<div class='executed'>operazione eseguita!</div>\n",
		"associate" => "<div class='alert'>Problema di integrit&agrave referenziale: il record &egrave associato ad un record di una tabella figlia. Devi prima rompere l'associazione.</div>\n",
		"no-id" => "<div class='alert'>Non &egrave definito alcun id della query</div>\n",
		"not-linked" => "<div class='alert'>Il record non &egrave associato, non puoi dissociarlo</div>",
		"linked" => "<div class='alert'>Il record &egrave gi&agrave associato, non puoi associarlo un'altra volta</div>"
	);
	
}
