<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class Lang
{
	public static $allowed = array('en','es','it');
	public static $current = 'en';
	
	public static $complete = array(
		'en'	=>	'gb.png,English',
		'es'	=>	'es.png,Spanish',
		'it'	=>	'it.png,Italian',
	);
	
	public static function sanitize($lang = 'en')
	{
		return (in_array($lang,self::$allowed)) ? sanitizeAll($lang) : 'en';
	}
}

class MyStrings
{
	
	public static $view = array(
		
		'en' =>	array(
		
			'notebooks' => array(
			
				'element' => 'notebook'
				
			),
			
			'wifi'		=>	array(
			
				'element' => 'wifi card'
				
			),
			
			'videocards'=>	array(
			
				'element' => 'video card'
				
			),
			
			'printers'=>	array(
			
				'element' => 'printer'
				
			),
			
			'scanners'=>	array(
			
				'element' => 'scanner'
				
			),
		),
		
		'fr' =>	array(
		
			'notebooks' => array(
			
				'element' => 'notebook'
				
			),
			
			'wifi'		=>	array(
			
				'element' => 'wifi card'
				
			),
			
			'videocards'=>	array(
			
				'element' => 'video card'
				
			),
			
			'printers'=>	array(
			
				'element' => 'printer'
				
			),
			
			'scanners'=>	array(
			
				'element' => 'scanner'
				
			),
		),
		
		'it' =>	array(
		
			'notebooks' => array(
			
				'element' => 'notebook'
				
			),
			
			'wifi'		=>	array(
			
				'element' => 'wifi card'
				
			),
			
			'videocards'=>	array(
			
				'element' => 'video card'
				
			),
			
			'printers'=>	array(
			
				'element' => 'printer'
				
			),
			
			'scanners'=>	array(
			
				'element' => 'scanner'
				
			),
		),
		
		'es' =>	array(
		
			'notebooks' => array(
			
				'element' => 'notebook'
				
			),
			
			'wifi'		=>	array(
			
				'element' => 'wifi card'
				
			),
			
			'videocards'=>	array(
			
				'element' => 'video card'
				
			),
			
			'printers'=>	array(
			
				'element' => 'printer'
				
			),
			
			'scanners'=>	array(
			
				'element' => 'scanner'
				
			),
		),
	);
	
	//type => controller
	public static $reverse = array(
		'notebook'	=>	'notebooks',
		'wifi'		=>	'wifi',
		'videocard'	=>	'videocards',
		'printer'	=>	'printers',
		'scanner'	=>	'scanners',
	);
	
	public static function getTypes()
	{
		return implode(',',array_keys(self::$reverse));
	}
	
}