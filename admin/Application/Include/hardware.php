<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');


class Hardware
{
	
	public static $controllers = array('notebooks','wifi','videocards','printers','scanners'); //used by UsersController::login()

	public static $commYear = 'not-specified,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,1994,1993,1992';

}

class Printer extends hardware
{
	public static $vendors = array(
		"brother"				=>	"brother",
		"Canon"					=>	"Canon",
		"EPSON"					=>	"EPSON",
		"Lexmark"				=>	"Lexmark",
		"KONICA-MINOLTA"		=>	"KONICA-MINOLTA",
		"Hewlett-Packard" 		=>	"Hewlett-Packard",
		"Panasonic" 			=>	"Panasonic",
		"RICOH"					=>	"RICOH",
		"SAMSUNG"				=>	"SAMSUNG",
		"SHARP"					=>	"SHARP",
		"TOSHIBA"				=>	"TOSHIBA",
		"XEROX"					=>	"XEROX",
	);
	
	public static $compatibility = array(
		"A Full" 				=>	"A-Full",
		"B Partial"				=>	"B-Partial",
		"C None"				=>	"C-None",
	);
	
	public static $interface = "not-specified,USB,Serial,Parallel,Firewire,SCSI,Ethernet";
	
	public static function vendorsList()
	{
		return implode(',',array_values(self::$vendors));
	}
	
	public static function compatibilityList()
	{
		return implode(',',array_values(self::$compatibility));
	}
	
}

class Wifi extends hardware
{
	public static $vendors = array(
		"A-LINK"			=>	"A-LINK",
		"Airlink101"		=>	"Airlink101",
		"Belkin" 			=>	"Belkin",
		"Broadcom"			=>	"Broadcom",
		"CANYON"			=>	"CANYON",
		"D-Link"			=>	"D-Link",
		"Hawking"			=>	"Hawking",
		"LevelOne"			=>	"LevelOne",
		"Linksys"			=>	"Linksys",
		"NEC"				=>	"NEC",
		"Netgear"			=>	"Netgear",
		"Ralink"			=>	"Ralink",
		"TOSHIBA"			=>	"TOSHIBA",
		"TP-LINK"			=>	"TP-LINK",
	);
	
	public static $interface = "not-specified,USB,PCI,PCI-E,mini-PCI,mini-PCI-E,ExpressCard,PC-Card";
	
	public static $wifiSelect = 'yes,no';
	
	public static function vendorsList()
	{
		return implode(',',array_values(self::$vendors));
	}
}

class Videocard extends hardware
{
	public static $vendors = array(
		"ATI" 			=>	"ATI",
		"NVIDIA"		=>	"NVIDIA",
		"Intel"			=>	"Intel",
	);
	
	public static $videoSelect = array(
		"works with 3D acceleration" 			=>	"works_with_3D",
		"works, but without 3D acceleration"		=>	"works_without_3D",
	);
	
	public static $videoReverse = array(
		"works_with_3D"		=>	"works with 3D acceleration",
		"works_without_3D"	=>	"works, but without 3D acceleration",
	);
	
	public static $interface = "not-specified,PCI,AGP,PCI-E,ISA";
	
	public static function vendorsList()
	{
		return implode(',',array_values(self::$vendors));
	}
	
	public static function videoList()
	{
		return implode(',',array_values(self::$videoSelect));
	}
}


class Notebooks extends Hardware
{
	
	public static $vendors = array(
		"Acer"					=>	"Acer",
		"Apple"					=>	"Apple",
		"Asus"					=>	"Asus",
		"Compal Electronics"	=>	"Compal-Electronics",
		"COMPAQ"				=>	"COMPAQ",
		"Dell"					=>	"Dell",
		"emachines"				=>	"emachines",
		"FUJITSU"				=>	"FUJITSU",
		"Gateway"				=>	"Gateway",
		"Hewlett Packard" 		=>	"Hewlett-Packard",
		"IBM" 					=>	"IBM",
		"Lenovo"				=>	"Lenovo",
		"LG"					=>	"LG",
		"Philips"				=>	"Philips",
		"Panasonic"				=>	"Panasonic",
		"Sony"					=>	"Sony",
		"SAMSUNG"				=>	"SAMSUNG",
		"Thomson"				=>	"Thomson",
		"TOSHIBA"				=>	"TOSHIBA",
	);

	public static $compatibility = array(
		"A Platinum" 			=>	"A-platinum",
		"B Gold"				=>	"B-gold",
		"C Silver"				=>	"C-silver",
		"D Bronze"				=>	"D-bronze",
		"E Garbage"				=>	"E-garbage"
	);
	
	public static $subtypeSelect = 'notebook,netbook,not-specified';
	
	public static $videoSelect = array(
		"not specified"							=>	'not-specified',
		"yes, with 3D acceleration" 			=>	"yes_with_3D",
		"yes, but without 3D acceleration"		=>	"yes_without_3D",
		"it does not work"						=>	"no",
	);

	public static $videoReverse = array(
		"yes_with_3D"		=>	"works with 3D acceleration",
		"yes_without_3D"	=>	"works but without 3D acceleration",
		"no"				=>	"it does not work",
		'not-specified'		=>	"not specified how it works",
		""					=>	""
	);
	
	public static $wifiSelect = array(
		"not specified"			=>	'not-specified',
		'yes'					=>	'yes',
		'no'					=>	'no',
		'there is no wifi card'	=>	'no-wifi-card',
	);
	
	public static $wifiReverse = array(
		"yes"			=>	"it works",
		"no"			=>	"it does not work",
		'not-specified'	=>	"not specified how it works",
		'no-wifi-card'	=>	'there is no wifi card',
		""				=>	""
	);
	
	public static function videoList()
	{
		return implode(',',array_values(self::$videoSelect));
	}

	public static function wifiList()
	{
		return implode(',',array_values(self::$wifiSelect));
	}
	
	public static function vendorsList()
	{
		return implode(',',array_values(self::$vendors));
	}
	
	public static function compatibilityList()
	{
		return implode(',',array_values(self::$compatibility));
	}
	
}