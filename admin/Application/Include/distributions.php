<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class Distributions
{
	
	public static $allowed = array(
		'blag_90001'	=>	'BLAG 90001',
		'blag_120000'	=>	'BLAG 120000',
		'dragora_1_1'	=>	'Dragora 1.1',
		'dynebolic_2_5_2'	=>	'Dynebolic 2.5.2 DHORUBA',
		'gnewsense_2_3'	=>	'gNewSense 2.3 Deltah',
		'gnewsense_3_0'	=>	'gNewSense 3.0 Metad',
		'musix_2_0'		=>	'Musix GNU+Linux 2.0 R0',
		'trisquel_3_5' 	=>	'Trisquel 3.5 Awen',
		'trisquel_4_0' 	=>	'Trisquel 4.0 Taranis',
		'ututo_xs_2009'	=>	'UTUTO XS 2009',
		'ututo_xs_2010'	=>	'UTUTO XS 2010',
		'venenux_0_8'	=>	'VENENUX 0.8',
	);
	
	public static function getName($distList = '')
	{
		$returnString = null;
		$returnArray = array();
		$distArray = explode(',',$distList);
		foreach ($distArray as $dist)
		{
			$dist = trim($dist);
			if (array_key_exists($dist,self::$allowed))
			{
				$returnArray[] = self::$allowed[$dist];
			}
		}
		return implode(" <br /> ",$returnArray);
	}
	
	public static function check($distString)
	{
		$distArray = explode(',',$distString);
		
		$allowedArray = array_keys(self::$allowed);
		
		foreach ($distArray as $dist)
		{
			$dist = trim($dist);
			if (!in_array($dist,$allowedArray)) return false;
		}
		
		return true;
	}
	
	public static function getFormHtml()
	{
		$str = "<div class='dist_checkboxes_hidden_box'>";
		$str .= "<div class='dist_checkboxes_hidden_box_inner'>";
		foreach (self::$allowed as $value => $label)
		{
			$str .= "<div class=\"hidden_box_item\"><input class=\"hidden_box_input $value\" type=\"checkbox\" name=\"$value\" value=\"$value\"/> $label</div>";
		}
		$str .= "</div>";
		$str .= "<input class=\"hidden_box_distribution_submit\" type=\"submit\" value=\"apply\">";
		$str .= "<input class=\"hidden_box_distribution_cancel\" type=\"submit\" value=\"cancel\">";
		$str .= "</div>";
		
		return $str;
	}
	
}