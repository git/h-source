<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

function encodeUrl($url)
{
	$url = str_replace(' ','-',$url);
	$url = urlencode($url);
// 	$url = html_entity_decode($url, ENT_QUOTES);
// 	$url = xml_encode($url);
	return $url;
}


function smartDate($uglyDate = null, $lang = 'en')
{
	switch ($lang)
	{
		case 'en':
			$smDate =  date('H:i, d F Y',strtotime($uglyDate));
			break;
		default:
			$smDate = date('H:i, d F Y',strtotime($uglyDate));
	}
	return $smDate;
}

function sanitizeString($string)
{
	$string = preg_match('/^[a-zA-Z0-9\-\_\.\+\s]+$/',$string) ? sanitizeAll($string) : 'undef';
	return $string;
}

function sanitizeAlphanum($string)
{
	$string = ctype_alnum($string) ? sanitizeAll($string) : 'undef';
	return $string;
}


function getOrderByClause($string)
{
	switch ($string)
	{
		case 'last-inserted':
			$orderBy = 'hardware.id_hard desc';
			break;
		case 'alphabetically':
			$orderBy = 'model';
			break;
		case 'alphabetically-desc':
			$orderBy = 'model desc';
			break;
		case 'compatibility':
			$orderBy = 'compatibility';
			break;
		case 'undef':
			$orderBy = 'hardware.id_hard desc';
			break;
		default:
			$orderBy = 'hardware.id_hard desc';
	}
	
	return $orderBy;
}


// function isEqual($str1, $str2)
// {
// // 	$str1 = str_replace("\n",'',$str1);
// // 	$str1 = str_replace("\r",null,$str1);
// // 	$str2 = str_replace("\n",'',$str1);
// // 	$str2 = str_replace("\r",null,$str1);
// 	
// 	return (strcmp($str1,$str2) === 0) ? true : false;
// }
// 
// function getNewKeys($array,$ovalue)
// {
// 	$res = array();
// 	for ($i = 0; $i < count($array); $i++)
// 	{
// 		if (isEqual($array[$i],$ovalue)) $res[] = $i;
// // 		if (strcmp($keys[$i],$ovalue) === 0) $res[] = $i;
// 	}
// 	return $res;
// }

function diff($old, $new){
		$maxlen = 0;
        foreach($old as $oindex => $ovalue){
// 				$nkeys = getNewKeys($new,$ovalue);
                $nkeys = array_keys($new, $ovalue);
                foreach($nkeys as $nindex){
                        $matrix[$oindex][$nindex] = isset($matrix[$oindex - 1][$nindex - 1]) ?
                                $matrix[$oindex - 1][$nindex - 1] + 1 : 1;
                        if($matrix[$oindex][$nindex] > $maxlen){
                                $maxlen = $matrix[$oindex][$nindex];
                                $omax = $oindex + 1 - $maxlen;
                                $nmax = $nindex + 1 - $maxlen;
                        }
                }       
        }
        if($maxlen == 0) return array(array('d'=>$old, 'i'=>$new));
        return array_merge(
                diff(array_slice($old, 0, $omax), array_slice($new, 0, $nmax)),
                array_slice($new, $nmax, $maxlen),
                diff(array_slice($old, $omax + $maxlen), array_slice($new, $nmax + $maxlen)));
}

function htmlDiff($old, $new){
		$old = str_replace("\r\n"," \r\n ",$old);
		$new = str_replace("\r\n"," \r\n ",$new);
		
		$ret = null;
        $diff = diff(explode(' ', $old), explode(' ', $new));
        foreach($diff as $k){
                if(is_array($k))
                        $ret .= (!empty($k['d'])?"<del>".implode(' ',$k['d'])."</del> ":'').
                                (!empty($k['i'])?"<ins>".implode(' ',$k['i'])."</ins> ":'');
                else $ret .= $k . ' ';
        }
        return $ret;
}


//a cosa serve?
function applyBreaks($values,$fields)
{
	$fieldsArray = explode(',',$fields);
	
	foreach ($fieldsArray as $field)
	{
		if (array_key_exists($field,$values))
		{
			$values[$field] = nl2br($values[$field]);
		}
	}
	return $values;
}


function getLinkToUser($user)
{
	if (strstr($user,'__'))
	{
		return str_replace('__',null,$user);
	}
	else
	{
		return "<a href='/users/meet/".Lang::$current."/$user'>$user</a>";
	}
}



//decode the text of the wiki
function decodeWikiText($string)
{
	
	$string = preg_replace('/(\[hr\])/', '<hr />',$string);

	$string = preg_replace_callback('/(\[a\])(.*?)(\[\/a\])/', 'linkTo',$string);

	$string = preg_replace_callback('/(\[a\])(.*?)\|(.*?)(\[\/a\])/', 'linkToWithText',$string);

	$string = preg_replace_callback('/(\[notebook\])([0-9]*?)(\[\/notebook\])/s', 'linkToNotebook',$string);
	
	$string = preg_replace_callback('/(\[wifi\])([0-9]*?)(\[\/wifi\])/s', 'linkToWifi',$string);
	
	$string = preg_replace_callback('/(\[videocard\])([0-9]*?)(\[\/videocard\])/s', 'linkToVideocard',$string);
	
	$string = preg_replace('/(\[b\])(.*?)(\[\/b\])/s', '<b>${2}</b>',$string);
	
	$string = preg_replace('/(\[u\])(.*?)(\[\/u\])/s', '<u>${2}</u>',$string);
	
	$string = preg_replace('/(\[i\])(.*?)(\[\/i\])/s', '<i>${2}</i>',$string);
	
	$string = preg_replace('/(\[del\])(.*?)(\[\/del\])/s', '<del>${2}</del>',$string);
	
	$string = preg_replace('/(\[\*\])(.*?)(\[\/\*\])/s', '<li>${2}</li>',$string);
	
	$string = preg_replace('/(\[list\])(.*?)(\[\/list\])/s', '<ul>${2}</ul>',$string);
	
	$string = preg_replace('/(\[enum\])(.*?)(\[\/enum\])/s', '<ol>${2}</ol>',$string);
 	
	$string = preg_replace('/(\[code\])(.*?)(\[\/code\])/s', '<pre class="code_pre">${2}</pre>',$string);
	
	$string = preg_replace('/(\[p\])(.*?)(\[\/p\])/s', '<p>${2}</p>',$string);
	
	$string = preg_replace('/(\[h1\])(.*?)(\[\/h1\])/s', '<div class="div_h1">${2}</div>',$string);
	
	$string = preg_replace('/(\[h2\])(.*?)(\[\/h2\])/s', '<div class="div_h2">${2}</div>',$string);
	
	$string = preg_replace('/(\[h3\])(.*?)(\[\/h3\])/s', '<div class="div_h3">${2}</div>',$string);
	
	return $string;
}

function checkUrl($url)
{
// 	$match = '/^http\:\/\/(www\.)?[a-zA-Z0-9\-\_]+(\.[a-zA-Z0-9\-\_]+)?\.(com|net|it|info|org|eu|uk)((\/[a-zA-Z0-9\_\-\+]+)+[\/]?)?(\?([a-zA-Z0-9\_\-\+\s]+\=[a-zA-Z0-9\_\-\s\+\&amp;]+)+)?(#[a-zA-Z0-9\_\-\+\s]+)?([\s]*)?$/';
	
	$match = '/^http\:\/\/(www\.)?[a-zA-Z0-9\-\_]+(\.[a-zA-Z0-9\-\_]+)?\.(com|net|it|info|org|eu|uk|ca)((\/[a-zA-Z0-9\_\-\+]+)*(\/([a-zA-Z0-9\_\-\.\+]+\.(php|html|htm|asp|aspx|jsp|cgi))?)?)?(\?([a-zA-Z0-9\_\-\+\s]+\=[a-zA-Z0-9\_\-\s\+\&amp;]+)+)?(#[a-zA-Z0-9\_\-\+\s]+)?([\s]*)?$/';
	
	if (preg_match($match,$url))
	{
		return true;
	}
	else
	{
		return false;
	}
}

function vitalizeUrl($string)
{
	if (checkUrl($string))
	{
		return "<a title = '".$string."' href='".$string."'>".$string."</a>";
	}
	return $string;
}

function linkTo($match)
{
	if (checkUrl($match[2]))
	{
		return "<a title = '".$match[2]."' href='".$match[2]."'>".$match[2]."</a>";
	}
	else
	{
		return $match[0];
	}
}

function linkToWithText($match)
{
	if (checkUrl($match[2]))
	{
		
		return "<a title = '".$match[2]."' href='".$match[2]."'>".$match[3]."</a>";
	}
	else
	{
		return $match[0];
	}
}

//create the link to the wiki page of the notebook
function linkToNotebook($match)
{
	$hardware = new HardwareModel();
	$clean['id_hard'] = (int)$match[2];
	$name = encodeUrl($hardware->getTheModelName($clean['id_hard']));
	$href = "/notebooks/view/".Lang::$current."/".$clean['id_hard']."/$name";
	return (strcmp($name,'') !== 0) ? "<a title = 'link to notebook $name:  $href' href='$href'>".$name."</a>" : $match[0];
}

//create the link to the wiki page of the wifi
function linkToWifi($match)
{
	$hardware = new HardwareModel();
	$clean['id_hard'] = (int)$match[2];
	$name = encodeUrl($hardware->getTheModelName($clean['id_hard']));
	$href = "/wifi/view/".Lang::$current."/".$clean['id_hard']."/$name";
	return (strcmp($name,'') !== 0) ? "<a title = 'link to wifi card $name:  $href' href='$href'>".$name."</a>" : $match[0];
}

//create the link to the wiki page of the videocard
function linkToVideocard($match)
{
	$hardware = new HardwareModel();
	$clean['id_hard'] = (int)$match[2];
	$name = encodeUrl($hardware->getTheModelName($clean['id_hard']));
	$href = "/videocards/view/".Lang::$current."/".$clean['id_hard']."/$name";
	return (strcmp($name,'') !== 0) ? "<a title = 'link to video card $name:  $href' href='$href'>".$name."</a>" : $match[0];
}

function getUserName($id_user = 0)
{
	$clean['id_user'] = (int)$id_user;
	$u = new UsersModel();
	return $u->getUser($clean['id_user']);
}

function getMotivation($row,$controller)
{
	if (strcmp($row['deletion']['object'],'duplication') === 0)
	{
		$clean['id_hard'] = (int)$row['deletion']['id_duplicate'];
		$hardware = new HardwareModel();
		$name = encodeUrl($hardware->getTheModelName($clean['id_hard']));
		return "<b>duplication</b> of the model having id <b><a href='/".$controller."/view/".Lang::$current."/".$clean['id_hard']."/".$name."'>".$clean['id_hard']."</a></b>";
	}
	else
	{
		return "<b>".$row['deletion']['object']."</b>";
	}
}
