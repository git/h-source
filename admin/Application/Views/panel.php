<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

?>

<div class="mainPanel">

	<div class='mainMenu'>
		<div class='logoutButton'>
			<a href ="<?php echo Url::getRoot('adminusers/logout');?>">LOGOUT</a>
		</div>
	</div>

	<div class='usersLoggedList'>
		Users logged:
		<?php foreach ($logged as $user) {?>
		<b><?php echo $user.'  ';?></b>
		<?php } ?>
	</div>

	<ul class='panelApplicationList'>
		<li><a href="<?php echo Url::getRoot('adminusers/main/1/undef/'.$token);?>">Admin users</a></li>
		<li><a href="<?php echo Url::getRoot('adminpassword/form/'.$token);?>">Password</a></li>
		<li><a href="<?php echo Url::getRoot('adminhardware/main/1/undef/undef/undef/'.$token);?>">Hardware</a></li>
		<li><a href="<?php echo Url::getRoot('admindeletion/main/1/undef/'.$token);?>">Ask for deletion</a></li>
		<li><a href="<?php echo Url::getRoot('adminissues/main/1/undef/undef/undef/undef/'.$token);?>">Issues</a></li>
		<li><a href="<?php echo Url::getRoot('adminmessages/main/1/undef/no/'.$token);?>">Messages to Issues</a></li>
		<li><a href="<?php echo Url::getRoot('admintalk/main/1/'.$token);?>">Talk</a></li>
		<li><a href="<?php echo Url::getRoot('adminregusers/main/1/undef/undef/undef/'.$token);?>">Registered users</a></li>
		<li><a href="<?php echo Url::getRoot('adminparams/form/'.$token);?>">Parameters</a></li>
		<li><a href="<?php echo Url::getRoot('adminnews/main/1/'.$token);?>">News</a></li>
		<li><a href="<?php echo Url::getRoot('adminboxes/main/1/'.$token);?>">Boxes</a></li>
		<li><a href="<?php echo Url::getRoot('adminhistory/main/1/undef/undef/undef/'.$token);?>">Moderators' actions history</a></li>
	</ul>
	
</div>