<?php if (!defined('EG')) die('Direct access not allowed!'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

?>
<head>

	<title>Admin</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrlSrc.'/Public/Css/scaffold.css';?>"></style>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrlSrc.'/Public/Css/mainmenu.css';?>"></style>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrlSrc.'/Public/Css/popupmenu.css';?>"></style>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrlSrc.'/Public/Css/form.css';?>"></style>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrlSrc.'/Public/Css/pagelist.css';?>"></style>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrlSrc.'/Public/Css/files.css';?>"></style>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrlSrc.'/Public/Css/login.css';?>"></style>

	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrlSrc.'/Public/Css/panel.css';?>"></style>
	
	<!--[if IE]>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl?>/Public/Css/explorer.css">
	<![endif] -->

	<script type="text/javascript" src="<?php echo $this->baseUrl;?>/Public/Js/jquery/jquery-1.4.2.min.js"></script>
	
	<!--markitup-->
	<script type="text/javascript" src="<?php echo $this->baseUrl;?>/Public/Js/markitup/jquery.markitup.js"></script>
	<script type="text/javascript" src="<?php echo $this->baseUrl;?>/Public/Js/markitup/sets/bbcode/set.js"></script>

	<!-- markItUp! skin -->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl;?>/Public/Js/markitup/skins/simple/style.css" />
	<!--  markItUp! toolbar skin -->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->baseUrl;?>/Public/Js/markitup/sets/bbcode/style.css" />
	
	<style type='text/css'>

	div#container{
		text-align: left;
		margin:0px;
	}
	
	.ask_for_deletion_internal
	{
		margin:10px;
		background:#B0E0E6;
		padding:10px;
	}
	.ask_for_deletion_item
	{
		font:normal 15px/1.3 sans-serif,arial,Verdana;
		margin:5px;
	}
	</style>
	
	<script>

	$(document).ready(function(){

		$(".delForm form").click(function () {
			if (window.confirm("do you really want to delete this record?")) {
				return true;
			}
			return false;
		});

	});

	</script>
	
</head>

<body>

<div id="container">

