<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

?>

<div class='mainMenu'>
	<?php echo $menu;?>
</div>

<?php echo $notice;?>

<div class='groupsList'>
	Associate the user <b><?php echo $user;?></b> to some groups:<br /><br />
	<form action = '<?php echo $action;?>' method = 'POST'>

	<select name='boxIdentifier'>
		<?php foreach ($groups as $name => $value) {?>
			<option value='<?php echo $value;?>'><?php echo $name;?></option>
		<?php } ?>
	</select>
	<input type='submit' name='associateAction' value='associate'>
	<input type='submit' name='dissociateAction' value='dissociate'>
	<input type='hidden' name='id_user' value='<?php echo (int)$_POST['id_user'];?>'>
	</form> 
</div>

<br /><hr>
<div class='groupsList'>
	The user <b><?php echo $user;?></b> is inserted inside the following Groups:
	<ul>
		<?php foreach ($groupsUser as $g) {?>
		<li><?php echo $g['reggroups']['name'];?></li>
		<?php } ?>
	</ul>
</div>