<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

?>

<div class='mainMenu'>
	<?php echo $menu;?>
</div>

<div class="ask_for_deletion_external">

	<?php foreach ($table as $row) { ?>
	<div class="ask_for_deletion_internal">
		<div class="ask_for_deletion_item">
			<b>id hardware to delete</b>: <?php echo $row['deletion']['id_hard'];?>
		</div>
		<div class="ask_for_deletion_item">
			<b>requested by</b>: <?php echo getUserName($row['deletion']['created_by']);?>
		</div>
		<div class="ask_for_deletion_item">
			<b>at</b>: <?php echo smartDate($row['deletion']['creation_date']);?>
		</div>
		<div class="ask_for_deletion_item">
			<b>why</b>: <?php echo $row['deletion']['object'];?>
		</div>
		<div class="ask_for_deletion_item">
			<b>hardware duplicated</b>: <?php echo $row['deletion']['id_duplicate'];?>
		</div>
		<div class="ask_for_deletion_item">
			<b>message</b>: <?php echo $row['deletion']['message'];?>
		</div>
	</div>
	<?php } ?>
</div>