<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php

// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

?>

<?php echo $notice; ?>

<div class="login_box">
	<form action = '<?php echo $action;?>' method = 'POST'>
	
		<table>
			<tr>
				<td>Username</td>
				<td><input type='text' name='username'></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><input type='password' name='password'></td>
			</tr>
			<tr>
				<td><input type = 'submit' value = 'login'></td>
			</tr>
		</table>
	
	</form>
</div>
