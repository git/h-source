
== Introduction ==

h-admin is a web software that has to be used together with h-source. It is formed by a set of files used to administrate the h-source tables


== Installation and configuration ==

In order to use h-source you have to install h-source (you can't use h-admin if you have not installed h-source before). In order to install h-source read the README.txt file inside the h-source package

In order to use h-admin you have to usa the same database already created for h-source. You have only to add some (5) new tables.

The new tables that have to be created are written inside the file tables.sql (of the h-admin package).

After you have created the new tables as indicated inside the tables.sql file, open the Config/Config.php file

You have to set the following constants:

	DB: name of the database (the database just created for h-source)
	USER: database user
	PWD: database password
	HOST: name of the mysql server (usually localhost)

	DOMAIN_NAME: the domain name of the website (use localhost or create a virtual host in your web server)

Now you have to copy the extracted h-admin files inside the DocumentRoot of your server. You can choose to leave all the files inside the DocumentRoot or not, see the explanations in this page (http://www.easygiant.org/learn/index/6).

You have now to assure that the allowOverride directive is active in the configuration file of your Apache server. You have to activate the allowOverride directive for the folder inside which you have saved the h-admin files (and the index.php file). If you have not activated the allowOverride directive, then your URL won't be interpreted by the mod_rewrite module (you won't be able to use h-admin). In a GNU/Linux system the apache configuration file is usually saved at /etc/apache2/apache2.conf.

Direct your browser to the following URL:

	http://DOMAIN_NAME/
	
where DOMAIN_NAME is the domain name you have previously set (perhaps localhost). You should see the login page of the h-admin software.

user: admin
password: admin


== Learn the EasyGiant PHP framework ==

Since h-admin is an application of the EasyGiant PHP framework you can use the EasyGiant features in order to modify the h-admin source code. Visit www.easygiant.org


// h-admin, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-admin-copyright.txt)
//
// This file is part of h-admin
//
// h-admin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-admin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-admin.  If not, see <http://www.gnu.org/licenses/>.

