<!--

function dist_list_helper()
{
	var dist_list = "";
	
	$(".input_distribution").click(function(){
		
		//show the form
		$(".dist_checkboxes_hidden_box").css({"display" : "block","z-index" : "500"});
		
		$(".hidden_x_explorer").css({"visibility" : "hidden"});
		
// 		$(".hidden_x_explorer").css({"visibility" : "hidden"});
		
		//remove checked attribute
		$(".hidden_box_input").each(function(){
			
			$(this).removeAttr("checked");
			
		});
		
		dist_list = $(".input_distribution").attr("value");
		
		//build the array by splitting the distribution string
		var dist_array = dist_list.split(",");
		
		for (i=0; i < dist_array.length; i++)
		{
			var this_class = dist_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
			$("." + this_class).attr({"checked" : "checked"});
		}
		
	});
	
	$(".hidden_box_distribution_submit").click(function(){
		
		$(".hidden_x_explorer").css({"visibility" : "visible"});
		
		dist_list = "";
		
		$(".hidden_box_input:checked").each(function(){
			
			dist_list += $(this).attr("value") + " , ";
			
		});
		
		$(".dist_checkboxes_hidden_box").css({"display" : "none"});
		
		if (dist_list.slice(-2) === ", ")
		{
			dist_list = dist_list.slice(0,dist_list.length-2);
		}
		
		$(".input_distribution").attr({"value" : dist_list});
		
		return false;
	});
	
	//click cancel
	$(".hidden_box_distribution_cancel").click(function(){
		
		$(".hidden_x_explorer").css({"visibility" : "visible"});
		
		$(".dist_checkboxes_hidden_box").css({"display" : "none"});
		
		return false;
	});
}

//md_type: message,issue,talk
//md_action:hide,show
function moderator_dialog(md_action,md_type)
{
	// Dialog Link
	$("." + md_action + "_" + md_type).click(function() {
		
		$("#delete_dialog").css("display","block");
		
		var md_id = $(this).attr("id");
		
		$('#notice_dialog').dialog({
			autoOpen: false,
			width: 500
		});
		
		$("#delete_dialog").dialog({
			autoOpen: false,
			width: 350,
			buttons: {
				"Send": function() {
					
					var md_message = encodeURIComponent($("#md_message").attr("value"));
					
					$.ajax({
						type: "POST",
						url: base_url + "/history/" + md_action + "/" + curr_lang + "/" + csrf_token,
						data: "id="+md_id+"&message="+md_message+"&type="+md_type+"&insertAction=save",
						async: false,
						cache:false,
						dataType: "html",
						success: function(html){
							$(".notice_dialog_inner").text(html);
							$('#notice_dialog').dialog('open');
						}
					});
					
					$(this).dialog("close"); 
				}, 
				"Cancel": function() { 
					$(this).dialog("close"); 
				} 
			}
		});
	
		$("#delete_dialog").dialog('open');
		
		return false;
	});
}

//-->