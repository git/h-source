<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//class to create the popup menu
class Helper_Popup extends Helper_Html {

	public $popupArray = array();  //array of popup objects (see popup.php)
	public $url = null; //the url (controller/action) to link by means of the popup menù
// 	public $fieldArg = null; //the key of the viewArgs array to set to the field in the where clause
// 	public $valueArg = null; //the key of the viewArgs array to be set to the value in the where clause 
	public $pageArg = null; //the key of the viewArgs array representing the page number. $this->viewArgs[$this->pageArg] is set to 1 if $this->pageArg !== null
	
	//the type of the popup. If type !=exclusive, than each voice selected is added to the where clause. If type=exclusive, the selection of a popup voice causes the unselection of the other voices
	public $type = null;
	
	//list of popup names
	public $popupItems = array();
	
	public function build($url, $popupArray = null, $type = 'exclusive', $pageArg = null) {
		$this->url = $url;
		$this->popupArray = $popupArray;
		$this->pageArg = $pageArg;
		$this->type = $type;
		
		foreach ($this->popupArray as $field => $popup)
		{
			$this->popupItems[] = $field;
		}
	}

	//check that the ViewArgs array is complete
	public function checkViewArgs()
	{
		foreach ($this->popupArray as $field => $popup)
		{
			if (!array_key_exists($field,$this->viewArgs)) return false;
		}
		return true;
	}

	//unselect the voices different from the current one
	public function unselect($currentVoice)
	{
		foreach ($this->popupItems as $item)
		{
			if (strcmp($item,$currentVoice) !== 0) $this->viewArgs[$item] = Params::$nullQueryValue;
		}
	}

	public function render() {
		$returnString = null;
		if ($this->checkViewArgs())
		{
			if (isset($this->viewArgs[$this->pageArg]))
			{
				$this->viewArgs[$this->pageArg] = 1;
			}
			foreach ($this->popupArray as $field => $popup)
			{
				if ($this->type === 'exclusive') $this->unselect($field);
				//save the value of the current ViewArg
				$tempArg = $this->viewArgs[$field];
				$returnString .= "<ul onMouseOver='DisplayTag(this,\"block\");' onMouseOut='DisplayTag(this,\"none\");' id='menuBlock'><li class='innerItem'>".$popup->name."<ul class='innerList'>\n";
				for ($i = 0; $i < count($popup->itemsValue); $i++)
				{
					$this->viewArgs[$field] = $popup->itemsValue[$i];
					$viewStatus = Url::createUrl(array_values($this->viewArgs));
					$returnString .=  "<li><a href='".Url::getRoot($this->url).$viewStatus."'>".$popup->itemsName[$i]."</a></li>\n";
				}
				$this->viewArgs[$field] = Params::$nullQueryValue;
				$viewStatus = Url::createUrl(array_values($this->viewArgs));
				$returnString .=  "<li><a href='".Url::getRoot($this->url).$viewStatus."'>All</a></li>\n";
				$returnString .= "</ul>\n</li>\n</ul>\n";
				$this->viewArgs[$field] = $tempArg;
			}
			if (count($this->popupArray)>0) {
				$returnString .= "<script type=\"text/javascript\" src=\"/admin/Public/Js/DisplayTag.js\"></script>\n";
			}
		}
		return $returnString;
	}

}
