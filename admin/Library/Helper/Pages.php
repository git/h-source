<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//Helper class to create the HTML of the page division list
class Helper_Pages extends Helper_Html
{

	protected $_urlViewAction; //url of the current page
	protected $_currentPage; //number of the page
	protected $_numbOfPages; //number of pages
	protected $_variableArg = ''; //value of the $viewArgs key that has to be modified

	public $previousString; //string of the link to the previous page
	public $nextString; //string of the link to the next page
	
	public function build($urlViewAction = '' , $variableArg = 'page', $previousString = 'previous', $nextString = 'next')
	{

		$this->_variableArg = $variableArg;
		$this->_urlViewAction =$urlViewAction; //url of the controller and (/) main action
		$this->previousString = $previousString;
		$this->nextString = $nextString;
		
	}

	//return the number of pages
	public function getNumbOfPages()
	{
		return $this->_numbOfPages;
	}

	//get the limit of the select query clause
	public function getLimit($currentPage,$recordNumber,$recordPerPage)
	{
		$this->_currentPage = $currentPage;
		$this->_numbOfPages=(($recordNumber%$recordPerPage)===0) ? (int) ($recordNumber/$recordPerPage) : ((int) ($recordNumber/$recordPerPage))+1;
		$start=(($currentPage-1)*$recordPerPage);
		return "$start,$recordPerPage";
	}

	//return the page list string
	public function render($pageNumber,$numberOfPages)
	{
		$pageList = null;
		$pageList .= $this->pageLink($this->_currentPage-1,$this->previousString);
		$pageList .= $this->recursiveLink($pageNumber,$numberOfPages);
		$pageList .= $this->pageLink($this->_currentPage+1,$this->nextString);
		return $pageList;
	}

	//recorsive function in order to write the page list
	public function recursiveLink($pageNumber,$numberOfPages)
	{
		
		if ($numberOfPages === 0) return null;
		
		if ($numberOfPages === 1) {
			return $this->pageLink($pageNumber);
		} else {
			return $this->pageLink($pageNumber) . $this->recursiveLink($pageNumber+1,$numberOfPages-1);
		}
	}

	public function pageLink($pageNumber, $string = null) {
		if ($pageNumber > 0 and $pageNumber <= $this->_numbOfPages) {
			return $this->html($pageNumber,$string);
		} else {
			return null;
		}
	} 

	//return the html link
	public function html($pageNumber,$string = null) {
		if (isset($string)) {
			$strNumber = $string;
			$strClass = "class='itemListPage'";
		} else {
			if ($pageNumber === $this->_currentPage)
			{
				$strNumber = $pageNumber;
				$strClass = "class='currentPage'";
			}
			else
			{
				$strNumber = $pageNumber;
				$strClass = "class='itemListPage'";
			}
		}
		$this->viewArgs[$this->_variableArg] = $pageNumber;
		$viewStatus = Url::createUrl(array_values($this->viewArgs));
		$href= Url::getRoot(null) . $this->_urlViewAction .$viewStatus;
		return $this->getATag($href,$strNumber,$strClass);
	}

	//get the HTMl of the tag
	//$href: href of the link
	//$text: the text of the link
	//$strClass: the class of the link
	public function getATag($href,$text,$strClass)
	{
		return "<a $strClass href='$href'>$text</a>";
	}

}