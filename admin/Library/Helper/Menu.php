<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//class to write the top menù of the view files
class Helper_Menu extends Helper_Html
{

	public $panelController; //panel controller
	public $controller;

	public $links = array(
	
		'back'	=>	array(
			'title'	=>	'back',
			'class'	=>	'mainMenuItem',
			'text'	=>	'Back',
			'url'	=>	'main'
		),
		
		'add'	=>	array(
			'title'	=>	'add a new record',
			'class'	=>	'mainMenuItem',
			'text'	=>	'Add',
			'url'	=>	'form/insert'
		),

		'panel'	=>	array(
			'title'	=>	'back to the Panel',
			'class'	=>	'mainMenuItem',
			'text'	=>	'Panel',
			'url'	=>	'main'
		)
		
	);
	
	public function build($controller = null, $panelController = null)
	{
		$this->controller = $controller;
		$this->panelController = $panelController;
	}

	//$voices: comma-separated list of links you want to print 
	public function render($linksList)
	{
		$linksArray = explode(',',$linksList);
		$menu = null;
		foreach ($linksArray as $linkName)
		{
			//check that the voice exists
			if (array_key_exists($linkName,$this->links))
			{
				//check that the text and the ure are defined
				if (isset($this->links[$linkName]['text']) and isset($this->links[$linkName]['url']))
				{
					$title = isset($this->links[$linkName]['title']) ? "title='".$this->links[$linkName]['title']."'" : null;
					$class = isset($this->links[$linkName]['class']) ? "class='".$this->links[$linkName]['class']."'" : null;
					
					//choose the controller (current or panel)
					$controller = (strcmp($linkName,'panel') === 0) ? $this->panelController.'/' : $this->controller.'/';
					$viewStatus = (strcmp($linkName,'panel') === 0) ? null : $this->viewStatus;
					
					$href = Url::getRoot($controller.$this->links[$linkName]['url'].$viewStatus);
					$text = $this->links[$linkName]['text'];
					$menu .= "<div $class><a $title href='$href'>$text</a></div>\n";
				}
			}
		}
		return $menu;
	}

}