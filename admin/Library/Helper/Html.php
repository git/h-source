<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//base class of all the Helper classes that returns HTML
class Helper_Html { 

	public $viewArgs = array(); //arguments of the view action (to mantain the status, ex: page,language,etc)
	public $viewStatus = null; //additional string to the url to define the status of the view action (ex: page,language,etc)

}