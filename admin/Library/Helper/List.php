<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//class to create the HTML for the view action
class Helper_List extends Helper_Html {

	protected $_itemsList = array(); //2-dimensional associative array containing the list items
	//keys: type,table:field,controller/action,value
	protected $_head = array(); //2-dimensional array containing the head of the table
	protected $_identifierName;

	protected $_recordNumber = null; //number of records from the table

	protected $_allowedItems = array('simpleLink','simpleText','delForm','editForm','associateForm','moveupForm','movedownForm'); //type of items allowed

	//set if the submit buttons have to be images or not (it can be yse or not)
	public $submitImageType = 'no';
	
	//set the files of the images
	public $submitImages = array(
		'edit'		=>	null,
		'del'		=>	null,
		'up'		=>	null,
		'down'		=>	null,
		'link'		=>	null
	);

	//set the titles of the input:submit
	public $submitTitles = array(
		'edit'		=>	'edit the record',
		'del'		=>	'delete the record',
		'up'		=>	'move up the record',
		'down'		=>	'move down the record',
		'link'		=>	'associate the record'
	);

	//$position: array. First element: page number, second element: number of pages
	public $position = array();
	
	//it can be: both, top, bottom, none
	protected $_boundaries = 'none';

	public function build($identifierName = 'identifier')
	{
		$this->_identifierName = $identifierName;
	}

	public function setIdentifierName($identifierName)
	{
		$this->_identifierName = $identifierName;
	}

	//add a list Item. $type: the type of the item, $field: the table.field to exctract (use colon to separate the table and the field),$action: controller/action,$value=if type == link->the value of the link
	public function addItem($type, $action = '', $field = '', $name = '') {
		if (!in_array($type,$this->_allowedItems)) {
			throw new Exception('"'.$type. '" argument not allowed in '.__METHOD__.' method');
		}
		$temp=array();
		$temp['type'] = $type;
		$temp['action'] = $action;
		$temp['field'] = $field;
		$temp['name'] = $name;
		$this->_itemsList[] = $temp;

		//set the $this->_head array
		$head = array();
		$head['type'] = $type;

		if ($type === 'simpleText') {
			$head['action'] = $this->extractFieldName($action);
		} else {
			$head['action'] = '&nbsp';
		}
		$this->_head[] = $head;
	}


	//set the head of the table
	//$columnsName: name of the columns. It has to be a comma-separated list of strings
	public function setHead($columnsName = '')
	{
		//get the array from the list
		$columnsArray = explode(',',$columnsName);
		for ($i = 0; $i < count($columnsArray); $i++)
		{
			if ($i < count($this->_itemsList)) $this->_head[$i]['action'] = $columnsArray[$i];
		}
	}


	//$method to extract the field name from the $action string (;table:field;)
	public function extractFieldName($string) {
		$string = str_replace(';','',$string);
		return $string;
	}

	//replace the strings wrapped by ; with their correspondent value taken by the $recordArray associative array (a row of the select query)
	public function replaceFields($string,$rowArray) {
		$stringArray = explode(';',$string);
		for ($i = 0; $i < count($stringArray); $i++) {
			if (strstr($stringArray[$i],':')) {
				//check if a function has been indicated
				if (strstr($stringArray[$i],'|'))
				{
					//get the function
					$firstArray = explode('|',$stringArray[$i]);
					$func = $firstArray[0];
					//replace the fields
					$temp =  explode(':',$firstArray[1]);
					$stringArray[$i] = $rowArray[$temp[0]][$temp[1]];
					
					if (!function_exists($func)) {
						throw new Exception('Error in <b>'.__METHOD__.'</b>: function <b>'.$func.'</b> does not exists..');
					}
					//apply the function
					$stringArray[$i] = call_user_func($func,$stringArray[$i]);
				}
				else
				{
					$temp = explode(':',$stringArray[$i]);
					$stringArray[$i] = $rowArray[$temp[0]][$temp[1]];
				}
			}
		}
		return implode('',$stringArray);
	}

	//call the method replaceFields upon the $items array using the associative array $rowArray
	public function replaceAll($item,$rowArray) {
		$item['action'] = $this->replaceFields($item['action'],$rowArray);
		$item['field'] = $this->replaceFields($item['field'],$rowArray);
		$item['name'] = $this->replaceFields($item['name'],$rowArray);
		return $item;
	}

	//wrap the column with the tag td
	public function wrapColumn($string, $className = null) {
		return wrap($string,array('td'=>$className));
	}

	//wrap the row with the tag tr
	public function wrapRow($string,$className = null) {
		return wrap($string,array('tr'=>$className));
	}

	public function wrapList($string) {
		return wrap($string,array('table'=>'listTable'));
	}

	//method to create the HTML of the head of the table
	public function createHead() {
		$htmlHead = null;
		foreach ($this->_head as $item) {
			$temp = $item['action'];
			$htmlHead .= $this->wrapColumn($temp,$item['type']);
		}
		return $htmlHead;
	}

	//create the HTML of a single row (values taken from the associative array $rowArray)
	public function getRowList($rowArray) {
		$htmlList = null;
		foreach ($this->_itemsList as $item) {
			$item = $this->replaceAll($item,$rowArray);

			if (($this->_boundaries === 'top' and $item['type'] === 'moveupForm') or ($this->_boundaries === 'bottom' and $item['type'] === 'movedownForm') or ($this->_boundaries === 'both' and ($item['type'] === 'moveupForm' or $item['type'] === 'movedownForm')))
			{
				$htmlList .= $this->wrapColumn('&nbsp',$item['type']);
			}
			else
			{
				$temp = call_user_func_array(array($this,$item['type']),array($item));
				$htmlList .= $this->wrapColumn($temp,$item['type']);
			}
		}
		return $htmlList;
	}

	//$index: record number
	public function ifInBoundaries($index)
	{
		$this->_boundaries = 'none';
		
		if (!empty($this->position))
		{
			if ($this->_recordNumber === 1 and strcmp($this->position[0],1) === 0)
			{
				$this->_boundaries = 'both';
			}
			else if ($index === 0 and strcmp($this->position[0],1) === 0)
			{
				$this->_boundaries = 'top';
			}
			else if ($index === ($this->_recordNumber-1) and strcmp($this->position[0],$this->position[1]) === 0)
			{
				$this->_boundaries = 'bottom';
			}
		}

	}

	//create the HTML of the entire list. $queryResult: the array coming from the select query
	public function render($queryResult)
	{
		//set the number of records
		$this->_recordNumber = count($queryResult);
		$htmlList = null;
		//create the HTML of the head of the record list
		$htmlList .= $this->wrapRow($this->createHead(),'listHead');
		for ($i = 0; $i < count($queryResult); $i++)
		{
			$this->ifInBoundaries($i);
			$temp = $this->getRowList($queryResult[$i]);
			$htmlList .= $this->wrapRow($temp,'listRow');
		}
		return $this->wrapList($htmlList);
	}

	public function generalForm($itemArray, $submitName, $submitValue)
	{
		$string = "<form class='listItemForm' action='".Url::getRoot(null).$itemArray['action'].$this->viewStatus."' method='POST'>\n";
		$name = (strcmp($itemArray['name'],'') !== 0) ? $itemArray['name'] : $submitName;
		
		if (strcmp($this->submitImageType,'yes') === 0)
		{
			$string .= "<input type='image' title='".$this->submitTitles[$submitValue]."' src='".$this->submitImages[$submitValue]."' value='$submitValue'>\n";
			$string .= "<input type='hidden' name='".$name."' value='$submitValue'>\n";
		}
		else
		{
			$string .= "<input type='submit' title='".$this->submitTitles[$submitValue]."' name='".$name."' value='$submitValue'>\n";
		}
		
		$string .= "<input type='hidden' name='".$this->_identifierName."' value='".$itemArray['field']."'>\n";
		$string .= "</form>\n";
		return $string;
	}

	public function moveupForm($itemArray)
	{
		return $this->generalForm($itemArray, 'moveupAction', 'up');
	}

	public function movedownForm($itemArray)
	{
		return $this->generalForm($itemArray, 'movedownAction', 'down');
	}

	public function editForm($itemArray)
	{
		return $this->generalForm($itemArray, 'generalAction', 'edit');
	}

	public function delForm($itemArray)
	{
		return $this->generalForm($itemArray, 'delAction', 'del');
	}

	public function associateForm($itemArray)
	{
		return $this->generalForm($itemArray, 'generalAction', 'link');
	}

	public function simpleText($itemArray) {
		$string = "<span class='textItem'>".$itemArray['action']."</span>\n";
		return $string;
	}

	public function simpleLink($itemArray) {
		$string = "<a class='linkItem' href='".Url::getRoot(null).$itemArray['action'].$this->viewStatus."'>".$itemArray['name']."</a>\n";
		return $string;
	}
	
}