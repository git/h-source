<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//class to call the hooks
class Hooks
{

	//include an hook file
	public static function load($path)
	{
		if (file_exists($path))
		{
			include_once($path);
		}
	}
	
}