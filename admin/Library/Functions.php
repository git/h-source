<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');


/*
	SANITIZE FUNCTIONS
*/

function none($string) {
	return $string;
}

function forceInt($string) {
	return (int)$string;
}

function forceNat($string)
{
	$string = (int)$string;
	if ($string <= 0) return 1;
	return $string;
}

function sanitizeDb($stringa) {

	if (DATABASE_TYPE === 'Mysql')
	{
		$stringa = mysql_real_escape_string($stringa);
		return $stringa;
	}

	if (DATABASE_TYPE === 'Mysqli')
	{
		$mysqli = Db_Mysqli::getInstance();
		$db = $mysqli->getDb();
		$stringa = $db->real_escape_string($stringa);
		return $stringa;
	}

	return $stringa;
}

function sanitizeAll($stringa) {

	$stringa=sanitizeHtml($stringa);
	$stringa=sanitizeDb($stringa);
	return $stringa;

}

function sanitizeHtml($stringa) {

	$charset = Params::$htmlentititiesCharset;
	$stringa=htmlentities($stringa,ENT_QUOTES,$charset);
	return $stringa;

}

//check if only alphabetic + optional characters are present in the string $string. Set $string to $altString if other characters are found
//$optChar: allowed characters divided by '|'  Ex: '+|-|;'
function sanitizeCustom($string,$optChar,$altString = 'EasyGiant')
{
	
	$optChar = html_entity_decode($optChar,ENT_QUOTES);
	$optCharArray = explode('|',$optChar);
	$temp = $string;
	foreach($optCharArray as $char)
	{
		$temp = str_replace($char,null,$temp);
	}
	if (ctype_alnum($temp))
	{
		return $string;
	}
	else
	{
		return $altString;
	}
}




/*
SANITIZE DEEP
*/

function stripslashesDeep($value) {
	if(get_magic_quotes_gpc()) {#if stripslashes
		return array_map('stripslashes', $value);
	}
	return $value;
}


function sanitizeHtmlDeep($value) {
	return array_map('sanitizeHtml', $value);
}


function sanitizeDbDeep($value) {
	return array_map('sanitizeDb', $value);
}


function sanitizeCustomDeep($stringArray,$optChar,$altString = 'EasyGiant')
{
	$result = array();
	foreach ($stringArray as $key => $value)
	{
		$result[$key] = sanitizeCustom($value,$optChar,$altString);
	}
	return $result;
}


function sanitizeAllDeep($value) {
	return array_map('sanitizeAll', $value);
}


function forceIntDeep($value) {
	return array_map('forceInt', $value);
}

function forceNatDeep($value) {
	return array_map('forceNat', $value);
}

function noneDeep($value) {
	return array_map('none', $value);
}


function md5Deep($value) 
{
	return array_map('md5', $value);
}

function sha1Deep($value)
{
	return array_map('sha1', $value);
}







function sanitizeAlnum($string)
{
	return ctype_alnum($string) ? sanitizeAll($string) : '';
}


function sanitizeIp($ip)
{
	return preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/',$ip) ? sanitizeAll($ip) : '';
}


/*
	CHECK FUNCTIONS
*/

//check if a string has the mail format (abc.efg@hij.klm.on)
//modification of the rule found at http://www.sastgroup.com/tutorials/8-espressioni-regolari-per-validare-un-po-di-tutto
//original rule: /^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/
function checkMail($string)
{
	if (preg_match('/^[a-zA-Z0-9_\-]+([.][a-zA-Z0-9_\-]+){0,2}[@][a-zA-Z0-9_\-]+([.][a-zA-Z0-9_\-]+){0,2}[.][a-zA-Z]{2,4}$/',$string))
	{
		return true;
	}
	else
	{
		return false;
	}
}



function wrap($string,$tag_class) {#wrap the string with the tag and its class
	#$tag_class has to be an associative array (tag1=>class1,$tag2=>class2,.. )!!
	$str_front=null;
	$str_rear=null;
	if (is_array($tag_class)) {
		foreach ($tag_class as $tag => $class) {
				$tag = str_replace('+','',$tag);
				$str_class=isset($class) ? " class=\"".$class."\"" : null;
				$str_front.="<".$tag.$str_class.">\n";
				$str_rear.="</".$tag.">\n";
		}
	}
	return $str_front.$string.$str_rear;
}

//generate a random password
//$start: start number of mt_rand
//$end: end number of mt_rand
function randString($length,$start = 33, $end = 126)
{
	$random = '';
	for ($i = 0; $i < $length; $i++)
	{
		$random .= chr(mt_rand($start, $end));
	}
	return $random;
}

//generate a random string
//$charNumb:number of characters of the final string
//$allowedChars: allowed characters
function generateString($charNumb = 8,$allowedChars = '0123456789abcdefghijklmnopqrstuvwxyz')
{
	$str = null;
	for ($i = 0; $i < $charNumb; $i++)
	{
		$str .= substr($allowedChars, mt_rand(0, strlen($allowedChars)-1), 1);
	}
	return $str;
}


function getIp()
{
    $ip = "";

    if (isset($_SERVER))
    {
        if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
            $ip = sanitizeIp($_SERVER["HTTP_X_FORWARDED_FOR"]);
        } else if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = sanitizeIp($_SERVER["HTTP_CLIENT_IP"]);
        } else {
            $ip = sanitizeIp($_SERVER["REMOTE_ADDR"]);
        }
    } else {
        if ( getenv( 'HTTP_X_FORWARDED_FOR' ) !== false ) {
            $ip = sanitizeIp(getenv( 'HTTP_X_FORWARDED_FOR' ));
        } else if ( getenv( 'HTTP_CLIENT_IP' ) !== false ) {
            $ip = sanitizeIp(getenv( 'HTTP_CLIENT_IP' ));
        } else {
            $ip = sanitizeIp(getenv( 'REMOTE_ADDR' ));
        }
    }
    return $ip;
}



function getUserAgent() {
	if (isset($_SERVER['HTTP_USER_AGENT']))
	{
		return md5($_SERVER['HTTP_USER_AGENT']);
	} 
	else
	{
		return md5('firefox');
	}
}


function xml_encode($string)
{
	$trans = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
	foreach ($trans as $k=>$v)
	{
		$trans[$k]= "&#".ord($k).";";
	}
	
	return strtr($string, $trans);
}