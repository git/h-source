<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');


function eg_strlen($string)
{
	return Params::$mbStringLoaded === true ? mb_strlen($string,DEFAULT_CHARSET) : strlen($string);
}


function eg_strtoupper($string)
{
	return Params::$mbStringLoaded === true ? mb_strtoupper($string,DEFAULT_CHARSET) : strtoupper($string);
}


function eg_strtolower($string)
{
	return Params::$mbStringLoaded === true ? mb_strtolower($string,DEFAULT_CHARSET) : strtolower($string);
}


// function eg_substr($string, $start, $length)
// {
// 	return Params::$mbStringLoaded === true ? mb_strtolower($string,DEFAULT_CHARSET) : strtolower($string);
// }