<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//aach module class has to inherits from this abstract class
abstract class ModAbstract
{
	
	//reference to a simpleXML object
	protected $simpleXmlObj = null;
	
	//type hinting: simplexmlelement
	public function __construct(SimpleXMLElement $simpleXmlObj)
	{
		$this->simpleXmlObj = $simpleXmlObj;
	}
	
	//define the abstract method to render (create the HTML) of the single module
	//$xmlObject: simpleXML object
	abstract public function render();

}
