<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//class to create a thumbnail
class Image_Gd_Thumbnail
{
	const DS = DIRECTORY_SEPARATOR;
	
	private $params = array(); //parameters of the object
	private $basePath = null; //the path of the folder inside which the images are saved
	
	public function __construct($basePath,$params = null)
	{
		$finalChar = $basePath[strlen($basePath) - 1];
		if (strcmp($finalChar,self::DS) !== 0) $basePath .= self::DS;
		
		$this->basePath = $basePath;
		
		$defaultParams = array(
			'imgWidth'		=>	100,
			'imgHeight'		=>	100,
			'defaultImage'	=>	null,
			'cropImage'		=>	'no',
			'horizAlign'	=>	'left',
			'vertAlign'		=>	'top'
		);

		//set the $this->scaffold->params array
		if (is_array($params))
		{
			foreach ($params as $key => $value)
			{
				$defaultParams[$key] = $value;
			}
		}
		$this->params = $defaultParams;
	}
	
	//create the thumbnail
	//$imageName: the name of the file inside $this->basePath
	public function render($imageFile)
	{
		$imagePath = $this->basePath . basename($imageFile);
		
		if (!file_exists($imagePath) and isset($this->params['defaultImage'])) $imagePath = $this->params['defaultImage'];
		
		$img = null;
		$ext = strtolower(end(explode('.', $imagePath)));
		
		if (strcmp($ext,'jpg') === 0 or strcmp($ext,'jpeg') === 0) {
			$img = @imagecreatefromjpeg($imagePath);
		} else if (strcmp($ext,'png') === 0) {
			$img = @imagecreatefrompng($imagePath);
		} else if (strcmp($ext,'gif') === 0) {
			$img = @imagecreatefromgif($imagePath);
		}
		
		//If an image was successfully loaded, test the image for size
		if ($img) {
			//image size
			$width = imagesx($img);
			$height = imagesy($img);
			
			if ($this->params['cropImage'] === 'no')
			{
				$scale = min($this->params['imgWidth']/$width, $this->params['imgHeight']/$height);
			}
			else if ($this->params['cropImage'] === 'yes')
			{
				$scale = max($this->params['imgWidth']/$width, $this->params['imgHeight']/$height);
			}
			
			if ($scale < 1) {
   
				$xSrc = 0;
				$ySrc = 0;
   
				if ($this->params['cropImage'] === 'no')
				{
					$newWidth = floor($scale*$width);
					$newHeight = floor($scale*$height);
				}
				else if ($this->params['cropImage'] === 'yes')
				{
			
					$newWidth = $this->params['imgWidth'];
					$newHeight = $this->params['imgHeight'];
					$oldWidth = $width;
					$oldHeight = $height;
					$width = floor($newWidth/$scale);
					$height = floor($newHeight/$scale);
					
					switch ($this->params['horizAlign'])
					{
						case 'left':
							$xSrc = 0;
							break;
						case 'right':
							$xSrc = floor(($oldWidth-$width));
							break;
						case 'center':
							$xSrc = floor(($oldWidth-$width)/2);
							break;
						default:
							$xSrc = $this->params['horizAlign'];
					}

					switch ($this->params['vertAlign'])
					{
						case 'top':
							$ySrc = 0;
							break;
						case 'bottom':
							$ySrc = floor(($oldHeight-$height));
							break;
						case 'center':
							$ySrc = floor(($oldHeight-$height)/2);
							break;
						default:
							$ySrc = $this->params['vertAlign'];
					}

				}

				//temp image
				$tmpImg = imagecreatetruecolor($newWidth, $newHeight);

				//copy and resize
				imagecopyresized($tmpImg, $img, 0, 0, $xSrc, $ySrc,$newWidth, $newHeight, $width, $height);
				imagedestroy($img);
				$img = $tmpImg;
			}
			
		}
		
		if (!$img) {
			$img = imagecreate($this->params['imgWidth'], $this->params['imgHeight']);
			imagecolorallocate($img,200,200,200);
		}

		//print the image
		header("Content-type: image/jpeg");
		imagejpeg($img);
		
	}
	
}