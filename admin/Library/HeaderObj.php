<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//manage the header
class HeaderObj {

	private $domainName; //the base path of the website (domainname)

	public function __construct($domainName)
	{
		$this->domainName = $domainName;
	}

	//redirect to $path after the time $time
	//string that  appears until the page is redirected
	public function redirect($path,$time = 0,$string = null)
	{
		$completePath = Url::getRoot().$path;
		header('Refresh: '.$time.';url='.$completePath);
		if (isset($string)) echo $string;
		exit;
	}

}
