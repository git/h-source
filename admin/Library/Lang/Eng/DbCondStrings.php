<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//error strings in the case database conditions are not satisfied
class Lang_Eng_DbCondStrings {

	//get the error string in the case that the value of the field $field is already present in the table $table
	public function getNotUniqueString($field)
	{
		return "<div class='alert'>The value of <i>". $field ."</i> is already present. Please choose a different value.</div>\n";
	}

}
