<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

class Lang_ResultStrings {

	public $string = array();
	
	//method to get the string $stringName
	public function getString($stringName)
	{
		if (isset($this->string[$stringName]))
		{
			return $this->string[$stringName];
		}
		else
		{
			return 'result string not defined!';
		}
	}

}
