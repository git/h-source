<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//manage the associative arrays inside the request ($_GET,$_POST,$_COOKIE)
class Request
{

	public function get($name, $default = null, $func = 'none')
	{
		if (!function_exists($func))
		{
			throw new Exception('Error in <b>'.__METHOD__.'</b>: function <b>'.$func. '</b> does not exists');
		}
		return isset($_GET[$name]) ? call_user_func($func,$_GET[$name]) : $default;
	}

	public function post($name, $default = null, $func = 'none')
	{
		if (!function_exists($func))
		{
			throw new Exception('Error in <b>'.__METHOD__.'</b>: function <b>'.$func. '</b> does not exists');
		}
		return isset($_POST[$name]) ? call_user_func($func,$_POST[$name]) : $default;
	}

	public function cookie($name, $default = null, $func = 'none')
	{
		if (!function_exists($func))
		{
			throw new Exception('Error in <b>'.__METHOD__.'</b>: function <b>'.$func. '</b> does not exists');
		}
		return isset($_COOKIE[$name]) ? call_user_func($func,$_COOKIE[$name]) : $default;
	}
	
}