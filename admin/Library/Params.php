<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//class containing all the parameters necessary to EasyGiant to work properly
class Params
{

	public static $allowedDb = array('Mysql','Mysqli','None'); //allowed database type

	public static $allowedSanitizeFunc = 'sanitizeAll,sanitizeDb,sanitizeHtml,forceInt,forceNat,none,md5,sha1'; //allowed sanitize functions

	public static $allowedHashFunc = array('md5','sha1'); //allowed hash functions
	
	//conventional null value for the value of the field in the createWhereClause method of the Model class
	public static $nullQueryValue = false;

	//class name of the div that contains the error strings
	public static $errorStringClassName = 'alert';
	
	//table name in the returning structure of the select queries in the case of an aggregate function. Ex count(*),sum(*)
	public static $aggregateKey = 'aggregate';
	
	//htmlentities function charset
	//see http://php.net/manual/en/function.htmlentities.php for a complete list of the allowed values
	public static $htmlentititiesCharset = DEFAULT_CHARSET;
	
	//list of symbols used in the statements of the where clause of the select queries
	public static $whereClauseSymbolArray = array('<','>','!=','<=','>=','in(','not in(');
	
	//is the mbstring extension enabled?
	public static $mbStringLoaded = false;

}