<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

require_once (ROOT . DS . 'Config' . DS . 'Reporting.php');
require_once (ROOT . DS . 'Config' . DS . 'Restricted.php');
require_once (ROOT . DS . 'Config' . DS . 'Autoload.php');
require_once (ROOT . DS . 'Library' . DS . 'Functions.php');
require_once (ROOT . DS . 'Library' . DS . 'Strings' . DS . 'Functions.php');
require_once (ROOT . DS . 'Library' . DS . 'ErrorReporting.php');
require_once (ROOT . DS . 'Library' . DS . 'Call.php');
