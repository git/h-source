<?php

/**
 * EasyGiant
 *
 * LICENSE
 *
 * All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * See COPYRIGHT.txt and LICENSE.txt.
 *  
 * @package		EasyGiant
 * @license   	http://www.gnu.org/licenses/gpl.html GNU General Public License version 3 or any later version
 */

if (!defined('EG')) die('Direct access not allowed!');

/** create the HTML of an input text entry */
class Form_Checkbox extends Form_Entry
{

	public function __construct($entryName = null)
	{
		$this->entryName = $entryName;
	}

	public function render($value = null)
	{
		$wrap = $this->getWrapElements();
		$returnString = "<div class='".$this->getEntryClass()."'>\n\t";
		$returnString .= $wrap[0];
		$returnString .= $this->getLabelTag();
		$returnString .= $wrap[1];
		$returnString .= Html_Form::checkbox($this->entryName, $value, $this->options, $this->className,$this->idName);
		$returnString .= $wrap[2];
		$returnString .="</div>\n";
		return $returnString;
	}

}
