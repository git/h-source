<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//create the HTML of a textarea entry
class Form_Textarea extends Form_Entry
{

	public function __construct($entryName = null)
	{
		$this->entryName = $entryName;
	}

	public function render($value = null)
	{
		$wrap = $this->getWrapElements();
		$returnString = "<div class='".$this->getEntryClass()."'>\n\t";
		$returnString .= $wrap[0];
		$returnString .= $this->getLabelTag();
		$returnString .= $wrap[1];
		$returnString .= Html_Form::textarea($this->entryName, $value, $this->className, $this->idName);
		$returnString .= $wrap[2];
		$returnString .="</div>\n";
		return $returnString;
	}

}
