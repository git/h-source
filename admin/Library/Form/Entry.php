<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//base class of the form entries
abstract class Form_Entry {

	public $entryName = null; //the name of the entry
	public $entryClass = null; //the class of the entry
	public $idName = null; //the id of the input entry
	public $className = null; //the class of the input entry
	public $labelString = null; //label of the form
	public $labelClass = null; //the class of the tag of the label
	public $options = array(); //options (if the entry is a <select> entry or a radio button). Associative array or comma-divided list.
	public $defaultValue = '';
	public $wrap = array();
	public $type = null; //the type of the entry

	//create the label of each entry of the form
	public function getLabelTag()
	{
		$labelTagClass = isset($this->labelClass) ? $this->labelClass : 'entryLabel';
		return isset($this->labelString) ? "<label class='$labelTagClass'>".$this->labelString."</label>\n\t" : null;
	}

	//get the class of the entry
	public function getEntryClass()
	{
		return isset($this->entryClass) ? $this->entryClass : 'formEntry';
	}

	public function getWrapElements()
	{
		$wrap[0] = isset($this->wrap[0]) ? $this->wrap[0] : null;
		$wrap[1] = isset($this->wrap[1]) ? $this->wrap[1] : null;
		$wrap[2] = isset($this->wrap[2]) ? $this->wrap[2] : null;
		return $wrap;
	}

	abstract public function render($value = null);

}
