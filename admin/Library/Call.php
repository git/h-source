<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');


/* SANITIZE SUPERGLOBAL ARRAYS */
function sanitizeSuperGlobal()
{
	$_GET = stripslashesDeep($_GET);

	$_POST   = stripslashesDeep($_POST);

	$_COOKIE = stripslashesDeep($_COOKIE);

	$_SERVER = stripslashesDeep($_SERVER);
}



function checkPostLength()
{
	if (MAX_POST_LENGTH !== 0)
	{
		foreach ($_POST as $key => $value)
		{
			if (strlen($value) > MAX_POST_LENGTH) die('the length of some of the $_POST values is too large');
		}
	}
}

function checkRequestUriLength()
{
	if (MAX_REQUEST_URI_LENGTH !== 0)
	{
		if (strlen($_SERVER['REQUEST_URI']) > MAX_REQUEST_URI_LENGTH) die('the length of the REQUEST_URI is too large');
	}
}

function checkRegisterGlobals()
{
    if (ini_get('register_globals')) die('register globals is on: easyGiant works only with register globals off');
}

function callHook()
{

	if (MOD_REWRITE_MODULE === true)
	{
		$url = isset($_GET['url']) ? $_GET['url'] : DEFAULT_CONTROLLER . '/' . DEFAULT_ACTION;
	}
	else
	{
		$url = (strcmp(getQueryString(),"") !== 0) ? getQueryString() : DEFAULT_CONTROLLER . '/' . DEFAULT_ACTION;
	}
	
	//rewrite the URL
// 	if (Route::$rewrite === 'yes')
// 	{
// 		$url = rewrite($url);
// 	}

// 	echo $url;
	
	$urlArray = array();
	$urlArray = explode("/",$url);

	$controller = DEFAULT_CONTROLLER;
	$action = DEFAULT_ACTION;
	
	if (isset($urlArray[0]))
	{
		$controller = (strcmp($urlArray[0],'') !== 0) ? strtolower(trim($urlArray[0])) : DEFAULT_CONTROLLER;
	}

	array_shift($urlArray);

	if (isset($urlArray[0]))
	{
		$action = (strcmp($urlArray[0],'') !== 0) ? strtolower(trim($urlArray[0])) : DEFAULT_ACTION;
	}

	//set ERROR_CONTROLLER and ERROR_ACTION
	$errorController = ERROR_CONTROLLER !== false ? ERROR_CONTROLLER : DEFAULT_CONTROLLER;
	$errorAction = ERROR_ACTION !== false ? ERROR_ACTION : DEFAULT_ACTION;

	/*
	VERIFY THE ACTION NAME
	*/
	if (method_exists('Controller', $action) or !ctype_alnum($action) or (strcmp($action,'') === 0))
	{
		$controller = $errorController;
		$action = $errorAction;
		$urlArray = array();
	}

	/*
	VERIFY THE CONTROLLER NAME
	*/
	if (!ctype_alnum($controller) or (strcmp($controller,'') === 0))
	{
		$controller = $errorController;
		$action = $errorAction;
		$urlArray = array();
	}

	//check that the controller class belongs to the application/controllers folder
	//otherwise set the controller to the default controller
	if (!file_exists(ROOT.DS.APPLICATION_PATH.DS.'Controllers'.DS.ucwords($controller).'Controller.php'))
	{
		$controller = $errorController;
		$action = $errorAction;
		$urlArray = array();
	}

	//set the controller class to DEFAULT_CONTROLLER if it doesn't exists
	if (!class_exists(ucwords($controller).'Controller'))
	{
		$controller = $errorController;
		$action = $errorAction;
		$urlArray = array();
	}

	//set the action to DEFAULT_ACTION if it doesn't exists
	if (!method_exists(ucwords($controller).'Controller', $action))
	{
		$controller = $errorController;
		$action = $errorAction;
		$urlArray = array();
	}

	/*
		CHECK COUPLES CONTROLLER,ACTION
	*/
	if (!in_array('all',Route::$allowed))
	{
		$couple = "$controller,$action";
		if (!in_array($couple,Route::$allowed))
		{
			$controller = $errorController;
			$action = $errorAction;
			$urlArray = array();
		}
	}
	
	array_shift($urlArray);
	$queryString = $urlArray;
	//set the name of the application
	$application = $controller;
	$controller = ucwords($controller);
	$model = $controller;
	$controller .= 'Controller';
	$model .= 'Model';

	//include the file containing the set of actions to carry out before the initialization of the controller class
	Hooks::load(ROOT . DS . APPLICATION_PATH . DS . 'Hooks' . DS . 'BeforeInitialization.php');

	if (class_exists($controller))
	{
		$dispatch = new $controller($model,$application,$queryString);
		
		//pass the action to the controller object
		$dispatch->action = $action;
		$dispatch->currPage = $dispatch->baseUrl.'/'.$dispatch->controller.'/'.$dispatch->action;
		
		//require the file containing the set of actions to carry out after the initialization of the controller class
		Hooks::load(ROOT . DS . APPLICATION_PATH . DS . 'Hooks' . DS . 'AfterInitialization.php');

		$templateFlag= true;

		if (method_exists($controller, $action))
		{
			//pass the action to the theme object
			$dispatch->theme->action = $action;
			$dispatch->theme->currPage = $dispatch->baseUrl.'/'.$dispatch->controller.'/'.$dispatch->action;
			
			call_user_func_array(array($dispatch,$action),$queryString);
		}
		else
		{
			$templateFlag= false;
		}

		if ($templateFlag)
		{
			$dispatch->theme->render();
		}

	}
	else
	{
		echo "<h2>the '$controller' controller is not present!</h2>";
	}

}


// //rewrite the URL
// function rewrite($url)
// {
// 	foreach (Route::$map as $key => $address)
// 	{
// 		if (preg_match('/^'.$key.'/',$url))
// 		{
// 			return preg_replace('/^'.$key.'/',$address,$url);
// 		}
// 	}
// 	return ERROR_CONTROLLER.'/'.ERROR_ACTION;
// }

function getQueryString()
{

	if (strstr($_SERVER['REQUEST_URI'],'index.php/'))
	{
		return Params::$mbStringLoaded === true ? mb_substr(mb_strstr($_SERVER['REQUEST_URI'],'index.php/'),10) : substr(strstr($_SERVER['REQUEST_URI'],'index.php/'),10);
	}

	return '';
}

function __autoload($className)
{

	$backupName = $className;

	if (strstr($className,'_'))
	{
		$parts = explode('_',$className);
		$className = implode(DS,$parts);
	}

	if (file_exists(ROOT . DS . 'Library' . DS . $className . '.php'))
	{
		require_once(ROOT . DS . 'Library' . DS . $className . '.php'); 
	}
	else if (file_exists(ROOT . DS . APPLICATION_PATH . DS . 'Controllers' . DS . $backupName . '.php'))
	{
		require_once(ROOT . DS . APPLICATION_PATH . DS . 'Controllers' . DS . $backupName . '.php');
	}
	else if (file_exists(ROOT . DS . APPLICATION_PATH . DS . 'Models' . DS . $backupName . '.php'))
	{
		require_once(ROOT . DS . APPLICATION_PATH . DS . 'Models' . DS . $backupName . '.php');
	}
	else if (file_exists(ROOT . DS . APPLICATION_PATH . DS . 'Modules' . DS . $backupName . '.php'))
	{
		require_once(ROOT . DS . APPLICATION_PATH . DS . 'Modules' . DS . $backupName . '.php');
	}
	else if (file_exists(ROOT . DS . APPLICATION_PATH . DS . 'Strings' . DS . $className . '.php'))
	{
		require_once(ROOT . DS . APPLICATION_PATH . DS . 'Strings' . DS . $className . '.php');
	}
	
}

try {

	//check the length of the $_POST values
	checkPostLength();
	
	//check the length of the REQUEST_URI
	checkRequestUriLength();
	
	//connect to the database
	Factory_Db::getInstance(DATABASE_TYPE,array(HOST,USER,PWD,DB));
	
	//set htmlentities charset
	switch (DEFAULT_CHARSET)
	{
		case 'SJIS':
			Params::$htmlentititiesCharset = 'Shift_JIS';
			break;
	}

	$allowedCharsets = array('UTF-8','ISO-8859-1','EUC-JP','SJIS');
	if (!in_array(DEFAULT_CHARSET,$allowedCharsets)) die('charset not-allowed');

	//check if the mbstring extension is loaded
	if (extension_loaded('mbstring'))
	{
		//set the internal encoding
		mb_internal_encoding(DEFAULT_CHARSET);
		Params::$mbStringLoaded = true;
	}
	
	//load the files defined inside Config/Autoload.php
	foreach (Autoload::$files as $file)
	{
		$ext = strtolower(end(explode('.', $file)));
		$path = ROOT . DS . APPLICATION_PATH . DS . 'Include' . DS . $file;
		if (file_exists($path) and $ext === 'php')
		{
			require_once($path);
		}
	}

	//include the file containing the set of actions to carry out before the check of the super global array
	Hooks::load(ROOT . DS . APPLICATION_PATH . DS . 'Hooks' . DS . 'BeforeChecks.php');

	//sanitize super global arrays
	sanitizeSuperGlobal();

	//report errors
	ErrorReporting();

	//verify that register globals is not active
	checkRegisterGlobals();

	//call the main hook
	callHook();

	//disconnect to the database
	Factory_Db::disconnect(DATABASE_TYPE);

} catch (Exception $e) {

	echo '<div class="alert">Message: '.$e->getMessage().'</div>';

}