<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

class Url {

	//get the url starting from the root folder
	public static function getRoot($pathFromRootFolder = null) {
		$url = MOD_REWRITE_MODULE === true ? '/admin/' . $pathFromRootFolder : '/admin/index.php/' . $pathFromRootFolder;
		return $url;
	}

	//create an url string (element1/element2/element4) from the values of the array $valuesArray considering only the elements indicated in the numeric string $numericString (in this case '1,2,4')
	public function createUrl($valuesArray,$numericString = null) {
		$elementsArray = explode(',',$numericString);
		$valuesArray = array_values($valuesArray);
		$urlString = null;
		for ($i = 0; $i < count($valuesArray); $i++)
		{
			if (isset($numericString)) {
				if (isset($valuesArray[$i]) and in_array($i,$elementsArray)) {
					$urlString .= "/".$valuesArray[$i];
				}
			} else {
				if (isset($valuesArray[$i])) {
					$urlString .= "/".$valuesArray[$i];
				}
			}
		}
		return $urlString;
	}

} 
