<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//create the HTML of the inputs of a form
class Html_Form {
	
	//return the HTML of a select
	//$name: name of the select
	//$value: the selected value of the select (set $value equal to null if you don't want to select an option)
	//$options: options of the select. This param can be a comma-separated list of options or an associative array ('name'=>'value')
	//$className: the class name of the select
	//$idName: name of the id
	static public function select($name, $value, $options, $className = null, $idName = null)
	{
		$strClass = isset($className) ? "class='".$className."'" : null;
		$idStr = isset($idName) ? "id='".$idName."'" : null;
		
		$returnString = null;
		$returnString .= "<select ".$idStr." $strClass name='".$name."'>\n";
		if (is_string($options)) {
			$tempArray = explode(',',$options);
			foreach ($tempArray as $item)
			{
				if (strstr($item,'optgroupOpen:'))
				{
					$temp = explode(':',$item);
					$optionsArray[$temp[1]] = "optgroupOpen";
				}
				else
				{
					$optionsArray[$item] = $item;
				}
			}
		}
		else
		{
			$optionsArray = $options;
		}

		$flag = 0;
		foreach ($optionsArray as $optionName => $optionValue) {
			if (strcmp($optionValue,'optgroupOpen') === 0)
			{
				if ($flag === 1) $returnString .= "</optgroup>\n";
				$returnString .= "<optgroup label=" . $optionName . ">\n";
				$flag = 1;
			}
			else
			{
				$str= (strcmp($value,$optionValue) === 0) ? "selected='$optionValue'" : null;
				$returnString .= "<option value='".$optionValue."' $str>$optionName</option>\n";
			}
		}
		if ($flag === 1) $returnString .= "</optgroup>\n";
		$returnString .= "</select>\n";
		return $returnString;
	}

	//return the HTML of an <input type='text' ...>
	//$name: the name of the input
	//$value: the value of the input
	//$className: the class name of the input
	//$idName: name of the id
	static public function input($name, $value, $className = null, $idName = null)
	{
		$strClass = isset($className) ? "class='".$className."'" : null;
		$idStr = isset($idName) ? "id='".$idName."'" : null;
		
		$returnString ="<input ".$idStr." $strClass type='text' name='" .$name. "' value = '$value'>\n";
		return $returnString;
	}

	//return the HTML of a checkBox
	//$name: name of the checkBox (string)
	//$value: the value of the checkBox (string or number)
	//$option: option of the checkBox (string or number)
	//$className: the class name of the checkBox (string)
	//$idName: name of the id
	static public function checkbox($name, $value, $option, $className = null, $idName = null)
	{
		$strClass = isset($className) ? "class='".$className."'" : null;
		$idStr = isset($idName) ? "id='".$idName."'" : null;
		
		$str = (strcmp($value,$option) === 0) ? "checked = 'checked'" : null;
		return "<input ".$idStr." $strClass type='checkbox' name='".$name."' value='".$option."' $str>\n";
	}
	
	//return the HTML of a hidden entry
	//$name: name of the hidden entry (string)
	//$value: the value of the hidden entry (string or number)
	static public function hidden($name, $value)
	{
		return "<input type='hidden' name='" .$name. "' value = '$value'>\n";
	}

	//return the HTML of a password entry
	//$name: name of the password entry (string)
	//$value: the value of the password entry (string or number)
	//$idName: name of the id
	static public function password($name, $value, $className = null, $idName = null)
	{
		$strClass = isset($className) ? "class='".$className."'" : null;
		$idStr = isset($idName) ? "id='".$idName."'" : null;
		
		return "<input ".$idStr." $strClass type='password' name='" .$name. "' value='$value'>\n";
	}

	//return the HTML of a textarea
	//$name: name of the textarea (string)
	//$value: the value of the textarea (string or number)
	//$idName: name of the id
	static public function textarea($name, $value, $className = null, $idName = null)
	{
		$strClass = isset($className) ? "class='".$className."'" : null;
		$idStr = isset($idName) ? "id='".$idName."'" : null;
		
		return "<textarea ".$idStr." $strClass name='" .$name. "'>$value</textarea>\n";
	}
	
	//return the HTML of a radio button
	//$name: name of the radio button
	//$value: the selected value of the radio button (set $value equal to null if you don't want to select an option)
	//$options: options of the radio button. This param can be a comma-separated list of options or an associative array ('name'=>'value')
	//$className: the class name of the radio button
	//$position: position of the strings of the radio with respect to the "circles". It can be before or after
	//$idName: name of the id
	static public function radio($name, $value, $options, $className = null, $position = 'after', $idName = null)
	{
		$strClass = isset($className) ? "class='".$className."'" : null;
		$idStr = isset($idName) ? "id='".$idName."'" : null;
		
		$returnString = null;
		
		if (is_string($options)) {
			$tempArray = explode(',',$options);
			foreach ($tempArray as $item)
			{
				$optionsArray[$item] = $item;
			}
		} else {
			$optionsArray = $options;
		}
		
		foreach ($optionsArray as $optionName => $optionValue) {
			
			if ($position === 'before')
			{
				$before = $optionName;
				$after = null;
			}
			else
			{
				$before = null;
				$after = $optionName;
			}
			
			$str= (strcmp($value,$optionValue) === 0) ? "checked='checked'" : null;
			$returnString .= "$before<input ".$idStr." $strClass type='radio' name='".$name."' value='".$optionValue."' $str>$after\n";
		}
		
		return $returnString;
	}
	
}