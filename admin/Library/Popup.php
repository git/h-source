<?php

// All EasyGiant code is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// See COPYRIGHT.txt and LICENSE.txt.

if (!defined('EG')) die('Direct access not allowed!');

//class to contain the values of the popup menù of the scaffold
class Popup {

	public $name; //the name of the popup
	public $itemsName = array(); //array containing the names of the different items of the list (popup)
	public $itemsValue = array(); //array containing the values of the different items of the list (popup)

}
