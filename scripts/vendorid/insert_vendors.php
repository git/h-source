#! /usr/bin/php
<?php

//created by Antonio Gallo (tonicucoz@yahoo.com) and P. J. McDermott
//Additional changes by Ian Gilfillan
// this script is in the Public Domain


//Accepts on argument, either -v or -s
//-v for verbose, which shows all the inserts/updates that are being made
//-s for safe, which shows all the inserts/updates to be made without actually making them
if (isset($argv[1])) {
	if ($argv[1]=='-v') {
		$verbose=1;
		$change=1;
	}
	elseif ($argv[1]=='-s'){
		$change=0;
		$verbose=1;
	}
}
else {
	$verbose=0;
	$change=1;
}

include("connect.php");
include("functions.php");

//insert the PCI vendors
$ids_fp = fopen('pci.ids', 'r');

while (($line = fgets($ids_fp)) !== false) {
	if (preg_match('/^([0-9a-f]{4})\s+(.*)\s*$/',
		$line, $matches) == 1) {
		$vendorid = $matches[1];
		$full_name = sanitizeDb(decode_soft($matches[2]));
		$clean_name = sanitizeDb(decode($matches[2]));

		// Check if record exists, insert if not
		$query = "SELECT vendorid, clean_name FROM vendors WHERE vendorid='$vendorid' AND bus='PCI'";
		if ($result = DB::$instance->query($query)) {
			$found = $result->num_rows;
		}
		if ($found == 0) {	
			//insert
			$query = "INSERT INTO vendors (bus,vendorid,clean_name,full_name) VALUES ('PCI','$vendorid','$clean_name','$full_name');";
			if ($verbose) {
				echo "\n Adding record: PCI $vendorid $clean_name $full_name";
			}
			if ($change) {
				DB::$instance->query($query);
			}
		}

		// Check if record has changed, update if so
		$query = "SELECT vendorid, clean_name, full_name FROM vendors WHERE vendorid='$vendorid' AND (BINARY clean_name!='$clean_name' OR BINARY full_name!='$full_name') AND bus='PCI'";
		if ($result = DB::$instance->query($query)) {
			$found = $result->num_rows;
		}
		if ($found > 0) {
			$row = $result->fetch_assoc();
			//update
			$query = "UPDATE vendors SET clean_name='$clean_name', full_name='$full_name' WHERE vendorid='$vendorid' AND bus='PCI';";
			if ($verbose) {
				echo "\n Updating record - old value: PCI " . $row['clean_name'] . " " . $row['full_name'];
				echo "\n Updating record - new value: PCI $clean_name $full_name";
			}
			if ($change) {
				DB::$instance->query($query);
			}
		}
	}
}

fclose($ids_fp);

//insert the USB vendors
$ids_fp = fopen('usb.ids', 'r');

while (($line = fgets($ids_fp)) !== false) {
	if (preg_match('/^([0-9a-f]{4})\s+(.*)\s*$/',
		$line, $matches) == 1) {
		$vendorid = $matches[1];
		$full_name = sanitizeDb(decode_soft($matches[2]));
		$clean_name = sanitizeDb(decode($matches[2]));

                $query = "SELECT vendorid FROM vendors WHERE vendorid='$vendorid' AND bus='USB'";
                if ($result = DB::$instance->query($query)) {
                        $found = $result->num_rows;
                }
                if ($found == 0) {
                        //insert
			$query = "INSERT INTO vendors (bus,vendorid,clean_name,full_name) VALUES ('USB','$vendorid','$clean_name','$full_name');";
			if ($verbose) {
				echo "\n Adding record: USB $vendorid $clean_name $full_name";
                        }
			if ($change) {
				DB::$instance->query($query);
			}
                }


                // Check if record has changed, update if so
                $query = "SELECT vendorid, clean_name, full_name FROM vendors WHERE vendorid='$vendorid' AND (BINARY clean_name!='$clean_name' OR BINARY full_name!='$full_name') AND bus='USB'";
                if ($result = DB::$instance->query($query)) {
                        $found = $result->num_rows;
                }
                if ($found >0) {
			$row = $result->fetch_assoc();
			$query = "UPDATE vendors SET clean_name='$clean_name', full_name='$full_name' WHERE vendorid='$vendorid' AND bus='USB';";
			if ($verbose) {
				echo "\n Updating record - old value: USB " . $row['clean_name'] . " " . $row['full_name'];
                        	echo "\n Updating record - new value: USB $clean_name $full_name";
                        }
			if ($change) {
				DB::$instance->query($query);
			}
                }

	}
}

fclose($ids_fp);
