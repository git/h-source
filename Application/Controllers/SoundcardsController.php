<?php 

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class SoundcardsController extends GenericController
{

	public function __construct($model, $controller, $queryString)
	{

		parent::__construct($model, $controller, $queryString);
		
		//load the model
		$this->model('HardwareModel');
		$this->model('RevisionsModel');
		$this->model('SoundcardsModel');
		$this->model('TalkModel');
		
		$this->mod = $this->m['SoundcardsModel'];
		
		$this->m['HardwareModel']->id_user = $this->s['registered']->status['id_user'];
		$this->m['HardwareModel']->type = 'soundcard';

		$this->m['HardwareModel']->setConditions(Soundcards::$audioSelect,'sound_card_works',Soundcards::$interface);

		$this->m['HardwareModel']->setFields('model,kernel,description,distribution,comm_year,sound_card_works,pci_id,interface,driver,other_names','sanitizeAll');
		
		$argKeys = array(
			'page:forceNat'						=>	1,
			'history_page:forceNat'				=>	1,
			'vendor:sanitizeString'				=>	'undef',
			'comm_year:sanitizeString'			=>	'undef',
			'sound_card_works:sanitizeString'	=>	'undef',
			'interface:sanitizeString'			=>	'undef',
			'sort-by:sanitizeString'			=>	'sound-card-works',
			'search_string:sanitizeString'		=>	'undef'
		);

		$this->setArgKeys($argKeys);
		
		$data['title'] = 'Soundcard';

		$data['intefaceOptions'] = Soundcards::$interface;
		$data['worksOptions'] = Soundcards::$audioSelect;
		$data['worksField'] = 'sound_card_works';

		$data['notFoundString'] = "No sound cards found";
		
		$this->append($data);
	}
	
	public function catalogue($lang = 'en')
	{		
		$this->shift(1);
		
		$whereArray = array(
			'type'				=>	$this->mod->type,
			'vendor'			=>	$this->viewArgs['vendor'],
			'comm_year'			=>	$this->viewArgs['comm_year'],
			'sound_card_works'	=>	$this->viewArgs['sound_card_works'],
			'interface'			=>	$this->viewArgs['interface'],
		);
		
		$this->mod->setWhereQueryClause($whereArray);
		
		parent::catalogue($lang);
	}

	public function view($lang = 'en', $id = 0, $name = null)
	{
		parent::view($lang, $id, $name);
	}

	public function history($lang = 'en', $id = 0)
	{
		parent::history($lang, $id);
	}

	public function revision($lang = 'en', $id_rev = 0)
	{
		parent::revision($lang, $id_rev);
	}

	public function insert($lang = 'en', $token = '')
	{
		parent::insert($lang, $token);
	}
	
	public function update($lang = 'en', $token = '')
	{
		parent::update($lang, $token);
	}

	public function differences($lang = 'en', $id_hard = 0, $id_rev = 0)
	{
		parent::differences($lang, $id_hard, $id_rev);
	}

	public function climb($lang = 'en', $id_rev = 0, $token = '')
	{
		parent::climb($lang, $id_rev, $token);
	}

	public function talk($lang = 'en', $id_hard = 0, $token = '')
	{
		parent::talk($lang, $id_hard, $token);
	}

}
