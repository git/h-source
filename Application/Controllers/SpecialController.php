<?php 

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class SpecialController extends BaseController
{

	public function __construct($model, $controller, $queryString)
	{
		parent::__construct($model, $controller, $queryString);
		
		$this->model('HistoryModel');
		$this->model('HardwareModel');
		
		$data['title'] = gtext('last modifications').' - '.Website::$generalName;
		$this->append($data);
		
		Params::$nullQueryValue = 'undef';
		
		$this->setArgKeys(array('page:forceNat'=>1,'username:sanitizeAll'=>'undef'));
	}

	protected function last($lang = 'en',$action = 'modifications')
	{
		switch ($action)
		{
			case 'modactions':
				$data['title'] = 'last moderations - '.Website::$generalName;
				$data['viewTitle'] = 'List of actions carried out by moderators';
				$data['user_status'] = 'MODERATOR';
				
				$whereClauseArray = array(
					'gr'	=>	'moderator',
				);

				$viewFile = 'modactions';
				
				break;
			case 'adminactions':
				$data['title'] = 'last admin actions - '.Website::$generalName;
				$data['viewTitle'] = 'List of actions carried out by administrators';
				$data['user_status'] = 'ADMINISTRATOR';
				
				$whereClauseArray = array(
					'gr'	=>	'admin',
				);

				$viewFile = 'modactions';

				break;

			case 'usersactions':
				$data['title'] = 'last registered users actions - '.Website::$generalName;
				$data['viewTitle'] = 'List of actions carried out by registered users';
				$data['user_status'] = 'USER';

				$whereClauseArray = array(
					'gr'	=>	'registered',
				);

				$viewFile = 'modactions';

				break;
		}
		$whereClauseArray['username'] = $this->viewArgs['username'];
		
		$this->helper('Pages','special/'.$this->action.'/'.$this->lang,'page');
		
		$this->m['HistoryModel']->clear()->left('regusers')->on('history.created_by=regusers.id_user')->select("history.*,regusers.username")->where($whereClauseArray)->orderBy('id_history desc');
		
		$recordNumber = $this->m['HistoryModel']->rowNumber();
		$page = $this->viewArgs['page'];

		$this->m['HistoryModel']->limit = $this->h['Pages']->getLimit($page,$recordNumber,30);

		$data['table'] = $this->m['HistoryModel']->send();

// 		echo $this->m['HistoryModel']->getQuery();
		
		$data['pageList'] = $this->h['Pages']->render($page-5,11);

		//the value of the filter
		$data['filterValue'] = strcmp($this->viewArgs['username'],'undef') === 0 ? null : $this->viewArgs['username'];

		$this->append($data);
		$this->load($viewFile);
		$this->right();
	}

	public function modifications($lang = 'en')
	{
		$this->shift(1);

		$data['title'] = 'last modifications - '.Website::$generalName;

		$whereClauseArray = array(
			'gr'	=>	'registered',
			'type'	=>	'hardware',
			'deleted'	=>	'no',
			'cleared'	=>	'no',
		);

		$this->helper('Pages','special/'.$this->action.'/'.$this->lang,'page');

		$this->m['HistoryModel']->clear()->select('history.*,hardware.*')->inner('hardware')->on('hardware.id_hard=history.id')->where($whereClauseArray)->orderBy('id_history desc');

		$recordNumber = $this->m['HistoryModel']->rowNumber();
		$page = $this->viewArgs['page'];

		$this->m['HistoryModel']->limit = $this->h['Pages']->getLimit($page,$recordNumber,30);

		$data['table'] = $this->m['HistoryModel']->send();
		
		$data['pageList'] = $this->h['Pages']->render($page-5,11);

		$this->append($data);
		$this->load('modifications');
		$this->right();
	}

	public function modactions($lang = 'en')
	{
		$this->shift(1);

		$this->s['registered']->check('moderator');
		
		$this->last($lang = 'en','modactions');
	}

	public function adminactions($lang = 'en')
	{
		$this->shift(1);

		$this->s['registered']->check('admin');

		$this->last($lang = 'en','adminactions');
	}

	public function usersactions($lang = 'en')
	{
		$this->shift(1);

		$this->s['registered']->check('moderator');

		$this->last($lang = 'en','usersactions');
	}

	protected function pagelist($lang = 'en', $type)
	{
		switch ($type)
		{
			case 'deleted':
				$whereArray = array('-deleted'=>'yes','approved'=>'yes');
				$data['titleString'] = 'hidden device pages';
				break;
			case 'notapproved':
				$whereArray = array('approved'=>'no');
				$data['titleString'] = 'Device pages that have to be approved';
				break;
		}
		
		$this->helper('Pages','special/'.$this->action.'/'.$this->lang,'page');

		$this->m['HardwareModel']->clear()->select()->where($whereArray)->orderBy('id_hard desc');

		$recordNumber = $this->m['HardwareModel']->rowNumber();
		$page = $this->viewArgs['page'];

		$this->m['HardwareModel']->limit = $this->h['Pages']->getLimit($page,$recordNumber,30);

		$data['table'] = $this->m['HardwareModel']->send();

		$data['pageList'] = $this->h['Pages']->render($page-5,11);

		$this->append($data);
		$this->load('pages_deleted');
		$this->right();
	}

	public function deleted($lang = 'en')
	{
		$this->shift(1);

		$this->s['registered']->check('admin');

		$this->pagelist($lang,'deleted');
	}

	public function notapproved($lang = 'en')
	{
		$this->shift(1);

		$this->s['registered']->check('admin');

		$this->pagelist($lang,'notapproved');
	}
	
}