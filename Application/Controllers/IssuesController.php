<?php 

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class IssuesController extends BaseController
{

	public function __construct($model, $controller, $queryString)
	{
   
		$this->_topMenuClasses['issues'] = " class='currentitem'";
		
		parent::__construct($model, $controller, $queryString);
		
		$this->model('IssuesModel');
		$this->model('MessagesModel');
		$this->model('UsersModel');
		
		$argKeys = array(
			'page:forceNat'				=>	1,
			'token:sanitizeAlphanum'	=>	$this->token,
		);

		$this->setArgKeys($argKeys);
		
		$data['title'] = 'issues - '.Website::$generalName;
		$this->append($data);
	}
	
	public function viewall($lang = 'en')
	{
		$this->shift(1);
		
		$this->m['IssuesModel']->setFields('title,topic,priority,message','sanitizeAll');
		
		$data['preview_message'] = null;
		
		if (isset($_POST['insertAction']))
		{
			if ($this->s['registered']->status['status'] === 'logged')
			{
				if (!$this->s['registered']->checkCsrf($this->viewArgs['token'])) $this->redirect('home/index/'.$this->lang,2,'wrong token..');
				
				if ($this->m['UsersModel']->isBlocked($this->s['registered']->status['id_user'])) $this->redirect('my/home/'.$this->lang,2,'your account has been blocked..');
				
				//set the page to 1 in the viewStatus
				$this->viewArgs['page'] = 1;
				$this->buildStatus();

				$clean['id_user'] = (int)$this->s['registered']->status['id_user'];
				
				$this->m['IssuesModel']->values['created_by'] = $clean['id_user'];
				$this->m['IssuesModel']->values['status'] = 'opened';
				$this->m['IssuesModel']->values['update_date'] = date('Y-m-d H:i:s');
				$this->m['IssuesModel']->updateTable('insert');

			}
		}
		
		//if preview
		if (isset($_POST['previewAction']))
		{
			if ($this->s['registered']->status['status'] === 'logged')
			{
				if (!$this->s['registered']->checkCsrf($this->viewArgs['token'])) $this->redirect('home/index/'.$this->lang,2,'wrong token..');
				
				$data['preview_message'] = $this->request->post('message','','sanitizeHtml');
				$this->m['IssuesModel']->result = false;
			}
		}
		
		$data['notice'] = $this->m['IssuesModel']->notice;

		$this->viewArgs['token'] = $this->token;
		$this->buildStatus();
		
		$this->m['IssuesModel']->setForm('issues/viewall/'.$this->lang.$this->viewStatus."#form",array('previewAction'=>'preview','insertAction'=>'submit'));
		
		$values = $this->m['IssuesModel']->getFormValues('insert','sanitizeHtml');
		
		$data['form'] = $this->m['IssuesModel']->form->render($values);
		
		//load the Pages helper
		$this->helper('Pages',$this->controller.'/viewall/'.$this->lang,'page');
		//get the number of records
		$this->m['IssuesModel']->from('issues')->left('messages')->using('id_issue')->groupBy('issues.id_issue')->orderBy('issues.update_date desc');
		
		if ($this->islogged === "no")
		{
			$this->m['IssuesModel']->where(array("n!issues.deleted"=>"no"));
		}
		
		$recordNumber = $this->m['IssuesModel']->rowNumber();
		$page = $this->viewArgs['page'];
		//set the limit clause
		$this->m['IssuesModel']->limit = $this->h['Pages']->getLimit($page,$recordNumber,30);
		$data['table'] = $this->m['IssuesModel']->select('issues.*,messages.message,count(*) as numb_mess')->send();
		
		$data['pageList'] = $this->h['Pages']->render($page-4,10);

		$this->append($data);
		$this->load('viewall');
		$this->right();
	}

	public function view($lang = 'en', $id_issue = 0)
	{
		$this->m['MessagesModel']->setFields('message','sanitizeAll');
		
		$this->shift(2);
		
		$clean['id_issue'] = (int)$id_issue;
		$data['id_issue'] = $clean['id_issue'];
		$data['preview_message'] = null;
		
		//if submit
		if (isset($_POST['insertAction']))
		{
			if ($this->s['registered']->status['status'] === 'logged')
			{
				if (!$this->s['registered']->checkCsrf($this->viewArgs['token'])) $this->redirect('home/index/'.$this->lang,2,'wrong token..');
				
				if ($this->m['UsersModel']->isBlocked($this->s['registered']->status['id_user'])) $this->redirect('my/home/'.$this->lang,2,'your account has been blocked..');

				$clean['id_user'] = (int)$this->s['registered']->status['id_user'];
				
				$this->m['MessagesModel']->values['created_by'] = $clean['id_user'];
				$this->m['MessagesModel']->values['id_issue'] = $clean['id_issue'];
				$this->m['MessagesModel']->updateTable('insert');

				if ($this->m['MessagesModel']->queryResult)
				{
					$this->m['IssuesModel']->values = array('update_date' => date('Y-m-d H:i:s'));
					$this->m['IssuesModel']->update($clean['id_issue']);

					$domainName = rtrim(Url::getRoot(),"/");
					header('Refresh: 0;url='.$domainName.$_SERVER['REQUEST_URI']);
					exit;
				}
			}
		}
		
		//if preview
		if (isset($_POST['previewAction']))
		{
			if ($this->s['registered']->status['status'] === 'logged')
			{
				if (!$this->s['registered']->checkCsrf($this->viewArgs['token'])) $this->redirect('home/index/'.$this->lang,2,'wrong token..');
				
				$data['preview_message'] = $this->request->post('message','','sanitizeHtml');
				$this->m['MessagesModel']->result = false;
			}
		}
		
		$data['notice'] = $this->m['MessagesModel']->notice;

		$this->viewArgs['token'] = $this->token;
		$this->buildStatus();

		//create the form
		$this->m['MessagesModel']->setForm('issues/view/'.$this->lang."/".$clean['id_issue'].$this->viewStatus."#form",array('previewAction'=>'preview','insertAction'=>'submit'));
		
		$values = $this->m['MessagesModel']->getFormValues('insert','sanitizeHtml');
		
		$data['form'] = $this->m['MessagesModel']->form->render($values);
		
		//retrieve the values from the table
		$data['table'] = $this->m['IssuesModel']->select()->where(array('id_issue'=>$clean['id_issue']))->send();
		
// 		javascript for moderator
		$data['md_javascript'] = "moderator_dialog(\"hide\",\"message\");moderator_dialog(\"show\",\"message\");moderator_dialog(\"open\",\"issue\");moderator_dialog(\"close\",\"issue\");moderator_dialog(\"issuehide\",\"issue_del\");moderator_dialog(\"issueshow\",\"issue_del\");";
		$data['go_to'] = $this->currPage."/".$this->lang."/".$clean['id_issue'];
		
		if (count($data['table']) > 0)
		{
			$data['messages'] = $this->m['MessagesModel']->select()->where(array('id_issue'=>$clean['id_issue']))->send();
			
			$this->append($data);
			$this->load('view');
			$this->load('moderator_dialog');
			$this->right();
		}
	}

}
