<?php 

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class NewsController extends BaseController
{

	public function __construct($model, $controller, $queryString)
	{
		
		$this->_topMenuClasses['news'] = " class='currentitem'";
		
		parent::__construct($model, $controller, $queryString);
		
		$this->model('NewsModel');
		
		$data['title'] = 'news - '.Website::$generalName;
		$this->append($data);
	}
	
	public function index($lang = 'en')
	{
		$argKeys = array(
			'page:forceNat'	=>	1,
		);

		$this->setArgKeys($argKeys);
		
		$this->shift(1);
		
		$this->helper('Pages',$this->controller.'/index/'.$this->lang,'page');
		$this->h['Pages']->nextString = 'older news';
		$this->h['Pages']->previousString = 'latest news';
		$page = $this->viewArgs['page'];
		$recordNumber =  $this->m['NewsModel']->rowNumber();
		$data['recordNumber'] = $recordNumber;
		
		//set the limit clause
		$limit = $this->h['Pages']->getLimit($page,$recordNumber,10);
			
		$data['table'] = $this->m['NewsModel']->select()->limit($limit)->send();
		$data['pageList'] = $this->h['Pages']->render($page,0);
		
		$this->append($data);
		$this->load('index');
		$this->right($lang);
	}

}