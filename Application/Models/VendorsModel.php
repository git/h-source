<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class VendorsModel extends Model_Tree {

	public function __construct() {
		$this->_tables = 'vendors';
		$this->_idFields = 'id_vendor';
		
		parent::__construct();
	}

	public function check($id,$interface = 'USB')
	{
		if (preg_match('/^[a-zA-Z0-9]{4}(\:)[a-zA-Z0-9]{4}$/',$id))
		{
			$clean['bus'] = strcmp($interface,'USB') === 0 ? "USB" : "PCI";
			
			$temp = explode(':',$id);
			$clean['vendorId'] = sanitizeAlphanum($temp[0]);

			$number = $this->clear()->where(array("vendorid" => $clean['vendorId'], "bus" => $clean['bus']))->rowNumber();
			
			if ($number > 0)
			{
				return true;
			}
		}
		return false;
	}
	
	public function getName($id,$interface)
	{
		if ($this->check($id,$interface))
		{
			$clean['bus'] = strcmp($interface,'USB') === 0 ? "USB" : "PCI";
			
			$temp = explode(':',$id);
			$clean['vendorId'] = sanitizeAlphanum($temp[0]);
			
			$res = $this->clear()->where(array("vendorid" => $clean['vendorId'], "bus" => $clean['bus']))->send();
			
			if (count($res) > 0)
			{
				return $res[0]['vendors']['clean_name'];
			}
		}
		return 'not-known';
	}
	
	public function getFullName($name)
	{
		$clean['name'] = sanitizeAll($name);
		
		$res = $this->clear()->where(array("clean_name" => $clean['name']))->send();
		
		if (count($res) > 0)
		{
			return $res[0]['vendors']['full_name'];
		}
		return $clean['name'];
	}
}