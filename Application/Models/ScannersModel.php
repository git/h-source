<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class ScannersModel extends GenericModel
{

	public $type = 'scanner'; //device type
	
	public function __construct()
	{
   
		$this->_popupItemNames = array(
			'vendor'		=>	'vendor',
			'compatibility'	=>	'compatibility',
			'comm_year'		=>	'comm_year',
			'interface'		=>	'interface',
		);
   
		$this->_popupLabels = array(
			'vendor'		=>	gtext("vendor"),
			'compatibility'	=>	gtext("compatibility"),
			'comm_year'		=>	gtext("year"),
			'interface'		=>	gtext("interface"),
		);

		$this->createPopupWhere('vendor,compatibility,comm_year,interface');

		$this->setPopupFunctions();
		
		$this->diffFields = array(
			'vendor' 		=>	gtext("vendor"),
			'model' 		=>	gtext('model name'),
			'other_names' 		=>	gtext('possible other names of the device'),
			'pci_id'		=>	gtext("VendorID:ProductID code of the device"),
			'comm_year'		=>	gtext('year of commercialization'),
			'interface'		=>	gtext("interface"),
			'distribution' 	=>	gtext('GNU/Linux distribution used for the test'),
			'compatibility'	=>	gtext('compatibility with free software'),
			'kernel'		=>	gtext('tested with the following kernel libre'),
			'driver'		=>	gtext("free driver used"),
			'description'	=>	gtext('Description'),
		);
		
		$this->fieldsWithBreaks = array(gtext('Description'),gtext('possible other names of the device'));
		
		parent::__construct();
	}

}