<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');

class Distributions
{
	
	public static $allowed = array();
	
	public static $allowed_active = array();
	
	public static function getList()
	{
		return implode(' , ',array_keys(self::$allowed));
	}
	
	//fill the $allowed property with the list of allowed distros from the distros MySQL table
	public static function setAllowedList()
	{
		$distros = new DistrosModel();

		self::$allowed = $distros->clear()->toList("clean_name","full_name")->orderBy("id_order")->send();
		self::$allowed_active = $distros->clear()->where(array('active'=>'1'))->toList("clean_name","full_name")->orderBy("id_order")->send();
	}
	
	public static function getName($distList = '')
	{
		$returnString = null;
		$returnArray = array();
		$distArray = explode(',',$distList);
		foreach ($distArray as $dist)
		{
			$dist = trim($dist);
			if (array_key_exists($dist,self::$allowed))
			{
				$returnArray[] = self::$allowed[$dist];
			}
		}
		return implode(" <br /> ",$returnArray);
	}
	
	public static function check($distString)
	{
		$distArray = explode(',',$distString);
		
		$allowedArray = array_keys(self::$allowed);
		
		foreach ($distArray as $dist)
		{
			$dist = trim($dist);
			if (!in_array($dist,$allowedArray)) return false;
		}
		
		return true;
	}
	
	public static function getFormHtml()
	{
		$str = "<div class='dist_checkboxes_hidden_box'>";
		$str .= "<div class='dist_checkboxes_hidden_box_inner'>";
		foreach (self::$allowed_active as $value => $label)
		{
			$str .= "<div class=\"hidden_box_item\"><input class=\"hidden_box_input $value\" type=\"checkbox\" name=\"$value\" value=\"$value\"/> $label</div>";
		}
		$str .= "</div>";
		$str .= "<input class=\"hidden_box_distribution_submit\" type=\"submit\" value=\"apply\">";
		$str .= "<input class=\"hidden_box_distribution_cancel\" type=\"submit\" value=\"cancel\">";
		$str .= "</div>";
		
		return $str;
	}
	
}
