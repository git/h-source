<?php

// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.

if (!defined('EG')) die('Direct access not allowed!');


function translate($string)
{
  return Hardware::translate($string);
}

function translate_and_gtext($string)
{
  return gtext(translate($string));
}

class Hardware
{

  public static $translations = array(
    "amd64"   =>  'x86-64/amd64',
    "PC-Card" =>  'PCMCIA/PC-Card',
    "works_with_3D"   =>  "works with 3D acceleration",
    "works_without_3D"  =>  "works, but without 3D acceleration",
    "does_not_work"   =>  "it does not work",
    "can-be-installed"  =>  "can be installed",
    "pcmcia-controller" => "PCMCIA Controller",
    "usb-controller"  => "USB Controller",
    "firewire-controller" => "Firewire Controller",
    "heci-controller" => "HECI Controller",
  );

  public static $regExpressions = array(
    "kernel"  =>  "/^[a-zA-Z0-9\-\_\.\+\s\(\)\,]+$/",
    "driver"  =>  "/^[a-zA-Z0-9\-\_\.\+\s\/\,\:\;\(\)\[\]]+$/",
    "vendorid_productid"  =>  "/^[a-zA-Z0-9]{4}(\:)[a-zA-Z0-9]{4}$/",
    "model"   =>  "/^[a-zA-Z0-9\-\_\.\+\s\(\)\@\[\]\/\,\']+$/",
  );

  public static function translate($string): string
  {
    if (array_key_exists($string,self::$translations))
    {
      return self::$translations[$string];
    }
    return $string;
  }

  //classId => controller
  public static $deviceClasses = array(
    '0403'  =>  'soundcards',
    '0280'  =>  'wifi',
    '0300'  =>  'videocards',
    '0200'  =>  'ethernetcards',
    '0400'  =>  'acquisitioncards',
    '0401'  =>  'acquisitioncards',
    '0480'  =>  'acquisitioncards',
    '0805'  =>  'sdcardreaders',
    '0703'  =>  'modems',
    '0104'  =>  'raidadapters',
    '0c00'  =>  'hostcontrollers',
    '0c03'  =>  'hostcontrollers',
    '0780'  =>  'hostcontrollers',
    '0607'  =>  'hostcontrollers',
    '0e00'  =>  'raidadapters',
  );

  public static $icons = array(
    'notebook'        =>  'H2O/computer-laptop_22.png',
    'wifi'          =>  'H2O/network-wireless_22.png',
    'videocard'       =>  'Crystal/1282042976_hardware.png',
    'printer'       =>  'H2O/printer_22.png',
    'scanner'       =>  'H2O/scanner_22.png',
    '3G-card'       =>  'Crystal/usb_22.png',
    'soundcard'       =>  'H2O/audio-card_22.png',
    'webcam'        =>  'H2O/camera-web_22.png',
    'bluetooth'       =>  'H2O/preferences-system-bluetooth-22.png',
    'acquisition-card'    =>  'Crystal/cam_mount-22.png',
    'fingerprint-reader'  =>  'fingerprint_icon-22.png',
    'ethernet-card'     =>  'H2O/network-wired_22.png',
    'sd-card-reader'    =>  'H2O/media-flash-sd-mmc_22.png',
    'modem'         =>  'Crystal/modem_22.png',
    'RAID-adapter'      =>  'Crystal/1282042976_hardware.png',
    'host-controller'   =>  'Crystal/1282042976_hardware.png',
  );

  public static $typeToController = array(
    'notebook'        =>  'notebooks',
    'wifi'          =>  'wifi',
    'videocard'       =>  'videocards',
    'printer'       =>  'printers',
    'scanner'       =>  'scanners',
    '3G-card'       =>  'threegcards',
    'soundcard'       =>  'soundcards',
    'webcam'        =>  'webcams',
    'bluetooth'       =>  'bluetooth',
    'acquisition-card'    =>  'acquisitioncards',
    'fingerprint-reader'  =>  'fingerprintreaders',
    'ethernet-card'     =>  'ethernetcards',
    'sd-card-reader'    =>  'sdcardreaders',
    'modem'         =>  'modems',
    'RAID-adapter'      =>  'raidadapters',
    'host-controller'   =>  'hostcontrollers',
  );

  public static $typeToWorksField = array(
    'notebook'        =>  'compatibility',
    'wifi'          =>  'wifi_works',
    'videocard'       =>  'video_card_works',
    'printer'       =>  'compatibility',
    'scanner'       =>  'compatibility',
    '3G-card'       =>  'wifi_works',
    'soundcard'       =>  'sound_card_works',
    'webcam'        =>  'webcam_works',
    'bluetooth'       =>  'bluetooth_works',
    'acquisition-card'    =>  'compatibility',
    'fingerprint-reader'  =>  'fingerprint_works',
    'ethernet-card'     =>  'ethernet_card_works',
    'sd-card-reader'    =>  'sd_card_works',
    'modem'         =>  'compatibility',
    'RAID-adapter'      =>  'compatibility',
    'host-controller'   =>  'compatibility',
  );

  public static function getTypes(): string
  {
    return implode(',',array_keys(self::$typeToController));
  }

  public static function getControllerFromType($type): ?string
  {
    if (array_key_exists($type,self::$typeToController))
    {
      return self::$typeToController[$type];
    }
    return null;
  }

  public static function getWorksFieldFromType($type): ?string
  {
    if (array_key_exists($type,self::$typeToWorksField))
    {
      return self::$typeToWorksField[$type];
    }
    return null;
  }

  //get the type from the controller
  public static function getTypeFromController($controller): ?array
  {
    $temp = array_flip(self::$typeToController);
    if (array_key_exists($controller,$temp))
    {
      return $temp[$controller];
    }
    return null;
  }

  public static function getTypeFromClass($class): ?array
  {
    if (array_key_exists($class,self::$deviceClasses))
    {
      return self::getTypeFromController(self::$deviceClasses[$class]);
    }
    return null;
  }

  public static function getIconFromType($type): ?string
  {
    if (array_key_exists($type,self::$icons))
    {
      return Url::getRoot()."Public/Img/".self::$icons[$type];
    }
    return null;
  }

  public static function getIconFromController($controller): ?string
  {
    if ($type = self::getTypeFromController($controller))
    {
      return self::getIconFromType($type);
    }
    return null;
  }

  public static function getIconFromClass($class): ?string
  {
    if (array_key_exists($class,self::$deviceClasses))
    {
      return self::getIconFromController(self::$deviceClasses[$class]);
    }
    return null;
  }

  //get the list of the allowed years of commercialization
  public static function getCommYears(): string
  {
    $currentYear = (int)date("Y");
    $allowed = 'not-specified';

    for ($i=$currentYear;$i>1991;$i--)
    {
      $allowed .= ",$i";
    }
    return $allowed;
  }

  public static $trackSelect = 'not-specified,no,yes';

}

class Printer extends Hardware
{

  public static $compatibility = array(
    "A-Full"    =>  "A-Full",
    "B-Partial"   =>  "B-Partial",
    "C-None"    =>  "C-None",
  );

  public static $interface = "not-specified,USB,Serial,Parallel,Firewire,SCSI,Ethernet";

  public static $subtype = "not-specified,laser,inkjet,other";

  public static function compatibilityList(): string
  {
    return implode(',',array_values(self::$compatibility));
  }

}

class Wifi extends Hardware
{

  public static $interface = array(
    "not-specified"   =>  "not-specified",
    "USB"       =>  "USB",
    "PCI"       =>  "PCI",
    "PCI-E"       =>  "PCI-E",
    "mini-PCI"      =>  "mini-PCI",
    "mini-PCI-E"    =>  "mini-PCI-E",
    "ExpressCard"   =>  "ExpressCard",
    "Ethernet"      =>  "Ethernet",
    "PCMCIA/PC-Card"  =>  "PC-Card",
    "M.2" => "M.2",
  );

  public static $wifiSelect = 'yes,no';

  public static function interfaceList(): string
  {
    return implode(',',array_values(self::$interface));
  }
}

class Videocard extends Hardware
{

  public static $videoSelect = array(
    "works with 3D acceleration"        =>  "works_with_3D",
    "works, but without 3D acceleration"    =>  "works_without_3D",
    "it does not work"              =>  "does_not_work",
  );

  public static $videoReverse = array(
    "works_with_3D"   =>  "works with 3D acceleration",
    "works_without_3D"  =>  "works, but without 3D acceleration",
    "does_not_work"   =>  "it does not work",
  );

  public static $interface = "not-specified,PCI,AGP,PCI-E,ISA,MCA,VLB,USB";

  public static function videoList(): string
  {
    return implode(',',array_values(self::$videoSelect));
  }
}


class Notebooks extends Hardware
{

  public static $vendors = array(
    "Acer"          =>  "Acer",
    "Apple"         =>  "Apple",
    "Asus"          =>  "Asus",
    "BenQ"          =>  "BenQ",
    "Blue Light"      =>  "Blue-Light",
    "CLEVO CO."       =>  "CLEVO-CO",
    "Compal Electronics"  =>  "Compal-Electronics",
    "COMPAQ"        =>  "COMPAQ",
    "Dell"          =>  "Dell",
    "Dynabook Inc" =>  "Dynabook-Inc",
    "emachines"       =>  "emachines",
    "FUJITSU"       =>  "FUJITSU",
    "Garlach44"       =>  "Garlach44",
    "Gateway"       =>  "Gateway",
    "Google"        =>  "Google",
    "GPD"           =>  "GPD",
    "Hasee"         =>  "Hasee",
    "Hewlett Packard"     =>  "Hewlett-Packard",
    "Honor" => "Honor",
    "Huawei" => "Huawei",
    "IBM"           =>  "IBM",
    "Intel"         =>  "Intel",
    "Lanix"         =>  "Lanix",
    "Lemote"        =>  "Lemote",
    "Lenovo"        =>  "Lenovo",
    "LG"          =>  "LG",
    "Libiquity"       =>  "Libiquity",
    "Minifree"        =>  "Minifree",
    "msi"         =>  "msi",
    "Olimex"        =>  "Olimex",
    "One Laptop per Child (Non-Profit)" =>  "One-Laptop-per-Child-Non-Profit",
    "Openpandora Ltd."    =>  "Openpandora-Ltd",
    "Oracle"        =>  "Oracle",
    "Packard Bell"      =>  "Packard-Bell",
    "Panasonic"                             =>      "Panasonic",
    "Philco"        =>  "Philco",
    "Philips"       =>  "Philips",
    "Positivo"        =>  "Positivo",
    "Purism"        =>  "Purism",
    "Qbex"          =>  "Qbex",
    "SAMSUNG"       =>  "SAMSUNG",
    "Slimbook"                              =>      "Slimbook",
    "Sony"          =>  "Sony",
    "System76"        =>  "System76",
    "ThinkPenguin"        =>  "ThinkPenguin",
    "Thomson"       =>  "Thomson",
    "TOSHIBA"       =>  "TOSHIBA",
    "VAIO Corporation" => "VAIO-Corporation",
    "ZaReason, Inc"     =>  "ZaReason-Inc",
  );

  public static $compatibility = array(
    "A Platinum"      =>  "A-platinum",
    "B Gold"        =>  "B-gold",
    "C Silver"        =>  "C-silver",
    "D Bronze"        =>  "D-bronze",
    "E Garbage"       =>  "E-garbage"
  );

  public static $subtypeSelect = 'notebook,netbook,tablet,not-specified';

  public static $architectureSelect = array(
    "not specified"     =>  'not-specified',
    "x86"         =>  'x86',
    "x86-64/amd64"      =>  'amd64',
    "MIPS"          =>  'MIPS',
    "ARM"         =>  'ARM',
  );

  public static $biosSelect = array(
    "not specified"   =>  'not-specified',
    "no"        =>  'no',
    "yes"       =>  'yes',
    "can be installed"  =>  'can-be-installed',
  );

  public static $preventWifiSelect = array(
    "not specified" =>  'not-specified',
    "no" => 'no',
    "yes (please specify in the description entry)" =>  'yes',
  );

  public static $installableSelect = array(
    "not specified" =>  'not-specified',
    "no (please specify in the description entry)" => 'no',
    "yes" =>  'yes',
  );

  //  public static $installableSelect = 'not-specified,no,yes';

  public static $videoSelect = array(
    "not specified"             =>  'not-specified',
    "yes, with 3D acceleration"       =>  "yes_with_3D",
    "yes, but without 3D acceleration"    =>  "yes_without_3D",
    "it does not work"            =>  "no",
  );

  public static $videoReverse = array(
    "yes_with_3D"   =>  "works with 3D acceleration",
    "yes_without_3D"  =>  "works, but without 3D acceleration",
    "no"        =>  "it does not work",
    'not-specified'   =>  "not specified how it works",
    ""          =>  ""
  );

  public static $wifiSelect = array(
    "not specified"     =>  'not-specified',
    'yes'         =>  'yes',
    'no'          =>  'no',
    'there is no wifi card' =>  'no-wifi-card',
  );

  public static $wifiReverse = array(
    "yes"     =>  "it works",
    "no"      =>  "it does not work",
    'not-specified' =>  "not specified how it works",
    'no-wifi-card'  =>  'there is no wifi card',
    ""        =>  ""
  );

  public static $webcamSelect = array(
    'not specified'     =>  'not-specified',
    'it works'        =>  'yes',
    'it does not work'    =>  'no',
    'there is no webcam'  =>  'there-is-no-webcam',
  );

  public static $webcamReverse = array(
    "yes"         =>  "it works",
    "no"          =>  "it does not work",
    'not-specified'     =>  "not specified how it works",
    'there-is-no-webcam'  =>  'there is no webcam',
  );

  public static function videoList(): string
  {
    return implode(',',array_values(self::$videoSelect));
  }

  public static function wifiList(): string
  {
    return implode(',',array_values(self::$wifiSelect));
  }

  public static function webcamList(): string
  {
    return implode(',',array_values(self::$webcamSelect));
  }

  public static function vendorsList(): string
  {
    return implode(',',array_values(self::$vendors));
  }

  public static function compatibilityList(): string
  {
    return implode(',',array_values(self::$compatibility));
  }

  public static function architectureList(): string
  {
    return implode(',',array_values(self::$architectureSelect));
  }

  public static function biosList(): string
  {
    return implode(',',array_values(self::$biosSelect));
  }

  public static function preventWifiList(): string
  {
    return implode(',',array_values(self::$preventWifiSelect));
  }

  public static function installableList(): string
  {
    return implode(',',array_values(self::$installableSelect));
  }
}

class ThreeGcards extends Wifi
{

  public static $select = 'yes,no';


}

class Soundcards extends Hardware
{

  public static $audioSelect = 'yes,no';

  public static $interface = "not-specified,PCI,ISA,USB,Firewire,Parallel,PCI-E,PCMCIA";

}

class Webcams extends Hardware
{

  public static $select = 'yes,no';

  public static $interface = "not-specified,USB,Firewire,Parallel,Wifi,Serial";

}

class Bluetooth extends Hardware
{

  public static $select = 'yes,no';

  public static $interface = "not-specified,USB,PCI,PCI-E,mini-PCI,mini-PCI-E,ExpressCard,PC-Card";

}

class Acquisitioncards extends Hardware
{

  public static $select = 'yes,no';

  public static $interface = "not-specified,USB,PCI,PCI-E,mini-PCI,mini-PCI-E,ExpressCard,PC-Card,Firewire,Parallel,Serial";

}

class Fingerprintreaders extends Acquisitioncards
{

}

class Ethernetcards extends Acquisitioncards
{

}

class Sdcardreaders extends Acquisitioncards
{

}

class Modems extends Acquisitioncards
{

}

class Raidadapters extends Acquisitioncards
{

}

class PCMCIAControllers extends Acquisitioncards
{

}

class Hostcontrollers extends Acquisitioncards
{

  public static $subtype = array(
    "PCMCIA Controller" => "pcmcia-controller",
    "USB Controller"  => "usb-controller",
    "Firewire Controller" => "firewire-controller",
    "HECI Controller" => "heci-controller",
  );

  public static function subtypeList(): string
  {
    return implode(',',array_values(self::$subtype));
  }

}
