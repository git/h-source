<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/search/form/$lang";?>"><?php echo gtext('Search form');?></a> &raquo; <?php echo gtext('Results of the search');?>
		</div>
		
		<div class="notebook_view_title">
			<?php echo gtext("Results of the search");?>:
		</div>
			
		<div class="search_form">

			<?php if (!$flag) { ?>
				<?php echo $notice;?>
				<div>
					<a href="<?php echo $this->baseUrl."/search/form/$lang";?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
				</div>
			<?php } ?>

			<div class="found_ext">
				<?php if (count($table)>0) { ?>
				<div class="search_item_found">
					<?php echo gtext("The following devices has been found in the database");?>:
				</div>
				<?php }?>

				<?php foreach ($table as $row) { ?>
				<div class="lspci_item_found">
					<div class="lspci_item_found_model">
						<img align="top" class="catalogue_item_icon" src="<?php echo Hardware::getIconFromType($row['hardware']['type']);?>"> <span class="search_result_model_name"><?php echo "<b>".$row['hardware']['type']."</b> - <a href='".$this->baseUrl."/".Hardware::getControllerFromType($row['hardware']['type'])."/view/$lang/".$row['hardware']['id_hard']."/".encodeUrl($row['hardware']['model'])."'>".$row['hardware']['model']."</a>";?></span>
					</div>
					<div class="lspci_item_found_compat">
					<?php
					echo gtext("does it work with free software?"). " <b>".gtext($row['hardware'][Hardware::getWorksFieldFromType($row['hardware']['type'])])."</b>";
					?>
					</div>
				</div>
				<?php } ?>
			</div>

			<div class="found_ext">
				<?php if (count($notFoundDevices)>0) { ?>
				<div class="search_item_found">
					<?php echo gtext("The following devices has not been found in the database");?>:<br />
					<?php echo gtext("can you please insert them?");?>
				</div>
				<?php }?>

				<?php foreach ($notFoundDevices as $device) { ?>
				<div class="lspci_item_not_found">
					<div class="lspci_item_found_model">
						<img align="top" class="catalogue_item_icon" src="<?php echo Hardware::getIconFromClass($device['classId']);?>"> <span class="search_result_model_name"><?php echo "<b>".Hardware::getTypeFromClass($device['classId'])."</b> - ".$device['deviceName'];?></span>
						<div class="lspci_item_found_compat">
							<ul>
								<li><b><?php echo gtext("device type");?>:</b><?php echo $device['className'];?></li>
								<li><b><?php echo gtext("vendor");?>:</b><?php echo $device['vendorName'];?></li>
								<li><b><?php echo gtext("VendorID:ProductID code of the device");?></b>: <?php echo $device['vendorId'].":".$device['deviceId'];?></li>
							</ul>
							<a href="<?php echo Go::toHardwareType(Hardware::getTypeFromClass($device['classId']));?>"><img align="top" src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/agt_forward.png"> insert</a>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>

	</div>
