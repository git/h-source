<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; h-project
		</div>
		
		<div class="credits_external_box">
			
			<div class="credits_item_title">
				Γιατί:
			</div>
			
			<div class="credits_item_description">
				Το h-node project στήθηκε για να βοηθήσει το Κίνημα Ελεύθερου Λογισμικού, με τη δημιουργία ενός αρχείου για όλο το υλικό (hardware) που μπορεί να δουλέψει με ένα <a href="http://www.gnu.org/distros/free-distros.html">πλήρως ελεύθερο Λειτουργικό Σύστημα</a>.
			</div>

			<div class="credits_item_title">
				Ποιος:
			</div>
			
			<div class="credits_item_description">
				Ο Antonio Gallo (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>tonicucoz">tonicucoz</a>), h-node.com developer πηγαίου κώδικα, η Giulia Fanin (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Julia">Julia</a>, designer του layout και των εικόνων αυτού του ιστοτόπου (ευχαριστούμε για τις συμβουλές σου και την υποστήριξή σου), ο Luis Alberto Guzman Garcia (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Ark74">Ark74</a>), μέλος της Ισπανικής μεταφραστικής ομάδας (ευχαριστούμε για όλες τις χρήσιμες ιδέες σου και τις προτάσεις σου), ο Henri (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Hardisk">Hardisk</a>), μέλος της Γαλλικής μεταφραστικής ομάδας, ο Joerg Kohne (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>joeko">joeko</a>), μέλος της Γερμανικής μεταφραστικής ομάδας, ο Benjamin Rochefort (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>oysterboy">oysterboy</a>), μέλος της Γαλλικής μεταφραστικής ομάδας, Kostas Mousafiris (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>kosmous">kosmous</a>), member of the Greek translation team.
				<br />Ευχαριστούμε, επίσης, όλους εσάς που πιστέψατε σε αυτό το project από τη γέννησή του, καθώς και όλους εσάς που δώσατε, δίνετε και θα δίνετε την συνεισφορά σας.
			</div>

		</div>
	
	</div>
