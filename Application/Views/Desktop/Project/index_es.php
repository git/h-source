<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; h-project
		</div>
		
		<div class="credits_external_box">
			
			<div class="credits_item_title">
				Por que:
			</div>
			
			<div class="credits_item_description">
				El proyecto h-node ha sido creado para ayudar al movimiento de software libre al crear un archivo de todo el hardware que puede funcionar con un <a href="http://www.gnu.org/distros/free-distros.es.html">sistema operativo completamente libre</a>.
			</div>

			<div class="credits_item_title">
				Quien:
			</div>
			
			<div class="credits_item_description">
				Antonio Gallo (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>tonicucoz">tonicucoz</a>), h-node.com source code developer, Giulia Fanin (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Julia">Julia</a>), diseñadora del tema e iconos del sitio, (gracias por su consejo y apoyo), Luis Alberto Guzman Garcia (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Ark74">Ark74</a>), miembro del equipo de traducción al Español (gracias por sus útiles ideas y sugerencias), Henri (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Hardisk">Hardisk</a>), miembro del equipo de traducción al Francés, Joerg Kohne (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>joeko">joeko</a>), miembro del equipo de traducción al Alemán, Benjamin Rochefort (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>oysterboy">oysterboy</a>), miembro del equipo de traducción al Francés, , Kostas Mousafiris (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>kosmous">kosmous</a>), member of the Greek translation team.
				<br />
				También gracias a todos aquellos que han creído en el proyecto desde que nació y a todos aquellos que dieron, dan y darán su contribución.
			</div>

		</div>
	
	</div>
