<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo gtext("create new account");?>
		</div>
		
		<div class="new_account_title">
			<?php echo gtext("create new account");?>
		</div>
		
		<?php echo $notice;?>
		
		<form  class='formClass' action='<?php echo $this->baseUrl."/users/add/$lang";?>' method='POST'>
		
			<div class='formEntry'>
				<span class='entryLabel'><?php echo gtext("choose the username");?> (<?php echo gtext("characters allowed");?>: a-z A-Z 0-9):</span>
				<?php echo Html_Form::input('username',$values['username']);?>
			</div>
			
			<div class='formEntry'>
				<span class='entryLabel'><?php echo gtext("your e-mail address");?> (<?php echo gtext("necessary to confirm the registration");?>):</span>
				<?php echo Html_Form::input('e_mail',$values['e_mail']);?>
			</div>
			
			<div class='formEntry'>
				<span class='entryLabel'><?php echo gtext("choose the password");?> (<?php echo gtext("characters allowed");?>: a-z A-Z 0-9 - _ !):</span>
				<?php echo Html_Form::password('password',$values['password']);?>
			</div>
			
			<div class='formEntry'>
				<span class='entryLabel'><?php echo gtext("confirm the password");?>:</span>
				<?php echo Html_Form::password('confirmation',$values['confirmation']);?>
			</div>
			
			<div class="captcha_box">
				<img src="<?php echo $this->baseUrl?>/image/captcha">
			</div>
			
			<div class='formEntry'>
				<span class='entryLabel'><?php echo gtext("write the code above");?>:</span>
				<input type="input" name="captcha" value="">
			</div>
			
			<div class='inputEntry'>
				<input id='insertAction' type='submit' name='insertAction' value='<?php echo gtext("create new account");?>'>
			</div>
			
		</form>

	</div>