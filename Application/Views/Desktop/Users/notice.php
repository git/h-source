<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; notice
		</div>
		
		<?php if ( isset($_SESSION['status']) ) { ?>
		
			<?php if ( strcmp($_SESSION['status'],'sent') === 0 ) { ?>
			<div class="confirm_notice">
				<p>An e-mail has been sent to your mailbox.</p>
				<p>If you have received no mail, then check inside the spam too</p>
				<p>Click on the confirmation link in the e-mail in order to confirm the registration of the new account.</p>
				<p>The confirmation link will expire in a hour.</p>
				<p>If you don't want to confirm the account registration then wait one hour and your username and e-mail will be deleted from the database.</p>
				<p>go to the <a href="<?php echo $this->baseUrl."/home/index/$lang";?>">homepage</a></p>
			</div>
			
			<?php } else if (strcmp($_SESSION['status'],'regerror') === 0) { ?>
			
			<div class="confirm_notice">
				<p>Registration failed</p>
				<p>go to the <a href="<?php echo $this->baseUrl."/home/index/$lang";?>">homepage</a></p>
			</div>
			
			<?php } else if (strcmp($_SESSION['status'],'sent_new') === 0) { ?>
			
			<div class="confirm_notice">
				<p>An e-mail has been sent to your mailbox.</p>
				<p>If you have received no mail, then check inside the spam too</p>
				<p>Click on the confirmation link in the e-mail in order to change the password of your account.</p>
				<p>The confirmation link will expire in a hour.</p>
				<p>go to the <a href="<?php echo $this->baseUrl."/home/index/$lang";?>">homepage</a></p>
			</div>
			
			<?php } else if (strcmp($_SESSION['status'],'sent_new_error') === 0) { ?>
			
			<div class="confirm_notice">
				<p>Registration failed</p>
				<p>go to the <a href="<?php echo $this->baseUrl."/home/index/$lang";?>">homepage</a></p>
			</div>
			
			<?php } else if (strcmp($_SESSION['status'],'sent_new_password') === 0) { ?>
			
			<div class="confirm_notice">
				<p>The new password has been sent to you by mail!</p>
				<p>If you have received no mail, then check inside the spam too</p>
				<p>go to the <a href="<?php echo $this->baseUrl."/home/index/$lang";?>">homepage</a></p>
			</div>
			
			<?php } else if (strcmp($_SESSION['status'],'sent_new_password_error') === 0) { ?>
			
			<div class="confirm_notice">
				<p>Operation failed</p>
				<p>go to the <a href="<?php echo $this->baseUrl."/home/index/$lang";?>">homepage</a></p>
			</div>
			
			<?php } else if (strcmp($_SESSION['status'],'deleted') === 0) { ?>
			
			<div class="confirm_notice">
				<p>Your account has been successfully deleted</p>
				<p>go to the <a href="<?php echo $this->baseUrl."/home/index/$lang";?>">homepage</a></p>
			</div>
			
			<?php } ?>
			
		<?php } else { ?>
		
			<div class="confirm_notice">
				<p>go to the <a href="<?php echo $this->baseUrl."/home/index/$lang";?>">homepage</a></p>
			</div>
			
		<?php } ?>
	</div>