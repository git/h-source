<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
	
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; confirmation
		</div>
		
		<?php if ($status_confirm) { ?>
		
		<div class="confirm_notice">
			<p>The account has been confirmed successfully!</p>
			<p>go to the <a href="<?php echo $this->baseUrl."/home/index/$lang";?>">homepage</a></p>
		</div>
		
		<?php } else { ?>
		
		<div class="confirm_notice">
			<p>The confirmation link has expired</p>
			<p>go to the <a href="<?php echo $this->baseUrl."/home/index/$lang";?>">homepage</a></p>
		</div>
		
		<?php } ?>
	</div>