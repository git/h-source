<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="right">

		<?php if (strcmp($this->action,'update') !== 0 and strcmp($this->action,'pciid') !== 0 and strcmp($this->action,'lspci') !== 0) { ?>
		<div class="language_links_box">
			<?php echo $language_links;?>
		</div>
		<?php } ?>

		<div class="version_div">
			<a href="<?php echo $this->baseUrl."/home/index/$lang?version=mobile";?>"><img src="<?php echo $this->baseUrl."/Public/Img/mobile.png";?>"></a>
		</div>
		
		<div class="login_table_box">
		
			<?php if ($islogged === 'yes') { ?>
				<div class="login_box_logged">
					<div class="who_you_are_and_logout">
						<?php echo gtext("Hello");?> <b><?php echo $username; ?></b> (<a href="<?php echo $this->baseUrl."/users/logout/$lang?redirect=".$currPos.$queryString;?>">logout</a>)
					</div>
					<div class="your_panel_link">
						<?php echo gtext("Your");?> <a href="<?php echo $this->baseUrl."/my/home/$lang";?>"><?php echo gtext("control panel");?></a>
					</div>
				</div>				
			<?php } else { ?>
			
			<div class="who_you_are_and_logout">
				<?php echo gtext("Login form:");?>
			</div>
			<!--login form-->
			<form action="<?php echo $this->baseUrl."/users/login/$lang?redirect=".$currPos.$queryString;?>" method="POST">
				
				<div class="login_right_box">
					<div class="login_right_item">
						<div class="login_right_label">
							<?php echo gtext("username");?>
						</div>
						<div class="login_right_form">
							<input class="login_input" type="text" name="username" value="">
						</div>
					</div>
					<div class="login_right_item">
						<div class="login_right_label">
							<?php echo gtext("password");?>
						</div>
						<div class="login_right_form">
							<input class="login_input" type="password" name="password" value="">
						</div>
					</div>
					<div>
						<input type="submit" name="login" value="<?php echo gtext("login");?>">
					</div>
				</div>
			</form>
			
			<div class="manage_account_link_box">
				<a href="<?php echo $this->baseUrl."/users/add/$lang";?>"><?php echo gtext("create new account");?></a>
			</div>
			
			<div class="manage_account_link_box">
				<a href="<?php echo $this->baseUrl."/users/forgot/$lang";?>"><?php echo gtext("request new password");?></a>
			</div>
			
			<?php } ?>
			
		</div>

		<?php echo $discoverYourHardwareLink;?>
		
		<div class="download_database">
			<a href="<?php echo $this->baseUrl."/download/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/download.png"></a>
		</div>

		<?php if ( strcmp($this->controller,'wiki') === 0 ) { ?>
		<div class="wiki_control_box">
			<div class="last_modifications_title">
				<?php echo gtext('special pages');?>
			</div>
			<ul>
				<li><a href="<?php echo $this->baseUrl."/wiki/pages/$lang";?>"><?php echo gtext('list of pages'); ?></a></li>
				<?php if ($isadmin) { ?>
					<li><a href="<?php echo $this->baseUrl."/wiki/deleted/$lang";?>"><?php echo gtext('list of deleted pages'); ?></a></li>
					<li><a href="<?php echo $this->baseUrl."/wiki/blocked/$lang";?>"><?php echo gtext('list of blocked pages'); ?></a></li>
				<?php } ?>
				<li><a href="<?php echo $this->baseUrl."/wiki/modifications/$lang";?>"><?php echo gtext('last modifications'); ?></a></li>
			</ul>
		</div>
		<?php } ?>

		<?php if ( strcmp($this->controller,'wiki') !== 0 ) { ?>
		<div class="last_modifications">
			<div class="last_modifications_title">
				<?php echo gtext('last modifications');?>
			</div>
			<ul>
				<?php foreach ($lastModif as $row) { ?>
					<li><a class="last_modifications_model" href="<?php echo Go::toHardwarePage($row['history']['id']);?>"><?php echo $hw->getTheModelName($row['history']['id']);?></a> <?php echo gtext('by');?> <?php echo $u->getLinkToUserFromId($row['history']['created_by'])?></li>
				<?php } ?>
			</ul>
			<div class="last_modifications_all">
				<span>
					<a href="<?php echo $this->baseUrl."/special/modifications/$lang";?>"><?php echo gtext('watch all modifications');?></a>
				</span>
			</div>
		</div>
		<?php } ?>

		<div class="statistics_ext_box">
			<div class="statistics_int_title">
				<?php echo gtext("website statistics");?>:
			</div>
			
			<div class="statistics_hard_title">
				<?php echo gtext("hardware in the database");?>:
			</div>
			
			<table width="100%">
				<?php $total_number=0;foreach ($stat as $type => $number) { $total_number+=$number; $type=displayName($type);?>
				<tr>
					<td><?php echo gtext($type);?></td>
					<td align="right"><?php echo "<b>".$number."</b>";?></td>
				</tr>
				<?php } ?>
				<tr>
					<td><?php echo gtext("<b>TOTAL</b>");?></td>
					<td align="right"><?php echo "<b>".$total_number."</b>";?></td>
				</tr>
			</table>
			
			<div class="statistics_hard_title">
				<?php echo gtext("users logged");?>: <span class="user_logged"><?php echo $numbLogged;?></span>
			</div>
		</div>

		
		<div class="rss_right_box">
			<a href="<?php echo $this->baseUrl."/rss/modifications/$lang";?>"><img src="<?php echo $this->baseUrl."/Public/Img/rss.png";?>"></a>
		</div>
		
		<div class="right_box_ext_box">
			<?php echo $htmlRightBox;?>
		</div>
		
	</div>
