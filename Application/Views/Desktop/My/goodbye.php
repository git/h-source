<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<script>

	$(document).ready(function(){

		$(".close_account_submit").click(function () {
			if (window.confirm("Are you really sure?")) {
				return true;
			}
			return false;
		});

	});

	</script>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/my/home/$lang/$token";?>">panel</a> &raquo; delete account
		</div>

		<div class="delete_account_notice_box">
			After your account cancellation:
			<ul>
				<li>you won't be able to log-in anymore</li>
				<li>all your data, except for your username, will be deleted from the database</li>
				<li>your public profile won't be accessible anymore</li>
				<li>you won't be able to get your old account back. If you want to register another time you have to choose a different username</li>
				<li>your username will remain in the history of the devices (notebooks,video cards..) you have modified</li>
			</ul>
		</div>

		<div class="climb_form_ext_box">
				
			<form action="<?php echo $this->currPage."/$lang".$this->viewStatus;?>" method="POST">
				I want to close my account: <input class="close_account_submit" type="submit" name="closeAction" value="confirm">
			</form>
		
		</div>
		
	</div>