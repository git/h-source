<?php if (!defined('EG')) die('Direct access not allowed!'); ?>
<?php echo "<?xml version='1.0' encoding='UTF-8'?>\n";?>
<rss version="2.0">

<channel>
<title><?php echo Website::$generalName;?></title>
<link><?php echo Url::getRoot();?></link>
<description><?php echo gtext("Database modifications");?></description>

<?php
$u = new UsersModel();
$translations = array('insert'=>'inserted','update'=>'updated');
?>

<?php foreach ($table as $row) { ?>
<item>
	<title><?php echo $row['hardware']['model'];?></title>
	<description><![CDATA[<?php echo "$statusnetText ".gtext('the model')." ".$row['hardware']['model']." ".gtext('has been '.$translations[$row['history']['action']].' by')." ".$u->getUser($row['history']['created_by']);?>]]></description>
	<link><?php echo $this->baseUrl."/".Hardware::$typeToController[$row['hardware']['type']]."/view/$lang/".$row['hardware']['id_hard']."/".encodeUrl($row['hardware']['model']);?></link>
	<pubDate><?php echo pubDateFormat($row['history']['creation_date']);?></pubDate>
</item>

<?php } ?>

</channel>
</rss>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>