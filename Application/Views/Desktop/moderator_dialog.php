<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

<div id="delete_dialog" title="Manage this item">
	<form>
		<p>Write below your motivation</p>
		<textarea name="md_message" id="md_message"></textarea>
	</form>
</div>

<div id="notice_dialog" title="Notice:">
	<div class="notice_dialog_inner">
	
	</div>
</div>

<script>

	$(document).ready(function() {
		
		<?php echo $md_javascript;?>

		$(".hidden_message_view_page").click(function(){
			$(".display_none").css("display","block");
			return false;
		});
		
		$(".hidden_message_view_details").click(function(){
			
			var md_id_ext = $(this).attr("id");
			var md_type_ext = $(this).parent().find(".md_type").text();
			var that = $(this);

			$.ajax({
				url: base_url + "/history/viewall/" + curr_lang + "/" + md_type_ext + "/" + md_id_ext,
				async: false,
				cache: false,
				dataType: "html",
				success: function(html){
					that.parent().find(".moderation_details_box").empty();
					that.parent().find(".moderation_details_box").append(html);
				}
			});
			
			that.parent().find(".details_of_hidden_message").show();
			return false;
		});
	});
	
</script>