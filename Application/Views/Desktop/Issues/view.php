<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<script>

		$(document).ready(function() {

			$("#bb_code").markItUp(mySettings);

		});

	</script>

	<div id="left">

		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/issues/viewall/$lang".$this->viewStatus;?>"><?php echo gtext("Issues");?></a> &raquo; <?php echo $id_issue;?>
		</div>

		<?php if (strcmp($table[0]['issues']['deleted'],'no') === 0 or $ismoderator) { ?>
			<?php foreach ($table as $row) { ?>

			<?php if ($ismoderator) { ?>
			<!--open/close an issue-->
			<div class="moderator_box">
				<?php if (strcmp($row['issues']['status'],'opened') == 0) { ?>
					<?php echo gtext("This issue is opened");?>

					<a id="<?php echo $row['issues']['id_issue'];;?>" class="close_issue block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Glaze/folder_blue.png"><?php echo gtext("close the issue");?></a>

				<?php } else {	?>
					<?php echo gtext("This issue is closed");?>

					<a id="<?php echo $row['issues']['id_issue'];;?>" class="open_issue block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Glaze/folder_blue_open.png"><?php echo gtext("open the issue again");?></a>

				<?php } ?>

				<?php if (strcmp($row['issues']['deleted'],'no') == 0) { ?>

					<a id="<?php echo $row['issues']['id_issue'];;?>" class="issuehide_issue_del block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/button_cancel.png"><?php echo gtext("hide the issue");?></a>

				<?php } else {	?>
					<div class="issue_hidden_notice"><?php echo gtext("This issue is hidden for all the users that are not moderators");?></div>

					<a id="<?php echo $row['issues']['id_issue'];;?>" class="issueshow_issue_del block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/button_ok.png"><?php echo gtext("show the issue");?></a>

				<?php } ?>

				<!--view details-->
				<div class="show_hidden_box_ext">
					<div class="md_type">issue</div>
					<a id="<?php echo $row['issues']['id_issue'];?>" class="hidden_message_view_details" href="<?php echo $this->baseUrl."/home/index/$lang";?>">view details</a>
					<div class="moderation_details_box"></div>
				</div>

			</div>
			<?php } ?>

			<div class="issues_external_box">

				<div class="issues_view_title">
					<?php echo $row['issues']['title'];?>
				</div>

				<div class="talk_message_item_date">
					submitted by <?php echo getLinkToUser($u->getUser($row['issues']['created_by']));?>, <?php echo smartDate($row['issues']['creation_date']);?>
				</div>

				<div class="issues_view_status_and_priority">
					<table>
						<tr>
							<td><?php echo gtext("TOPIC");?>:</td>
							<td><b><?php echo str_replace('-',' ',$row['issues']['topic']);?></b></td>
						</tr>
						<tr>
							<td><?php echo gtext("STATUS");?>:</td>
							<td><b><?php echo $row['issues']['status'];?></b></td>
						</tr>
						<tr>
							<td><?php echo gtext("PRIORITY");?>:</td>
							<td><b><?php echo $row['issues']['priority'];?></b></td>
						</tr>
					</table>
				</div>

				<div class="issues_view_description_title">
					<?php echo gtext("Description");?>:
				</div>

				<div class="issues_view_description">
					<?php echo decodeWikiText($row['issues']['message']);?>
				</div>

					<?php if (strcmp($row['issues']['notice'],'') !== 0) { ?>

						<div class="issues_view_description_title">
							Response message (from h-node.com):
						</div>

						<div class="issues_view_description">
							<?php echo decodeWikiText($row['issues']['notice']);?>
						</div>

					<?php } ?>

				<?php } ?>
			</div>

			<!--print the messages to this issue-->
			<div class="issues_external_box">
				<div class="add_message_form_title">
					<?php echo gtext("Messages");?>:
				</div>
				<?php
				$mess_count = 0;
				foreach ($messages as $row) {
				$mess_count++;
				?>

					<?php if (strcmp($row['messages']['deleted'],'no') === 0) { ?>


						<a name="message-<?php echo $row['messages']['id_mes'];?>"></a><div class="issues_message_item">
							<div class="issues_message_item_user">
								<div class="issues_message_item_user_inner">
									<?php echo $u->getUser($row['messages']['created_by']);?>:
								</div>
								<?php if ($ismoderator) { ?>
									<a id="<?php echo $row['messages']['id_mes'];?>" class="hide_message hide_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/button_cancel.png">hide</a>
								<?php } ?>

							</div>

							<div class="message_view_description">
								<?php echo decodeWikiText($row['messages']['message']);?>
							</div>
							<div class="talk_message_item_date">
								submitted by <?php echo getLinkToUser($u->getUser($row['messages']['created_by']));?>, <?php echo smartDate($row['messages']['creation_date']);?>
							</div>

							<?php if ($ismoderator) { ?>
								<!--view details-->
								<div class="show_hidden_box_ext">
									<div class="md_type">message</div>
									<a id="<?php echo $row['messages']['id_mes'];?>" class="hidden_message_view_details" href="<?php echo $this->baseUrl."/home/index/$lang";?>">view details</a>
									<div class="moderation_details_box"></div>
								</div>
							<?php } ?>

						</div>

					<?php } else { ?>

						<?php if ($ismoderator) { ?>
							<a name="message-<?php echo $row['messages']['id_mes'];?>"></a>
							<div class="issues_message_item_hidden">
								<?php echo gtext("this message has been deleted");?>

									<a id="<?php echo $row['messages']['id_mes'];?>" class="show_message hide_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/button_ok.png">make visible</a>

									<!--view details-->
									<div class="show_hidden_box_ext">
										<div class="md_type">message</div>

										<a id="<?php echo $row['messages']['id_mes'];?>" class="hidden_message_view_details" href="<?php echo $this->baseUrl."/home/index/$lang";?>">view details</a>

										<div class="details_of_hidden_message">
											<div class="details_of_hidden_message_inner">
												<div class="talk_message_item_date">
													submitted by <?php echo getLinkToUser($u->getUser($row['messages']['created_by']));?>, <?php echo smartDate($row['messages']['creation_date']);?>
												</div>
												<div class="message_view_description_hidden">
													<?php echo decodeWikiText($row['messages']['message']);?>
												</div>
											</div>
											<div class="moderation_details_box"></div>
										</div>
									</div>
							</div>
						<?php } ?>
					<?php } ?>

				<?php } ?>

				<?php if ($mess_count === 0) { ?>
					<?php echo gtext("there are no messages");?>..
				<?php } ?>

			</div>

			<!--insert a message notice-->
			<?php if ($islogged === 'yes') { ?>

				<div class="add_issue_form">
					<div class="add_message_form_title">
						<a name="form"><?php echo gtext("Add a message to this issue");?></a>
					</div>

					<?php echo $notice;?>

					<!--preiview-->
					<?php if (isset($preview_message)) { ?>
						<div class="message_preview_notice">
							<?php echo gtext("preview of the message");?>:
						</div>
						<div class="issues_message_item_preview">
							<div class="message_view_description">
								<?php echo decodeWikiText($preview_message);?>
							</div>
						</div>
					<?php } ?>

					<?php echo $form;?>
				</div>

			<?php } else { ?>

				<div class="talk_login_notice">
					<a name="form"><?php echo gtext("You have to");?> <a href="<?php echo $this->baseUrl."/users/login/$lang?redirect=".$currPos.$queryString;?>"><?php echo gtext("login");?></a> <?php echo gtext("in order to submit a message to this issue");?></a>
				</div>

			<?php } ?>

		<?php } else { ?>

		<div style="margin:10px;"><?php echo gtext("This issue has been deleted"); ?></div>

		<?php } ?>
	</div>

