<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<script>
	
		$(document).ready(function() {
			
			$("#bb_code").markItUp(mySettings);
			
		});
		
	</script>
	
	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo gtext("Issues");?>
		</div>

		<div class="issues_external_box">
			<div class="issues_viewall_title">
				<?php echo gtext("List of issues");?>:
			</div>
			
			<table class="issues_viewall_table">
				<thead>
					<tr>
						<th><?php echo gtext("TITLE");?></th>
						<th><?php echo gtext("TOPIC");?></th>
						<th><?php echo gtext("OPENED BY");?></th>
						<th><?php echo gtext("LAST UPDATE");?></th>
						<th><?php echo gtext("REPLIES");?></th>
						<th><?php echo gtext("PRIORITY");?></th>
						<th><?php echo gtext("STATUS");?></th>
					</tr>
				</thead>
				
				<?php foreach ($table as $row) { ?>

				<?php if (strcmp($row['issues']['deleted'],'no') === 0 or $ismoderator) { ?>
				<tr class="issue_deleted_<?php echo $row['issues']['deleted']?>">
					<td><a href="<?php echo $this->baseUrl."/issues/view/$lang/".$row['issues']['id_issue'].$this->viewStatus;?>"><?php echo $row['issues']['title'];?></a> <span>(<?php echo gtext("hidden for those who are not moderators");?>)</span></td>
					<td><?php echo str_replace('-',' ',$row['issues']['topic']);?></td>
					<td><?php echo getLinkToUser($u->getUser($row['issues']['created_by']));?></td>
					<td><?php echo smartDate($row['issues']['update_date']);?></td>
					<td>
					<?php 
						if (strcmp($row['messages']['message'],'') !== 0)
						{
							echo $row['aggregate']['numb_mess'];
						}
						else
						{
							echo $row['aggregate']['numb_mess']-1;
						}
					?>
					</td>
					<td><?php echo $row['issues']['priority'];?></td>
					<td><?php echo $row['issues']['status'];?></td>
				</tr>
				<?php } ?>
				
				<?php } ?>
			</table>
		</div>
		
		<div class="history_page_list">
			<?php echo gtext("page list");?>: <?php echo $pageList;?>
		</div>
		
		<?php if ($islogged === 'yes') { ?>
			
			<div class="add_issue_form">
				<div class="add_issue_form_title">
					<a name="form"><?php echo gtext("Add a new issue");?></a>
				</div>
			
				<?php echo $notice;?>
				
				<!--preiview-->
				<?php if (isset($preview_message)) { ?>
					<div class="message_preview_notice">
						<?php echo gtext("preview of the new issue message");?>:
					</div>
					<div class="issues_message_item_preview">
						<div class="message_view_description">
							<?php echo decodeWikiText($preview_message);?>
						</div>
					</div>
				<?php } ?>
				
				<?php echo $form;?>
			</div>
			
		<?php } else { ?>
		
			<div class="talk_login_notice">
				<a name="form"><?php echo gtext("You have to");?> <a href="<?php echo $this->baseUrl."/users/login/$lang?redirect=".$currPos.$queryString;?>"><?php echo gtext("login");?></a> <?php echo gtext("in order to submit an issue");?></a>
			</div>
			
		<?php } ?>
		
	</div>
