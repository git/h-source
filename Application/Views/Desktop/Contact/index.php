<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; contact
		</div>
		
		<div class="credits_external_box">
			
			<div class="contact_div">
				You can suggest to implement new features or add new types of hardware in the <a href="<?php echo $this->baseUrl."/issues/viewall/$lang/1/$token";?>">issues</a> page
			</div>

			<div class="contact_div">
				Discussions about <b><?php echo Website::$generalName;?></b> also take place on the <a href="https://lists.nongnu.org/mailman/listinfo/h-source-users">h-source-users mailing list</a> and <a href="irc://libera.chat/h-node">#h-node libera.chat IRC channel</a>.
			</div>
			
		</div>
	
	</div>
