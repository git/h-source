<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; contact
		</div>
		
		<div class="credits_external_box">
			
			<div class="contact_div">
				Sie können neu zu implementierende Funktionen vorschlagen oder neue Hardware-Typen in der Seite <a href="<?php echo $this->baseUrl."/issues/viewall/$lang/1/$token";?>">Web-Log</a> hinzufügen
			</div>

			<div class="contact_div">
				Wenn Sie das <b>h-node-Team</b> direkt kontaktieren möchten: <b>info@h-node.com</b>
			</div>
			
		</div>
	
	</div>
