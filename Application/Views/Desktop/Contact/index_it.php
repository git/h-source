<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; contact
		</div>
		
		<div class="credits_external_box">
			
			<div class="contact_div">
				Potete suggerire nuove caratteristiche da implementare o aggiungere nuovi tipi di hardware nella pagina <a href="<?php echo $this->baseUrl."/issues/viewall/$lang/1/$token";?>">issues</a>
			</div>

			<div class="contact_div">
				Se volete direttamente contattare il team di <b><?php echo Website::$generalName;?></b> potete farlo a questo indirizzo e-mail: <b>info@h-node.com</b>
			</div>
			
		</div>
	
	</div>
