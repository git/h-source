<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
	
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/meet/user/$lang/$meet_username";?>"><?php echo gtext("meet");?> <b><?php echo $meet_username;?></b></a> &raquo; <?php echo gtext("contributions");?>
		</div>
		
		<div class="contrib_explain_box">
			<?php echo gtext("contributions of");?> <?php echo $meet_username;?>
		</div>
		
		<div class="external_users_contrib">
		
			<!--hardware contributions-->
			<?php if ($hasHardware) { ?>
			<div class="user_hardware_pages">
				<a href="<?php echo $this->baseUrl."/meet/hardware/$lang/$meet_username";?>">hardware inserted/updated by <b><?php echo $meet_username;?></b></a>
			</div>
			<?php } ?>
			
			<!--talk messages submitted (hardware pages)-->
			<?php if ($hasTalk) { ?>
			<div class="user_hardware_pages">
				<a href="<?php echo $this->baseUrl."/meet/talk/$lang/$meet_username".$this->viewStatus;?>">talk messages submitted by <b><?php echo $meet_username;?></b> (hardware pages)</a>
			</div>
			<?php } ?>
			
			<!--issues opened-->
			<?php if ($hasIssues) { ?>
			<div class="user_hardware_pages">
				<a href="<?php echo $this->baseUrl."/meet/issues/$lang/$meet_username".$this->viewStatus;?>">issues opened by <b><?php echo $meet_username;?></b></a>
			</div>
			<?php } ?>
			
			<!--messages submitted-->
			<?php if ($hasMessages) { ?>
			<div class="user_hardware_pages">
				<a href="<?php echo $this->baseUrl."/meet/messages/$lang/$meet_username".$this->viewStatus;?>">messages submitted by <b><?php echo $meet_username;?></b></a>
			</div>
			<?php } ?>

			<!--messages submitted-->
			<?php if ($hasWiki) { ?>
			<div class="user_hardware_pages">
				<a href="<?php echo $this->baseUrl."/meet/wiki/$lang/$meet_username".$this->viewStatus;?>">wiki pages inserted/updated by <b><?php echo $meet_username;?></b></a>
			</div>
			<?php } ?>

			<!--talk messages submitted (wiki pages)-->
			<?php if ($hasWikiTalk) { ?>
			<div class="user_hardware_pages">
				<a href="<?php echo $this->baseUrl."/meet/wikitalk/$lang/$meet_username".$this->viewStatus;?>">talk messages submitted by <b><?php echo $meet_username;?></b> (wiki pages)</a>
			</div>
			<?php } ?>
			
		</div>
	
	</div>