<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
	
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo gtext("meet");?> <b><?php echo $meet_username;?></b>
		</div>
		
		<?php if ($isadmin) { ?>
		<div class="moderator_box">
			<?php if ($isBlocked) { ?>
				This user has been blocked
				
				<a id="<?php echo $meet_id_user;?>" class="unblock_user block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/H2O/im-user.png">unblock the user</a>
				
			<?php } else {	?>
			
				<a id="<?php echo $meet_id_user;?>" class="block_user block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/H2O/im-ban-user.png">block the user</a>
				
			<?php } ?>
			
			<!--view details-->
			<div class="show_hidden_box_ext">
				<div class="md_type">user</div>
				<a id="<?php echo $meet_id_user;?>" class="hidden_message_view_details" href="<?php echo $this->baseUrl."/home/index/$lang";?>">view details</a>
				<div class="moderation_details_box"></div>
			</div>
			
		</div>
		<?php } ?>
		
		<div class="meet_contrib_link">
			<u><?php echo gtext("Public profile of");?> <?php echo $meet_username;?></u>. <?php echo gtext("See all the contributions of");?> <a href="<?php echo $this->baseUrl."/meet/contributions/$lang/$meet_username";?>"> <b><?php echo $meet_username;?></b></a>
		</div>
	
		<?php foreach ($table as $item) {?>
		<div class="users_meet_box">

			<div class="meet_item">
				<div class="meet_item_inner">Username:</div> <?php echo $item['regusers']['username'];?>
			</div>
			
			<?php if (strcmp($item['profile']['website'],'') !== 0) { ?>
			<div class="meet_item">
				<div class="meet_item_inner"><?php echo gtext("My website");?>:</div> <?php echo vitalizeUrl($item['profile']['website']);?>
			</div>
			<?php } ?>
			
			<?php if (strcmp($item['profile']['real_name'],'') !== 0) { ?>
			<div class="meet_item">
				<div class="meet_item_inner"><?php echo gtext("My real name");?>:</div> <?php echo $item['profile']['real_name'];?>
			</div>
			<?php } ?>

			<?php if (strcmp($item['profile']['publish_mail'],'yes') === 0) { ?>
			<div class="meet_item">
				<div class="meet_item_inner"><?php echo gtext("My e-mail address");?>:</div> <?php echo $item['regusers']['e_mail'];?>
			</div>
			<?php } ?>
			
			<?php if (strcmp($item['profile']['where_you_are'],'') !== 0) { ?>
			<div class="meet_item">
				<div class="meet_item_inner"><?php echo gtext("I'm from");?>:</div> <?php echo $item['profile']['where_you_are'];?>
			</div>
			<?php } ?>
			
			<?php if (strcmp($item['profile']['birth_date'],'') !== 0) { ?>
			<div class="meet_item">
				<div class="meet_item_inner"><?php echo gtext("Birthdate");?>:</div> <?php echo $item['profile']['birth_date'];?>
			</div>
			<?php } ?>

			<?php if (strcmp($item['profile']['fav_distro'],'') !== 0) { ?>
			<div class="meet_item">
				<div class="meet_item_inner"><?php echo gtext("My favourite distribution");?>:</div> <?php echo $item['profile']['fav_distro'];?>
			</div>
			<?php } ?>

			<?php if (strcmp($item['profile']['projects'],'') !== 0) { ?>
			<div class="meet_item">
				<div class="meet_item_inner"><?php echo gtext("Free software projects I'm working on");?>:</div> <div><?php echo nl2br($item['profile']['projects']);?></div>
			</div>
			<?php } ?>
			
			<?php if (strcmp($item['profile']['description'],'') !== 0) { ?>
			<div class="meet_item">
				<div class="meet_item_inner"><?php echo gtext("My description");?>:</div> <div><?php echo nl2br($item['profile']['description']);?></div>
			</div>
			<?php } ?>
			
		</div>
		<?php } ?>
	
	</div>