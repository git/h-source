<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

<?php $u = new UsersModel();?>
<?php
	$mess_count = 0;
	foreach ($res as $row) {
	$mess_count++;
?>
<div class="<?php echo $box_class;?>">
	<div class="talk_message_item_date">this <?php echo $object;?> has been <?php echo $md_action[$row['history']['action']];?> by <?php echo getLinkToUser($u->getUser($row['history']['created_by']));?> at <?php echo smartDate($row['history']['creation_date']);?> with the following motivation:
	</div>
	<div class="deleted_message_show"><?php echo $row['history']['message'];?></div>
</div>
<?php } ?>

<?php if ($mess_count === 0) { ?>
	<div class="<?php echo $box_class;?>">
		there are no details..
	</div>
<?php } ?>