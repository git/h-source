<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010, 2011 Antonio Gallo (h-source-copyright.txt)
// Copyright (C) 2011 Joerg Kohne
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

<div class="help_external_box">

	<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo gtext("Help");?>
	</div>

	<div class="help_tables_of_contents">
		Table of contents
		<ul>
			<li><a href="<?php echo $this->currPage."/$lang#wiki-syntax";?>">Sintassi della Wiki</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#compatibility";?>">Classi di compatibilità</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#discover-hardware";?>">Scopri il tuo hardware</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#fully-free";?>">Lista delle distribuzioni Gnu/Linux completamente libere</a></li>
		</ul>
	</div>
	
	<a name="wiki-syntax"></a><h2>Sintassi della Wiki</h2>
	
	<h3 name="wiki-tag">Lista dei tag della wiki di <?php echo Website::$generalName;?>
	</h3>
	
	<table class="help_wiki_table" width="100%" border="1">
		<thead>
			<tr>
				<th>nome</th>
				<th>tag</th>
				<th>risultato</th>
				<th width="40%">descrizione</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>grassetto</td>
				<td>[b]text[/b]</td>
				<td><b>text</b></td>
				<td>testo in grassetto</td>
			</tr>
			<tr>
				<td>corsivo</td>
				<td>[i]text[/i]</td>
				<td><i>text</i></td>
				<td>testo corsivo</td>
			</tr>
			<tr>
				<td>cancellare</td>
				<td>[del]text[/del]</td>
				<td><del>text</del></td>
				<td>testo cancellato</td>
			</tr>
			<tr>
				<td>sottolineare</td>
				<td>[u]text[/u]</td>
				<td><u>text</u></td>
				<td>testo sottolineato</td>
			</tr>
			<tr>
				<td>head 1</td>
				<td>[h1]text[/h1]</td>
				<td>
					<div class="div_h1">
					text</div>
				</td>
				<td>head 1</td>
			</tr>
			<tr>
				<td>head 2</td>
				<td>[h2]text[/h2]</td>
				<td>
					<div class="div_h2">
					text</div>
				</td>
				<td>head 2</td>
			</tr>
			<tr>
				<td>head 3</td>
				<td>[h3]text[/h3]</td>
				<td>
					<div class="div_h3">
					text</div>
				</td>
				<td>head 3</td>
			</tr>
			<tr>
				<td>paragrafo</td>
				<td>…[p]nuovo paragrafo[/p]</td>
				<td>… <p>nuovo paragrafo</p>
				</td>
				<td>crea un nuovo paragrafo</td>
			</tr>
			<tr>
				<td>Lista </td>
				<td><ul style="list-style-type:none;">
						<li>[list]</li>
						<li><ul style="list-style-type:none;">
								<li>[*]primo elemento[/*]</li>
								<li>[*]secondo elemento[/*]</li>
							</ul>
						</li>
						<li>[/list]</li>
					</ul>
				</td>
				<td><ul>
						<li>primo elemento</li>
						<li>secondo elemento</li>
					</ul>
				</td>
				<td>crea un elenco di item</td>
			</tr>
			<tr>
				<td>Lista numerata </td>
				<td><ul style="list-style-type:none;">
						<li>[enum]</li>
						<li><ul style="list-style-type:none;">
								<li>[*]primo elemento[/*]</li>
								<li>[*]secondo elemento[/*]</li>
							</ul>
						</li>
						<li>[/enum]</li>
					</ul>
				</td>
				<td><ol>
						<li>primo elemento</li>
						<li>secondo elemento</li>
					</ol>
				</td>
				<td>crea un elenco numerato di item</td>
			</tr>
			<tr>
				<td>item di un elenco</td>
				<td>[*]elemento[/*]</td>
				<td><ul>
						<li>elemento</li>
					</ul>
				</td>
				<td>aggiungi un item a un elenco</td>
			</tr>
			<tr>
				<td>codice</td>
				<td>[code]some code[/code]</td>
				<td><pre class="code_pre">some code</pre>
				</td>
				<td>aggiungi del codice</td>
			</tr>
			<tr>
				<td>link semplice</td>
				<td>[a]url[/a]</td>
				<td><a href="url">url</a></td>
				<td>crea un link semplice</td>
			</tr>
			<tr>
				<td>link con testo</td>
				<td>[a]url|text[/a]</td>
				<td><a href="url">text</a></td>
				<td>crea un link con testo</td>
			</tr>
			<tr>
				<td>notebook</td>
				<td>[notebook]id[/notebook]</td>
				<td>id</td>
				<td>crea un link al notebook con l'identificatore corrispondente a id (l'identificatore di ogni modello di
					dispositivo si trova nella pagina del dispositivo stesso, accanto al nome del modello</td>
			</tr>
			<tr>
				<td>wifi</td>
				<td>[wifi]id[/wifi]</td>
				<td>id</td>
				<td>crea un link alla wifi con l'identificatore corrispondente a id (l'identificatore di ogni modello di
					dispositivo si trova nella pagina del dispositivo stesso, accanto al nome del modello)</td>
			</tr>
			<tr>
				<td>scheda video</td>
				<td>[videocard]id[/videocard]</td>
				<td>id</td>
				<td>crea un link alla scheda video con l'identificatore corrispondente a id (l'identificatore di ogni modello di
					dispositivo si trova nella pagina del dispositivo stesso, accanto al nome del modello)</td>
			</tr>
		</tbody>
	</table>
	
	<p>&nbsp;</p>
	
	<a name="compatibility"></a><h2>Classi di compatibilità</h2>
	
	<a name="notebook-compatibility"></a><h3>Notebooks</h3>
	<dl>
		<dt>Classe A (Platino)</dt>
			<dd>Tutti i dispositivi del portatile funzionano ad alte prestazioni. </dd>
			<dd class="example">Per esempio: funzionano tutti i dispositivi, l'accelerazione 3D è supportata.</dd>
		<dt>Classe B (Oro)</dt>
			<dd>Tutti i dispositivi del portatile funzionano ma non a piene prestazioni. </dd>
			<dd class="example">Esempio tipico: funzionano tutti i dispositivi, ma l'accelerazione 3D non è supportata.</dd>
		<dt>Classe C (Argento)</dt>
			<dd>Uno dei dispositivi principali non è supportato. </dd>
			<dd class="example">Per esempio: la scheda wifi interna non funziona e serve una wifi esterna USB.</dd>
		<dt>Classe D (Bronzo)</dt>
			<dd>Più di uno dei dispositivi principali non è supportato.</dd>
		<dt>Classe E (Spazzatura)</dt>
			<dd>Il portatile non funziona con software libero.</dd>
	</dl>
	
	<a name="printer-compatibility"></a><h3>Stampanti</h3>
	<dl>
		<dt>Classe A (Piena)</dt>
			<dd>Sono supportate tutte le funzioni e le caratteristiche della stampante.</dd>
		<dt>Classe B (Parziale)</dt>
			<dd>La funzione di stampa è supportata, ma a velocità limitata o a scarsa qualità. Su alcune stampanti
				multifunzione possono non essere supportate le funzioni di scanner e/o di fax.</dd>
		<dt>Classe C (Nessuna)</dt>
			<dd>La stampante non funziona con software libero.</dd>
	</dl>
	
	<a name="scanner-compatibility"></a><h3>Scanner</h3>
	<dl>
		<dt>Classe A (Piena)</dt>
			<dd>Sono supportate tutte le funzioni e le caratteristiche dello scanner.</dd>
		<dt>Classe B (Parziale)</dt>
			<dd>La funzione di scannerizzazione è supportata, ma a velocità limitata o a scarsa qualità. Qualche altra
				caratteristica può non essere supportata.</dd>
		<dt>Classe C (Nessuna)</dt>
			<dd>Lo scanner non funziona con software libero.</dd>
	</dl>
	
	<p></p>
	
	<a name="discover-hardware"></a><h2>Scopri il tuo hardware</h2>
	<cite>(Grazie <a href="<?php echo $this->baseUrl;?>/issues/view/en/3/1/token">lluvia</a>)</cite> 
	
	<p>Per sapere le caratteristiche e i dettagli del tuo hardware puoi seguire queste istruzioni:</p>
	<dl>
		<dt>Come scoprire il nome del modello del portatile</dt>
			<dd>Guarda sotto al tuo notebook o al tuo netbook</dd>
		<a name="model-name"></a><dt>Come scoprire il nome del modello del tuo hardware (se non è un portatile)</dt>
			<dd class="opt">Se il dispositivo è integrato (ad esempio: una scheda video)</dd>
			<dd>Apri un terminale e digita il seguente comando:</dd>
			<dd><pre class="terminal">lspci</pre>
			</dd>
			<dd>o</dd>
			<dd><pre class="terminal">lspci &gt; FILENAME          # Puoi anche scrivere l'output del comandi lspci su un file digitando<!-- l'output del comandi un file digitando :)--></pre>
			</dd>
		<!-- dd>dove "filename" è il nome del file</dd -->
			<dd>Otterrai la lista di tutti i tuoi dispositivi PCI, simile a quella scritta sotto</dd>
			<dd><pre class="terminal">
00:18.3 Host bridge: <b>Advanced Micro Devices [AMD] K8 [Athlon64/Opteron] Miscellaneous Control</b>
03:00.0 Network controller: <b>Broadcom Corporation BCM4311 802.11b/g WLAN (rev 02)</b>
05:00.0 VGA compatible controller: <b>nVidia Corporation G86 [GeForce 8400M GS] (rev a1)</b>
			</pre>
			</dd>
			<dd>Il nome di ogni dispositivo è scritto dopo i due punti (guarda il testo in grassetto nella
				lista superiore)</dd>
			<dd class="opt">Se è un dispositivo USB (ad esempio: una stampante USB)</dd>
			<dd>Apri un terminale e digita il seguente comando:</dd>
			<dd><pre class="terminal">lsusb -v</pre>
			</dd>
			<dd>o</dd>
			<dd><pre class="terminal">lsusb -v &gt; FILENAME          # l'output del comandi un file digitando</pre>
			</dd>
			<!-- dd>dove "filename" è il nome del file</dd -->
			<dd>Otterrai la lista di tutti i dispositivi USB, simile a quella scritta sotto</dd>
			<dd><pre class="terminal">
Bus 001 Device 002: ID 0846:4260 NetGear, Inc. WG111v3 54 Mbps Wireless [realtek RTL8187B]
Device Descriptor:
	bLength 18
	bDescriptorType 1
	bcdUSB 2.00
	bDeviceClass 0 (Defined at Interface level)
	bDeviceSubClass 0
	bDeviceProtocol 0
	bMaxPacketSize0 64
	idVendor 0x0846 NetGear, Inc.
	idProduct 0x4260 <b>WG111v3 54 Mbps Wireless [realtek RTL8187B]</b>
	bcdDevice 2.00
	iManufacturer 1
	iProduct 2
	iSerial 3
	...
	...

Bus 002 Device 003: ID 08ff:2580 AuthenTec, Inc. AES2501 Fingerprint Sensor
Device Descriptor:
	bLength 18
	bDescriptorType 1
	bcdUSB 1.10
	bDeviceClass 255 Vendor Specific Class
	bDeviceSubClass 255 Vendor Specific Subclass
	bDeviceProtocol 255 Vendor Specific Protocol
	bMaxPacketSize0 8
	idVendor 0x08ff AuthenTec, Inc.
	idProduct 0x2580 <b>AES2501 Fingerprint Sensor</b>
	bcdDevice 6.23
	iManufacturer 0
	iProduct 1 Fingerprint Sensor
	iSerial 0
	bNumConfigurations 1
	...
	...
			</pre>
			</dd>
			<dd>Il nome di ogni dispositivo è scritto alla riga che inizia con la stringa "idProduct" (guarda il testo in
				grassetto nella lista superiore)</dd>
		<dt>Come scoprire che versione del kernel libre stai usando</dt>
			<dd>Apri un terminale e digita questo comando</dd>
			<dd><pre class="terminal">uname -r</pre>
			</dd>
		<dt>Come scoprire il nome della tua scheda video</dt>
			<dd>Apri un terminale e digita questo comando:</dd>
			<dd><pre class="terminal">sudo lspci</pre>
			</dd>
			<dd>Poi cerca la riga contenente la stringa <b>VGA</b> o <b>Display controller</b>. Puoi anche provare con uno di
				questi comandi:</dd>
			<dd><pre class="terminal">lspci | grep "Display controller"</pre>
			</dd>
			<dd>o</dd>
			<dd><pre class="terminal">lspci | grep "VGA"</pre>
			</dd>
		<a name="vendoridproductid"></a><dt>Come scoprire il VendorID e il ProductID del tuo dispositivo (VendorID:ProductID
		code)</dt>
			<dd><cite>(Grazie <a
				href="http://trisquel.info/en/forum/h-nodecom-new-website-hardware-database#comment-5839">Michał Masłowski</a>
				e <a href="http://trisquel.info/en/forum/h-nodecom-new-website-hardware-database#comment-5837">Julius22</a>)
				</cite></dd>
			<dd class="opt">Se il dispositivo è integrato (per esempio una scheda video)</dd>
			<dd>Apri un terminale e digita il seguente comando:</dd>
			<dd><pre class="terminal">sudo lspci -nnk</pre>
			</dd>
			<dd>Dovresti ottenere una lista di hardware simile a quella scritta qui sotto</dd>
			<dd><pre class="terminal">
03:00.0 Network controller [0280]: Broadcom Corporation BCM4311 802.11b/g WLAN [<b>14e4:4311</b>] (rev 02)
	Kernel driver in use: b43-pci-bridge
	Kernel modules: ssb
05:00.0 VGA compatible controller [0300]: nVidia Corporation G86 [GeForce 8400M GS] [<b>10de:0427</b>] (rev a1)
	Kernel modules: nouveau, nvidiafb</pre>
			<dd class="note"><span class="note">Note:</span> Le stringhe in <b>grassetto</b> e tra parentesi quadre (nella
				lista qui sopra) sono il codice che stai cercando. Il primo gruppo di cifre (prima dei due punti) è il
				<b>VendorID</b>, il secondo gruppo è il <b>ProductID</b>. Nell'esempio qui sopra: il codice VendorID:ProductID
				della scheda wifi (nota le stringhe "Network controller" e "WLAN") è <b>14e4:4311</b> mentre il codice
				VendorID:ProductID della scheda video (nota la stringa "VGA") è <b>10de:0427</b></dd>
			<dd class="opt">Se si tratta di un dispositivo USB (per esempio una wifi esterna USB)</dd>
			<dd>Apri un terminale e digita questo comando:</dd>
			<dd><pre class="terminal">sudo lsusb</pre>
			</dd>
			<dd>Dovresti ottenere una lista di hardware simile a quella scritta qui sotto</dd>
			<dd><pre class="terminal">
Bus 001 Device 002: ID <b>0846:4260</b> NetGear, Inc. WG111v3 54 Mbps Wireless [realtek RTL8187B]
Bus 001 Device 001: ID <b>1d6b:0002</b> Linux Foundation 2.0 root hub
Bus 002 Device 003: ID <b>08ff:2580</b> AuthenTec, Inc. AES2501 Fingerprint Sensor</pre>
			</dd>
			<dd class="note"><span class="note">Note:</span> Le stringhe in <b>grassetto</b> (nella lista qui sopra) sono il
				codice che stai cercando. Il primo gruppo di cifre (prima dei due punti) è il <b>VendorID</b>, il secondo gruppo
				è il <b>ProductID</b>. Nell'esempio qui sopra: il codice VendorID:ProductID della wifi esterna USB (nota la
				stringa "Wireless") è <b>0846:4260</b></dd>
		<dt>Come scoprire se funziona la scheda video</dt>
			<dd>Installa <code><a href="http://rss-glx.sourceforge.net/">rss-glx</a></code> tramite il gestore di pacchetti
				della tua distribuzione o compilando dai sorgenti e prova degli screensaver (per esempio <b>Skyrocket</b> o
				<b>Solarwinds</b>). Controlla se parte lo screensaver (e/o se si vede "fluido")</dd>
		<dt>Come scoprire se funziona l'accelerazione 3D</dt>
			<dd>Prova ad attivare compiz</dd>
		<dt>Come scoprire il nome della tua scheda wifi</dt>
			<dd>Apri un terminale e digita questo comando:</dd>
			<dd><pre class="terminal">sudo lspci</pre>
			</dd>
			<dd>Poi cerca la riga contenente la stringa <samp><b>Wireless</b></samp> o <samp><b>Network controller</b></samp>.
				Puoi anche provare uno dei seguenti comandi:</dd>
			<dd><pre class="terminal">lspci | grep "Wireless"</pre>
			</dd>
			<dd>o</dd>
			<dd><pre class="terminal">lspci | grep "Network"</pre>
			</dd>
		<dt>Come scoprire che driver per la stampante stai usando</dt>
			<dd class="opt">Se stai usando cups</dd>
			<dd>Apri un terminale e digita il seguente comando:</dd>
			<dd><pre class="terminal">dpkg-query -W -f '${Version}\n' cups</pre>
			</dd>
		<dt>Come scoprire l'architettura del tuo portatile:</dt>
			<dd>Apri un terminale e digita il seguente comando:</dd>
			<dd><pre class="terminal">cat /proc/cpuinfo | grep "lm"</pre>
			</dd>
			<dd>Se ottieni un messaggio come il seguente:</dd>
			<dd><pre class="terminal">flags		: fpu vme de pse tsc msr pae mce cx8 apic mtrr pge mca cmov pat pse36 clflush dts<br/>acpi mmx fxsr sse sse2 ss ht tm pbe nx lm constant_tsc arch_perfmon pebs bts aperfmperf pni dtes64<br/>monitor ds_cpl est tm2 ssse3 cx16 xtpr pdcm lahf_lm</pre>
			</dd>
			<dd>allora il tuo portatile è x86-64/amd64 e puoi usare una distribuzione x86-64/amd64</dd>
	</dl>
	
	<p> </p>
	
	<a name="fully-free"></a><h2>Lista di distribuzioni GNU/Linux completamente libere</h2>
	
	<!-- p class="attention"><span class="highlight">La <strong>h-node</strong> non è responsabile del contenuto di altri siti
	web, né di quanto il contenuto di tali siti sia aggiornato.</span></p -->
	
	<p>Le seguenti sono le distribuzioni <a href="http://www.gnu.org/gnu/linux-and-gnu.it.html">GNU/Linux</a> a noi note
	che hanno una rigorosa politica di inclusione e indicazione di solo software libero. Queste distribuzioni escludono
	applicazioni, piattaforme di programmazione, driver e firmware ("blob") che non siano liberi, e rimuovono quelli
	inclusi per errore. Per saperne di più su cosa rende <a href="http://www.gnu.org/distros/free-distros.it.html">libera
	una distribuzione GNU/Linux</a>, si vedano le GNU <a
	href="http://www.gnu.org/distros/free-system-distribution-guidelines.it.html">linee guida per le distribuzioni
	libere</a>.</p>
	
	<p><strong>Tutte le distribuzioni che seguono sono installabili sul disco di un computer e la maggior parte può essere
	eseguita da un supporto</strong> (CD, memoria USB) senza che nulla debba essere installato sul computer.</p> 
	
	<p>In ordine alfabetico</p>
	
	<ul>
		<li><a href="http://www.dragora.org">Dragora GNU/Linux</a></li>
		<li><a href="http://dynebolic.org/">Dynebolic GNU/Linux</a></li>
		<li><a href="http://www.musix.org.ar/">Musix GNU+Linux</a></li>
		<li><a href="https://parabolagnulinux.org/">Parabola GNU/Linux</a></li>
		<li><a href="http://trisquel.info/">Trisquel GNU/Linux</a></li>
		<li><a href="http://www.ututo.org/www/">Ututo GNU/Linux</a></li>
		<li><a href="http://venenux.org/">Venenux GNU/Linux</a></li>
	</ul>
	</div>
	
