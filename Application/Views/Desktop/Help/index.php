<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010, 2011  Antonio Gallo (h-source-copyright.txt)
// Copyright (C) 2011 Joerg Kohne
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

<div class="help_external_box">

	<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo gtext("Help");?>
	</div>

	<div class="help_tables_of_contents">
		Table of contents
		<ul>
			<li><a href="<?php echo $this->currPage."/$lang#wiki-syntax";?>">Wiki syntax</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#compatibility";?>">Compatibility classes</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#discover-hardware";?>">Discover your hardware</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#fully-free";?>">List of fully free GNU/Linux distributions</a></li>
		</ul>
	</div>
	
	<a name="wiki-syntax"></a><h2>Wiki Syntax</h2>
	
	<h3>List of <?php echo Website::$generalName;?>
	Wiki Tags</h3>
	
	<table class="wiki">
		<thead>
			<tr>
				<th>Name</th>
				<th>Tag</th>
				<th>Result</th>
				<th>Description</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>bold</td>
				<td>text [b]bold[/b]</td>
				<td>text <b>bold</b></td>
				<td>text bold</td>
			</tr>
			<tr>
				<td>italic</td>
				<td>text [i]italic[/i]</td>
				<td>text <i>italic</i></td>
				<td>text italic</td>
			</tr>
			<tr>
				<td>del</td>
				<td>text [del]deleted[/del]</td>
				<td>text <del>deleted</del></td>
				<td>text deleted</td>
			</tr>
			<tr>
				<td>underline</td>
				<td>text [u]underlined[/u]</td>
				<td>text <u>underlined</u></td>
				<td>text underlined</td>
			</tr>
			<tr>
				<td>head 1</td>
				<td>[h1]heading[/h1]</td>
				<td>
					<div class="div_h1">
					heading</div>
				</td>
				<td>head 1</td>
			</tr>
			<tr>
				<td>head 2</td>
				<td>[h2]heading[/h2]</td>
				<td>
					<div class="div_h2">
					heading</div>
				</td>
				<td>head 2</td>
			</tr>
			<tr>
				<td>head 3</td>
				<td>[h3]heading[/h3]</td>
				<td>
					<div class="div_h3">
					heading</div>
				</td>
				<td>head 3</td>
			</tr>
			<tr>
				<td>paragraph</td>
				<td>…[p]new paragraph[/p]</td>
				<td>… 
	
					<p>new paragraph</p>
				</td>
				<td>new paragraph</td>
			</tr>
			<tr>
				<td>Ordered Lists </td>
				<td><ul style="list-style-type:none;">
						<li>[list]</li>
						<li><ul style="list-style-type:none;">
								<li>[*]first item[/*]</li>
								<li>[*]second item[/*]</li>
							</ul>
						</li>
						<li>[/list]</li>
					</ul>
				</td>
				<td><ul>
						<li>first item</li>
						<li>second item</li>
					</ul>
				</td>
				<td>make a list of items</td>
			</tr>
			<tr>
				<td>Numbered List </td>
				<td><ul style="list-style-type:none;">
						<li>[enum]</li>
						<li><ul style="list-style-type:none;">
								<li>[*]first item[/*]</li>
								<li>[*]second item[/*]</li>
							</ul>
						</li>
						<li>[/enum]</li>
					</ul>
				</td>
				<td><ol>
						<li>first item</li>
						<li>second item</li>
					</ol>
				</td>
				<td>make a numbered list of items</td>
			</tr>
			<tr>
				<td>list item</td>
				<td>[*]item[/*]</td>
				<td><ul>
						<li>item</li>
					</ul>
				</td>
				<td>add an item to a list</td>
			</tr>
			<tr>
				<td>code</td>
				<td>[code]some code[/code]</td>
				<td><pre class="code_pre">some code</pre>
				</td>
				<td>some code</td>
			</tr>
			<tr>
				<td>simple link</td>
				<td>[a]url[/a]</td>
				<td><a href="url">url</a></td>
				<td>simple link</td>
			</tr>
			<tr>
				<td>link with text</td>
				<td>[a]url|text[/a]</td>
				<td><a href="url">text</a></td>
				<td>link with text</td>
			</tr>
			<tr>
				<td>notebook</td>
				<td>[notebook]id[/notebook]</td>
				<td><samp>id</samp></td>
				<td>link to the notebook with the identifier equal to id (the identifier of each device model is written in the
					page of the device itself, next to the model name)</td>
			</tr>
			<tr>
				<td>wifi</td>
				<td>[wifi]id[/wifi]</td>
				<td><samp>id</samp></td>
				<td>link to the wifi with the identifier equal to id (the identifier of each device model is written in the page
					of the device itself, next to the model name)</td>
			</tr>
			<tr>
				<td>videocard</td>
				<td>[videocard]id[/videocard]</td>
				<td class="sample"><samp>id</samp></td>
				<td>link to the videocard with the identifier equal to id (the identifier of each device model is written in the
					page of the device itself, next to the model name)</td>
			</tr>
		</tbody>
	</table>
	
	<p> </p>
	
	<a name="compatibility"></a><h2>Compatibility classes</h2>
	
	<a name="notebook-compatibility"></a><h3>Notebooks</h3>
	<dl>
		<dt>Class A (Platinum)</dt>
			<dd>All the notebook devices work with a very good performance. </dd>
			<dd class="example">Example: all the devices work, the 3D acceleration is supported</dd>
		<dt>Class B (Gold)</dt>
			<dd>All the notebook devices work but not at full performance. </dd>
			<dd class="example">A typical example: all the devices work, but the 3D acceleration is not supported</dd>
		<dt>Class C (Silver)</dt>
			<dd>One main device is not supported. </dd>
			<dd class="example">Example: the internal wifi card does not work. You need an external USB card</dd>
		<dt>Class D (Bronze)</dt>
			<dd>More than one device is not supported</dd>
		<dt>Class E (Garbage)</dt>
			<dd>The notebook does not work with free software</dd>
	</dl>
	
	<a name="printer-compatibility"></a><h3>Printers</h3>
	<dl>
		<dt>Class A (Full)</dt>
			<dd>All device functions and features are supported</dd>
		<dt>Class B (Partial)</dt>
			<dd>Printing supported but possibly at limited speed or print quality; scanning and/or faxing on some multifunction
				devices may not be supported</dd>
		<dt>Class C (None)</dt>
			<dd>The printer does not work with free software</dd>
	</dl>
	
	<a name="scanner-compatibility"></a><h3>Scanners</h3>
	<dl>
		<dt>Class A (Full)</dt>
			<dd>All device functions and features are supported</dd>
		<dt>Class B (Partial)</dt>
			<dd>Scanning supported but possibly at limited speed or quality; some other features may not be supported</dd>
		<dt>Class C (None)</dt>
			<dd>The scanner does not work with free software</dd>
	</dl>
	
	<p> </p>
	
	<a name="discover-hardware"></a><h2>Discover your hardware</h2>
	
	<p><cite>(Thanks <a href="<?php echo $this->baseUrl;?>/issues/view/en/3/1/token">lluvia</a>)</cite></p>
	
	<p>In order to know the details of your hardware you can carry out the following actions:</p>
	<dl>
		<dt>How to discover the model name of your notebook</dt>
			<dd>See below your notebook or netbook<dd>
		<a name="model-name"></a><dt>How to discover the model name of your device (if it is not a notebook)</dt>
			<dd class="opt"><strong>If the device is integrated (example: a video card)</strong></dd>
			<dd>Open a terminal and type the following command:</dd>
			<dd><pre class="terminal">lspci</pre>
			</dd>
			<dd>or</dd>
			<dd><pre class="terminal">lspci &gt; FILENAME          # output to a file</pre>
			</dd>
		<!-- dd>where "filename" is the name of the file</dd -->
			<dd>You will obtain the list of your PCI devices, similar to the one written below</dd>
			<dd><pre class="terminal">
00:18.3 Host bridge: <b>Advanced Micro Devices [AMD] K8 [Athlon64/Opteron] Miscellaneous Control</b>
03:00.0 Network controller: <b>Broadcom Corporation BCM4311 802.11b/g WLAN (rev 02)</b>
05:00.0 VGA compatible controller: <b>nVidia Corporation G86 [GeForce 8400M GS] (rev a1)</b>
			</pre>
			</dd>
			<dd class="note"><b>Note:</b> The name of each device is written after the colon (see the text
				in bold in the above list)</dd>
			<dd>If the device is an USB device (example: an external USB wifi card)</dd>
			<dd>Open a terminal and type the following command:</dd>
			<dd><pre class="terminal">lsusb -v</pre>
			</dd>
			<dd>or</dd>
			<dd><pre class="terminal">lsusb -v &gt; FILENAME          # output to a file</pre>
			</dd>
			<!-- dd>where "filename" is the name of the file</dd -->
			<dd>You will obtain the list of your USB devices, similar to the one written below</dd>
			<dd><pre class="terminal">
Bus 001 Device 002: ID 0846:4260 NetGear, Inc. WG111v3 54 Mbps Wireless [realtek RTL8187B]
Device Descriptor:
	bLength						18
	bDescriptorType				1
	bcdUSB						2.00
	bDeviceClass				0 (Defined at Interface level)
	bDeviceSubClass				0
	bDeviceProtocol				0
	bMaxPacketSize0				64
	idVendor					0x0846 NetGear, Inc.
	idProduct					0x4260 <b>WG111v3 54 Mbps Wireless [realtek RTL8187B]</b>
	bcdDevice					2.00
	iManufacturer				1
	iProduct					2
	iSerial						3
	...
	...

Bus 002 Device 003: ID 08ff:2580 AuthenTec, Inc. AES2501 Fingerprint Sensor
Device Descriptor:
	bLength						18
	bDescriptorType				1
	bcdUSB						1.10
	bDeviceClass				255 Vendor Specific Class
	bDeviceSubClass			 	255 Vendor Specific Subclass
	bDeviceProtocol			 	255 Vendor Specific Protocol
	bMaxPacketSize0				8
	idVendor					0x08ff AuthenTec, Inc.
	idProduct					0x2580 <b>AES2501 Fingerprint Sensor</b>
	bcdDevice					6.23
	iManufacturer				0
	iProduct					1 Fingerprint Sensor
	iSerial						0
	bNumConfigurations			1
	...
	...
			</pre>
			</dd>
			<dd class="note"><b>Note:</b> The name of each device is written at the row starting with "idProduct" (see the text
				in bold in the above list)</dd>
		<dl>
			<dt>How to discover the model name of your notebook</dt>
				<dd>See below your notebook or netbook<dd>
			<a name="model-name"></a><dt>How to discover the model name of your device (if it is not a notebook)</dt>
				<dd class="opt"><strong>If the device is integrated (example: a video card)</strong></dd>
				<dd>Open a terminal and type the following command:</dd>
				<dd><pre class="terminal">lspci</pre>
				</dd>
				<dd>or</dd>
				<dd><pre class="terminal">lspci &gt; FILENAME          # output to a file</pre>
				</dd>
			<dt>How to discover the kernel libre version you are using</dt>
				<dd>Open a terminal and type the following command:</dd>
				<dd> <pre class="terminal">uname -r</pre>
				</dd>
			<dt>How to discover the name of your video card</dt>
				<dd>Open a terminal and type the following command:</dd>
				<dd><pre class="terminal">sudo lspci</pre>
				</dd>
				<dd>Then look for the row containing the string <b>VGA</b> or <b>Display controller</b>. You can also try one of
					the following commands:</dd>
				<dd><pre class="terminal">lspci | grep "Display controller"</pre>
				</dd>
				<dd>or</dd>
				<dd><pre class="terminal">lspci | grep "VGA"</pre>
				</dd>
			<a name="vendoridproductid"></a><dt>How to discover the VendorID and the ProductID of your device (VendorID:ProductID
			code)</dt>
				<dd><cite>(Thanks <a
					href="http://trisquel.info/en/forum/h-nodecom-new-website-hardware-database#comment-5839">Michał
					Masłowski</a> and <a
					href="http://trisquel.info/en/forum/h-nodecom-new-website-hardware-database#comment-5837">Julius22</a>)</cite></dd>
				<dd>If the device is integrated (example: a video card) </dd>
				<dd>Open a terminal and type the following command:</dd>
			<dd><pre class="terminal">sudo lspci -nnk</pre></dd>
				<dd>You should obtain a list of hardware similar to the one written below</dd>
				<dd><pre class="terminal">
03:00.0 Network controller [0280]: Broadcom Corporation BCM4311 802.11b/g WLAN [<b>14e4:4311</b>] (rev 02)
	Kernel driver in use: b43-pci-bridge
	Kernel modules: ssb
05:00.0 VGA compatible controller [0300]: nVidia Corporation G86 [GeForce 8400M GS] [<b>10de:0427</b>] (rev a1)
	Kernel modules: nouveau, nvidiafb</pre>
				</dd>
				<dd>The strings in <b>bold</b> and placed inside the square brackets (in the above list) are the code you are
					looking for. The first set of digits (before the colon) are the <b>VendorID</b>, the second set of digits are
					the <b>ProductID</b>. In the above example: the VendorID:ProductID code of the wifi card (note the strings
					"Network controller" and "WLAN") is <b>14e4:4311</b> while the VendorID:ProductID code of the video card (note
					the string "VGA") is <b>10de:0427</b></dd>
				<dd class="opt">If the device is an USB device (example: an external USB wifi card) </dd>
				<dd>Open a terminal and type the following command:</dd>
				<dd><pre class="terminal">sudo lsusb</pre>
				</dd>
				<dd>You should obtain a list of hardware similar to the one written below</dd>
				<dd><pre class="terminal">
Bus 001 Device 002: ID <b>0846:4260</b> NetGear, Inc. WG111v3 54 Mbps Wireless [realtek RTL8187B]
Bus 001 Device 001: ID <b>1d6b:0002</b> Linux Foundation 2.0 root hub
Bus 002 Device 003: ID <b>08ff:2580</b> AuthenTec, Inc. AES2501 Fingerprint Sensor</pre>
				</dd>
				<dd>The strings in <b>bold</b> (in the above list) are the code you are looking for. The first set of digits
					(before the colon) are the <b>VendorID</b>, the second set of digits are the <b>ProductID</b>. In the above
					example: the VendorID:ProductID code of the external USB wifi card (note the strings "Wireless") is
					<b>0846:4260</b></dd>
			<dt>How to discover if the video card works</dt>
				<dd>Install <a href="http://rss-glx.sourceforge.net/"><code>rss-glx</code></a> by means of the package manager of
					your distribution or compiling it from source and try some screensavers (for example <b>Skyrocket</b> or
					<b>Solarwinds</b>). Check if you can play the screensaver (and/or if you can play it smoothly)</dd>
			<dt>How to discover if the 3D acceleration works</dt>
				<dd>Try to enable compiz</dd>
			<dt>How to discover the name of your wifi card</dt>
				<dd><p>Open a terminal and type the following command:</p>
				</dd>
				<dd><pre class="terminal">sudo lspci</pre>
				</dd>
				<dd>Then look for the row containing the string <b>Wireless</b> or <b>Network controller</b>. You can also try
					one of the following commands:</dd>
				<dd><pre class="terminal">lspci | grep "Wireless" </pre>
				</dd>
				<dd>or</dd>
				<dd><pre class="terminal">lspci | grep "Network" </pre>
				</dd>
			<dt>How to discover the printer driver you are using</dt>
				<dd class="opt"><strong>If you are using cups</strong></dd>
				<dd>Open a terminal and type the following command:</dd>
				<dd><pre class="terminal">dpkg-query -W -f '${Version}\n' cups</pre>
				</dd>
			<dt>How to discover the architecture of your notebook</dt>
				<dd>Open a terminal and type the following command:</dd>
				<dd><pre class="terminal">cat /proc/cpuinfo | grep "lm"</pre>
				</dd>
				<dd>If you get a message like this:</dd>
				<dd><pre class="terminal">flags		: fpu vme de pse tsc msr pae mce cx8 apic mtrr pge mca cmov pat pse36 clflush dts<br/>acpi mmx fxsr sse sse2 ss ht tm pbe nx lm constant_tsc arch_perfmon pebs bts aperfmperf pni dtes64<br/>monitor ds_cpl est tm2 ssse3 cx16 xtpr pdcm lahf_lm</pre>
				</dd>
				<dd>then you machine is x86-64/amd64 capable and you could choose a x86-64/amd64 distro to run on it</dd>
		</dl>
	<p> </p>

	<a name="fully-free"></a><h2>Fully free GNU/Linux Distributions</h2>
	
	<p>Following are the <a href="http://www.gnu.org/gnu/linux-and-gnu.html">GNU/Linux</a> distributions we know of which have a firm policy
	commitment to only include and only propose free software. They reject non-free applications, non-free programming
	platforms, non-free drivers, or non-free firmware “blobs”. If by mistake they do include any, they remove it. To
	learn more about what makes for a <a href="http://www.gnu.org/distros/free-distros.html">free GNU/Linux
	distribution</a>, see GNU <a href="http://www.gnu.org/distros/free-system-distribution-guidelines.html">Guidelines for 
	Free System Distributions</a>.</p>
	
	<p><strong>All of the distributions that follow are installable to a computer's hard drive and most can be run
	live.</strong></p>
	
	<p>(listed in alphabetical order)</p>
	
	<!-- p>They are listed in alphabetical order</p -->
		<ul>
			<li><a href="http://www.dragora.org">Dragora GNU/Linux</a></li>
			<li><a href="http://dynebolic.org/">Dynebolic GNU/Linux</a></li>
			<li><a href="http://www.musix.org.ar/">Musix GNU+Linux</a></li>
			<li><a href="https://parabolagnulinux.org/">Parabola GNU/Linux</a></li>
			<li><a href="http://trisquel.info/">Trisquel GNU/Linux</a></li>
			<li><a href="http://www.ututo.org/www/">Ututo GNU/Linux</a></li>
			<li><a href="http://venenux.org/">Venenux GNU/Linux</a></li>
		</ul>
	</dl>
	</div>
	
