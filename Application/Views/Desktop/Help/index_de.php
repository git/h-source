<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010, 2011 Antonio Gallo (h-source-copyright.txt)
// Copyright (C) 2011 Joerg Kohne
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

<div class="help_external_box">

	<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Startseite</a> &raquo; <?php echo gtext("Help");?>
	</div>

	<div class="help_tables_of_contents">
		Inhalt
		<ul>
			<li><a href="<?php echo $this->currPage."/$lang#wiki-syntax";?>">Wiki-Syntax</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#compatibility";?>">Kompatibilitätsklassen</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#discover-hardware";?>">Entdecken Sie Ihre Hardware</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#fully-free";?>">Vollständig Freie GNU/Linux-Distributionen</a></li>
		</ul>
	</div>
	
	<a name="wiki-syntax"></a><h2>Wiki-Syntax</h2>
	
	<h3><?php echo Website::$generalName;?>
	Wiki-Elemente</h3>
	
	<table class="help_wiki_table">
		<thead>
			<tr>
				<th>Name</th>
				<th>Element</th>
				<th>Ergebnis</th>
				<th>Beschreibung</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Fett</td>
				<td>[b]Fette[/b] Schrift</td>
				<td><b>Fette</b> Schrift</td>
				<td>Fettschrift</td>
			</tr>
			<tr>
				<td>Kursiv</td>
				<td>[i]Kursive[/i] Schrift</td>
				<td><i>Kursive</i> Schrift</td>
				<td>Kursivschrift</td>
			</tr>
			<tr>
				<td>Gelöschter Text</td>
				<td>[del]Gelöschter[/del] Text</td>
				<td><del>Gelöschter</del> Text</td>
				<td>Gelöschter Text</td>
			</tr>
			<tr>
				<td>Unterstrichen</td>
				<td>[u]Unterstrichener[/u] Text</td>
				<td><u>Unterstrichener</u> Text</td>
				<td>Unterstrichener Text</td>
			</tr>
			<tr>
				<td>Überschrift 1</td>
				<td>[h1]Überschrift[/h1]</td>
				<td>
					<div class="div_h1">
					Überschrift</div>
				</td>
				<td>Überschrift Ebene 1</td>
			</tr>
			<tr>
				<td>Überschrift 2</td>
				<td>[h2]Überschrift[/h2]</td>
				<td>
					<div class="div_h2">
					Überschrift</div>
				</td>
				<td>Überschrift Ebene 2</td>
			</tr>
			<tr>
				<td>Überschrift 3</td>
				<td>[h3]Überschrift[/h3]</td>
				<td>
					<div class="div_h3">
					Überschrift</div>
				</td>
				<td>Überschrift Ebene 3</td>
			</tr>
			<tr>
				<td>Absatz</td>
				<td>…[p](Neuer) Absatz[/p]</td>
				<td>… 
	
					<p>(Neuer) Absatz</p>
				</td>
				<td>(Neuer) Absatz</td>
			</tr>
			<tr>
				<td>Auflistung</td>
				<td><ul style="list-style-type:none;">
						<li>[list]</li>
						<li><ul style="list-style-type:none;">
								<li>[*]Erstes Element[/*]</li>
								<li>[*]Zweites Element[/*]</li>
							</ul>
						</li>
						<li>[/list]</li>
					</ul>
				</td>
				<td><ul>
						<li>Erstes Element</li>
						<li>Zweites Element</li>
					</ul>
				</td>
				<td>Auflistung</td>
			</tr>
			<tr>
				<td>Nummerierung</td>
				<td><ul style="list-style-type:none;">
						<li>[enum]</li>
						<li><ul style="list-style-type:none;">
								<li>[*]Erstes Element[/*]</li>
								<li>[*]Zweites Element[/*]</li>
							</ul>
						</li>
						<li>[/enum]</li>
					</ul>
				</td>
				<td><ol>
						<li>Erstes Element</li>
						<li>Zweites Element</li>
					</ol>
				</td>
				<td>Geordnete Liste</td>
			</tr>
			<tr>
				<td>Listenelement</td>
				<td>[*]Listenelement[/*]</td>
				<td><ul>
						<li>Listenelement</li>
					</ul>
				</td>
				<td>Listenelement hinzufügen</td>
			</tr>
			<tr>
				<td>Quelltext</td>
				<td>[code]Quelltext[/code]</td>
				<td><pre class="code_pre">Quelltext</pre>
				</td>
				<td>Codefragment</td>
			</tr>
			<tr>
				<td>Verweis (einfach)</td>
				<td>[a]Internetadresse[/a]</td>
				<td><a href="http://Internetadresse">Internetadresse</a></td>
				<td>Verweis (einfach)</td>
			</tr>
			<tr>
				<td>Verweis (mit Text)</td>
				<td>[a]Internetadresse|mit Text[/a]</td>
				<td><a href="http://Internetadresse">mit Text</a></td>
				<td>Verweis (mit Text)</td>
			</tr>
			<tr>
				<td>Notebook</td>
				<td>[notebook]ID[/notebook]</td>
				<td><samp>ID</samp></td>
				<td>Verweis auf das Notebook mit der Kennung gleich ID (die jeweilige Geräte-Modellkennung wird auf der Seite
					des Gerätes selbst, unter „Modellbezeichnung“, eingepflegt)</td>
			</tr>
			<tr>
				<td>WLAN</td>
				<td>[wifi]ID[/wifi]</td>
				<td><samp>ID</samp></td>
				<td>Verweis auf das WLAN mit der Kennung gleich ID (die jeweilige Geräte-Modellkennung wird auf der Seite des
					Gerätes selbst, unter „Modellbezeichnung“, eingepflegt)</td>
			</tr>
			<tr>
				<td>Grafikkarte</td>
				<td>[videocard]ID/videocard]</td>
				<td><samp>ID</samp></td>
				<td>Verweis auf die Grafikkarte mit der Kennung gleich ID (die jeweilige Geräte-Modellkennung wird auf der Seite
					des Gerätes selbst, unter „Modellbezeichnung“, eingepflegt)</td>
			</tr>
		</tbody>
	</table>
	
	<p> </p>
	
	<a name="compatibility"></a><h2>Kompatibilitätsklassen</h2>
	
	<a name="notebook-compatibility"></a><h3>Notebooks</h3>
	<dl title="Notebook-Kompatibilität">
		<dt class="label">Klasse A (Platin)</dt>
			<dd>Alle Notebook-Geräte arbeiten mit sehr guter Leistung.</dd>
			<dd class="sample"><strong>Beispiel</strong>: Alle Geräte funktionieren, auch die 3D-Beschleunigung wird
				unterstützt.</dd>
		<dt>Klasse B (Gold)</dt>
			<dd>Alle Notebook-Geräte funktionieren, jedoch nicht mit voller Leistung. </dd>
			<dd class="sample"><strong>Beispiel</strong>: Alle Geräte funktionieren, aber die 3D-Beschleunigung wird nicht
				unterstützt.</dd>
		<dt>Klasse C (Silber)</dt>
			<dd>Ein wichtiges Gerät wird nicht unterstützt.</dd>
			<dd class="sample"><strong>Beispiel</strong>: Die interne WLAN-Karte funktioniert nicht. Sie benötigen eine
				externe USB-Karte.</dd>
		<dt>Klasse D (Bronze)</dt>
			<dd>Mehr als ein Gerät wird nicht unterstützt.</dd>
		<dt>Klasse E (E-Schrott)</dt>
			<dd>Das Notebook kann von Freie Software nicht unterstützt werden.</dd>
	</dl>
	
	<a name="printer-compatibility"></a><h3>Drucker</h3>
	<dl title="Drucker-Kompatibilität">
		<dt>Klasse A (Vollständig)</dt>
			<dd>Alle Gerätefunktionen und -merkmale werden unterstützt.</dd>
		<dt>Klasse B (Teilweise)</dt>
			<dd>Drucken wird unterstützt, aber möglicherweise mit eingeschränkter Geschwindigkeit oder Druckqualität;
				Scannen und/oder Faxen wird (bei einigen Multifunktionsgeräten möglicherweise) nicht unterstützt</dd>
		<dt>Klasse C (E-Schrott)</dt>
			<dd>Der Drucker kann von Freie Software nicht unterstützt werden.</dd>
	</dl>
	
	<a name="scanner-compatibility"></a><h3>Scanner</h3>
	<dl title="Scanner-Kompatibilität">
		<dt>Klasse A (Vollständig)</dt>
			<dd>Alle Gerätefunktionen und -merkmale werden unterstützt.</dd>
		<dt>Klasse B (Teilweise)</dt>
			<dd>Scannen unterstützt, aber möglicherweise bei eingeschränkter Geschwindigkeit oder Qualität, einige andere
				Funktionen werden nicht unterstützt.</dd>
		<dt>Klasse C (E-Schrott)</dt>
			<dd>Der Scanner kann von Freie Software nicht unterstützt werden.</dd>
	</dl>
	
	<p> </p>
	
	<a name="discover-hardware"></a><h2>Entdecken Sie Ihre Hardware</h2>
	<cite>(Vielen Dank <a href="<?php echo $this->baseUrl;?>/issues/view/en/3/1/token">lluvia</a>)</cite> 
	
	<p>Um mehr Details über Ihre Hardware zu erfahren, beachten Sie bitte folgende Punkte:</p>
	<dl>
		<dt>Wie man die Modellbezeichnung des Notebooks herausfindet</dt>
			<dd>Siehe das Typenschild unterhalb Ihres tragbaren Klapprechners.</dd>
		<a name="model-name"></a><dt>Wie man den Modellnamen der Geräte herausfindet (wenn kein tragbarer Klapprechner)</dt>
			<dd class="opt">Wenn das Gerät eingebaut ist (z. B. eine Grafikkarte)</dd>
			<dd>Öffnen Sie ein Terminal („Eingabeaufforderung“) und geben Sie folgenden Befehl ein:</dd>
			<dd><pre class="terminal">lspci</pre>
			</dd>
			<dd>oder</dd>
			<dd><pre class="terminal">lspci &gt; DATEINAME          # Ausgabe als Datei speichern</pre>
			</dd>
		<!--<dd>where "filename" is the name of the file</dd>-->
			<dd>Es sollten PCI-Geräte ähnlich der folgenden angezeigt werden:</dd>
			<dd><pre class="terminal">
00:18.3 Host bridge: <b>Advanced Micro Devices [AMD] K8 [Athlon64/Opteron] Miscellaneous Control</b>
03:00.0 Network controller: <b>Broadcom Corporation BCM4311 802.11b/g WLAN (rev 02)</b>
05:00.0 VGA compatible controller: <b>nVidia Corporation G86 [GeForce 8400M GS] (rev a1)</b></pre>
			</dd>
			<dd class="note"><b>Hinweis:</b> The name of each device is written after the colon (see the text
				in bold in the above list).</dd>
			<dd class="opt">Wenn das Gerät ein USB-Gerät ist (bspw. ein externer WLAN-Stick)</dd>
			<dd>Öffnen Sie ein Terminal („Eingabeaufforderung“) und geben Sie folgenden Befehl ein:</dd>
			<dd><pre class="terminal">lsusb -v</pre>
			</dd>
			<dd>oder</dd>
			<dd><pre class="terminal">lsusb -v &gt; DATEINAME          # Ausgabe als Datei speichern</pre>
			</dd>
			<!-- dd>where "filename" is the name of the file.</dd -->
			<dd>Es sollten USB-Geräte ähnlich der folgenden angezeigt werden:</dd>
			<dd><pre class="terminal">
Bus 001 Device 002: ID 0846:4260 NetGear, Inc. WG111v3 54 Mbps Wireless [realtek RTL8187B]
Device Descriptor:
	bLength						18
	bDescriptorType				1
	bcdUSB						2.00
	bDeviceClass				0 (Defined at Interface level)
	bDeviceSubClass				0
	bDeviceProtocol				0
	bMaxPacketSize0				64
	idVendor					0x0846 NetGear, Inc.
	idProduct					0x4260 <b>WG111v3 54 Mbps Wireless [realtek RTL8187B]</b>
	bcdDevice					2.00
	iManufacturer				1
	iProduct					2
	iSerial						3
	...
	...

Bus 002 Device 003: ID 08ff:2580 AuthenTec, Inc. AES2501 Fingerprint Sensor
Device Descriptor:
	bLength						18
	bDescriptorType				1
	bcdUSB						1.10
	bDeviceClass				255 Vendor Specific Class
	bDeviceSubClass				255 Vendor Specific Subclass
	bDeviceProtocol				255 Vendor Specific Protocol
	bMaxPacketSize0				8
	idVendor					0x08ff AuthenTec, Inc.
	idProduct					0x2580 <b>AES2501 Fingerprint Sensor</b>
	bcdDevice					6.23
	iManufacturer				0
	iProduct					1 Fingerprint Sensor
	iSerial						0
	bNumConfigurations			1
	...
	...
			</pre>
			</dd>
			<dd class="note"><b>Hinweis:</b> Die Gerätebezeichnung wird in der Zeile „idProduct“ nach dem Doppelpunkt
				angegeben (siehe Text in Fettdruck).</dd>
		<!-- <dt>Wie man das Jahr der Vermarktung des Notebooks herausfindet</dt>
		<dd>Öffnen Sie ein Terminal („Eingabeaufforderung“) und geben Sie folgenden Befehl ein:</dd>
		<dd><pre class="terminal">sudo dmidecode| grep "Release Date"</pre></dd>
		<dd class="sample">Es sollte ein Datum ähnlich dem <code>Release Date: 05/28/2011</code> (MM/TT/JJJJ) angezeigt werden.</dd -->
		<dt>Wie man den verwendeten Betriebssystemkern („Kernel“) herausfindet</dt>
			<dd>Öffnen Sie ein Terminal („Eingabeaufforderung“) und geben Sie folgenden Befehl ein:</dd>
			<dd><pre class="terminal">uname -r</pre>
			</dd>
		<dt name="video-card">Wie man den Namen der Grafikkarte herausfindet</dt>
			<dd>Öffnen Sie ein Terminal („Eingabeaufforderung“) und geben Sie folgenden Befehl ein:</dd>
			<dd><pre class="terminal">sudo lspci</pre>
			</dd>
			<dd>Suchen Sie dann nach der Zeile mit der Zeichenfolge <strong><code>VGA</code></strong> oder
				<strong><code>Display Controller</code></strong>. Alternativ können Sie auch folgenden Befehl versuchen:
			</dd>
			<dd><pre class="terminal">lspci | grep "Display controller"</pre>
			</dd>
			<dd>oder</dd>
			<dd><pre class="terminal">lspci | grep "VGA"</pre>
			</dd>
		<a name="vendoridproductid"></a><dt>Wie man Anbieter- und Produkt-ID des Geräts herausfindet (VendorID:ProductID)</dt>
			<dd><cite>(Vielen Dank an <a
				href="http://trisquel.info/de/forum/h-nodecom-new-website-hardware-database#comment-5839">Michał Masłowski</a>
				und <a href="http://trisquel.info/de/forum/h-nodecom-new-website-hardware-database#comment-5837">Julius22</a>)
				</cite></dd>
			<dd class="opt">Wenn das Gerät eingebaut ist (z. B. eine Grafikkarte)</dd>
			<dd>Öffnen Sie ein Terminal („Eingabeaufforderung“) und geben Sie folgenden Befehl ein:</dd>
			<dd><pre class="terminal">sudo lspci -nnk</pre>
			</dd>
			<dd>Es sollte eine Hardwareliste ähnlich der folgenden angezeigt werden:</dd>
			<dd><pre class="terminal">
03:00.0 Network controller [0280]: Broadcom Corporation BCM4311 802.11b/g WLAN [<b>14e4:4311</b>] (rev 02)
	Kernel driver in use: b43-pci-bridge
	Kernel modules: ssb
05:00.0 VGA compatible controller [0300]: nVidia Corporation G86 [GeForce 8400M GS] [<b>10de:0427</b>] (rev a1)
	Kernel modules: nouveau, nvidiafb</pre>
			</dd>
			<dd class="note"><b>Hinweis:</b> Die Zeichenfolgen in <b>fetter Schrift</b> und in den eckigen Klammern […
				<strong>:</strong> …] sind die Daten, nach denen Sie gesucht haben. Die erste Zeichengruppe (vor dem
				Doppelpunkt) ist die <b>Anbieter-ID</b>, die zweite Zeichengruppe (nach dem Doppelpunkt) die <b>Produkt-ID</b>.
				Im obigen Beispiel wären Anbieter- und Produkt-ID-Code der WLAN-Karte (beachten Sie die Zeichenfolgen „Network
				Controller“ und „WLAN“) <b>14e4:4311</b>, die der Grafikkarte (beachten Sie die Zeichenfolge „VGA“)
				<b>10DE:0427</b>.</dd>
			<dd class="opt">Wenn das Gerät ein USB-Gerät ist (bspw. ein externer USB-WLAN-Stick)</dd>
			<dd>Öffnen Sie ein Terminal („Eingabeaufforderung“) und geben Sie folgenden Befehl ein:</dd>
			<dd><pre class="terminal">sudo lsusb</pre>
			</dd>
			<dd>Es sollte eine Hardwareliste ähnlich der folgenden angezeigt werden:</dd>
			<dd><pre class="terminal">Bus 001 Device 002: ID <b>0846:4260</b> NetGear, Inc. WG111v3 54 Mbps Wireless [realtek RTL8187B]<br>Bus 001 Device 001: ID <b>1d6b:0002</b> Linux Foundation 2.0 root hub<br>Bus 002 Device 003: ID <b>08ff:2580</b> AuthenTec, Inc. AES2501 Fingerprint Sensor<br></pre>
			</dd>
			<dd class="note"><b>Hinweis:</b> Die Zeichenfolgen in <b>fetter Schrift</b> und in den eckigen Klammern […] sind
				die Daten, nach denen Sie gesucht haben. Die erste Zeichengruppe (vor dem Doppelpunkt) ist die
				<b>Anbieter-ID</b>, die zweite Zeichengruppe (nach dem Doppelpunkt) die <b>Produkt-ID</b>. Im obigen Beispiel
				wären Anbieter- und Produkt-ID-Code des externen USB-WLAN-Sticks (beachten Sie die Zeichenfolge „Wireless“)
				<b>0846:4260</b>.</dd>
		<dt name="vga">Wie man herausfindet, ob die Grafikkarte (über den VGA-Standard hinaus) funktioniert</dt>
			<dd>Installieren Sie das Paket <code><a href="http://rss-glx.sourceforge.net/">rss-glx</a></code> mittels
				Paketverwaltung Ihrer Distribution oder Kompilieren Sie den Quellcode und testen einige Bildschirmschoner (z. B.
				<b>Skyrocket</b> oder <b>Solarwinds</b>). Überprüfen Sie, ob die Bildschirmschoner angezeigt werden können
				(und/oder ruckelfrei wiedergegeben werden).
			</dd>
		<dt>Wie man herausfindet, ob die 3D-Beschleunigung funktioniert</dt>
			<dd>Versuchen Sie, „compiz“ zu aktivieren.</dd>
		<dt>Wie man den Namen Ihrer WLAN-Karte herausfindet</dt>
			<dd>Öffnen Sie ein Terminal („Eingabeaufforderung“) und geben Sie folgenden Befehl ein:</dd>
			<dd><pre class="terminal">sudo lspci</pre>
			</dd>
			<dd>Suchen Sie dann nach der Zeile mit der Zeichenfolge <b><samp>Wireless</samp></b> oder <b><samp>Network
				Controller</samp></b>. Alternativ können Sie auch folgenden Befehle versuchen:
			</dd>
			<dd><pre class="terminal">lspci | grep "Wireless"</pre></dd>
			<dd>oder</dd>
			<dd><pre class="terminal">lspci | grep "Network"</pre>
			</dd>
		<dt>Wie man den verwendeten Druckertreiber herausfindet</dt>
			<dd class="opt">Wenn Sie „cups“ verwenden</dd>
			<dd>Öffnen Sie ein Terminal („Eingabeaufforderung“) und geben Sie folgenden Befehl ein:</dd>
			<dd><pre class="terminal">dpkg-query -W -f '${Version}\n' cups</pre>
			</dd>
		<dt>How to discover the architecture of your notebook</dt>
			<dd>Open a terminal and type the following command:</dd>
			<dd><pre class="terminal">cat /proc/cpuinfo | grep "lm"</pre>
			</dd>
			<dd>If you get a message like this:</dd>
			<dd><pre class="terminal">flags		: fpu vme de pse tsc msr pae mce cx8 apic mtrr pge mca cmov pat pse36 clflush dts<br/>acpi mmx fxsr sse sse2 ss ht tm pbe nx lm constant_tsc arch_perfmon pebs bts aperfmperf pni dtes64<br/>monitor ds_cpl est tm2 ssse3 cx16 xtpr pdcm lahf_lm</pre>
			</dd>
			<dd>then you machine is x86-64/amd64 capable and you could choose a x86-64/amd64 distro to run on it</dd>
	</dl>
	
	<p> </p>
	
	<a name="fully-free"></a><h2>Freie GNU/Linux-Distributionen</h2>

	<p>Dies sind <a href="http://www.gnu.org/gnu/linux-and-gnu.de.html">GNU/Linux</a>-Distributionen, von denen wir von einer
	festgelegten Richtlinie wissen, ausschließlich Freie Software zu verwenden und anzubieten. Unfreie Anwendungen,
	unfreie Programmierplattformen, unfreie Treiber oder unfreie Firmware („BLOBs“) werden, auch wenn versehentlich
	enthalten, entfernt. Weitere Informationen über <a href="http://www.gnu.org/distros/free-distros.html">Freie
	GNU/Linux-Distribution</a> finden Sie unter <a 
	href="http://www.gnu.org/distros/free-system-distribution-guidelines.html">Richtlinien für Freie
	Distributionen</a>.</p>
	
	<p><strong>Alle Distributionen können auf der Festplatte Ihres Rechners installiert und die meisten Live ausgeführt
	werden.</strong></p>
	
	<p>(In alphabetischer Reihenfolge)</p>
	
	<ul>
		<li><a href="http://www.dragora.org">Dragora GNU/Linux</a></li>
		<li><a href="http://dynebolic.org/">Dynebolic GNU/Linux</a></li>
		<li><a href="http://www.musix.org.ar/">Musix GNU+Linux</a></li>
		<li><a href="https://parabolagnulinux.org/">Parabola GNU/Linux</a></li>
		<li><a href="http://trisquel.info/">Trisquel GNU/Linux</a></li>
		<li><a href="http://www.ututo.org/www/">Ututo GNU/Linux</a></li>
		<li><a href="http://venenux.org/">Venenux GNU/Linux</a></li>
	</ul>	
	</div>
	
