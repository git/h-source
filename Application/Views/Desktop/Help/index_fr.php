<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010, 2011 Antonio Gallo (h-source-copyright.txt)
// Copyright (C) 2011 Joerg Kohne
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

<div class="help_external_box">

	<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo gtext("Help");?>
	</div>

	<div class="help_tables_of_contents">
		Sommaire
		<ul>
			<li><a href="<?php echo $this->currPage."/$lang#wiki-syntax";?>">Syntaxe Wiki</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#compatibility";?>">Niveau de compatibilitée</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#discover-hardware";?>">Découvrez votre matériel</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#fully-free";?>">Liste de distribution GNU/Linux entièrement libre</a></li>
		</ul>
	</div>
	
	<a name="wiki-syntax"></a><h2>Syntaxe Wiki</h2>
	
	<h3>Liste des tags Wiki sur <?php echo Website::$generalName;?>
	</h3>
	
	<table class="help_wiki_table" width="100%" border="1">
		<thead>
			<tr>
				<th>name</th>
				<th>tag</th>
				<th>result</th>
				<th width="40%">description</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>bold</td>
				<td>[b]text[/b]</td>
				<td><b>text</b></td>
				<td>text bold</td>
			</tr>
			<tr>
				<td>italic</td>
				<td>[i]text[/i]</td>
				<td><i>text</i></td>
				<td>text italic</td>
			</tr>
			<tr>
				<td>del</td>
				<td>[del]text[/del]</td>
				<td><del>text</del></td>
				<td>text deleted</td>
			</tr>
			<tr>
				<td>underline</td>
				<td>[u]text[/u]</td>
				<td><u>text</u></td>
				<td>text underlined</td>
			</tr>
			<tr>
				<td>head 1</td>
				<td>[h1]text[/h1]</td>
				<td>
					<div class="div_h1">
					text</div>
				</td>
				<td>head 1</td>
			</tr>
			<tr>
				<td>head 2</td>
				<td>[h2]text[/h2]</td>
				<td>
					<div class="div_h2">
					text</div>
				</td>
				<td>head 2</td>
			</tr>
			<tr>
				<td>head 3</td>
				<td>[h3]text[/h3]</td>
				<td>
					<div class="div_h3">
					text</div>
				</td>
				<td>head 3</td>
			</tr>
			<tr>
				<td>paragraph</td>
				<td>[p]text[/p]</td>
				<td><p>text</p>
				</td>
				<td>new paragraph</td>
			</tr>
			<tr>
				<td>list item</td>
				<td>[*]item[/*]</td>
				<td><ul>
						<li>item</li>
					</ul>
				</td>
				<td>ad an item to a list</td>
			</tr>
			<tr>
				<td>List </td>
				<td><ul style="list-style-type:none;">
						<li>[list]</li>
						<li><ul style="list-style-type:none;">
								<li>[*]first item[/*]</li>
								<li>[*]second item[/*]</li>
							</ul>
						</li>
						<li>[/list]</li>
					</ul>
				</td>
				<td><ul>
						<li>first item</li>
						<li>second item</li>
					</ul>
				</td>
				<td>make a list of items</td>
			</tr>
			<tr>
				<td>Numbered list </td>
				<td><ul style="list-style-type:none;">
						<li>[enum]</li>
						<li><ul style="list-style-type:none;">
								<li>[*]first item[/*]</li>
								<li>[*]second item[/*]</li>
							</ul>
						</li>
						<li>[/enum]</li>
					</ul>
				</td>
				<td><ol>
						<li>first item</li>
						<li>second item</li>
					</ol>
				</td>
				<td>make a numbered list of items</td>
			</tr>
			<tr>
				<td>code</td>
				<td>[code]some code[/code]</td>
				<td><pre class="code_pre">some code</pre>
				</td>
				<td>ad some code</td>
			</tr>
			<tr>
				<td>simple link</td>
				<td>[a]url[/a]</td>
				<td><a href="url">url</a></td>
				<td>simple link</td>
			</tr>
			<tr>
				<td>link with text</td>
				<td>[a]url|text[/a]</td>
				<td><a href="url">text</a></td>
				<td>link with text</td>
			</tr>
			<tr>
				<td>notebook</td>
				<td>[notebook]id[/notebook]</td>
				<td> </td>
				<td>link to the notebook with the identifier equal to id (the identifier of each device model is written in the
					page of the device itself, next to the model name)</td>
			</tr>
			<tr>
				<td>wifi</td>
				<td>[wifi]id[/wifi]</td>
				<td> </td>
				<td>link to the wifi with the identifier equal to id (the identifier of each device model is written in the page
					of the device itself, next to the model name)</td>
			</tr>
			<tr>
				<td>videocard</td>
				<td>[videocard]id[/videocard]</td>
				<td> </td>
				<td>link to the videocard with the identifier equal to id (the identifier of each device model is written in the
					page of the device itself, next to the model name)</td>
			</tr>
		</tbody>
	</table>
	
	<p> </p>
	
	<a name="compatibility"></a><h2>Niveau de compatibilitée</h2>
	
	<a name="notebook-compatibility"></a><h3>Notebooks</h3>
	<dl>
		<dt>Classe A (Platinium)</dt>
			<dd>Tout le matériel du notebook fonctionne avec du logiciel libre. </dd>
			<dd class="example">Exemple : la 3D fonctionne, le son et la wifi également</dd>
		<dt>Classe B (Or)</dt>
			<dd>Tout le matériel du notebook fonctionne, mais avec des performances réduites. </dd>
			<dd class="example">Exemple : la carte graphique est reconnues mais la 3D ne fonctionne pas</dd>
		<dt>Classe C (Argent)</dt>
			<dd>Un matériel principal ne fonctionne pas. </dd>
			<dd class="example">Exemple : La carte wifi ne fonctionne pas</dd>
		<dt>Classe D (Bronze)</dt>
			<dd>Plus d'un matériel ne fonctionne pas avec du logiciel libre</dd>
		<dt>Classe E (Poubelle)</dt>
			<dd>Aucun matériel ne fonctionne avec du logiciel libre</dd>
	</dl>
	
	<a name="printer-compatibility"></a><h3>Imprimantes</h3>
	<dl>
		<dt>Classe A (Complet)</dt>
			<dd>Toutes les fonctionnalitées de l'imprimante fonctionne avec du logiciel libre</dd>
		<dt>Classe B (Partielle)</dt>
			<dd>La fonction d'impression fonctionne, mais à une vitesse ou qualitée limitée. </dd>
			<dd class="example">Exemple : Le scan ou le fax sur certains appareils peut ne pas être supportés</dd>
		<dt>Classe C (Aucun)</dt>
			<dd>L'imprimante ne fonctionne pas avec du logiciel libre</dd>
	</dl>
	
	<a name="scanner-compatibility"></a><h3>Scanners</h3>
	<dl>
		<dt>Classe A (Complet)</dt>
			<dd>Toutes les fonctionnalitées du scanner sont supportées</dd>
		<dt>Classe B (Partielle)</dt>
			<dd>Le scanner fonctionne mais à une vitesse ou qualitée limitée, d'autres fonctionnalitées peuvent ne pas
				fonctionner</dd>
		<dt>Classe C (Aucun)</dt>
			<dd>Le scanner ne fonctionne pas avec du logiciel libre</dd>
	</dl>
	
	<a name="discover-hardware"></a><h2>Découvrez votre matériel</h2>
	<cite>(Merci <a href="<?php echo $this->baseUrl;?>/issues/view/en/3/1/token">lluvia</a>) </cite> 
	
	<p>Pour connaitre votre matériel en détails, vous pouviez faire les choses suivantes:</p>
	<dl>
		<dt>Comment découvrir le modèle de votre notebook</dt>
			<dd>See below your notebook or netbook</dd>
		<a name="model-name"></a><dt>How to discover the model name of your device (if it is not a notebook)</dt>
			<dd class="opt">If the device is integrated (example: a video card)</dd>
			<dd>Open a terminal and type the following command:</dd>
			<dd><pre class="terminal">lspci</pre>
			</dd>
			<dd>or</dd>
			<dd><pre class="terminal">lspci &gt; FILENAME          # output to a file</pre>
			</dd>
			<!-- dd>where "filename" is the name of the file</dd -->
			<dd>You will obtain the list of your PCI devices, similar to the one written below</dd>
			<dd><pre class="terminal">
00:18.3 Host bridge: <b>Advanced Micro Devices [AMD] K8 [Athlon64/Opteron] Miscellaneous Control</b>
03:00.0 Network controller: <b>Broadcom Corporation BCM4311 802.11b/g WLAN (rev 02)</b>
05:00.0 VGA compatible controller: <b>nVidia Corporation G86 [GeForce 8400M GS] (rev a1)</b>
			</pre>
			</dd>
			<dd>The name of each device is written after the colon (see the text in bold in the above
			list)</dd>
			<dd class="opt">If the device is an USB device (example: an external USB wifi card)</dd>
			<dd>Open a terminal and type the following command:</dd>
			<dd><pre class="terminal">lsusb -v</pre>
			</dd>
			<dd>or</dd>
			<dd><pre class="terminal">lsusb -v &gt; FILENAME          # output to a file</pre>
			</dd>
			<!-- dd>where "filename" is the name of the file</dd -->
			<dd>You will obtain the list of your USB devices, similar to the one written below</dd>
			<dd><pre class="terminal">
Bus 001 Device 002: ID 0846:4260 NetGear, Inc. WG111v3 54 Mbps Wireless [realtek RTL8187B]
Device Descriptor:
	bLength						18
	bDescriptorType				1
	bcdUSB						2.00
	bDeviceClass				0 (Defined at Interface level)
	bDeviceSubClass				0
	bDeviceProtocol				0
	bMaxPacketSize0				64
	idVendor					0x0846 NetGear, Inc.
	idProduct					0x4260 <b>WG111v3 54 Mbps Wireless [realtek RTL8187B]</b>
	bcdDevice					2.00
	iManufacturer				1
	iProduct					2
	iSerial						3
	...
	...

Bus 002 Device 003: ID 08ff:2580 AuthenTec, Inc. AES2501 Fingerprint Sensor
Device Descriptor:
	bLength						18
	bDescriptorType				1
	bcdUSB						1.10
	bDeviceClass				255 Vendor Specific Class
	bDeviceSubClass				255 Vendor Specific Subclass
	bDeviceProtocol			 	255 Vendor Specific Protocol
	bMaxPacketSize0				8
	idVendor					0x08ff AuthenTec, Inc.
	idProduct					0x2580 <b>AES2501 Fingerprint Sensor</b>
	bcdDevice					6.23
	iManufacturer				0
	iProduct					1 Fingerprint Sensor
	iSerial						0
	bNumConfigurations			1
	...
	...
			</pre>
			</dd>
			<dd>The name of each device is written at the row starting with "idProduct" (see the text in bold in the above
			list)</dd>
		<dt>Trouver la version du noyau linux libre que vous utilisez</dt>
			<dd>Ouvrez un terminal et tapez la commande suivant:</dd>
			<dd><pre class="terminal">uname -r</pre>
			</dd>
		<dt>Comment trouver le modèle de votre carte graphique</dt>
			<dd>Ouvrez un terminal et taper la commande suivante:</dd>
			<dd><pre class="terminal">sudo lspci</pre>
			</dd>
			<dd>Chercher la ligne qui contient le mot <b>VGA</b> ou <b>Display Controller</b>. Vous pouvez aussi essayer l'une
				des commandes suivantes:</dd>
			<dd><pre class="terminal">lspci | grep "Display controller"</pre>
			</dd>
			<dd>ou</dd>
			<dd><pre class="terminal">lspci | grep "VGA"</pre>
			</dd>
		<a name="vendoridproductid"></a><dt>Comment connaitre le VendorID ou le ProductID (VendorID:ProductID code)</dt>
			<dd><cite>(Merci à <a
				href="http://trisquel.info/en/forum/h-nodecom-new-website-hardware-database#comment-5839">Michał Masłowski</a>
				et <a
				href="http://trisquel.info/en/forum/h-nodecom-new-website-hardware-database#comment-5837">Julius22</a>)</cite>
			</dd>
			<dd class="opt">Si le matériel est intégré (example : une puce vidéo)</dd>
			<dd>Ouvrez un terminal sudo et taper la commande suivante:</dd>
			<dd><pre class="terminal">sudo lspci -nnk</pre>
			</dd>
			<dd>Vous devriez obtenir une liste de matériel similaire à celle-ci</dd>
			<dd><pre class="terminal">
03:00.0 Network controller [0280]: Broadcom Corporation BCM4311 802.11b/g WLAN [<b>14e4:4311</b>] (rev 02)
	Kernel driver in use: b43-pci-bridge
	Kernel modules: ssb
05:00.0 VGA compatible controller [0300]: nVidia Corporation G86 [GeForce 8400M GS] [<b>10de:0427</b>] (rev a1)
	Kernel modules: nouveau, nvidiafb</pre>
			</dd>
			<dd class="note"><span class="note">Note :</span>Les lignes en <b>gras</b> et placée entre crochet (dans la liste
				ci-dessus) sont les lignes que vous recherchez. Le premier packet de numéros (avant la virgule) sont le
				<b>VendorID</b>, le second sont le <b>ProductID</b>. Dans l’exemple ci dessus, le code VendorID:ProductID de la
				carte wifi ( vous pouvez la remarquez grace aux mots "Network Controller" et "WLAN" ) est <b>14e4:4311</b></dd>
			<dd class="opt">Si le périphérique est un périphérique USB : (exemple : une clé usb wifi)</dd>
			<dd>Ouvrez un terminal et tapez:</dd>
			<dd><pre class="terminal">sudo lsusb</pre>
			</dd>
			<dd>Vous devriez obtenir une liste de matériel similaire à celle ci</dd>
			<dd><pre class="terminal">
Bus 001 Device 002: ID <b>0846:4260</b> NetGear, Inc. WG111v3 54 Mbps Wireless [realtek RTL8187B]
Bus 001 Device 001: ID <b>1d6b:0002</b> Linux Foundation 2.0 root hub
Bus 002 Device 003: ID <b>08ff:2580</b> AuthenTec, Inc. AES2501 Fingerprint Sensor</pre>
			</dd>
			<dd class="note"><span class="note">Note :</span> Les lignes en <b>gras</b> (dans la liste du dessus) sont les
				lignes que vous recherchez. Les premiers nombres (avant les deux points) sont le <b>VendorID</b>, les autres sont
				le <b>ProductID</b>. Dans l'exemple ci-dessus : le code VendorID:ProductID de la carte usb wifi externe
				(Remarquez la ligne Wireless) est <b>0846:4260</b></dd>
		<dt>Comment savoir si votre carte graphique fonctionne</dt>
			<dd>Installer <code><a href="http://rss-glx.sourceforge.net/">rss-glx</a></code> en utilisant le gestionnaire de
				paquet de votre distribution ou en le compilant depuis les sources et essayez certains écran de veille (par
				exemple <b>Skyrocket</b> ou <b>Solarwinds</b>). Essayer de faire fonctionner le fond d'écran, et/ou le faire
				fonctionner fluidement.</dd>
		<dt>Comment savoir si l'accélération 3D fonctionne</dt>
			<dd>Essayer d’activer compiz</dd>
		<dt>Comment decouvrir le nom de votre carte wifi</dt>
			<dd>Ouvrez un terminal et taper la commande suivante:</dd>
			<dd><pre class="terminal">sudo lspci</pre>
			</dd>
			<dd>Regardez ensuite les lignes <samp><b>Wireless</b></samp> ou <samp><b>Network controller</b></samp>. Vous pouvez
				aussi essayer l'une de ses commandes:</dd>
			<dd><pre class="terminal">lspci | grep "Wireless"</pre>
			</dd>
			<dd>ou</dd>
			<dd><pre class="terminal">lspci | grep "Network"</pre>
			</dd>
		<dt>Comment connaitre le pilote d’imprimante que vous utilisez</dt>
			<dd class="opt">Si vous utilisez cups</dd>
			<dd>Ouvrez un terminal et taper ceci:</dd>
			<dd><pre class="terminal">dpkg-query -W -f '${Version}\n' cups</pre>
			</dd>
		<dt>How to discover the architecture of your notebook</dt>
			<dd>Open a terminal and type the following command:</dd>
			<dd><pre class="terminal">cat /proc/cpuinfo | grep "lm"</pre>
			</dd>
			<dd>If you get a message like this:</dd>
			<dd><pre class="terminal">flags		: fpu vme de pse tsc msr pae mce cx8 apic mtrr pge mca cmov pat pse36 clflush dts<br/>acpi mmx fxsr sse sse2 ss ht tm pbe nx lm constant_tsc arch_perfmon pebs bts aperfmperf pni dtes64<br/>monitor ds_cpl est tm2 ssse3 cx16 xtpr pdcm lahf_lm</pre>
			</dd>
			<dd>then you machine is x86-64/amd64 capable and you could choose a x86-64/amd64 distro to run on it</dd>
	</dl>
	
	<p> </p>
	
	<a name="fully-free"></a><h2>Liste de dristributions GNU/Linux entièrement libre</h2>

	<p>Following are the <a href="/gnu/linux-and-gnu.html">GNU/Linux</a> distributions we know of which have a firm policy
	commitment to only include and only propose free software. They reject non-free applications, non-free programming
	platforms, non-free drivers, or non-free firmware “blobs”. If by mistake they do include any, they remove it. To
	learn more about what makes for a <a href="http://www.gnu.org/distros/free-distros.html">free GNU/Linux
	distribution</a>, see GNU <a href="http://www.gnu.org/distros/free-system-distribution-guidelines.html">Guidelines for
	Free System Distributions</a>.</p>
	
	<p>Toutes les distributions qui suivent sont installables sur un disque dur d'ordinateur et la plupart peuvent être
	exécutées à partir d'un CD.</p>
	
	<p>(Dans l'ordre alphabétique)</p>
	
	<ul>
		<li><a href="http://www.dragora.org">Dragora GNU/Linux</a></li>
		<li><a href="http://dynebolic.org/">Dynebolic GNU/Linux</a></li>
		<li><a href="http://www.musix.org.ar/">Musix GNU+Linux</a></li>
		<li><a href="https://parabolagnulinux.org/">Parabola GNU/Linux</a></li>
		<li><a href="http://trisquel.info/">Trisquel GNU/Linux</a></li>
		<li><a href="http://www.ututo.org/www/">Ututo GNU/Linux</a></li>
		<li><a href="http://venenux.org/">Venenux GNU/Linux</a></li>
	</ul>
	
	</div>
	
