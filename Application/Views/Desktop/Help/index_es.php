<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010, 2011 Antonio Gallo (h-source-copyright.txt)
// Copyright (C) 2011 Joerg Kohne
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

<div class="help_external_box">

	<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Inicio</a> &raquo; <?php echo gtext("Help");?>
	</div>

	<div class="help_tables_of_contents">
		Tabla de contenidos
		<ul>
			<li><a href="<?php echo $this->currPage."/$lang#wiki-syntax";?>">Sintaxis del Wiki</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#compatibility";?>">Clases de Compatibilidad</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#discover-hardware";?>">Descubra su hardware</a></li>
			<li><a href="<?php echo $this->currPage."/$lang#fully-free";?>">Lista de las distribuciones GNU/Linux completamente libres</a></li>
		</ul>
	</div>
	
	<a name="wiki-syntax"></a><h2>Sintaxis del Wiki</h2>
	
	<h3>Lista de las etiquetas wiki de <?php echo Website::$generalName;?>
	</h3>
	
	<table class="wiki">
		<thead>
			<tr>
				<th>nombre</th>
				<th>etiqueta</th>
				<th>resultado</th>
				<th>descripción</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>bold</td>
				<td>[b]texto[/b]</td>
				<td><b>texto</b></td>
				<td>texto en negrita</td>
			</tr>
			<tr>
				<td>italic</td>
				<td>[i]texto[/i]</td>
				<td><i>texto</i></td>
				<td>texto en cursiva</td>
			</tr>
			<tr>
				<td>del</td>
				<td>[del]texto[/del]</td>
				<td><del>texto</del></td>
				<td>texto eliminado</td>
			</tr>
			<tr>
				<td>underline</td>
				<td>[u]texto[/u]</td>
				<td><u>texto</u></td>
				<td>texto subrayado</td>
			</tr>
			<tr>
				<td>head 1</td>
				<td>[h1]texto[/h1]</td>
				<td>
					<div class="div_h1">
					texto</div>
				</td>
				<td>encabezado 1</td>
			</tr>
			<tr>
				<td>head 2</td>
				<td>[h2]texto[/h2]</td>
				<td>
					<div class="div_h2">
					texto</div>
				</td>
				<td>encabezado 2</td>
			</tr>
			<tr>
				<td>head 3</td>
				<td>[h3]texto[/h3]</td>
				<td>
					<div class="div_h3">
					texto</div>
				</td>
				<td>encabezado 3</td>
			</tr>
			<tr>
				<td>paragraph</td>
				<td>[p]texto[/p]</td>
				<td><p>texto</p>
				</td>
				<td>nuevo párrafo</td>
			</tr>
			<tr>
				<td>list item</td>
				<td>[*]objeto[/*]</td>
				<td><ul>
						<li>objeto</li>
					</ul>
				</td>
				<td>agrega un objeto a la lista</td>
			</tr>
			<tr>
				<td>lista </td>
				<td><ul style="list-style-type:none;">
						<li>[list]</li>
						<li><ul style="list-style-type:none;">
								<li>[*]first item[/*]</li>
								<li>[*]second item[/*]</li>
							</ul>
						</li>
						<li>[/list]</li>
					</ul>
				</td>
				<td><ul>
						<li>first item</li>
						<li>second item</li>
					</ul>
				</td>
				<td>hace una lista de objetos</td>
			</tr>
			<tr>
				<td>numbered list</td>
				<td><ul style="list-style-type:none;">
						<li>[enum]</li>
						<li><ul style="list-style-type:none;">
								<li>[*]first item[/*]</li>
								<li>[*]second item[/*]</li>
							</ul>
						</li>
						<li>[/enum]</li>
					</ul>
				</td>
				<td><ol>
						<li>first item</li>
						<li>second item</li>
					</ol>
				</td>
				<td>hace una lista numerada de objetos</td>
			</tr>
			<tr>
				<td>code</td>
				<td>[code]código[/code]</td>
				<td><pre class="code_pre">código</pre>
				</td>
				<td>agrega código</td>
			</tr>
			<!--
			<tr>
			<td>simple link</td>
			<td>[a]url[/a]</td>
			<td>&lt;a href="url"&gt;url&lt;/a&gt;</td>
			<td>enlace simple</td>
			</tr>
			-->
			<tr>
				<td>link with text</td>
				<td>[a]url|text[/a]</td>
				<td><a href="url">text</a></td>
				<td>enlace con texto</td>
			</tr>
			<tr>
				<td>notebook</td>
				<td>[notebook]1234:5678[/notebook]</td>
				<td>1234:5678</td>
				<td>enlace al computador portátil con el identificador id (el identificador de cada modelo de dispositivo esta escrito en la
					página del dispositivo mismo, seguido del nombre del modelo)</td>
			</tr>
			<tr>
				<td>wifi</td>
				<td>[wifi]1234:5678[/wifi]</td>
				<td>1234:5678</td>
				<td>enlace al wifi con el identificador id (el identificador de cada modelo de dispositivo esta escrito en la
					página del dispositivo mismo, seguido del nombre del modelo)</td>
			</tr>
			<tr>
				<td>videocard</td>
				<td>[videocard]1234:5678/videocard]</td>
				<td>1234:5678</td>
				<td>enlace a la tarjeta de video con el identificador id (el identificador de cada modelo de dispositivo esta
					escrito en la página del dispositivo mismo, seguido del nombre del modelo)</td>
			</tr>
		</tbody>
	</table>
	
	<p></p>
	
	<a name="compatibility"></a><h2>Clases de Compatibilidad</h2>
	
	<a name="notebook-compatibility"></a><h3>Computadores portátiles</h3>
	<dl>
		<dt>Clase A (Platino)</dt>
			<dd>Todos los dispositivos funcionan con un buen desempeño. </dd>
			<dd class="example">Ejemplo: todos los dispositivos funcionan, la aceleración 3D esta soportada</dd>
		<dt>Clase B (Oro)</dt>
			<dd>Todos los dispositivos funcionan pero no a su rendimiento completo. </dd>
			<dd class="example">Un ejemplo típico es: todos los dispositivos funcionan, pero la aceleración 3D no esta
				soportada</dd>
		<dt>Clase C (Plata)</dt>
			<dd>Un dispositivo principal no esta soportado. </dd>
			<dd class="example">Ejemplo: la tarjeta inalámbrica interna no funciona. Necesita una tarjeta USB externa</dd>
		<dt>Clase D (Bronce)</dt>
			<dd>Más de un dispositivo no esta soportado</dd>
		<dt>Clase E (Basura)</dt>
			<dd>El equipo no funciona con software libre</dd>
	</dl>
	
	<a name="printer-compatibility"></a><h3>Impresoras</h3>
	<dl>
		<dt>Clase A (Completo)</dt>
			<dd>Todos los dispositivos funcionan y las características soportadas</dd>
		<dt>Clase B (Parcial)</dt>
			<dd>La impresión esta soportada pero a velocidad o calidad limitada; escaneo y/o envío por fax en algunos
				dispositivos multifuncionales pueden no estar soportados</dd>
		<dt>Clase C (Ninguno)</dt>
			<dd>La impresora no funciona con software libre</dd>
	</dl>
	
	<a name="scanner-compatibility"></a><h3>Escáners</h3>
	<dl>
		<dt>Clase A (Completo)</dt>
			<dd>Todos los dispositivos funcionan y las características soportadas</dd>
		<dt>Clase B (Parcial)</dt>
			<dd>El escanéo es soportado pero a velocidad o calidad limitada; otras características pueden no estar soportadas</dd>
		<dt>Clase C (Ninguno)</dt>
			<dd>El escaner no funciona con software libre</dd>
	</dl>
	
	<p> </p>
	
	<a name="discover-hardware"></a><h2>Descubra su hardware</h2>
	<cite>(Gracias <a href="<?php echo $this->baseUrl;?>/issues/view/en/3/1/token">lluvia</a>)</cite> 
	
	<p>En orden de conocer los detalles de su hardware puede seguir las siguientes acciones:</p>
	<dl>
		<dt>Como descubrir el modelo de su computador portátil</dt>
			<dd>Vea debajo de su computador portátil o subportátil</dd>
		<a name="model-name"></a><dt>Como descubrir el nombre del modelo de su dispositivo (si no es un computador portátil)</dt>
			<dd class="opt">Si el dispositivo es integrado (ejemplo: una tarjeta de video)</dd>
			<dd>Abra una terminal y escriba la siguiente orden:</dd>
			<dd><pre class="terminal">lspci</pre>
			</dd>
			<dd>o</dd>
			<dd><pre class="terminal">lspci &gt; FILENAME          # output to a file</pre>
			</dd>
			<!-- dd>where "filename" is the name of the file</dd -->
			<dd>Obtendrá la lista de sus dispositivos PCI, similar a la mostrada debajo</dd>
			<dd><pre class="terminal">
00:18.3 Host bridge: <b>Advanced Micro Devices [AMD] K8 [Athlon64/Opteron] Miscellaneous Control</b>
03:00.0 Network controller: <b>Broadcom Corporation BCM4311 802.11b/g WLAN (rev 02)</b>
05:00.0 VGA compatible controller: <b>nVidia Corporation G86 [GeForce 8400M GS] (rev a1)</b>
			</pre>
			</dd>
			<dd>El nombre de cada dispositivo esta escrito después de los dos puntos (vea el texto en negrita en la lista superior)</dd>
			<dd class="opt">Si el dispositivo es un dispositivo USB (ejemplo: una tarjeta externa USB de red inalámbrica)</dd>
			<dd>Abra una terminal y escriba la siguiente orden:</dd>
			<dd><pre class="terminal">lsusb -v</pre>
			</dd>
			<dd>o</dd>
			<dd><pre class="terminal">lsusb -v &gt; filename          # write the output to a file</pre>
			</dd>
		<!-- dd>where "filename" is the name of the file</dd -->
			<dd>Obtendrá la lista de sus dispositivos USB, similar a la mostrada debajo</dd>
			<dd><pre class="terminal">
Bus 001 Device 002: ID 0846:4260 NetGear, Inc. WG111v3 54 Mbps Wireless [realtek RTL8187B]
Device Descriptor:
	bLength						18
	bDescriptorType				1
	bcdUSB						2.00
	bDeviceClass				0 (Defined at Interface level)
	bDeviceSubClass				0
	bDeviceProtocol				0
	bMaxPacketSize0				64
	idVendor					0x0846 NetGear, Inc.
	idProduct					0x4260 <b>WG111v3 54 Mbps Wireless [realtek RTL8187B]</b>
	bcdDevice					2.00
	iManufacturer				1
	iProduct					2
	iSerial						3
	...
	...

Bus 002 Device 003: ID 08ff:2580 AuthenTec, Inc. AES2501 Fingerprint Sensor
Device Descriptor:
	bLength						18
	bDescriptorType				1
	bcdUSB						1.10
	bDeviceClass				255 Vendor Specific Class
	bDeviceSubClass			 	255 Vendor Specific Subclass
	bDeviceProtocol			 	255 Vendor Specific Protocol
	bMaxPacketSize0				8
	idVendor					0x08ff AuthenTec, Inc.
	idProduct					0x2580 <b>AES2501 Fingerprint Sensor</b>
	bcdDevice					6.23
	iManufacturer				0
	iProduct					1 Fingerprint Sensor
	iSerial						0
	bNumConfigurations			1
	...
	...
		</pre>
			</dd>
			<dd>El nombre de cada disposiitivo es escrito en la fila comenzando con "idProduct" (vea el texto en negritas en la lista superior)</dd>
		<dt>Como descubrir el año de comercialización de su computador portátil</dt>
			<dd>Abra una terminal y escriba la siguiente orden:</dd>
			<dd><pre class="terminal">sudo dmidecode| grep "Release Date"</pre>
			</dd>
		<dt>Como descubrir la versión de kernel que esta usando</dt>
			<dd>Abra una terminal y escriba la siguiente orden:</dd>
			<dd><pre class="terminal">uname -r</pre>
			</dd>
		<dt>Como descubrir el nombre de su tarjeta de video</dt>
			<dd>Abra una terminal y escriba la siguiente orden:</dd>
			<dd><pre class="terminal">sudo lspci</pre>
			</dd>
			<dd>Después busque por la linea que contenga la cadena VGA o Display controller. También puede usar uno de las
				siguientes ordenes:</dd>
			<dd><pre class="terminal">lspci | grep "Display controller"</pre>
			</dd>
			<dd>o</dd>
			<dd><pre class="terminal">lspci | grep "VGA"</pre>
			</dd>
		<a name="vendoridproductid"></a><dt>Como descubrir el ID del vendedor y el ID del producto de su dispositivo (código
		VendorID:ProductID)</dt>
			<dd><cite>(Gracias <a
				href="http://trisquel.info/es/forum/h-nodecom-new-website-hardware-database#comment-5839">Michał Masłowski</a>
				y <a
				href="http://trisquel.info/es/forum/h-nodecom-new-website-hardware-database#comment-5837">Julius22</a>)</cite>
			</dd>
			<dd class="opt">Si el dispositivo es integrado (ejemplo: una tarjeta de video)</dd>
			<dd>Abra una terminal y escriba la siguiente orden:</dd>
			<dd><pre class="terminal">sudo lspci -nnk</pre>
			</dd>
			<dd>Debe de obtener una lista de hardware similar a la escriba debajo</dd>
			<dd><pre class="terminal">
03:00.0 Network controller [0280]: Broadcom Corporation BCM4311 802.11b/g WLAN [<b>14e4:4311</b>] (rev 02)
	Kernel driver in use: b43-pci-bridge
	Kernel modules: ssb
05:00.0 VGA compatible controller [0300]: nVidia Corporation G86 [GeForce 8400M GS] [<b>10de:0427</b>] (rev a1)
	Kernel modules: nouveau, nvidiafb</pre>
			</dd>
			<dd>Las cadenas en <b>negritas</b> y colocadas en los corchetes (en la lista superior) son los códigos que esta
				buscando. El primer grupo de dígitos (antes de los dos puntos) son el <b>VendorID</b>, el segundo grupo de
				dígitos son el <b>ProductID</b>. En el ejemplo superior: el código VendorID:ProductID de la tarjeta
				inalámbrica (note las cadenas "Network controller" y "WLAN") es <b>14e4:4311</b> mientras el código
				VendorID:ProductID de la tarjeta de video (note la cadena "VGA") es <b>10de:0427</b></dd>
			<dd class="opt">Si el dispositivo es un dispositivo USB (ejemplo: una tarjeta USB externa)</dd>
			<dd>Abra una terminal y escriba la siguiente orden:</dd>
			<dd><pre class="terminal">sudo lsusb</pre>
			</dd>
			<dd>Debe de obtener una lista de hardware similar a la descrita a continuación</dd>
			<dd><pre class="terminal">
Bus 001 Device 002: ID <b>0846:4260</b> NetGear, Inc. WG111v3 54 Mbps Wireless [realtek RTL8187B]
Bus 001 Device 001: ID <b>1d6b:0002</b> Linux Foundation 2.0 root hub
Bus 002 Device 003: ID <b>08ff:2580</b> AuthenTec, Inc. AES2501 Fingerprint Sensor</pre>
			</dd>
			<dd>Las cadenas en <b>negritas</b> (en la lista superior) son el código que busca. El primer grupo de dígitos
				(antes de los dos puntos) son el <b>VendorID</b>, el segundo grupo de dígitos son el <b>ProductID</b>. En el
				ejemplo superior: el código VendorID:ProductID de la tarjeta inalámbrica USB externa (note la cadena
				"Wireless") es <b>0846:4260</b></dd>
		<dt>Como descubrir si la tarjeta de video funciona</dt>
			<dd>Instale <code><a href="http://rss-glx.sourceforge.net/">rss-glx</a></code> por lo medios del administrador
				de paquetes de su distribución o por medio de compilar el código fuente y pruebe algunos protectores de
				pantalla (por ejemplo <b>Skyrocket</b> o <b>Solarwinds</b>). Revise si puede ejecutar el protector de pantalla
				(y/o si puede mostrarlo suavemente)
			</dd>
		<dt>Como descubrir si la aceleración 3D funciona</dt>
			<dd>Intente activar compiz</dd>
		<dt>Como descubrir el nombre de su tarjeta de inalámbrica</dt>
			<dd>Abra una terminal y escriba la siguiente orden:</dd>
			<dd><pre class="terminal">sudo lspci</pre>
			</dd>
			<dd>Después busque por la linea que contenga la cadena <samp><b>Wireless</b></samp> o <samp><b>Network
				controller</b></samp>. También puede intentar una de las siguientes ordenes</dd>
			<dd><pre class="terminal">lspci | grep "Wireless"</pre>
			</dd>
			<dd>o</dd>
			<dd><pre class="terminal">lspci | grep "Network"</pre>
			</dd>
		<dt>Como descubrir el driver de la impresora que esta usando</dt>
			<dd class="opt">Si usa cups</dd>
			<dd>Abra una terminal y escriba la orden siguiente:</dd>
			<dd><pre class="terminal">dpkg-query -W -f '${Version}\n' cups</pre>
			</dd>
		<dt>Como descubrir la arquitectura de su computador portátil</dt>
			<dd>Abra una terminal y escriba la orden siguiente:</dd>
			<dd><pre class="terminal">cat /proc/cpuinfo | grep "lm"</pre>
			</dd>
			<dd>Si obtiene un mensaje como este:</dd>
			<dd><pre class="terminal">flags		: fpu vme de pse tsc msr pae mce cx8 apic mtrr pge mca cmov pat pse36 clflush dts<br/>acpi mmx fxsr sse sse2 ss ht tm pbe nx lm constant_tsc arch_perfmon pebs bts aperfmperf pni dtes64<br/>monitor ds_cpl est tm2 ssse3 cx16 xtpr pdcm lahf_lm</pre>
			</dd>
			<dd>entonces su equipo es compatible con x86-64/amd64 y podría elegir una distro x86-64/amd64 para correr en él.</dd>
	</dl>
	
	<p> </p>
	
	<a name="fully-free"></a><h2>Lista de las distribuciones GNU/Linux completamente libres</h2>

	<p>A continuación se enlistan las distribuciones <a href="http://www.gnu.org/gnu/linux-and-gnu.es.html">GNU/Linux</a> que sabemos tiene un 
	compromiso firme en su política para solo incluir y solo proponer software libre. Estas rechazan aplicaciones no libres, plataformas de 
	programación  no libres, controladores no libres, o “blobs” de firmware no libre. Si por error llegan a incluirlo, lo eliminarán. Para 
	aprender más acerca de que hace una <a href="http://www.gnu.org/distros/free-distros.es.html">distribución GNU/Linux libre</a>,
	vea las <a href="http://www.gnu.org/distros/free-system-distribution-guidelines.es.html">pautas para distribuciones de sistemas
	libres</a> de GNU.</p>
	
	<p><strong>Todas las distribuciones que aparecen a continuación se pueden instalar en el disco duro y muchas funcionan
	sin instalación.</strong></p>
	
	<p>(En orden alfabético)</p>
	
	<ul>
		<li><a href="http://www.dragora.org">Dragora GNU/Linux</a></li>
		<li><a href="http://dynebolic.org/">Dynebolic GNU/Linux</a></li>
		<li><a href="http://www.musix.org.ar/">Musix GNU+Linux</a></li>
		<li><a href="https://parabolagnulinux.org/">Parabola GNU/Linux</a></li>
		<li><a href="http://trisquel.info/">Trisquel GNU/Linux</a></li>
		<li><a href="http://www.ututo.org/www/">Ututo GNU/Linux</a></li>
		<li><a href="http://venenux.org/">Venenux GNU/Linux</a></li>
	</ul>
	</div>
	
