<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
	
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/hardware/catalogue/$lang";?>">Hardware</a> &raquo; <?php echo $tree;?>
		</div>
	
		<?php if (strcmp($this->action,'view') === 0) { ?>

			<!--delete the page-->
			<?php if ($isadmin) { ?>
			<div class="moderator_box_deleted clear_right">
				<?php if ($isDeleted) { ?>
				
					<?php if ($isApproved) { ?>
					
						<?php echo gtext('This device page has been hidden'); ?>

						<a id="<?php echo $id_hard;?>" class="deviceshow_device block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/button_ok.png"><?php echo gtext('restore the device page'); ?></a>
						
					<?php } else { ?>
					
						<?php echo gtext("This device page has not been approved yet");?>

						<a id="<?php echo $id_hard;?>" class="deviceapprove_device_app block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/button_ok.png"><?php echo gtext('approve the device page'); ?></a>
						
					<?php } ?>

				<?php } else {	?>

					<a id="<?php echo $id_hard;?>" class="devicehide_device block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/button_cancel.png"><?php echo gtext('hide the device page'); ?></a>

				<?php } ?>

				<a id="<?php echo $id_hard;?>" class="deviceclear_device_cl block_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/clear.png"><?php echo gtext('permanently delete the device page'); ?></a>
				
				<!--view details-->
				<div class="show_hidden_box_ext">
					<div class="md_type">device</div>
					<?php if ($isDeleted) { ?>
					<a class="hidden_message_view_page" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><?php echo gtext("see the page");?></a> |
					<?php } ?>
					<a id="<?php echo $id_hard;?>" class="hidden_message_view_details" href="<?php echo $this->baseUrl."/home/index/$lang";?>">view details</a>
					<div class="details_of_hidden_message">
						<div class="moderation_details_box"></div>
					</div>
				</div>
				
			</div>
			<?php } ?>
			
			<div class="notebook_view_title">
				<?php echo singular($this->controller);?> <b><?php echo $ne_name;?></b>
			</div>
			
			<div class="notebook_insert_link">
				<div class="view_page_back_button">
					<a title="Back to the list of <?php echo plural($this->controller);?>" href="<?php echo $this->baseUrl."/".$this->controller."/catalogue/$lang".$this->viewStatus;?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
				</div>

				<?php if (!$isDeleted) { ?>
				<div class="view_page_history_button">
					<a title="talk page" href="<?php echo $this->baseUrl."/".$this->controller."/talk/$lang/$id_hard/$token".$this->viewStatus;?>"><img class="top_left_note_image" src="<?php echo $this->baseUrl;?>/Public/Img/talk-60.png"></a>
				</div>
				
				<div class="view_page_history_button">
					<a title="history page" href="<?php echo $this->baseUrl."/".$this->controller."/history/$lang/$id_hard".$this->viewStatus;?>"><img class="top_left_note_image" src="<?php echo $this->baseUrl;?>/Public/Img/history-60.png"></a>
				</div>

				<div class="view_page_update_button">
					<form action="<?php echo $this->baseUrl."/".$this->controller."/update/$lang/$token".$this->viewStatus;?>" method="POST">
						<input title="edit page" class="update_submit_class" type="image" src="<?php echo $this->baseUrl;?>/Public/Img/edit-60.png" value="xedit">
						<input type="hidden" name="id_hard" value="<?php echo $id_hard;?>">
					</form>
				</div>
				<?php } ?>
			</div>

			<?php if (!$isDeleted) { ?>
			<div class="talk_numb_ext">
				<a href="<?php echo $this->baseUrl."/".$this->controller."/talk/$lang/$id_hard/$token".$this->viewStatus;?>"><?php echo gtext("talk messages");?>: <?php echo $talk_number;?></a>
			</div>
			<?php } ?>
		
		<?php } else if (strcmp($this->action,'catalogue') === 0) { ?>
		
			<div class="notebook_view_title">
				<?php echo gtext("List of");?> <b><?php echo plural($this->controller);?></b>
			</div>
		
			<div class="notebook_insert_link">
				<a title="Insert a new <?php echo singular($this->controller);?>" href="<?php echo $this->baseUrl."/".$this->controller."/insert/$lang/$token".$this->viewStatus;?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/new-60.png"></a>
			</div>

			<?php echo $topNotice;?>
		
		<?php } else if (strcmp($this->action,'history') === 0) { ?>
		
			<div class="notebook_view_title">
				<?php echo gtext("History");?> - <?php echo singular($this->controller).' <b>'.$ne_name.'</b>';?>
			</div>
			
			<div class="notebook_insert_link">
				<a title="Back to the specifications of the <?php echo singular($this->controller).' '.$name;?>" href="<?php echo $this->baseUrl."/".$this->controller."/view/$lang/$id/$name".$this->viewStatus;?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
			</div>
		
		<?php } else if (strcmp($this->action,'differences') === 0) { ?>

			<?php if ($showDiff === true) { ?>
			<div class="notebook_view_title">
				<?php echo gtext("Differences between the revision of");?> <b><?php echo smartDate($update_new);?></b>, <?php echo gtext("created by");?> <b><?php echo getLinkToUser($u->getUser($updated_by));?></b>, <?php echo gtext("and the revision of");?> <b><?php echo smartDate($update_old);?></b>
			</div>
			<?php } ?>
		
			<div class="notebook_insert_link">
				<a title="Back to the history of the <?php echo singular($this->controller);?> <?php echo $name;?>" href="<?php echo $this->baseUrl."/".$this->controller."/history/$lang/$id_hard".$this->viewStatus;?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
			</div>
	
		<?php } else if (strcmp($this->action,'climb') === 0) { ?>

			<div class="notebook_view_title">
				Make current this revision of the <?php echo singular($this->controller).' <b>'.$ne_name.'</b>';?>
			</div>
			
			<div class="notebook_insert_link">
				<a title="Back to the history of the <?php echo singular($this->controller);?> <?php echo $name;?>" href="<?php echo $this->baseUrl."/".$this->controller."/history/$lang/$id_hard".$this->viewStatus;?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
			</div>
	
		<?php } else if (strcmp($this->action,'revision') === 0) { ?>

			<div class="notebook_view_title">
				<?php echo gtext("Revision");?> - <?php echo singular($this->controller).' <b>'.$ne_name.'</b>';?>
			</div>
			
			<div class="notebook_insert_link">
				<a title="Back to the history of the <?php echo singular($this->controller);?> <?php echo $name;?>" href="<?php echo $this->baseUrl."/".$this->controller."/history/$lang/$id_hard".$this->viewStatus;?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
			</div>
		
		<?php } else if (strcmp($this->action,'insert') === 0) { ?>

			<div class="notebook_view_title">
				<?php echo gtext("Insert");?> - <?php echo singular($this->controller);?>
			</div>

			<div class="notebook_insert_link">
				<a title="Back to the list of <?php echo plural($this->controller);?>" href="<?php echo $this->baseUrl."/".$this->controller."/catalogue/$lang".$this->viewStatus;?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
			</div>
		
		<?php } else if (strcmp($this->action,'update') === 0) { ?>

			<div class="notebook_view_title">
				<?php echo gtext("Edit");?> - <?php echo singular($this->controller).' <b>'.$ne_name.'</b>';?>
			</div>
			
			<div class="notebook_insert_link">
				<a title="Back to the <?php echo singular($this->controller);?> specifications" href="<?php echo $this->baseUrl."/".$this->controller."/view/$lang/".$id_hard."/$name".$this->viewStatus;?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
			</div>
		
		<?php } else if (strcmp($this->action,'talk') === 0) { ?>

			<div class="notebook_view_title">
				<?php echo gtext("Talk page");?> - <?php echo singular($this->controller).' <b>'.$ne_name.'</b>';?>
			</div>

			<div class="notebook_insert_link">
				<a title="Back to the <?php echo singular($this->controller);?> specifications" href="<?php echo $this->baseUrl."/".$this->controller."/view/$lang/".$id_hard."/$name".$this->viewStatus;?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
			</div>
			
		<?php } ?>