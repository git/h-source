<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; credits
		</div>
		
		<div class="credits_external_box">
			
			<div class="credits_item_title">
				Icons:
			</div>
			
			<div class="credits_item_description">
				The icons used inside <?php echo Website::$generalName;?> are taken from the <a href="http://kde-look.org/content/show.php/ACUN+Simgeleri?content=83018">ACUN Simgeleri 0.7</a> icon theme and from the <a href="http://kde-look.org/content/show.php/H2O+Icon+Theme?content=127149">H2O Icon Theme 0.0.5</a>, both licensed under the GNU GPL license, from the <a href="http://www.everaldo.com/crystal/?action=downloads">Crystal Projects</a> icons, licensed under the LGPL, from the <a href="http://www.notmart.org/index.php/Graphics">glaze icons set</a> (LGPL) and from the <a href="http://kde-look.org/content/show.php/Dark-Glass+reviewed?content=67902">DarkGlass_Reworked icons theme</a> (GPL). The flag icons are taken from the <a href="http://www.famfamfam.com/lab/icons/flags/">FAMFAMFAM flag icons set</a> (Public Domain).
			</div>
			
			<div class="credits_item_title">
				jQuery:
			</div>
			
			<div class="credits_item_description">
				The <a href="http://jquery.com/">jQuery</a>, the <a href="http://jqueryui.com/home">jQuery UI</a> and the <a href="http://jquerymobile.com/">jQuery Mobile</a> javascript libraries (licensed under MIT/GPL) have been used through the website
			</div>

			<div class="credits_item_title">
				markitup:
			</div>
			
			<div class="credits_item_description">
				The <a href="http://markitup.jaysalvat.com/home/">markitup</a> jQuery plugin (licensed under MIT/GPL) has been used in order to help the user to insert wiki tags
			</div>
			
			<div class="credits_item_title">
				php diff algorithm:
			</div>
			
			<div class="credits_item_description">
				<a href="http://compsci.ca/v3/viewtopic.php?p=142539">This</a> algorithm (licensed under the zlib free license) has been used in order to highlight the differences between two different revisions of the same hardware model.
			</div>

		</div>
	
	</div>
