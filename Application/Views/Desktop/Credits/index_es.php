<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; credits
		</div>
		
		<div class="credits_external_box">
			
			<div class="credits_item_title">
				Iconos:
			</div>
			
			<div class="credits_item_description">
				Los iconos usados dentro de <?php echo Website::$generalName;?> son tomados de el tema de iconos <a href="http://kde-look.org/content/show.php/ACUN+Simgeleri?content=83018">ACUN Simgeleri 0.7</a> y de <a href="http://kde-look.org/content/show.php/H2O+Icon+Theme?content=127149">H2O Icon Theme 0.0.5</a>, ambos licenciados bajo la licencia GNU GPL, de <a href="http://www.everaldo.com/crystal/?action=downloads">Crystal Projects</a>, licenciado bajo la LGPL, de <a href="http://www.notmart.org/index.php/Graphics">glaze icons set</a> (LGPL) y de <a href="http://kde-look.org/content/show.php/Dark-Glass+reviewed?content=67902">DarkGlass_Reworked icons theme</a> (GPL). Los iconos de las banderas son tomados la colección de iconos de banderas <a href="http://www.famfamfam.com/lab/icons/flags/">FAMFAMFAM</a> (Dominio Público)
			</div>
			
			<div class="credits_item_title">
				jQuery:
			</div>
			
			<div class="credits_item_description">
				Las bibliotecas javascript <a href="http://jquery.com/">jQuery</a>, <a href="http://jqueryui.com/home">jQuery UI</a> y <a href="http://jquerymobile.com/">jQuery Mobile</a> (licenciadas bajo MIT/GPL) han sido usadas en el sitio
			</div>

			<div class="credits_item_title">
				markitup:
			</div>
			
			<div class="credits_item_description">
				El complemento <a href="http://markitup.jaysalvat.com/home/">markitup</a> jQuery (licenciado bajo MIT/GPL) ha sido usado en orden de ayudar al usuario a insertar etiquetas wiki
			</div>
			
			<div class="credits_item_title">
				php diff algorithm:
			</div>
			
			<div class="credits_item_description">
				<a href="http://compsci.ca/v3/viewtopic.php?p=142539">Este</a> algoritmo (licenciado bajo la licencia libre de zlib) ha sido usado en orden de remarcar las diferencias entre dos diferentes revisiones del mismo modelo de hardware.
			</div>

		</div>
	
	</div>
