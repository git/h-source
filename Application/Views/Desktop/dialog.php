<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

<script type="text/javascript">

	$(function(){
		
		$("#dialog-form").css("display","block");
		
		$('#n_dialog').dialog({
			autoOpen: false,
			width: 500
		});
		
		// Dialog			
		$('#dialog-form').dialog({
			autoOpen: false,
			width: 500,
			buttons: {
				"Send": function() {
					
					var d_id_hard = $(".dialod_hidden_id_hard").attr("value");
					var d_object = encodeURIComponent($("#object").attr("value"));
					var d_message = encodeURIComponent($("#message").attr("value"));
					var d_id_duplicate = encodeURIComponent($("#id_duplicate").attr("value"));
					
					$.ajax({
						type: "POST",
						url: "<?php echo $this->baseUrl.'/generic/del/'.$lang.'/'.$token;?>",
						data: "id_hard="+d_id_hard+"&object="+d_object+"&message="+d_message+"&id_duplicate="+d_id_duplicate+"&insertAction=save",
						async: false,
						cache:false,
						dataType: "html",
						success: function(html){
							$(".n_dialog_inner").text(html);
							$('#n_dialog').dialog('open');
						}
					});
					
					$(this).dialog("close"); 
				}, 
				"Cancel": function() { 
					$(this).dialog("close"); 
				} 
			}
		});
			
		// Dialog Link
		$('.ask_for_removal_class_link').click(function(){
			$('#dialog-form').dialog('open');
			return false;
		});
			
	});
</script>


<div id="dialog-form" title="Ask for the removal of this device">
<!-- 	<p class="validateTips">Ask for removal:</p> -->
	<form>
		<table>
			<tr>
				<td><label for="object">why?</label></td>
				<td><?php echo Html_Form::select('object','duplicated','duplication,other',null,"object");?></td>
			</tr>
			<tr>
				<td><label for="message">message</label></td>
				<td><textarea name="message" id="message">Write here your message..</textarea></td>
			</tr>
			<tr>
				<td><label for="id_duplicate">duplicated model (write the id)</label></td>
				<td><input type="text" id="id_duplicate" type="hidden" name="id_duplicate" value=""></td>
			</tr>
			<input class="dialod_hidden_id_hard" type="hidden" name="id_hard" value="<?php echo $id_hard;?>">
		</table>
	</form>
</div>

<div id="n_dialog" title="Notice:">
	<div class="n_dialog_inner">

	</div>
</div>