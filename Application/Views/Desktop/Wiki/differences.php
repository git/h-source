<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">

		<?php if (!$isDeleted) { ?>
		
			<div class="position_tree_box">
				<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo $tree;?>
			</div>

			<?php if ($showDiff === true) { ?>
			<div class="notebook_view_title">
				<?php echo gtext("Differences between the revision of");?> <b><?php echo smartDate($update_new);?></b>, <?php echo gtext("created by");?> <b><?php echo getLinkToUser($u->getUser($created_by));?></b>, <?php echo gtext("and the revision of");?> <b><?php echo smartDate($update_old);?></b>
			</div>
			<?php } ?>

			<div class="notebook_insert_link">
				<a title="Back to the history of the page <?php echo $tree_name;?>" href="<?php echo $this->baseUrl."/".$this->controller."/history/$lang/".$id_wiki.$this->viewStatus;?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
			</div>

			<div class="wiki_external_box">

				<div class="diff_color_notice">
					<?php echo gtext("<b>Notice</b>: the text in <del>red</del> has been deleted from the previous revision, the text in <ins>green</ins> has been added in this revision and the text in <span class='gray_text_notice'>gray</span> has not been changed.");?>
				</div>

				<?php foreach ($diffArray as $label => $text) { ?>

				<div class="diff_ext_box">

					<div class="diff_item_label">
						<?php echo gtext("differences in the entry");?>: <b><?php echo $label;?></b>
					</div>

					<div class="diff_item_text">
						<?php echo in_array($label,$fieldsWithBreaks) ? nl2br($text) : $text;?>
					</div>

				</div>

				<?php } ?>

			</div>
		<?php } ?>
