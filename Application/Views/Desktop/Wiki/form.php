<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<script>

		$(document).ready(function() {

			$("#bb_code").markItUp(mySettings);

		});

	</script>
	
	<div id="left">

		<?php if ( strcmp($this->action,'insert') === 0 ) { ?>
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/wiki/page/$lang/Main-Page";?>">Wiki</a> &raquo; <?php echo gtext('Insert'); ?>
		</div>

		<div class="notebook_view_title">
			<?php echo gtext('Insert a new wiki page'); ?>
		</div>
		
		<div class="notebook_insert_link">
			<a title="Back to the wiki main page" href="<?php echo $this->baseUrl."/".$this->controller."/page/$lang/Main-Page";?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
		</div>
		
		<?php } else if ( strcmp($this->action,'update') === 0 ) { ?>

		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/wiki/page/$lang/Main-Page";?>">Wiki</a> &raquo; <a href="<?php echo $this->baseUrl."/wiki/page/$lang/".encodeUrl($tree_name);?>"><?php echo $tree_name;?></a> &raquo; <?php echo gtext('Update'); ?>
		</div>

		<div class="notebook_view_title">
			<?php echo gtext('Edit the wiki page'); ?> <b><?php echo $tree_name;?></b>
		</div>
			
		<div class="notebook_insert_link">
			<a title="Back to the page <?php echo $tree_name;?>" href="<?php echo $this->baseUrl."/".$this->controller."/page/$lang/".encodeUrl($tree_name);?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
		</div>
		
		<?php } ?>

		<div class="top_licence_notice">
			<div><b><?php echo gtext("License information");?>:</b></div>
			<?php echo License::getSubmissionNotice();?>
		</div>
		
		<?php echo $notice;?>

		<div class="notebooks_insert_form">

			<form action="<?php echo $this->baseUrl."/".$this->controller."/".$this->action."/$lang";?>" method="POST">

				<div class="edit_form">

					<div class="form_entry">
						<div class="entry_label"><?php echo gtext("the title");?>:</div>
						<?php echo Html_Form::input('title',$values['title'],'input_entry');?>
					</div>

					<div class="form_entry">
						<div class="entry_label"><?php echo gtext("the text of the wiki page");?>:<br /><a href="<?php echo $this->baseUrl."/help/index/$lang#wiki-syntax";?>"><?php echo gtext("discover all the wiki tags");?></a></div>

						<?php if (isset($pagePreview)) { ?>
						<div class="description_preview_title"><?php echo gtext("Page preview");?>:</div>
						<div class="description_preview">
							<?php echo decodeWikiText($pagePreview); ?>
						</div>
						<?php } ?>
					
						<?php echo Html_Form::textarea('page',$values['page'],'textarea_entry','bb_code');?>
					</div>

					<?php echo $hiddenInput;?>

					<input type="submit" name="previewAction" value="<?php echo gtext("Preview");?>">
					<input type="submit" name="<?php echo $submitName;?>" value="<?php echo gtext("Save");?>">

				</div>

			</form>
		</div>

