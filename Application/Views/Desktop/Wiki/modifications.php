<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/wiki/page/$lang/Main-Page";?>">Wiki</a> &raquo; <?php echo gtext('Wiki modifications');?>
		</div>
		
		<div class="contrib_explain_box">
			<?php echo gtext('List of the wiki modifications carried out by users');?>
		</div>
		
		<div class="notebooks_viewall">
			<ul class="page_history">
			<?php foreach ($table as $row) { ?>
				<li class="page_history_item"><?php echo gtext('the text of the wiki page');?> <a href="<?php echo $wiki->toWikiPage($row['history']['id']);?>"><?php echo $wiki->getTheModelName($row['history']['id']);?></a> <?php echo gtext('has been '.$translations[$row['history']['action']].' by');?> <?php echo $u->getLinkToUserFromId($row['history']['created_by'])?> <?php echo gtext('at');?> <?php echo smartDate($row['history']['creation_date']);?></li>
			<?php } ?>
			</ul>
		</div>

		<div class="history_page_list">
			<?php echo gtext("page list");?>: <?php echo $pageList;?>
		</div>
		
	</div>
