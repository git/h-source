<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<script>
	
		$(document).ready(function() {
			
			$("#bb_code").markItUp(mySettings);
			
		});
		
	</script>

	<div id="left">

		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <?php echo $tree; ?>
		</div>

		<div class="notebook_view_title">
			<?php echo gtext('Talk page of the wiki page'); ?> <b><?php echo $tree_name;?></b>
		</div>

		<div class="notebook_insert_link">
			<a title="Back to the page <?php echo $tree_name;?>" href="<?php echo $wiki->toWikiPage($id_wiki);?>"><img class="top_left_images" src="<?php echo $this->baseUrl;?>/Public/Img/back-60.png"></a>
		</div>

		<?php if (!$isDeleted) { ?>
			<?php if (!$isBlocked) { ?>

				<div class="wiki_external_box">

					<?php foreach ($table as $message) { ?>

						<?php if (strcmp($message['wiki_talk']['deleted'],'no') === 0) { ?>

							<a name="wiki-talk-<?php echo $message['wiki_talk']['id_talk'];?>"></a>
							<div class="talk_message_item">

								<div class="talk_message_item_title_date">
									<?php if ($ismoderator) { ?>
										<a id="<?php echo $message['wiki_talk']['id_talk'];?>" class="hide_wiki_talk hide_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/button_cancel.png">hide</a>
									<?php } ?>

									<div class="talk_message_item_title">
										<?php echo $message['wiki_talk']['title'];?>
									</div>

									<div class="talk_message_item_date">
										<?php echo gtext("by");?> <?php echo getLinkToUser($u->getUser($message['wiki_talk']['created_by']));?>, <?php echo smartDate($message['wiki_talk']['creation_date']);?>
									</div>
								</div>

								<div class="talk_message_item_content">
									<?php echo decodeWikiText($message['wiki_talk']['message']);?>
								</div>

								<?php if ($ismoderator) { ?>
								<!--view details-->
								<div class="show_hidden_box_ext">
									<div class="md_type">wiki_talk</div>
									<a id="<?php echo $message['wiki_talk']['id_talk'];?>" class="hidden_message_view_details" href="<?php echo $this->baseUrl."/home/index/$lang";?>">view details</a>
									<div class="moderation_details_box"></div>
								</div>
							<?php } ?>

							</div>

						<?php } else { ?>

							<?php if ($ismoderator) { ?>
								<a name="wiki-talk-<?php echo $message['wiki_talk']['id_talk'];?>"></a>
								<div class="talk_message_item_hidden">
									this message has been deleted

										<a id="<?php echo $message['wiki_talk']['id_talk'];?>" class="show_wiki_talk hide_general" href="<?php echo $this->baseUrl."/home/index/$lang";?>"><img src="<?php echo $this->baseUrl;?>/Public/Img/Crystal/button_ok.png">make visible</a>

										<!--view details-->
										<div class="show_hidden_box_ext">
											<div class="md_type">wiki_talk</div>

											<a id="<?php echo $message['wiki_talk']['id_talk'];?>" class="hidden_message_view_details" href="<?php echo $this->baseUrl."/home/index/$lang";?>">view details</a>

											<div class="details_of_hidden_message">
												<div class="details_of_hidden_message_inner">
													<div class="talk_message_item_date">
														submitted by <?php echo getLinkToUser($u->getUser($message['wiki_talk']['created_by']));?>, <?php echo smartDate($message['wiki_talk']['creation_date']);?>
													</div>
													<div class="message_view_description_hidden">
														<?php echo decodeWikiText($message['wiki_talk']['message']);?>
													</div>
												</div>
												<div class="moderation_details_box"></div>
											</div>
										</div>


								</div>
							<?php } ?>

						<?php } ?>

					<?php } ?>
				</div>

				<?php if ($islogged === 'yes') { ?>

					<div class="talk_form_external_box">
						<div class="talk_login_notice">
							<a name="form"><?php echo gtext("Add a message");?></a>
						</div>

						<?php echo $notice;?>

						<div class="notebooks_insert_form">
							<form action="<?php echo $this->baseUrl."/".$this->controller."/talk/$lang/$id_wiki";?>#form" method="POST">

								<div class="edit_form">

									<div class="form_entry">
										<div class="entry_label"><?php echo gtext("TITLE");?>:</div>
										<?php echo Html_Form::input('title',$values['title'],'talk_input_entry');?>
									</div>

									<div class="form_entry">
										<div class="entry_label"><?php echo gtext("MESSAGE");?>:</div>
										<?php echo Html_Form::textarea('message',$values['message'],'talk_textarea_entry','bb_code');?>
									</div>

									<input type="submit" name="insertAction" value="<?php echo gtext("Save");?>">

								</div>

							</form>
						</div>
					</div>

				<?php } else { ?>

					<div class="talk_login_notice">
						<a name="form"><?php echo gtext("You have to");?> <a href="<?php echo $this->baseUrl."/users/login/$lang?redirect=".$currPos.$queryString;?>">login</a> <?php echo gtext("in order to add a message");?></a>
					</div>

				<?php } ?>

			<?php } else { ?>
				<!--is blocked-->
				<div class="wiki_external_box">
					<?php echo gtext('This wiki page has been blocked'); ?>
				</div>
			<?php } ?>
			<!--close deleted-->
		<?php } ?>
	</div>