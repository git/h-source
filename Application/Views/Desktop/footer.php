<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="footer">
		<div class="copyright_notice_box">
			The <a href="<?php echo $this->baseUrl."/project/index/$lang";?>"><?php echo Website::$projectName;?></a> Project
		</div>
		
		<div class="footer_credits_box">
			<a href="<?php echo $this->baseUrl."/credits/index/$lang";?>">credits</a>
		</div>
		
		<div class="footer_credits_box">
			<a href="<?php echo $this->baseUrl."/contact/index/$lang";?>">contact</a>
		</div>
	</div> <!--fine footer-->
	<div style="padding:5px 0px;font-size:12px;">h-node.org is a hardware database project. It runs the <a href="http://savannah.nongnu.org/projects/h-source/">h-source</a> PHP software, commit HEAD, available under the GNU General Public (GPLv3) License.</div>
	<div style="padding:5px 0px;font-size:12px;"><a href="<?php echo $this->baseUrl;?>/static/licenses.html" rel="jslicense">JavaScript license information</a>
	</div>
</div> <!--fine container-->

<?php
//$sid="23552";
//include("/var/www/h-node.org/traffica/write_logs.php");
?>

</body>
</html>
