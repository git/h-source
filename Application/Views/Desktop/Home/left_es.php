<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			Home
		</div>
		
		<?php echo $htmlNewsBox;?>
		
		<div class="home_container">
			<div class="home_objectives_title">
				Objetivos:
			</div>
			<div class="home_objectives_description">
				<img src="<?php echo $this->baseUrl;?>/Public/Img/H2O/applications-internet.png"> El proyecto <b><?php echo Website::$projectName;?></b> tiene como meta la construcción de una base de datos de todo el hardware de manera que se pueda identificar que dispositivos funcionan con un <a href="http://www.gnu.org/distros/free-distros.es.html">sistema operativo completamente libre</a>. El sitio <?php echo Website::$generalName;?> esta estructurado como un wiki, en el cual todos los usuarios pueden modificar o integrar nuevos contenidos. El proyecto h-node es desarrollado en colaboración y como una actividad de la <a href="http://www.fsf.org">FSF</a>.
			</div>
			
			<div class="home_objectives_title">
				Contribuir:
			</div>
			<div class="home_objectives_description">
				Puede contribuir creando una cuenta en <?php echo Website::$generalName;?> y editar los contenidos generados. Todas sus modificaciones serás guardadas en el historial del producto que edite/agregue. Cada revisión (actual o previa) será marcada con el nombre del usuario que la creo.<br />También puede contribuir al <b>sugerir nuevo hardware</b> que deba ser agregado a la base de datos o <b>características que deberían ser implementadas.</b>
			</div>

			<div class="home_objectives_title">
				Software Libre:
			</div>
			<div class="home_objectives_description">
				<img height="100px" src="https://savannah.nongnu.org/images/Savannah.theme/floating.png">
				
				In order to add a device to the h-node database, you must verify that it works using only free software. For this purpose, you must be running either:

				<p>1) A GNU/Linux distribution that is on the <a   href="http://www.gnu.org/distros/free-distros.html">FSF's list of   endorsed distributions</a></p>

				<p>2) <a href="http://www.debian.org">Debian GNU/Linux</a>, <strong>with only the main archive area enabled</strong>. The "contrib" and   "non-free" areas must not be enabled when testing hardware.   Double-check this by running <code>apt-cache policy</code>. The only package archive area mentioned in the output should be <strong>main</strong>.</p>
				
				<p>h-node lists only hardware that works with free drivers and without non-free firmware. Other GNU/Linux distributions (or Debian with contrib, non-free, or some 3rd-party archives enabled) include non-free firmware files, so they cannot be used for testing.</p>
			</div>

			<div class="home_objectives_title">
				Licencias:
			</div>
			<div class="home_objectives_description">
				Cualquier texto agregado por usted sera colocado en Dominio Publico (vea la <a href="http://creativecommons.org/publicdomain/zero/1.0/deed.es_CL">página CC0</a> para información detallada). Cualquiera es libre de copiar, modificar, publicar, usar, vender o distribuir el texto que ha integrado a h-node.org, para cualquier propósito, comercial o no comercial, y por cualquier método.
			</div>

			<div class="home_objectives_title">
				Otros recursos en la red:
			</div>
			<div class="home_objectives_description">
				<p>Aquí una lista de otras actividades recolectando información acerca de hardware funcionando con software libre:</p>
				<ul>
<!-- 					<li><a href="http://www.fsf.org/resources/hw">Free Software Foundation archive</a></li> -->
					<li><a href="http://libreplanet.org/wiki/Hardware/Freest">LibrePlanet Freest Hardware Page</a></li>
					<li><a href="http://libreplanet.org/wiki/Group:LibrePlanet_Italia/Progetti/hardware_libero">LibrePlanet Italia - progetti hardware database</a></li>
				</ul>
			</div>
			
			<div class="home_objectives_title">
				Acerca del sitio <?php echo Website::$generalName;?>:
			</div>
			<div class="home_objectives_description">
				El sitio <?php echo Website::$generalName;?> debe ser considerado una <b>versión beta</b>. Esta en constantemente crecimiento y varias características no han sido implementadas aun (por ejemplo, nuevos dispositivos de hardware tienen que ser incluidos en la base de datos). Algunos problemas pueden ocurrir: Si encuentra un error por favor agregue una incidencia <a href="<?php echo $this->baseUrl."/issues/viewall/$lang/1/$token";?>">aquí</a> bajo el tema <b>maybe a bug</b>.
			</div>
		</div>
	</div>
