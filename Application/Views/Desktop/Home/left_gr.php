<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			Home
		</div>
		
		<?php echo $htmlNewsBox;?>
		
		<div class="home_container">
			<div class="home_objectives_title">
				Σκοποί:
			</div>
			<div class="home_objectives_description">
				<img src="<?php echo $this->baseUrl;?>/Public/Img/H2O/applications-internet.png">
				Το <b><?php echo Website::$projectName;?></b> project έχει ως σκοπό την κατασκευή μίας βάσης δεδομένων υλικού, έτσι ώστε να μπορεί να εντοπισθεί ποιες συσκευές δουλεύουν με ένα <a href="http://www.gnu.org/distros/free-distros.html">πλήρως ελεύθερο Λειτουργικό Σύστημα</a>. Ο ιστότοπος <?php echo Website::$generalName;?> είναι δομημένος σαν ένα wiki, εντός του οποίου όλοι οι χρήστες μπορούν να τροποποιούν ή να εισαγάγουν νέα περιεχόμενα. Το h-node project αναπτύχθηκε σε συνεργασία με και σαν δραστηριότητα του <a href="http://www.fsf.org">FSF</a>.
			</div>
			
			<div class="home_objectives_title">
				Συνεισφέρετε:
			</div>
			<div class="home_objectives_description">
				Μπορείτε να συνεισφέρετε φτιάχνοντας έναν λογαριασμό στο <?php echo Website::$generalName;?> και τροποποιώντας τα περιεχόμενα που δημιούργησαν οι ίδιοι οι χρήστες. Όλες σας οι τροποποιήσεις θα αποθηκεύονται στο ιστορικό του προϊόντος το οποίο επεξεργάζεσθε/εμπλουτίζετε. Κάθε αναθεώρηση (τόσο η τρέχουσα όσο και οι παλιές) θα επισημαίνεται με το όνομα του χρήστη που την δημιούργησε. Μπορείτε, επίσης, να συνεισφέρετε <b>προτείνοντας νέο υλικό</b> που θα έπρεπε να προστεθεί στη βάση δεδομένων, ή <b>εισηγούμενοι χαρακτηριστικά που θα έπρεπε να υλοποιηθούν</b>.
			</div>

			<div class="home_objectives_title">
				Ελεύθερο Λογισμικό
			</div>
			<div class="home_objectives_description">
				<img height="100px" src="https://savannah.nongnu.org/images/Savannah.theme/floating.png">
				
				In order to add a device to the h-node database, you must verify that it works using only free software. For this purpose, you must be running either:

				<p>1) A GNU/Linux distribution that is on the <a   href="http://www.gnu.org/distros/free-distros.html">FSF's list of   endorsed distributions</a></p>

				<p>2) <a href="http://www.debian.org">Debian GNU/Linux</a>, <strong>with only the main archive area enabled</strong>. The "contrib" and   "non-free" areas must not be enabled when testing hardware.   Double-check this by running <code>apt-cache policy</code>. The only package archive area mentioned in the output should be <strong>main</strong>.</p>
				
				<p>h-node lists only hardware that works with free drivers and without non-free firmware. Other GNU/Linux distributions (or Debian with contrib, non-free, or some 3rd-party archives enabled) include non-free firmware files, so they cannot be used for testing.</p>
				
			</div>

			<div class="home_objectives_title">
				Άδεια:
			</div>
			<div class="home_objectives_description">
				Οποιοδήποτε κείμενο που υποβάλετε, θα μπαίνει σε Δημόσια Διάθεση/Public Domain (δείτε τη σελίδα <a href="http://creativecommons.org/publicdomain/zero/1.0/">CC0 page</a> για πιο λεπτομερείς πληροφορίες). Οποιοσδήποτε είναι ελεύθερος να αντιγράφει, να τροποποιεί, να δημοσιεύει, να χρησιμοποιεί, να πωλεί, ή και να ανδιανέμει το κείμενο που εσείς υποβάλλατε στο h-node.com, για οποιοδήποτε λόγο, εμπορικό ή μη-εμπορικό, και με οποιονδήποτε τρόπο.
			</div>

			<div class="home_objectives_title">
				Άλλοι πόροι στο Διαδίκτυο:
			</div>
			<div class="home_objectives_description">
				<p>Ακολουθεί, εδώ, ένας κατάλογος άλλων αρχείων που συγκεντρώνουν πληροφορίες σχετικά με υλικό που δουλεύει με ελεύθερο λογισμικό:</p>
				<ul>
<!-- 					<li><a href="http://www.fsf.org/resources/hw">Free Software Foundation archive</a></li> -->
					<li><a href="http://libreplanet.org/wiki/Hardware/Freest">LibrePlanet Freest Hardware Page</a></li>
					<li><a href="http://libreplanet.org/wiki/Group:LibrePlanet_Italia/Progetti/hardware_libero">LibrePlanet Italia - progetti hardware database</a></li>
				</ul>
			</div>
			
			<div class="home_objectives_title">
				Σχετικά με τον ιστότοπο <?php echo Website::$generalName;?>:
			</div>
			<div class="home_objectives_description">
				Το <?php echo Website::$generalName;?> πρέπει να θεωρείται πως βρίκεται σε <b>έκδοση beta</b>. Αναπτύσσεται συνεχώς και πολλά χαρακτηριστικά δεν έχουν ακόμη υλοποιηθεί (π.χ. πρέπει ακόμη να προστεθούν νέες συσκευές υλικού στη βάση δεδομένων). Μπορεί να παρατηρηθούν ορισμένα προβλήματα: αν εντοπίσετε ένα σφάλμα (bug), παρακαλώ προσθέστε μία αναφορά προβλήματος <a href="<?php echo $this->baseUrl."/issues/viewall/$lang/1/$token";?>">εδώ</a> με θέμα <b>ίσως να πρόκειται για ένα bug</b>.
			</div>
		</div>
	</div>
