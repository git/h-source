<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="left">
		
		<div class="position_tree_box">
			Home
		</div>
		
		<?php echo $htmlNewsBox;?>
		
		<div class="home_container">
			<div class="home_objectives_title">
				Ziele:
			</div>
			<div class="home_objectives_description">
				<img src="<?php echo $this->baseUrl;?>/Public/Img/H2O/applications-internet.png"> Mit dem Projekt <b><?php echo Website::$projectName;?></b> soll eine Hardwaredatenbank mit Peripheriegeräten aufgebaut werdem, die mit einem <a href="http://www.gnu.org/distros/free-distros.html">vollständig freien Betriebssystem</a> arbeiten. Die Webseite von h-node.org ist wie ein Wiki aufgebaut, dessen Inhalte von allen Benutzer geändert und ergänzt werden können. Das Projekt <b>h-node</b> wird in Zusammenarbeit und als eine Aktivität der <a href="http://www.fsf.org">FSF</a> entwickelt.
			</div>
			
			<div class="home_objectives_title">
				Mithelfen:
			</div>
			<div class="home_objectives_description">
				Sie können nach Ihrer Anmeldung bei h-node.org dazu beitragen und die benutzergenerierten Inhalte bearbeiten. Alle Änderungen die Sie bearbeiten oder hinzufügen, werden im Produktverlauf protokolliert. Jede Version (die aktuelle oder die alte) wird der/dem BenutzerIn zugeordnet, die/der sie erstellte.<br />
				<b>Neue Hardware</b>, die in die Datenbank aufgenommen, oder <b>neue Funktionen</b>, die hinzugefügt werden sollten, können Sie ebenfalls vorschlagen.
			</div>

			<div class="home_objectives_title">
				Freie Software:
			</div>
			<div class="home_objectives_description">
				<img height="100px" src="https://savannah.nongnu.org/images/Savannah.theme/floating.png">
				
				In order to add a device to the h-node database, you must verify that it works using only free software. For this purpose, you must be running either:

				<p>1) A GNU/Linux distribution that is on the <a   href="http://www.gnu.org/distros/free-distros.html">FSF's list of   endorsed distributions</a></p>

				<p>2) <a href="http://www.debian.org">Debian GNU/Linux</a>, <strong>with only the main archive area enabled</strong>. The "contrib" and   "non-free" areas must not be enabled when testing hardware.   Double-check this by running <code>apt-cache policy</code>. The only package archive area mentioned in the output should be <strong>main</strong>.</p>
				
				<p>h-node lists only hardware that works with free drivers and without non-free firmware. Other GNU/Linux distributions (or Debian with contrib, non-free, or some 3rd-party archives enabled) include non-free firmware files, so they cannot be used for testing.</p>
			</div>

			<div class="home_objectives_title">
				Lizenz:
			</div>
			<div class="home_objectives_description">
				Jeder von Ihnen übermittelte Text wird in die Gemeinfreiheit („Public Domain“) entlassen (siehe <a href="http://creativecommons.org/publicdomain/zero/1.0/deed.de">CC0 1.0 Universell (CC0 1.0)</a> für detaillierte Informationen). Jede Person darf das Werk/den Inhalt, den Sie an h-node.org übermittelt haben, kopieren, verändern, verbreiten und aufführen, sogar zu kommerziellen Zwecken, ohne um weitere Erlaubnis bitten zu müssen und auf jedem Medium.
			</div>

			<div class="home_objectives_title">
				Weitere Ressourcen im Internet:
			</div>
			<div class="home_objectives_description">
				<p>Weitere Archive mit gesammelten Informationen über Hardware, die mit Freie Software arbeitet:</p>
				<ul>
<!-- 					<li><a href="http://www.fsf.org/resources/hw">Free Software Foundation archive</a></li> -->
					<li><a href="http://libreplanet.org/wiki/Hardware/Freest">LibrePlanet Freest Hardware Page</a></li>
					<li><a href="http://libreplanet.org/wiki/Group:LibrePlanet_Italia/Progetti/hardware_libero">LibrePlanet Italia - progetti hardware database</a></li>
				</ul>
			</div>
			
			<div class="home_objectives_title">
				Über h-node.org:
			</div>
			<div class="home_objectives_description">
				<b>h-node</b> muss als im Beta-Stadium betrachtet werden. Es wird ständig ergänzt haben und viele Funktionen wurden noch nicht implementiert (z. B. müssen neue Hardware-Geräte in die Datenbank eingepflegt werden). Es können Probleme auftreten: Wenn Sie einen Fehler finden, melden Sie diesen bitte im <a href="<?php echo $this->baseUrl."/issues/viewall/$lang/1/$token";?>">Web-Log</a> mit dem Thema <b>evtl. ein Fehler</b>.
			</div>
		</div>
	</div>
