<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<?php if (!$isDeleted or $isadmin) { ?>
	
		<?php
		$tableName = (strcmp($this->action,'view') === 0) ? 'hardware' : 'revisions';
		$displayClass = ($isDeleted) ? 'display_none' : null;
		?>
		
		<?php foreach ($table as $item) { ?>
		<div class="notebooks_viewall <?php echo $displayClass;?>">
		
			<!--if revision-->
			<?php if (strcmp($this->action,'revision') === 0) { ?>
			<div class="revision_alert">
				<?php echo gtext("This is an old revision of this page, as edited by");?> <b><?php echo $u->getLinkToUserFromId($updated_by);?></b> <?php echo gtext('at');?> <b><?php echo smartDate($update_date); ?></b>. <?php echo gtext("It may differ significantly from the");?> <a href="<?php echo $this->baseUrl."/".$this->controller."/view/$lang/$id_hard/".$name.$this->viewStatus;?>"><?php echo gtext("Current revision");?></a>.
			</div>
			<?php } ?>

			<?php if ( strcmp($item[$tableName]['it_tracks_users'],'yes') === 0 ) { ?>
			<?php echo $tracksHelpTop;?>
			<?php } ?>
			
			<h3 class="model_name">
				<img align="top" class="catalogue_item_icon" src="<?php echo Hardware::getIconFromType($item[$tableName]['type']);?>"> <b><?php echo $item[$tableName]['model'];?></b>
			</h3>

			<div class="model_id">(<?php echo gtext("model id");?>: <b><?php echo $id_hard;?>)</b></div>

			<?php if ( strcmp($item[$tableName]['other_names'],'') !== 0 ) { ?>
			<div class="ui-grid-a main_content_element">
				<div class="ui-block-a"><?php echo gtext("possible other names of the device");?>:</div>
				<div class="ui-block-b"><b><?php echo nl2br($item[$tableName]['other_names']);?></b></div>
			</div>
			<?php } ?>

			<div class="ui-grid-a main_content_element">
				<div class="ui-block-a"><?php echo gtext("type");?>:</div>
				<div class="ui-block-b"><b><?php echo singular($this->controller);?></b></div>
			</div>
			
			<?php if (strcmp($this->controller,'printers') === 0 or strcmp($this->controller,'hostcontrollers') === 0) { ?>
			<div class="ui-grid-a main_content_element">
				<div class="ui-block-a"><?php echo gtext("subtype");?> (<?php echo $subtypeHelpLabel;?>):</div>
				<div class="ui-block-b"><b><?php echo translate_and_gtext($item[$tableName]['subtype']);?></b></div>
			</div>
			<?php } ?>

			<div class="ui-grid-a main_content_element">
				<div class="ui-block-a"><?php echo gtext("vendor");?>:</div>
				<div class="ui-block-b"><b><?php echo betterVendor($item[$tableName]['vendor']);?></b></div>
			</div>
			
			<div class="ui-grid-a main_content_element">
				<div class="ui-block-a"><?php echo gtext("VendorID:ProductID code of the device");?>:</div>
				<div class="ui-block-b"><b><?php echo $item[$tableName]['pci_id'];?></b></div>
			</div>
			
			<div class="ui-grid-a main_content_element">
				<div class="ui-block-a"><?php echo gtext("year of commercialization");?>:</div>
				<div class="ui-block-b"><b><?php echo gtext($item[$tableName]['comm_year']);?></b></div>
			</div>

			<div class="ui-grid-a main_content_element">
				<div class="ui-block-a"><?php echo gtext("interface");?>:</div>
				<div class="ui-block-b"><b><?php echo gtext(translate($item[$tableName]['interface']));?></b></div>
			</div>

			<div class="ui-grid-a main_content_element">
				<div class="ui-block-a"><?php echo gtext("tested on");?>:</div>
				<div class="ui-block-b"><b><?php echo Distributions::getName($item[$tableName]['distribution']);?></b></div>
			</div>

			<div class="ui-grid-a main_content_element">
				<div class="ui-block-a"><?php echo gtext("tested with the following kernel libre");?>:</div>
				<div class="ui-block-b"><b><?php echo implode("<br />",explode(',',$item[$tableName]['kernel']));?></b></div>
			</div>

			<div class="ui-grid-a main_content_element">
				<?php if (strcmp($this->controller,'videocards') === 0 ) { ?>
				<div class="ui-block-a"><?php echo gtext("tested with the following kernel libre");?>:</div>
				<?php } else if (strcmp($this->controller,'printers') === 0 ) { ?>
				<div class="ui-block-a"><?php echo gtext("compatibility with free software");?>:</div>
				<?php } else if (strcmp($this->controller,'scanners') === 0 ) { ?>
				<div class="ui-block-a"><?php echo gtext("compatibility with free software");?>:</div>
				<?php } else { ?>
				<div class="ui-block-a"><?php echo gtext("does it work with free software?");?></div>
				<?php } ?>
				<div class="ui-block-b"><b><?php echo gtext(translate($item[$tableName][$worksField]));?></b></div>
			</div>

			<?php if (strcmp($this->controller,'printers') === 0 ) { ?>
			<div class="ui-grid-a main_content_element">
				<div class="ui-block-a"><?php echo gtext("does it adopt any techniques to track users?");?><div class="box_module_label"><?php echo $tracksHelpLabel;?></div></div>
				<div class="ui-block-b"><b><?php echo gtext($item[$tableName]['it_tracks_users']);?></b></div>
			</div>
			<?php } ?>

			<div class="ui-grid-a main_content_element">
				<div class="ui-block-a"><?php echo gtext("free driver used");?>:</div>
				<div class="ui-block-b"><b><?php echo $item[$tableName]['driver'];?></b></div>
			</div>

			<?php if (strcmp($item[$tableName]['description'],'') !== 0) { ?>
				<h4><?php echo gtext("Description");?></h4>
				<div class="device_description"><?php echo decodeWikiText($item[$tableName]['description']);?></div>
			<?php } ?>
				

			
		</div>
		<?php } ?>
		
	<?php } ?>