<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div data-role="content">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; h-project
		</div>
		
		<div class="credits_external_box">

			<div class="credits_item_title">
				Pourquoi:
			</div>

			<div class="credits_item_description">
				Le projet h-node a été créer pour aider le mouvement du logiciel libre en créant une archive de matériel fonctionnel avec des <a href="http://www.gnu.org/distros/free-distros.fr.html">systèmes entièrement libres</a>.
			</div>

			<div class="credits_item_title">
				Qui:
			</div>

			<div class="credits_item_description">
				Antonio Gallo (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>tonicucoz">tonicucoz</a>), h-node.com source code developer, Giulia Fanin (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Julia">Julia</a>), graphiste du site et des icones (merci pour tes conseils et ton support), Luis Alberto Guzman Garcia (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Ark74">Ark74</a>), membre de l’équipe de traduction espagnole (merci pour toutes vos bonnes idée et suggestion), Henri (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Hardisk">Hardisk</a>), membre de l’équipe de traduction française, Joerg Kohne (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>joeko">joeko</a>), membre de l’équipe de traduction allemande, Benjamin Rochefort (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>oysterboy">oysterboy</a>), membre de l’équipe de traduction française, Kostas Mousafiris (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>kosmous">kosmous</a>), member of the Greek translation team.
				<br />Remerciement également à tout ceux qui ont crus à ce projet depuis sa naissance et à tout ceux qui ont donné, donne ou donnerons leurs contributions.
			</div>

		</div>
	
	</div>
