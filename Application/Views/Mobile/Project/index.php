<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div data-role="content">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; h-project
		</div>
		
		<div class="credits_external_box">

			<div class="credits_item_title">
				Why:
			</div>

			<div class="credits_item_description">
				The h-node project has been created to help the free software movement by creating an archive of all the hardware that can work with a <a href="http://www.gnu.org/distros/free-distros.html">fully free operating system</a>.
			</div>

			<div class="credits_item_title">
				Who:
			</div>

			<div class="credits_item_description">
				Antonio Gallo (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>tonicucoz">tonicucoz</a>), h-node.com source code developer, Giulia Fanin (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Julia">Julia</a>), designer of the website layout and icons (thanks for your advice and support), Luis Alberto Guzman Garcia (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Ark74">Ark74</a>), member of the Spanish translation team (thanks for all of your useful ideas and suggestions), Henri (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Hardisk">Hardisk</a>), member of the French translation team, Joerg Kohne (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>joeko">joeko</a>), member of the German translation team, Benjamin Rochefort (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>oysterboy">oysterboy</a>), member of the French translation team, Kostas Mousafiris (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>kosmous">kosmous</a>), member of the Greek translation team.
				<br />Also thanks to all of you who have believed in the project since it was born and to all of you who gave, give and will give their contribution.
			</div>

		</div>
	
	</div>
