<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div data-role="content">
		
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; h-project
		</div>
		
		<div class="credits_external_box">

			<div class="credits_item_title">
				Why:
			</div>

			<div class="credits_item_description">
				Il progetto h-node è stato creato per aiutare il movimento del software libero creando un archivio di tutto l' hardware che funzioni con un <a href="http://www.gnu.org/distros/free-distros.it.html">sistema operativo completamente libero</a>.
			</div>

			<div class="credits_item_title">
				Who:
			</div>

			<div class="credits_item_description">
				Antonio Gallo (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>tonicucoz">tonicucoz</a>), sviluppatore del codice di h-node.com, Giulia Fanin (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Julia">Julia</a>), designer del layout del sito e delle icone  (grazie per i consigli e il supporto), Luis Alberto Guzman Garcia (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Ark74">Ark74</a>), membro del team di traduzione spagnolo (grazie per tutte le idee e gli utili suggerimenti), Henri (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>Hardisk">Hardisk</a>), membro del team di traduzione francese, Joerg Kohne (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>joeko">joeko</a>), membro del team di traduzione tedesco, Benjamin Rochefort (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>oysterboy">oysterboy</a>), membro del team di traduzione francese, , Kostas Mousafiris (<a href="<?php echo $this->baseUrl."/meet/user/$lang/";?>kosmous">kosmous</a>), membro del team di traduzione greco.
				<br />Grazie anche a tutti Voi che avete creduto nel progetto sin dalla sua nascita e a tutti quelli che hanno dato, danno e daranno il loro contributo.
			</div>

		</div>
	
	</div>
