<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div data-role="content">
		
		<div class="position_tree_box">
			Home
		</div>
		
		<?php echo $htmlNewsBox;?>
		
		<div class="home_container">
			<div class="home_objectives_title">
				Objectifs:
			</div>

			<div class="home_objectives_description">
				<img src="https://www.h-node.org/Public/Img/H2O/applications-internet.png"> Le but du projet <b>h-node est</b> de créer une base de données de matériel afin d'identifier les dispositifs qui fonctionne avec un <a href="http://www.gnu.org/distros/free-distros.fr.html">système d’exploitation entièrement libre</a>. Le site h-node est structuré comme un wiki, dont tout les utilisateurs peuvent modifier ou insérer des contenus. Le projet h-node est dévellopé en collaboration et en tant qu'activitée de la <a href="http://www.fsf.org">FSF</a>.
			</div>

			<div class="home_objectives_title">
				Contribution:
			</div>
			<div class="home_objectives_description">
				Vous pouvez contribuer en créant un compte sur h-node et modifier le contenus créé par les utilisateurs. Toutes vos modifications seront enregistrées dans l’historique de l’élément que vous modifiez ou ajouter. Toutes versions (l’actuelle ou les précédentes) seront marquées au nom de l’utilisateur qui les a crées. <br />Vous pouvez aussi contribuer en <b>suggérant un autre type de matériel</b> qui devrais être ajouté à la base de données ou une nouvelle <b>fonctionnalitée qui devrais être implémentée</b>
			</div>

			<div class="home_objectives_title">
				Logiciel libre:
			</div>
			<div class="home_objectives_description">
				<img height="100px" src="https://savannah.nongnu.org/images/Savannah.theme/floating.png">In order to add a device to the h-node database, you must verify that it works using only free software. For this purpose, you must be running either:

				<p>1) A GNU/Linux distribution that is on the <a   href="http://www.gnu.org/distros/free-distros.html">FSF's list of   endorsed distributions</a></p>

				<p>2) <a href="http://www.debian.org">Debian GNU/Linux</a>, <strong>with only the main archive area enabled</strong>. The "contrib" and   "non-free" areas must not be enabled when testing hardware.   Double-check this by running <code>apt-cache policy</code>. The only package archive area mentioned in the output should be <strong>main</strong>.</p>
				
				<p>h-node lists only hardware that works with free drivers and without non-free firmware. Other GNU/Linux distributions (or Debian with contrib, non-free, or some 3rd-party archives enabled) include non-free firmware files, so they cannot be used for testing.</p>
			</div>

			<div class="home_objectives_title">
				License:
			</div>

			<div class="home_objectives_description">
				Tout les textes que vous publierez sur le site seront dans le Domain public (Consultez la <a href="http://creativecommons.org/publicdomain/zero/1.0/deed.fr">page CC0</a> pour plus d'informations). Tout un chacun est autorisé à copier, modifier, publier, utiliser, vendre ou distribuer le texte que vous avez envoyé sur h-node.org, pour quelque utilisation que ce soit et par tout les moyens.
			</div>

			<div class="home_objectives_title">
				Ressource supplémentaire sur Internet:
			</div>
			<div class="home_objectives_description">
				<p>Voici une liste d'autres archives qui collecte des informations à propos du matériel fonctionnant avec du logiciel libre:</p>
				<ul>
<!-- 					<li><a href="http://www.fsf.org/resources/hw">Free Software Foundation archive</a></li> -->
					<li><a href="http://libreplanet.org/wiki/Hardware/Freest">LibrePlanet Freest Hardware Page</a></li>
					<li><a href="http://libreplanet.org/wiki/Group:LibrePlanet_Italia/Progetti/hardware_libero">LibrePlanet Italia - progetti hardware database</a></li>
				</ul>
			</div>

			<div class="home_objectives_title">
				A propos du site <?php echo Website::$generalName;?>:
			</div>
			<div class="home_objectives_description">
				Le site doit être considéré comme en <b>version beta</b>, il s’aggrandit constamment et beaucoup de fonctionnalitée n’ont pas encore été implémentée ( par exemple, de nouveau type d’appareil doivent être ajouter dans la base de données) . Des problèmes peuvent arriver, si vous trouver un bug, merci de remplir un <a href="<?php echo $this->baseUrl."/issues/viewall/$lang/1/$token";?>">ticket bug</a> (<b>maybe a bug</b>).
			</div>
		</div>
		
	</div>
