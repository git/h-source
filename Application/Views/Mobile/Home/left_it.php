<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div data-role="content">
		
		<div class="position_tree_box">
			Home
		</div>
		
		<?php echo $htmlNewsBox;?>
		
		<div class="home_container">
			<div class="home_objectives_title">
				Obiettivi:
			</div>

			<div class="home_objectives_description">
				<img src="https://www.h-node.org/Public/Img/H2O/applications-internet.png"> Il progetto <b>h-node</b> mira alla costruzione di un database di hardware allo scopo di identificare quali dispositivi funzionino con un <a href="http://www.gnu.org/distros/free-distros.it.html">sistema operativo completamente libero</a>. Il sito h-node.org è strutturato come una wiki in cui tutti gli utenti possono inserire nuovi contenuti o modificare i contenuti già presenti. Il progetto h-node è sviluppato in collaborazione e come attività della <a href="http://www.fsf.org">FSF</a>.
			</div>

			<div class="home_objectives_title">
				Contribuire:
			</div>
			<div class="home_objectives_description">
				Puoi contribuire creandoti un account su h-node.org e modificando i suoi contenuti generati dagli utenti. Tutte le tue modifiche saranno salvate nella storia del prodotto che stai modificando/aggiungendo. Ogni revisione (quella attuale o le precedenti) sarà contrassegnata col nome dell'utente che l'ha creata.<br />Puoi inoltre contribuire <b>suggerendo del nuovo hardware</b> da aggiungere al database o delle <b>caratteristiche da implementare</b>.
			</div>

			<div class="home_objectives_title">
				Software libero:
			</div>
			<div class="home_objectives_description">
				<img height="100px" src="https://savannah.nongnu.org/images/Savannah.theme/floating.png">Per poter aggiungere un dispositivo nel database di h-node, devi verificare che funzioni usando solo software libero. Per questo scopo, puoi unicamente  utilizzare:

				<p>1) Una delle distribuzioni GNU/Linux elencate <a   href="http://www.gnu.org/distros/free-distros.html">nella lista delle distribuzioni approvate dalla FSF</a></p>

				<p>2) <a href="http://www.debian.org">Debian GNU/Linux</a>, <strong>con solo la zona archivio main attivata</strong>. Le aree "contrib" e "non-free" non devono essere abilitate durante il test dell'hardware. Controlla che sia così lanciando <code>apt-cache policy</code>. La sola zona archivio dei pacchetti deve essere <strong>main</strong>.</p>
				
				<p>h-node elenca solo hardware che funziona con driver liberi e senza firmware non libero. Le altre distribuzioni GNU/Linux (oppure Debian con contrib, non-free, o archivi di terze parti abilitati) includono file di firmware non liberi, quindi non possono essere utilizzate per i test.</p>
			</div>

			<div class="home_objectives_title">
				Licenza:
			</div>

			<div class="home_objectives_description">
				Qualsiasi testo da te inserito diventerà di Pubblico Dominio (visita la <a href="http://creativecommons.org/publicdomain/zero/1.0/deed.it">pagina CC0</a> per informazioni dettagliate). Chiunque è libero di copiare, modificare, pubblicare, usare, vendere o distribuire il testo che hai inserito su h-node.org, per qualsiasi fine, commerciale o non commerciale, e con ogni mezzo.
			</div>

			<div class="home_objectives_title">
				Altre risorse in rete:
			</div>
			<div class="home_objectives_description">
				<p>Di seguito una lista di altri archivi contenenti informazioni sull' hardware funzionante con software libero:</p>
				<ul>
<!-- 					<li><a href="http://www.fsf.org/resources/hw">Free Software Foundation archive</a></li> -->
					<li><a href="http://libreplanet.org/wiki/Hardware/Freest">LibrePlanet Freest Hardware Page</a></li>
					<li><a href="http://libreplanet.org/wiki/Group:LibrePlanet_Italia/Progetti/hardware_libero">LibrePlanet Italia - progetti hardware database</a></li>
				</ul>
			</div>

			<div class="home_objectives_title">
				A proposito del sito <?php echo Website::$generalName;?>:
			</div>
			<div class="home_objectives_description">
				<?php echo Website::$generalName;?> deve essere considerato una <b>versione beta</b>. E' in costante sviluppo e molte caratteristiche non sono ancora state implementate (per esempio nuovi dispositivi hardware devono essere aggiunti al database). Potrebbero esserci dei problemi: se scopri un bug per favore invia una segnalazione <a href="<?php echo $this->baseUrl."/issues/viewall/$lang/1/$token";?>">qui</a> con l'argomento <b>maybe a bug</b>.
			</div>
		</div>
		
	</div>
