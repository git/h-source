<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div data-role="content">
		
		<div class="position_tree_box">
			Home
		</div>
		
		<?php echo $htmlNewsBox;?>
	
		<div class="home_container">
			<div class="home_objectives_title">
				Objectives:
			</div>
			<div class="home_objectives_description">
				<img src="<?php echo $this->baseUrl;?>/Public/Img/H2O/applications-internet.png"> The <b><?php echo Website::$projectName;?></b> project aims at the construction of a hardware database in order to identify what devices work with a <a href="http://www.gnu.org/distros/free-distros.html">fully free operating system</a>. The <?php echo Website::$generalName;?> website is structured like a wiki in which all the users can modify or insert new contents. The h-node project is developed in collaboration and as an activity of the <a href="http://www.fsf.org">FSF</a>.
			</div>

			<div class="home_objectives_title">
				Contribute:
			</div>
			<div class="home_objectives_description">
				You can contribute by creating an account at <?php echo Website::$generalName;?> and editing its user-generated contents. All your modification will be saved in the history of the product you are editing/adding. Each revision (the current one or the old ones) will be marked by the name of the user who created it.<br />You can also contribute by <b>suggesting new hardware</b> that should be added to the database or <b>features that should be implemented</b>.
			</div>

			<div class="home_objectives_title">
				Free software:
			</div>
			<div class="home_objectives_description">
				<img height="100px" src="https://savannah.nongnu.org/images/Savannah.theme/floating.png">
				
				In order to add a device to the h-node database, you must verify that it works using only free software. For this purpose, you must be running either:

				<p>1) A GNU/Linux distribution that is on the <a   href="http://www.gnu.org/distros/free-distros.html">FSF's list of   endorsed distributions</a></p>

				<p>2) <a href="http://www.debian.org">Debian GNU/Linux</a>, <strong>with only the main archive area enabled</strong>. The "contrib" and   "non-free" areas must not be enabled when testing hardware.   Double-check this by running <code>apt-cache policy</code>. The only package archive area mentioned in the output should be <strong>main</strong>.</p>
				
				<p>h-node lists only hardware that works with free drivers and without non-free firmware. Other GNU/Linux distributions (or Debian with contrib, non-free, or some 3rd-party archives enabled) include non-free firmware files, so they cannot be used for testing.</p>
			</div>

			<div class="home_objectives_title">
				License:
			</div>
			<div class="home_objectives_description">
				Any text submitted by you will be put in the Public Domain (see the <a href="http://creativecommons.org/publicdomain/zero/1.0/">CC0 page</a> for detailed information). Anyone is free to copy, modify, publish, use, sell, or distribute the text you have submitted to h-node.org, for any purpose, commercial or non-commercial, and by any means.
			</div>

			<div class="home_objectives_title">
				Other resources on the net:
			</div>
			<div class="home_objectives_description">
				<p>Here is a list of other archives collecting information about hardware working with free software:</p>
				<ul>
<!-- 					<li><a href="http://www.fsf.org/resources/hw">Free Software Foundation archive</a></li> -->
					<li><a href="http://libreplanet.org/wiki/Hardware/Freest">LibrePlanet Freest Hardware Page</a></li>
					<li><a href="http://libreplanet.org/wiki/Group:LibrePlanet_Italia/Progetti/hardware_libero">LibrePlanet Italia - progetti hardware database</a></li>
				</ul>
			</div>

			<div class="home_objectives_title">
				About the <?php echo Website::$generalName;?> website:
			</div>
			<div class="home_objectives_description">
				The <?php echo Website::$generalName;?> has to be considered in <b>beta version</b>. It is constantly growing and many features have not been implemented yet (for example new hardware devices have to be inserted in the database). Some problems may occur: if you find out a bug please add an issue <a href="<?php echo $this->baseUrl."/issues/viewall/$lang/1/$token";?>">here</a> with the topic <b>maybe a bug</b>.
			</div>
		</div>
	</div>
