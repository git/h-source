<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div data-role="content">
	
		<div class="position_tree_box">
			<a href="<?php echo $this->baseUrl."/home/index/$lang";?>">Home</a> &raquo; <a href="<?php echo $this->baseUrl."/hardware/catalogue/$lang";?>">Hardware</a> &raquo; <?php echo $tree;?>
		</div>
	
		<?php if (strcmp($this->action,'view') === 0) { ?>

			<div class="back_button">
				<a rel="external" href="<?php echo $this->baseUrl."/".$this->controller."/catalogue/$lang".$this->viewStatus;?>" data-inline="true" data-icon="arrow-l" data-iconpos="left" data-theme="b" data-role="button"><?php echo gtext('back');?></a>
			</div>

			<!--<div class="device_view_title">
				<?php echo singular($this->controller);?> <b><?php echo $ne_name;?></b>
			</div>-->
		
		<?php } else if (strcmp($this->action,'catalogue') === 0) { ?>

			<?php echo $topNotice;?>
			
		<?php } ?>