<?php if (!defined('EG')) die('Direct access not allowed!'); ?>

<?php
// h-source, a web software to build a community of people that want to share their hardware information.
// Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
//
// This file is part of h-source
//
// h-source is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// h-source is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with h-source.  If not, see <http://www.gnu.org/licenses/>.
?>

	<div id="footer" data-role="footer">

		<div data-theme='b' data-role="navbar">
			<ul>
				<li><a <?php echo $tm['contact']; ?> rel='external' href="<?php echo $this->baseUrl."/contact/index/$lang";?>"><?php echo gtext("contact us");?></a></li>
				<li><a <?php echo $tm['credits']; ?> rel='external' href="<?php echo $this->baseUrl."/credits/index/$lang";?>"><?php echo gtext("credits");?></a></li>
				<li><a <?php echo $tm['project']; ?> rel='external' href="<?php echo $this->baseUrl."/project/index/$lang";?>">The <?php echo Website::$projectName;?> Project</a></li>
			</ul>
		</div><!-- /navbar -->
		<div data-role="navbar">
			<ul>
				<li><a href="<?php echo $this->baseUrl."/home/index/$lang?version=desktop";?>"><?php echo gtext("desktop version");?></a></li>
			</ul>
		</div><!-- /navbar -->

<?php
//$sid="23552";
//include("/var/www/h-node.org/traffica/write_logs.php");
?>

	</div> <!--fine footer-->

</div> <!--fine page-->

<!-- Start of second page: language dialog -->
<div data-role="page" id="language-dialog">

	<div data-role="header" data-theme="a">
		<h1><?php echo gtext('Choose the language');?></h1>
	</div><!-- /header -->

	<div data-role="content" data-theme="d">
		<?php echo $language_links;?>
	</div><!-- /content -->

	<div data-role="footer">
		<p><?php echo gtext('list of languages');?></p>
	</div><!-- /footer -->
</div><!-- /page popup -->

</body>
</html> 
