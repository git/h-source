== Introduction ==

h-source is web software that can be used to build a community of people that want to share their hardware information. It is based on the EasyGiant PHP framework (www.easygiant.org) and can be considered an EasyGiant application.


== Requirements ==

TODO: It is not clear what are the exact php modules required for the site to run.
      Help with formalizing the module dependencies would be appreciated.
      On a debian-like distro, typically it is sufficient to have the following
      extra modules installed on top of a standard php 7 installation:

* Web server with PHP and URL rewrite (such as Apache with mod-rewrite)
* Any version of MariaDB, or MySQL 5 or newer
* php (currently v7)
* php-fpm (also needs to be enabled)
* php-cli
* php-gd
* php-mbstring
* php-mysql
* php-xml

On a Debian-like system, the following command should be sufficient:

  # apt install apache2 mysql-server php libapache2-mod-php php-mysql php-xml
  # a2enmod rewrite

You may need to turn off the STRICT_TRANS_TABLES sql mode to avoid query errors.

In a sql shell, do

  > SET GLOBAL sql_mode = REPLACE(@@GLOBAL.SQL_MODE, 'STRICT_TRANS_TABLES', '');

* FIXME: apparently this is not needed in some cases.  Find the root cause.


== Installation and configuration ==

Extract the tarball or clone the VCS to your filesystem:

git clone https://git.savannah.gnu.org/git/h-source.git

You have to create the MySQL database used by h-source. The tables of the database that have to be created are written inside the file tables.sql and distros.sql. A way to carry out this operation is the following:

Open a terminal and type:

  # cp Config/Config.php{.sample,}
  # cp admin/Config/Config.php{.sample,}

And if necessary:

  # cp .htaccess{.sample,}

Now edit those configuration files, at least, defining the DB password 'PWD' under "/*db parameters*/" section in Config/Config.php.

The following constants are mandatory:

* DB          // name of the database (the database we will create now)
* USER        // database user
* PWD         // database password
* HOST        // hostname of the mysql server (usually localhost)
* DOMAIN_NAME // domain name of the website (use 'localhost'
                 or create a virtual host in your web server)

Note that the <USERNAME>, <PASSWORD>, and <DB> in the commands below, must correspond to the "/*db parameters*/" in Config/Config.php and admin/Config/Config.php.

Back at the terminal, type:

  # mysql -u <USERNAME> -p

where <USERNAME> is the database user. Write the password and type the following command:

  > create database <DB>;

where <DB> is the database name you want to use. Close Mysql by typing the following command:

  > exit

Now you have to move inside the folder that contains the tables.sql file and type the following commands:

  # mysql -u <USERNAME> -p<PASSWORD> <DB> < tables.sql
  # mysql -u <USERNAME> -p<PASSWORD> <DB> < distros.sql
  # mysql -u <USERNAME> -p<PASSWORD> <DB> < admin/tables.sql

Now you have created the database named <DB> and populated it with the default accepted/recognized distros. This will also create the default web user having the following credentials:

  username: admin
  password: admin

Make sure you change the password of this user!
  
* FIXME: apparently, this is not created sometimes.

  > CREATE USER '<USERNAME>'@'localhost' IDENTIFIED BY '<PASSWORD>';
  > GRANT ALL ON <DB>.* TO '<USERNAME>'@'localhost' IDENTIFIED BY '<PASSWORD>' WITH GRANT OPTION;
  > FLUSH PRIVILEGES;

Create an apache virtual host conf pointing to the h-source directory (say it's /var/www/h-source), enable it and reload the apache web server.  Here's a simple example of the Apache virtual host conf:

  <VirtualHost *:80>
    ServerName <DOMAIN_NAME>
  
    DocumentRoot /var/www/h-source
  
  	<Directory /var/www/h-source>
  		Options Indexes FollowSymLinks MultiViews
  		AllowOverride All
  		Order allow,deny
  		allow from all
  	</Directory>
  	
  	ErrorLog ${APACHE_LOG_DIR}/error.log
  </VirtualHost>
  

Direct your browser to the following URL:

  http://<DOMAIN_NAME>/

where DOMAIN_NAME is the domain name you have previously set (eg: localhost). You should see the home page of the h-source software.

You will be able to login to the website using the 'admin' user and change the website preferences and the admin password.


== Account issues ==

If you want that the system can send e-mails you have to specify the name of the SMTP server that has to be used. Copy the file Application/Include/params.php.sample to Application/Include/params.php and set the following static attributes of the Website class:

  $mailServer = "";   // set the mail server (if $useSMTP = true)
  $generalMail = "";  // set the username of your mail account (always needed)
  $mailPassword = ""; // set the password of your mail account (if $useSMTP = true)
  $fromEmail = "";    // this is the "from address e-mail" used inside the mails
                         sent by h-source (example: confirmation requesta e-mail,
                         change password e-mail, ...)
  $useSMTP = true;    // if you want to use a SMTP account. It can be true or false.
                         Set $useSMTP to false if you want that the software rely on
                         the mail() PHP function.

You can also set these constants:

  $generalName = "";  // the string that you want to use inside the <title> tag of your website
  $projectName = "";  // the name of your project

To configure for the admin app, do the same for admin/Application/Include/params.php.sample.

== Configure the HTML template (right column, top notices, top menu, etc.) ==

Modify the file config.xml inside the ROOT folder of your h-source installation.

If you want to change the position of the config.xml file change the following static attribute of the Website class inside the Application/Include/params.php file:

  static public $xmlConfigFileFolder = ROOT; // the constant ROOT contains the
                                                path to the root folder of your
                                                h-source installaton


== Change the homepage ==

Modify the PHP files inside the folder Application/Views/Home


== Change the hardware ==

Modify the PHP file Application/Include/hardware.php


== Change the allowed distributions ==

Modify the PHP file Application/Include/distributions.php


== Change the allowed languages ==

Modify the PHP file Application/Include/languages.php


== How to fill the vendors table ==

After the installation, you have to fill the MySQL table containing the USB/PCI vendors IDs.  Note this corresponds to vendors for the USB/PCI interface, which are different from say the notebook vendors which are stored in Application/Include/hardware.php.

To carry out this task, use the scripts in the scripts sub-directory.

Change into the scripts/vendorid/ directory. Copy the file connect.php.sample to connect.php and set the following variables with the same values as your h-node instance:

  $host     = '<DOMAIN_NAME>';
  $user     = '<USERNAME>';
  $password = '<PASSWORD>';
  $dbName   = '<DB>';

You will find two files containing the list of vendors: pci.ids and usb.ids. If you want more up-to-date versions of those files then delete them and write the following commands (or download those files from the URLs indicated below):

  wget pciids.sourceforge.net/v2.2/pci.ids -O pci.ids
  wget http://www.linux-usb.org/usb.ids -O usb.ids

Be sure you have installed php-cli, then write the following command:

  php insert_vendors.php

It will fill the 'vendors' MySql table.

== Learn the EasyGiant PHP framework ==

Since h-source is an application of the EasyGiant PHP framework you can use the EasyGiant features in order to modify the h-source source code. Visit www.easygiant.org (archived at http://web.archive.org/web/20161027164429/http://www.easygiant.org/).

You may find a guide to EasyGiant at http://www.easygiant.org/media/guide/20150326.pdf (archived at http://web.archive.org/web/20161027162658/http://www.easygiant.org/media/guide/20150326.pdf).


<!-- NOTE: this file is legal mediawiki syntax -->

<!-- LICENSE
  h-source, a web software to build a community of people that want to share their hardware information.

  Copyright (C) 2010  Antonio Gallo (h-source-copyright.txt)
  Copyright (C) 2022 bill-auger <h-source-users@nongnu.org>

  This file is part of h-source

  h-source is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  h-source is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with h-source.  If not, see <http://www.gnu.org/licenses/>.
-->
